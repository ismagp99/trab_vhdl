// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (win64) Build 2902540 Wed May 27 19:54:49 MDT 2020
// Date        : Sun Dec 13 13:41:39 2020
// Host        : DESKTOP-H3E0PE0 running 64-bit major release  (build 9200)
// Command     : write_verilog -mode timesim -nolib -sdf_anno true -force -file
//               C:/Users/INES/Desktop/uni/Cuarto/SED/Trab_VHDL/Maquina_expendedora/Maquina_expendedora.sim/sim_1/impl/timing/xsim/maquina_ref_tb_time_impl.v
// Design      : maquina_ref
// Purpose     : This verilog netlist is a timing simulation representation of the design and should not be modified or
//               synthesized. Please ensure that this netlist is used with the corresponding SDF file.
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps
`define XIL_TIMING

(* ECO_CHECKSUM = "402d90d9" *) 
(* NotValidForBitStream *)
module maquina_ref
   (CLR_N,
    CLK,
    B1,
    B2,
    B3,
    B4,
    B5,
    Error,
    Salida);
  input CLR_N;
  input CLK;
  input B1;
  input B2;
  input B3;
  input B4;
  input B5;
  output Error;
  output Salida;

  wire B1;
  wire B1_IBUF;
  wire B2;
  wire B2_IBUF;
  wire B3;
  wire B3_IBUF;
  wire B4;
  wire B4_IBUF;
  wire B5;
  wire B5_IBUF;
  wire CLK;
  wire CLK_IBUF;
  wire CLK_IBUF_BUFG;
  wire CLR_N;
  wire CLR_N_IBUF;
  wire Error;
  wire Error_OBUF;
  wire \FSM_sequential_state[0]_i_2_n_0 ;
  wire \FSM_sequential_state[1]_i_2_n_0 ;
  wire \FSM_sequential_state[2]_i_2_n_0 ;
  wire \FSM_sequential_state[3]_i_1_n_0 ;
  wire \FSM_sequential_state[3]_i_3_n_0 ;
  wire \FSM_sequential_state[3]_i_4_n_0 ;
  wire \FSM_sequential_state[3]_i_5_n_0 ;
  wire Salida;
  wire Salida_OBUF;
  wire [3:0]nxt_state__0;
  wire [3:0]state;

initial begin
 $sdf_annotate("maquina_ref_tb_time_impl.sdf",,,,"tool_control");
end
  IBUF B1_IBUF_inst
       (.I(B1),
        .O(B1_IBUF));
  IBUF B2_IBUF_inst
       (.I(B2),
        .O(B2_IBUF));
  IBUF B3_IBUF_inst
       (.I(B3),
        .O(B3_IBUF));
  IBUF B4_IBUF_inst
       (.I(B4),
        .O(B4_IBUF));
  IBUF B5_IBUF_inst
       (.I(B5),
        .O(B5_IBUF));
  BUFG CLK_IBUF_BUFG_inst
       (.I(CLK_IBUF),
        .O(CLK_IBUF_BUFG));
  IBUF CLK_IBUF_inst
       (.I(CLK),
        .O(CLK_IBUF));
  IBUF CLR_N_IBUF_inst
       (.I(CLR_N),
        .O(CLR_N_IBUF));
  OBUF Error_OBUF_inst
       (.I(Error_OBUF),
        .O(Error));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h0400)) 
    Error_OBUF_inst_i_1
       (.I0(state[2]),
        .I1(state[1]),
        .I2(state[0]),
        .I3(state[3]),
        .O(Error_OBUF));
  LUT6 #(
    .INIT(64'h00CEFFFF00CE0000)) 
    \FSM_sequential_state[0]_i_1 
       (.I0(B2_IBUF),
        .I1(B1_IBUF),
        .I2(state[0]),
        .I3(state[1]),
        .I4(state[3]),
        .I5(\FSM_sequential_state[0]_i_2_n_0 ),
        .O(nxt_state__0[0]));
  LUT6 #(
    .INIT(64'h00FF00FFFF00084D)) 
    \FSM_sequential_state[0]_i_2 
       (.I0(state[2]),
        .I1(B3_IBUF),
        .I2(state[1]),
        .I3(state[0]),
        .I4(B2_IBUF),
        .I5(B1_IBUF),
        .O(\FSM_sequential_state[0]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h0DFF0D00)) 
    \FSM_sequential_state[1]_i_1 
       (.I0(B1_IBUF),
        .I1(state[0]),
        .I2(state[1]),
        .I3(state[3]),
        .I4(\FSM_sequential_state[1]_i_2_n_0 ),
        .O(nxt_state__0[1]));
  LUT6 #(
    .INIT(64'h333B333FC3CCC3CF)) 
    \FSM_sequential_state[1]_i_2 
       (.I0(state[2]),
        .I1(state[1]),
        .I2(B1_IBUF),
        .I3(B2_IBUF),
        .I4(B3_IBUF),
        .I5(state[0]),
        .O(\FSM_sequential_state[1]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \FSM_sequential_state[2]_i_1 
       (.I0(\FSM_sequential_state[2]_i_2_n_0 ),
        .I1(state[3]),
        .O(nxt_state__0[2]));
  LUT6 #(
    .INIT(64'h6A6A6A6A5A155A00)) 
    \FSM_sequential_state[2]_i_2 
       (.I0(state[2]),
        .I1(state[0]),
        .I2(state[1]),
        .I3(B2_IBUF),
        .I4(B3_IBUF),
        .I5(B1_IBUF),
        .O(\FSM_sequential_state[2]_i_2_n_0 ));
  LUT5 #(
    .INIT(32'h0BFF0800)) 
    \FSM_sequential_state[3]_i_1 
       (.I0(B5_IBUF),
        .I1(state[1]),
        .I2(state[2]),
        .I3(state[3]),
        .I4(\FSM_sequential_state[3]_i_4_n_0 ),
        .O(\FSM_sequential_state[3]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'h74)) 
    \FSM_sequential_state[3]_i_2 
       (.I0(state[1]),
        .I1(state[3]),
        .I2(\FSM_sequential_state[3]_i_5_n_0 ),
        .O(nxt_state__0[3]));
  LUT1 #(
    .INIT(2'h1)) 
    \FSM_sequential_state[3]_i_3 
       (.I0(CLR_N_IBUF),
        .O(\FSM_sequential_state[3]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \FSM_sequential_state[3]_i_4 
       (.I0(B4_IBUF),
        .I1(B2_IBUF),
        .I2(B3_IBUF),
        .I3(B1_IBUF),
        .O(\FSM_sequential_state[3]_i_4_n_0 ));
  LUT6 #(
    .INIT(64'h80808080A0EAA0FF)) 
    \FSM_sequential_state[3]_i_5 
       (.I0(state[2]),
        .I1(state[0]),
        .I2(state[1]),
        .I3(B2_IBUF),
        .I4(B3_IBUF),
        .I5(B1_IBUF),
        .O(\FSM_sequential_state[3]_i_5_n_0 ));
  (* FSM_ENCODED_STATES = "s3_30:0011,s4_40:0100,s2_20:0010,s11_error:1010,s10_sal:1011,s1_10:0001,s0_esp:0000,s9_90:1001,s7_70:0111,s6_60:0110,s8_80:1000,s5_50:0101" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_sequential_state_reg[0] 
       (.C(CLK_IBUF_BUFG),
        .CE(\FSM_sequential_state[3]_i_1_n_0 ),
        .CLR(\FSM_sequential_state[3]_i_3_n_0 ),
        .D(nxt_state__0[0]),
        .Q(state[0]));
  (* FSM_ENCODED_STATES = "s3_30:0011,s4_40:0100,s2_20:0010,s11_error:1010,s10_sal:1011,s1_10:0001,s0_esp:0000,s9_90:1001,s7_70:0111,s6_60:0110,s8_80:1000,s5_50:0101" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_sequential_state_reg[1] 
       (.C(CLK_IBUF_BUFG),
        .CE(\FSM_sequential_state[3]_i_1_n_0 ),
        .CLR(\FSM_sequential_state[3]_i_3_n_0 ),
        .D(nxt_state__0[1]),
        .Q(state[1]));
  (* FSM_ENCODED_STATES = "s3_30:0011,s4_40:0100,s2_20:0010,s11_error:1010,s10_sal:1011,s1_10:0001,s0_esp:0000,s9_90:1001,s7_70:0111,s6_60:0110,s8_80:1000,s5_50:0101" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_sequential_state_reg[2] 
       (.C(CLK_IBUF_BUFG),
        .CE(\FSM_sequential_state[3]_i_1_n_0 ),
        .CLR(\FSM_sequential_state[3]_i_3_n_0 ),
        .D(nxt_state__0[2]),
        .Q(state[2]));
  (* FSM_ENCODED_STATES = "s3_30:0011,s4_40:0100,s2_20:0010,s11_error:1010,s10_sal:1011,s1_10:0001,s0_esp:0000,s9_90:1001,s7_70:0111,s6_60:0110,s8_80:1000,s5_50:0101" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_sequential_state_reg[3] 
       (.C(CLK_IBUF_BUFG),
        .CE(\FSM_sequential_state[3]_i_1_n_0 ),
        .CLR(\FSM_sequential_state[3]_i_3_n_0 ),
        .D(nxt_state__0[3]),
        .Q(state[3]));
  OBUF Salida_OBUF_inst
       (.I(Salida_OBUF),
        .O(Salida));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT4 #(
    .INIT(16'h4000)) 
    Salida_OBUF_inst_i_1
       (.I0(state[2]),
        .I1(state[0]),
        .I2(state[1]),
        .I3(state[3]),
        .O(Salida_OBUF));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif

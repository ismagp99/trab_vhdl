// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// --------------------------------------------------------------------------------
// Tool Version: Vivado v.2020.1 (win64) Build 2902540 Wed May 27 19:54:49 MDT 2020
// Date        : Thu Jan 28 18:29:43 2021
// Host        : LAPTOP-7JGFUE6N running 64-bit major release  (build 9200)
// Command     : write_verilog -mode timesim -nolib -sdf_anno true -force -file {C:/Users/maviv/Documents/SED/Trabajo
//               VHDL/trab_vhdl/Maquina_expendedora/Maquina_expendedora.sim/sim_1/impl/timing/xsim/timer_tb_time_impl.v}
// Design      : top
// Purpose     : This verilog netlist is a timing simulation representation of the design and should not be modified or
//               synthesized. Please ensure that this netlist is used with the corresponding SDF file.
// Device      : xc7a100tcsg324-1
// --------------------------------------------------------------------------------
`timescale 1 ps / 1 ps
`define XIL_TIMING

module BCD_decoder
   (CO,
    O,
    letra21_carry__2_i_6,
    letra21__37_carry__1_i_7,
    letra21__37_carry__2_i_1,
    letra21__37_carry__2_i_1_0,
    letra21__74_carry__0_i_4,
    letra21__74_carry__1_i_5,
    letra21__74_carry__2_i_3,
    letra21__104_carry_i_4,
    letra21__104_carry__2_i_1,
    letra21_carry__2_0,
    letra21_carry__2_1,
    letra21__208_carry__2_i_10,
    letra21__208_carry__4_i_8_0,
    letra21__208_carry__5_0,
    letra21__336_carry__0_i_4_0,
    letra10__1_carry__0_0,
    letra21__479_carry_0,
    letra21__479_carry_1,
    letra10__1_carry__0_1,
    letra21_carry__2_2,
    g0_b0__1_i_13,
    g0_b0__1_i_6,
    \salida_reg[3] ,
    g0_b0__1_i_12_0,
    letra21__479_carry_2,
    \salida_reg[2] ,
    letra10__1_carry__0_2,
    \salida_reg[7] ,
    DI,
    S,
    letra21_carry__1_i_12,
    letra21_carry__1_i_12_0,
    letra21__208_carry__0_i_5_0,
    letra21__208_carry__0_i_5_1,
    letra21__208_carry__1_i_6_0,
    letra21__208_carry_i_4,
    letra21__208_carry_i_6_0,
    letra21__208_carry_i_6_1,
    letra21__420_carry__1_0,
    letra21__420_carry__1_1,
    letra21__420_carry__0_0,
    letra21__208_carry__0_i_5_2,
    letra21__208_carry__3_i_8,
    letra21__208_carry_i_6_2,
    letra21__208_carry__0_i_1_0,
    letra21__208_carry__0_i_1_1,
    letra21__420_carry__0_1,
    letra21__208_carry__4_i_5_0,
    letra21__208_carry__1_i_6_1,
    letra21__208_carry__2_i_1,
    letra21__208_carry__0_i_5_3,
    letra21__208_carry__0_i_5_4,
    letra21__208_carry__1_i_6_2,
    letra21__208_carry__2_i_11,
    letra21__208_carry__2_i_11_0,
    letra21__208_carry__3_i_5_0,
    letra21__208_carry__3_i_8_0,
    letra21__208_carry__3_i_5_1,
    letra21__208_carry__3_i_5_2,
    letra21__208_carry__4_i_5_1,
    letra21__208_carry__5_i_5_0,
    letra21__208_carry__3_i_5_3,
    letra21__208_carry__3_i_5_4,
    letra21__208_carry__4_i_5_2,
    letra21__208_carry__4_i_5_3,
    letra21__208_carry__5_i_5_1,
    letra21__208_carry__5_i_5_2,
    letra21__208_carry__6_i_6_0,
    letra21__208_carry__0_0,
    letra21__208_carry__3_0,
    letra21__208_carry__3_1,
    letra21__208_carry__4_0,
    letra21__208_carry__4_1,
    letra21__336_carry__5_0,
    letra21__336_carry__5_1,
    letra21__420_carry__0_2,
    letra21__420_carry__1_2,
    letra21__420_carry__2_0,
    Q,
    letra230_out,
    letra21__208_carry__2_i_6,
    letra21__420_carry__0_3,
    letra21__420_carry__0_4,
    letra21__420_carry__0_5,
    g0_b7__1,
    g0_b7__1_0,
    g0_b7__1_1,
    letra21__208_carry__2_0,
    letra21__420_carry_0);
  output [0:0]CO;
  output [0:0]O;
  output [0:0]letra21_carry__2_i_6;
  output [1:0]letra21__37_carry__1_i_7;
  output [0:0]letra21__37_carry__2_i_1;
  output [0:0]letra21__37_carry__2_i_1_0;
  output [2:0]letra21__74_carry__0_i_4;
  output [1:0]letra21__74_carry__1_i_5;
  output [0:0]letra21__74_carry__2_i_3;
  output [0:0]letra21__104_carry_i_4;
  output [0:0]letra21__104_carry__2_i_1;
  output [0:0]letra21_carry__2_0;
  output [3:0]letra21_carry__2_1;
  output [0:0]letra21__208_carry__2_i_10;
  output [0:0]letra21__208_carry__4_i_8_0;
  output [2:0]letra21__208_carry__5_0;
  output [2:0]letra21__336_carry__0_i_4_0;
  output letra10__1_carry__0_0;
  output letra21__479_carry_0;
  output letra21__479_carry_1;
  output letra10__1_carry__0_1;
  output letra21_carry__2_2;
  output g0_b0__1_i_13;
  output g0_b0__1_i_6;
  output \salida_reg[3] ;
  output g0_b0__1_i_12_0;
  output letra21__479_carry_2;
  output \salida_reg[2] ;
  output [1:0]letra10__1_carry__0_2;
  output [1:0]\salida_reg[7] ;
  input [0:0]DI;
  input [3:0]S;
  input [1:0]letra21_carry__1_i_12;
  input [1:0]letra21_carry__1_i_12_0;
  input [1:0]letra21__208_carry__0_i_5_0;
  input [2:0]letra21__208_carry__0_i_5_1;
  input [3:0]letra21__208_carry__1_i_6_0;
  input [3:0]letra21__208_carry_i_4;
  input [3:0]letra21__208_carry_i_6_0;
  input [3:0]letra21__208_carry_i_6_1;
  input letra21__420_carry__1_0;
  input letra21__420_carry__1_1;
  input letra21__420_carry__0_0;
  input [2:0]letra21__208_carry__0_i_5_2;
  input [2:0]letra21__208_carry__3_i_8;
  input [3:0]letra21__208_carry_i_6_2;
  input [3:0]letra21__208_carry__0_i_1_0;
  input [3:0]letra21__208_carry__0_i_1_1;
  input letra21__420_carry__0_1;
  input [1:0]letra21__208_carry__4_i_5_0;
  input [3:0]letra21__208_carry__1_i_6_1;
  input [0:0]letra21__208_carry__2_i_1;
  input [0:0]letra21__208_carry__0_i_5_3;
  input [2:0]letra21__208_carry__0_i_5_4;
  input [3:0]letra21__208_carry__1_i_6_2;
  input [0:0]letra21__208_carry__2_i_11;
  input [3:0]letra21__208_carry__2_i_11_0;
  input [2:0]letra21__208_carry__3_i_5_0;
  input [3:0]letra21__208_carry__3_i_8_0;
  input [0:0]letra21__208_carry__3_i_5_1;
  input [3:0]letra21__208_carry__3_i_5_2;
  input [3:0]letra21__208_carry__4_i_5_1;
  input [0:0]letra21__208_carry__5_i_5_0;
  input [2:0]letra21__208_carry__3_i_5_3;
  input [3:0]letra21__208_carry__3_i_5_4;
  input [3:0]letra21__208_carry__4_i_5_2;
  input [3:0]letra21__208_carry__4_i_5_3;
  input [3:0]letra21__208_carry__5_i_5_1;
  input [3:0]letra21__208_carry__5_i_5_2;
  input [0:0]letra21__208_carry__6_i_6_0;
  input [0:0]letra21__208_carry__0_0;
  input [1:0]letra21__208_carry__3_0;
  input [2:0]letra21__208_carry__3_1;
  input [0:0]letra21__208_carry__4_0;
  input [0:0]letra21__208_carry__4_1;
  input [1:0]letra21__336_carry__5_0;
  input [2:0]letra21__336_carry__5_1;
  input [1:0]letra21__420_carry__0_2;
  input [1:0]letra21__420_carry__1_2;
  input [0:0]letra21__420_carry__2_0;
  input [4:0]Q;
  input [3:0]letra230_out;
  input letra21__208_carry__2_i_6;
  input letra21__420_carry__0_3;
  input letra21__420_carry__0_4;
  input letra21__420_carry__0_5;
  input g0_b7__1;
  input g0_b7__1_0;
  input g0_b7__1_1;
  input letra21__208_carry__2_0;
  input letra21__420_carry_0;

  wire [0:0]CO;
  wire [0:0]DI;
  wire [0:0]O;
  wire [4:0]Q;
  wire [3:0]S;
  wire g0_b0__1_i_10_n_0;
  wire g0_b0__1_i_11_n_0;
  wire g0_b0__1_i_12_0;
  wire g0_b0__1_i_12_n_0;
  wire g0_b0__1_i_13;
  wire g0_b0__1_i_6;
  wire g0_b7__1;
  wire g0_b7__1_0;
  wire g0_b7__1_1;
  wire letra10__1_carry__0_0;
  wire letra10__1_carry__0_1;
  wire [1:0]letra10__1_carry__0_2;
  wire letra10__1_carry_n_0;
  wire letra21__104_carry__0_n_0;
  wire letra21__104_carry__0_n_4;
  wire letra21__104_carry__0_n_5;
  wire letra21__104_carry__0_n_6;
  wire letra21__104_carry__0_n_7;
  wire letra21__104_carry__1_n_0;
  wire letra21__104_carry__1_n_4;
  wire letra21__104_carry__1_n_5;
  wire letra21__104_carry__1_n_6;
  wire letra21__104_carry__1_n_7;
  wire [0:0]letra21__104_carry__2_i_1;
  wire letra21__104_carry__2_n_7;
  wire [0:0]letra21__104_carry_i_4;
  wire letra21__104_carry_n_0;
  wire letra21__104_carry_n_4;
  wire letra21__104_carry_n_5;
  wire letra21__130_carry__0_n_0;
  wire letra21__130_carry__0_n_4;
  wire letra21__130_carry__0_n_5;
  wire letra21__130_carry__0_n_6;
  wire letra21__130_carry__0_n_7;
  wire letra21__130_carry__1_n_0;
  wire letra21__130_carry__1_n_4;
  wire letra21__130_carry__1_n_5;
  wire letra21__130_carry__1_n_6;
  wire letra21__130_carry__1_n_7;
  wire letra21__130_carry__2_n_0;
  wire letra21__130_carry__2_n_4;
  wire letra21__130_carry__2_n_5;
  wire letra21__130_carry__2_n_6;
  wire letra21__130_carry__2_n_7;
  wire letra21__130_carry__3_n_0;
  wire letra21__130_carry__3_n_4;
  wire letra21__130_carry__3_n_5;
  wire letra21__130_carry__3_n_6;
  wire letra21__130_carry__3_n_7;
  wire letra21__130_carry__4_n_0;
  wire letra21__130_carry__4_n_4;
  wire letra21__130_carry__4_n_5;
  wire letra21__130_carry__4_n_6;
  wire letra21__130_carry__4_n_7;
  wire letra21__130_carry__5_n_0;
  wire letra21__130_carry__5_n_4;
  wire letra21__130_carry__5_n_5;
  wire letra21__130_carry__5_n_6;
  wire letra21__130_carry__5_n_7;
  wire letra21__130_carry_n_0;
  wire letra21__130_carry_n_4;
  wire letra21__130_carry_n_5;
  wire letra21__130_carry_n_6;
  wire letra21__130_carry_n_7;
  wire [0:0]letra21__208_carry__0_0;
  wire [3:0]letra21__208_carry__0_i_1_0;
  wire [3:0]letra21__208_carry__0_i_1_1;
  wire letra21__208_carry__0_i_1_n_0;
  wire letra21__208_carry__0_i_2_n_0;
  wire letra21__208_carry__0_i_3_n_0;
  wire letra21__208_carry__0_i_4_n_0;
  wire [1:0]letra21__208_carry__0_i_5_0;
  wire [2:0]letra21__208_carry__0_i_5_1;
  wire [2:0]letra21__208_carry__0_i_5_2;
  wire [0:0]letra21__208_carry__0_i_5_3;
  wire [2:0]letra21__208_carry__0_i_5_4;
  wire letra21__208_carry__0_i_5_n_0;
  wire letra21__208_carry__0_i_6_n_0;
  wire letra21__208_carry__0_i_7_n_0;
  wire letra21__208_carry__0_i_8_n_0;
  wire letra21__208_carry__0_n_0;
  wire letra21__208_carry__10_i_1_n_0;
  wire letra21__208_carry__10_i_2_n_0;
  wire letra21__208_carry__10_i_3_n_0;
  wire letra21__208_carry__10_i_4_n_0;
  wire letra21__208_carry__10_i_5_n_0;
  wire letra21__208_carry__10_i_6_n_0;
  wire letra21__208_carry__10_i_7_n_0;
  wire letra21__208_carry__10_i_8_n_0;
  wire letra21__208_carry__10_n_0;
  wire letra21__208_carry__10_n_4;
  wire letra21__208_carry__10_n_5;
  wire letra21__208_carry__10_n_6;
  wire letra21__208_carry__10_n_7;
  wire letra21__208_carry__11_n_5;
  wire letra21__208_carry__11_n_6;
  wire letra21__208_carry__11_n_7;
  wire letra21__208_carry__1_i_1_n_0;
  wire letra21__208_carry__1_i_2_n_0;
  wire letra21__208_carry__1_i_3_n_0;
  wire letra21__208_carry__1_i_4_n_0;
  wire letra21__208_carry__1_i_5_n_0;
  wire [3:0]letra21__208_carry__1_i_6_0;
  wire [3:0]letra21__208_carry__1_i_6_1;
  wire [3:0]letra21__208_carry__1_i_6_2;
  wire letra21__208_carry__1_i_6_n_0;
  wire letra21__208_carry__1_i_7_n_0;
  wire letra21__208_carry__1_i_8_n_0;
  wire letra21__208_carry__1_i_9_n_0;
  wire letra21__208_carry__1_n_0;
  wire letra21__208_carry__2_0;
  wire [0:0]letra21__208_carry__2_i_1;
  wire [0:0]letra21__208_carry__2_i_10;
  wire [0:0]letra21__208_carry__2_i_11;
  wire [3:0]letra21__208_carry__2_i_11_0;
  wire letra21__208_carry__2_i_4_n_0;
  wire letra21__208_carry__2_i_6;
  wire letra21__208_carry__2_i_8_n_0;
  wire letra21__208_carry__2_n_0;
  wire [1:0]letra21__208_carry__3_0;
  wire [2:0]letra21__208_carry__3_1;
  wire letra21__208_carry__3_i_10_n_0;
  wire letra21__208_carry__3_i_11_n_0;
  wire letra21__208_carry__3_i_1_n_0;
  wire letra21__208_carry__3_i_2_n_0;
  wire letra21__208_carry__3_i_3_n_0;
  wire [2:0]letra21__208_carry__3_i_5_0;
  wire [0:0]letra21__208_carry__3_i_5_1;
  wire [3:0]letra21__208_carry__3_i_5_2;
  wire [2:0]letra21__208_carry__3_i_5_3;
  wire [3:0]letra21__208_carry__3_i_5_4;
  wire letra21__208_carry__3_i_5_n_0;
  wire letra21__208_carry__3_i_6_n_0;
  wire letra21__208_carry__3_i_7_n_0;
  wire [2:0]letra21__208_carry__3_i_8;
  wire [3:0]letra21__208_carry__3_i_8_0;
  wire letra21__208_carry__3_i_9_n_0;
  wire letra21__208_carry__3_n_0;
  wire [0:0]letra21__208_carry__4_0;
  wire [0:0]letra21__208_carry__4_1;
  wire letra21__208_carry__4_i_10_n_0;
  wire letra21__208_carry__4_i_11_n_0;
  wire letra21__208_carry__4_i_12_n_0;
  wire letra21__208_carry__4_i_1_n_0;
  wire letra21__208_carry__4_i_2_n_0;
  wire letra21__208_carry__4_i_3_n_0;
  wire letra21__208_carry__4_i_4_n_0;
  wire [1:0]letra21__208_carry__4_i_5_0;
  wire [3:0]letra21__208_carry__4_i_5_1;
  wire [3:0]letra21__208_carry__4_i_5_2;
  wire [3:0]letra21__208_carry__4_i_5_3;
  wire letra21__208_carry__4_i_5_n_0;
  wire letra21__208_carry__4_i_6_n_0;
  wire letra21__208_carry__4_i_7_n_0;
  wire [0:0]letra21__208_carry__4_i_8_0;
  wire letra21__208_carry__4_i_8_n_0;
  wire letra21__208_carry__4_i_9_n_0;
  wire letra21__208_carry__4_n_0;
  wire [2:0]letra21__208_carry__5_0;
  wire letra21__208_carry__5_i_10_n_0;
  wire letra21__208_carry__5_i_11_n_0;
  wire letra21__208_carry__5_i_12_n_0;
  wire letra21__208_carry__5_i_1_n_0;
  wire letra21__208_carry__5_i_2_n_0;
  wire letra21__208_carry__5_i_3_n_0;
  wire letra21__208_carry__5_i_4_n_0;
  wire [0:0]letra21__208_carry__5_i_5_0;
  wire [3:0]letra21__208_carry__5_i_5_1;
  wire [3:0]letra21__208_carry__5_i_5_2;
  wire letra21__208_carry__5_i_5_n_0;
  wire letra21__208_carry__5_i_6_n_0;
  wire letra21__208_carry__5_i_7_n_0;
  wire letra21__208_carry__5_i_8_n_0;
  wire letra21__208_carry__5_i_9_n_0;
  wire letra21__208_carry__5_n_0;
  wire letra21__208_carry__5_n_4;
  wire letra21__208_carry__5_n_5;
  wire letra21__208_carry__5_n_6;
  wire letra21__208_carry__5_n_7;
  wire letra21__208_carry__6_i_1_n_0;
  wire letra21__208_carry__6_i_2_n_0;
  wire letra21__208_carry__6_i_3_n_0;
  wire letra21__208_carry__6_i_4_n_0;
  wire letra21__208_carry__6_i_5_n_0;
  wire [0:0]letra21__208_carry__6_i_6_0;
  wire letra21__208_carry__6_i_6_n_0;
  wire letra21__208_carry__6_i_7_n_0;
  wire letra21__208_carry__6_i_8_n_0;
  wire letra21__208_carry__6_n_0;
  wire letra21__208_carry__6_n_4;
  wire letra21__208_carry__6_n_5;
  wire letra21__208_carry__6_n_6;
  wire letra21__208_carry__6_n_7;
  wire letra21__208_carry__7_i_1_n_0;
  wire letra21__208_carry__7_i_2_n_0;
  wire letra21__208_carry__7_i_3_n_0;
  wire letra21__208_carry__7_i_4_n_0;
  wire letra21__208_carry__7_i_5_n_0;
  wire letra21__208_carry__7_i_6_n_0;
  wire letra21__208_carry__7_i_7_n_0;
  wire letra21__208_carry__7_i_8_n_0;
  wire letra21__208_carry__7_n_0;
  wire letra21__208_carry__7_n_4;
  wire letra21__208_carry__7_n_5;
  wire letra21__208_carry__7_n_6;
  wire letra21__208_carry__7_n_7;
  wire letra21__208_carry__8_i_1_n_0;
  wire letra21__208_carry__8_i_2_n_0;
  wire letra21__208_carry__8_i_3_n_0;
  wire letra21__208_carry__8_i_4_n_0;
  wire letra21__208_carry__8_i_5_n_0;
  wire letra21__208_carry__8_i_6_n_0;
  wire letra21__208_carry__8_i_7_n_0;
  wire letra21__208_carry__8_i_8_n_0;
  wire letra21__208_carry__8_n_0;
  wire letra21__208_carry__8_n_4;
  wire letra21__208_carry__8_n_5;
  wire letra21__208_carry__8_n_6;
  wire letra21__208_carry__8_n_7;
  wire letra21__208_carry__9_i_1_n_0;
  wire letra21__208_carry__9_i_2_n_0;
  wire letra21__208_carry__9_i_3_n_0;
  wire letra21__208_carry__9_i_4_n_0;
  wire letra21__208_carry__9_i_5_n_0;
  wire letra21__208_carry__9_i_6_n_0;
  wire letra21__208_carry__9_i_7_n_0;
  wire letra21__208_carry__9_i_8_n_0;
  wire letra21__208_carry__9_n_0;
  wire letra21__208_carry__9_n_4;
  wire letra21__208_carry__9_n_5;
  wire letra21__208_carry__9_n_6;
  wire letra21__208_carry__9_n_7;
  wire letra21__208_carry_i_1_n_0;
  wire letra21__208_carry_i_2_n_0;
  wire letra21__208_carry_i_3_n_0;
  wire [3:0]letra21__208_carry_i_4;
  wire letra21__208_carry_i_5_n_0;
  wire [3:0]letra21__208_carry_i_6_0;
  wire [3:0]letra21__208_carry_i_6_1;
  wire [3:0]letra21__208_carry_i_6_2;
  wire letra21__208_carry_i_6_n_0;
  wire letra21__208_carry_i_7_n_0;
  wire letra21__208_carry_i_8_n_0;
  wire letra21__208_carry_n_0;
  wire letra21__336_carry__0_i_1_n_0;
  wire letra21__336_carry__0_i_2_n_0;
  wire letra21__336_carry__0_i_3_n_0;
  wire [2:0]letra21__336_carry__0_i_4_0;
  wire letra21__336_carry__0_i_4_n_0;
  wire letra21__336_carry__0_n_0;
  wire letra21__336_carry__0_n_6;
  wire letra21__336_carry__1_i_1_n_0;
  wire letra21__336_carry__1_i_2_n_0;
  wire letra21__336_carry__1_i_3_n_0;
  wire letra21__336_carry__1_i_4_n_0;
  wire letra21__336_carry__1_n_0;
  wire letra21__336_carry__1_n_4;
  wire letra21__336_carry__1_n_5;
  wire letra21__336_carry__1_n_6;
  wire letra21__336_carry__1_n_7;
  wire letra21__336_carry__2_i_1_n_0;
  wire letra21__336_carry__2_i_2_n_0;
  wire letra21__336_carry__2_i_3_n_0;
  wire letra21__336_carry__2_i_4_n_0;
  wire letra21__336_carry__2_n_0;
  wire letra21__336_carry__2_n_4;
  wire letra21__336_carry__2_n_5;
  wire letra21__336_carry__2_n_6;
  wire letra21__336_carry__2_n_7;
  wire letra21__336_carry__3_i_1_n_0;
  wire letra21__336_carry__3_i_2_n_0;
  wire letra21__336_carry__3_i_3_n_0;
  wire letra21__336_carry__3_i_4_n_0;
  wire letra21__336_carry__3_n_0;
  wire letra21__336_carry__3_n_4;
  wire letra21__336_carry__3_n_5;
  wire letra21__336_carry__3_n_6;
  wire letra21__336_carry__3_n_7;
  wire letra21__336_carry__4_i_1_n_0;
  wire letra21__336_carry__4_i_2_n_0;
  wire letra21__336_carry__4_i_3_n_0;
  wire letra21__336_carry__4_i_4_n_0;
  wire letra21__336_carry__4_n_0;
  wire letra21__336_carry__4_n_4;
  wire letra21__336_carry__4_n_5;
  wire letra21__336_carry__4_n_6;
  wire letra21__336_carry__4_n_7;
  wire [1:0]letra21__336_carry__5_0;
  wire [2:0]letra21__336_carry__5_1;
  wire letra21__336_carry__5_i_1_n_0;
  wire letra21__336_carry__5_i_2_n_0;
  wire letra21__336_carry__5_i_3_n_0;
  wire letra21__336_carry__5_n_0;
  wire letra21__336_carry__5_n_4;
  wire letra21__336_carry__5_n_5;
  wire letra21__336_carry__5_n_6;
  wire letra21__336_carry__5_n_7;
  wire letra21__336_carry__6_n_7;
  wire letra21__336_carry_i_1_n_0;
  wire letra21__336_carry_i_2_n_0;
  wire letra21__336_carry_i_3_n_0;
  wire letra21__336_carry_n_0;
  wire letra21__336_carry_n_7;
  wire letra21__37_carry__0_n_0;
  wire letra21__37_carry__0_n_4;
  wire letra21__37_carry__0_n_5;
  wire letra21__37_carry__0_n_6;
  wire letra21__37_carry__0_n_7;
  wire [1:0]letra21__37_carry__1_i_7;
  wire letra21__37_carry__1_n_0;
  wire letra21__37_carry__1_n_6;
  wire letra21__37_carry__1_n_7;
  wire [0:0]letra21__37_carry__2_i_1;
  wire [0:0]letra21__37_carry__2_i_1_0;
  wire letra21__37_carry_n_0;
  wire letra21__37_carry_n_4;
  wire letra21__37_carry_n_5;
  wire letra21__37_carry_n_6;
  wire letra21__37_carry_n_7;
  wire letra21__420_carry_0;
  wire letra21__420_carry__0_0;
  wire letra21__420_carry__0_1;
  wire [1:0]letra21__420_carry__0_2;
  wire letra21__420_carry__0_3;
  wire letra21__420_carry__0_4;
  wire letra21__420_carry__0_5;
  wire letra21__420_carry__0_i_1_n_0;
  wire letra21__420_carry__0_i_2_n_0;
  wire letra21__420_carry__0_i_3_n_0;
  wire letra21__420_carry__0_i_4_n_0;
  wire letra21__420_carry__0_i_5_n_0;
  wire letra21__420_carry__0_i_6_n_0;
  wire letra21__420_carry__0_n_0;
  wire letra21__420_carry__1_0;
  wire letra21__420_carry__1_1;
  wire [1:0]letra21__420_carry__1_2;
  wire letra21__420_carry__1_i_1_n_0;
  wire letra21__420_carry__1_i_2_n_0;
  wire letra21__420_carry__1_i_3_n_0;
  wire letra21__420_carry__1_i_4_n_0;
  wire letra21__420_carry__1_i_5_n_0;
  wire letra21__420_carry__1_n_0;
  wire [0:0]letra21__420_carry__2_0;
  wire letra21__420_carry__2_i_1_n_0;
  wire letra21__420_carry__2_i_2_n_0;
  wire letra21__420_carry__2_i_3_n_0;
  wire letra21__420_carry__2_i_4_n_0;
  wire letra21__420_carry__2_n_0;
  wire letra21__420_carry__3_i_1_n_0;
  wire letra21__420_carry__3_i_2_n_0;
  wire letra21__420_carry__3_i_3_n_0;
  wire letra21__420_carry__3_i_4_n_0;
  wire letra21__420_carry__3_n_0;
  wire letra21__420_carry__4_i_1_n_0;
  wire letra21__420_carry__4_i_2_n_0;
  wire letra21__420_carry__4_i_3_n_0;
  wire letra21__420_carry__4_i_4_n_0;
  wire letra21__420_carry__4_n_0;
  wire letra21__420_carry__5_i_1_n_0;
  wire letra21__420_carry__5_i_2_n_0;
  wire letra21__420_carry__5_i_3_n_0;
  wire letra21__420_carry__5_i_4_n_0;
  wire letra21__420_carry__5_n_0;
  wire letra21__420_carry__6_i_1_n_0;
  wire letra21__420_carry__6_i_2_n_0;
  wire letra21__420_carry__6_n_2;
  wire letra21__420_carry_i_1_n_0;
  wire letra21__420_carry_i_2_n_0;
  wire letra21__420_carry_i_3_n_0;
  wire letra21__420_carry_i_5_n_0;
  wire letra21__420_carry_i_6_n_0;
  wire letra21__420_carry_n_0;
  wire letra21__479_carry_0;
  wire letra21__479_carry_1;
  wire letra21__479_carry_2;
  wire letra21__479_carry_i_1_n_0;
  wire letra21__479_carry_n_4;
  wire letra21__479_carry_n_5;
  wire letra21__479_carry_n_6;
  wire letra21__479_carry_n_7;
  wire [2:0]letra21__74_carry__0_i_4;
  wire letra21__74_carry__0_n_0;
  wire letra21__74_carry__0_n_7;
  wire [1:0]letra21__74_carry__1_i_5;
  wire letra21__74_carry__1_n_0;
  wire letra21__74_carry__1_n_4;
  wire letra21__74_carry__1_n_5;
  wire [0:0]letra21__74_carry__2_i_3;
  wire letra21__74_carry__2_n_5;
  wire letra21__74_carry__2_n_6;
  wire letra21__74_carry__2_n_7;
  wire letra21__74_carry_n_0;
  wire letra21__74_carry_n_4;
  wire letra21__74_carry_n_5;
  wire letra21__74_carry_n_6;
  wire letra21_carry__0_n_0;
  wire letra21_carry__0_n_4;
  wire letra21_carry__0_n_5;
  wire [1:0]letra21_carry__1_i_12;
  wire [1:0]letra21_carry__1_i_12_0;
  wire letra21_carry__1_n_0;
  wire letra21_carry__1_n_4;
  wire letra21_carry__1_n_5;
  wire letra21_carry__1_n_6;
  wire letra21_carry__1_n_7;
  wire [0:0]letra21_carry__2_0;
  wire [3:0]letra21_carry__2_1;
  wire letra21_carry__2_2;
  wire [0:0]letra21_carry__2_i_6;
  wire letra21_carry__2_n_5;
  wire letra21_carry__2_n_6;
  wire letra21_carry__2_n_7;
  wire letra21_carry_n_0;
  wire letra21_carry_n_7;
  wire [3:0]letra230_out;
  wire \salida_reg[2] ;
  wire \salida_reg[3] ;
  wire [1:0]\salida_reg[7] ;
  wire [2:0]NLW_letra10__1_carry_CO_UNCONNECTED;
  wire [3:0]NLW_letra10__1_carry_O_UNCONNECTED;
  wire [3:0]NLW_letra10__1_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_letra10__1_carry__0_O_UNCONNECTED;
  wire [2:0]NLW_letra21__104_carry_CO_UNCONNECTED;
  wire [0:0]NLW_letra21__104_carry_O_UNCONNECTED;
  wire [2:0]NLW_letra21__104_carry__0_CO_UNCONNECTED;
  wire [2:0]NLW_letra21__104_carry__1_CO_UNCONNECTED;
  wire [3:0]NLW_letra21__104_carry__2_CO_UNCONNECTED;
  wire [3:1]NLW_letra21__104_carry__2_O_UNCONNECTED;
  wire [2:0]NLW_letra21__130_carry_CO_UNCONNECTED;
  wire [2:0]NLW_letra21__130_carry__0_CO_UNCONNECTED;
  wire [2:0]NLW_letra21__130_carry__1_CO_UNCONNECTED;
  wire [2:0]NLW_letra21__130_carry__2_CO_UNCONNECTED;
  wire [2:0]NLW_letra21__130_carry__3_CO_UNCONNECTED;
  wire [2:0]NLW_letra21__130_carry__4_CO_UNCONNECTED;
  wire [2:0]NLW_letra21__130_carry__5_CO_UNCONNECTED;
  wire [2:0]NLW_letra21__130_carry__6_CO_UNCONNECTED;
  wire [2:0]NLW_letra21__208_carry_CO_UNCONNECTED;
  wire [3:0]NLW_letra21__208_carry_O_UNCONNECTED;
  wire [2:0]NLW_letra21__208_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_letra21__208_carry__0_O_UNCONNECTED;
  wire [2:0]NLW_letra21__208_carry__1_CO_UNCONNECTED;
  wire [3:0]NLW_letra21__208_carry__1_O_UNCONNECTED;
  wire [2:0]NLW_letra21__208_carry__10_CO_UNCONNECTED;
  wire [3:0]NLW_letra21__208_carry__11_CO_UNCONNECTED;
  wire [3:3]NLW_letra21__208_carry__11_O_UNCONNECTED;
  wire [2:0]NLW_letra21__208_carry__2_CO_UNCONNECTED;
  wire [3:0]NLW_letra21__208_carry__2_O_UNCONNECTED;
  wire [2:0]NLW_letra21__208_carry__3_CO_UNCONNECTED;
  wire [3:0]NLW_letra21__208_carry__3_O_UNCONNECTED;
  wire [2:0]NLW_letra21__208_carry__4_CO_UNCONNECTED;
  wire [2:0]NLW_letra21__208_carry__4_O_UNCONNECTED;
  wire [2:0]NLW_letra21__208_carry__5_CO_UNCONNECTED;
  wire [2:0]NLW_letra21__208_carry__6_CO_UNCONNECTED;
  wire [2:0]NLW_letra21__208_carry__7_CO_UNCONNECTED;
  wire [2:0]NLW_letra21__208_carry__8_CO_UNCONNECTED;
  wire [2:0]NLW_letra21__208_carry__9_CO_UNCONNECTED;
  wire [2:0]NLW_letra21__336_carry_CO_UNCONNECTED;
  wire [2:0]NLW_letra21__336_carry__0_CO_UNCONNECTED;
  wire [2:0]NLW_letra21__336_carry__1_CO_UNCONNECTED;
  wire [2:0]NLW_letra21__336_carry__2_CO_UNCONNECTED;
  wire [2:0]NLW_letra21__336_carry__3_CO_UNCONNECTED;
  wire [2:0]NLW_letra21__336_carry__4_CO_UNCONNECTED;
  wire [2:0]NLW_letra21__336_carry__5_CO_UNCONNECTED;
  wire [3:0]NLW_letra21__336_carry__6_CO_UNCONNECTED;
  wire [3:1]NLW_letra21__336_carry__6_O_UNCONNECTED;
  wire [2:0]NLW_letra21__37_carry_CO_UNCONNECTED;
  wire [2:0]NLW_letra21__37_carry__0_CO_UNCONNECTED;
  wire [2:0]NLW_letra21__37_carry__1_CO_UNCONNECTED;
  wire [3:0]NLW_letra21__37_carry__2_CO_UNCONNECTED;
  wire [3:1]NLW_letra21__37_carry__2_O_UNCONNECTED;
  wire [2:0]NLW_letra21__420_carry_CO_UNCONNECTED;
  wire [3:0]NLW_letra21__420_carry_O_UNCONNECTED;
  wire [2:0]NLW_letra21__420_carry__0_CO_UNCONNECTED;
  wire [3:0]NLW_letra21__420_carry__0_O_UNCONNECTED;
  wire [2:0]NLW_letra21__420_carry__1_CO_UNCONNECTED;
  wire [3:0]NLW_letra21__420_carry__1_O_UNCONNECTED;
  wire [2:0]NLW_letra21__420_carry__2_CO_UNCONNECTED;
  wire [3:0]NLW_letra21__420_carry__2_O_UNCONNECTED;
  wire [2:0]NLW_letra21__420_carry__3_CO_UNCONNECTED;
  wire [3:0]NLW_letra21__420_carry__3_O_UNCONNECTED;
  wire [2:0]NLW_letra21__420_carry__4_CO_UNCONNECTED;
  wire [3:0]NLW_letra21__420_carry__4_O_UNCONNECTED;
  wire [2:0]NLW_letra21__420_carry__5_CO_UNCONNECTED;
  wire [3:0]NLW_letra21__420_carry__5_O_UNCONNECTED;
  wire [3:0]NLW_letra21__420_carry__6_CO_UNCONNECTED;
  wire [3:0]NLW_letra21__420_carry__6_O_UNCONNECTED;
  wire [3:0]NLW_letra21__479_carry_CO_UNCONNECTED;
  wire [2:0]NLW_letra21__74_carry_CO_UNCONNECTED;
  wire [0:0]NLW_letra21__74_carry_O_UNCONNECTED;
  wire [2:0]NLW_letra21__74_carry__0_CO_UNCONNECTED;
  wire [2:0]NLW_letra21__74_carry__1_CO_UNCONNECTED;
  wire [2:0]NLW_letra21__74_carry__2_CO_UNCONNECTED;
  wire [3:3]NLW_letra21__74_carry__2_O_UNCONNECTED;
  wire [2:0]NLW_letra21_carry_CO_UNCONNECTED;
  wire [3:1]NLW_letra21_carry_O_UNCONNECTED;
  wire [2:0]NLW_letra21_carry__0_CO_UNCONNECTED;
  wire [0:0]NLW_letra21_carry__0_O_UNCONNECTED;
  wire [2:0]NLW_letra21_carry__1_CO_UNCONNECTED;
  wire [2:0]NLW_letra21_carry__2_CO_UNCONNECTED;
  wire [3:3]NLW_letra21_carry__2_O_UNCONNECTED;

  LUT6 #(
    .INIT(64'h00000000DDDFDDD5)) 
    g0_b0__1_i_1
       (.I0(g0_b7__1_0),
        .I1(letra21__208_carry__4_i_8_0),
        .I2(letra21__336_carry__6_n_7),
        .I3(letra21__420_carry__6_n_2),
        .I4(letra21__479_carry_n_7),
        .I5(g0_b7__1_1),
        .O(g0_b0__1_i_6));
  LUT4 #(
    .INIT(16'hABA8)) 
    g0_b0__1_i_10
       (.I0(letra21__208_carry__5_n_5),
        .I1(letra21__336_carry__6_n_7),
        .I2(letra21__420_carry__6_n_2),
        .I3(letra21__479_carry_n_4),
        .O(g0_b0__1_i_10_n_0));
  LUT4 #(
    .INIT(16'hABA8)) 
    g0_b0__1_i_11
       (.I0(letra21__208_carry__5_n_6),
        .I1(letra21__336_carry__6_n_7),
        .I2(letra21__420_carry__6_n_2),
        .I3(letra21__479_carry_n_5),
        .O(g0_b0__1_i_11_n_0));
  LUT6 #(
    .INIT(64'h0000000533333305)) 
    g0_b0__1_i_12
       (.I0(letra21__479_carry_n_6),
        .I1(letra21__208_carry__5_n_7),
        .I2(letra21__479_carry_n_7),
        .I3(letra21__420_carry__6_n_2),
        .I4(letra21__336_carry__6_n_7),
        .I5(letra21__208_carry__4_i_8_0),
        .O(g0_b0__1_i_12_n_0));
  LUT6 #(
    .INIT(64'h5DD55D5DD5D5D5D5)) 
    g0_b0__1_i_4
       (.I0(g0_b7__1),
        .I1(g0_b7__1_0),
        .I2(g0_b0__1_i_10_n_0),
        .I3(g0_b0__1_i_11_n_0),
        .I4(g0_b0__1_i_12_n_0),
        .I5(letra230_out[3]),
        .O(g0_b0__1_i_13));
  LUT6 #(
    .INIT(64'h8787878887878777)) 
    g0_b0__1_i_7
       (.I0(letra21__479_carry_0),
        .I1(letra230_out[3]),
        .I2(letra21__208_carry__5_n_7),
        .I3(letra21__336_carry__6_n_7),
        .I4(letra21__420_carry__6_n_2),
        .I5(letra21__479_carry_n_6),
        .O(letra21__479_carry_2));
  LUT6 #(
    .INIT(64'h01FD01FDFE0201FD)) 
    g0_b0__1_i_8
       (.I0(letra21__479_carry_n_5),
        .I1(letra21__420_carry__6_n_2),
        .I2(letra21__336_carry__6_n_7),
        .I3(letra21__208_carry__5_n_6),
        .I4(letra230_out[3]),
        .I5(g0_b0__1_i_12_n_0),
        .O(g0_b0__1_i_12_0));
  LUT5 #(
    .INIT(32'h8700FF87)) 
    g0_b0__2_i_10
       (.I0(letra21__479_carry_0),
        .I1(letra230_out[3]),
        .I2(letra21__479_carry_1),
        .I3(Q[4]),
        .I4(CO),
        .O(letra10__1_carry__0_1));
  LUT5 #(
    .INIT(32'hC3B43C4B)) 
    g0_b0__2_i_11
       (.I0(letra21__479_carry_1),
        .I1(letra230_out[3]),
        .I2(g0_b0__1_i_11_n_0),
        .I3(letra21__479_carry_0),
        .I4(Q[1]),
        .O(\salida_reg[3] ));
  LUT4 #(
    .INIT(16'h5457)) 
    g0_b0__2_i_12
       (.I0(letra21__208_carry__5_n_7),
        .I1(letra21__336_carry__6_n_7),
        .I2(letra21__420_carry__6_n_2),
        .I3(letra21__479_carry_n_6),
        .O(letra21__479_carry_1));
  LUT4 #(
    .INIT(16'hABA8)) 
    g0_b0__2_i_5
       (.I0(letra21__208_carry__4_i_8_0),
        .I1(letra21__336_carry__6_n_7),
        .I2(letra21__420_carry__6_n_2),
        .I3(letra21__479_carry_n_7),
        .O(letra21__479_carry_0));
  LUT6 #(
    .INIT(64'h6A95956A956A6A95)) 
    g0_b0__2_i_7
       (.I0(Q[0]),
        .I1(letra21__479_carry_0),
        .I2(letra230_out[3]),
        .I3(letra21__479_carry_1),
        .I4(Q[4]),
        .I5(CO),
        .O(letra10__1_carry__0_0));
  LUT6 #(
    .INIT(64'h8778788700000000)) 
    g0_b0__2_i_9
       (.I0(letra21__479_carry_0),
        .I1(letra230_out[3]),
        .I2(letra21__479_carry_1),
        .I3(Q[4]),
        .I4(CO),
        .I5(Q[0]),
        .O(\salida_reg[2] ));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "PROPCONST SWEEP" *) 
  CARRY4 letra10__1_carry
       (.CI(1'b0),
        .CO({letra10__1_carry_n_0,NLW_letra10__1_carry_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,DI,1'b0}),
        .O(NLW_letra10__1_carry_O_UNCONNECTED[3:0]),
        .S(S));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 letra10__1_carry__0
       (.CI(letra10__1_carry_n_0),
        .CO({NLW_letra10__1_carry__0_CO_UNCONNECTED[3:2],CO,NLW_letra10__1_carry__0_CO_UNCONNECTED[0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,letra21_carry__1_i_12}),
        .O(NLW_letra10__1_carry__0_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,letra21_carry__1_i_12_0}));
  (* OPT_MODIFIED = "PROPCONST SWEEP" *) 
  CARRY4 letra21__104_carry
       (.CI(1'b0),
        .CO({letra21__104_carry_n_0,NLW_letra21__104_carry_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({letra21__208_carry__3_i_8,1'b0}),
        .O({letra21__104_carry_n_4,letra21__104_carry_n_5,letra21__104_carry_i_4,NLW_letra21__104_carry_O_UNCONNECTED[0]}),
        .S(letra21__208_carry__3_i_8_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 letra21__104_carry__0
       (.CI(letra21__104_carry_n_0),
        .CO({letra21__104_carry__0_n_0,NLW_letra21__104_carry__0_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({letra21__208_carry__3_i_5_1,letra21__208_carry__0_i_1_0[2:0]}),
        .O({letra21__104_carry__0_n_4,letra21__104_carry__0_n_5,letra21__104_carry__0_n_6,letra21__104_carry__0_n_7}),
        .S(letra21__208_carry__3_i_5_2));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 letra21__104_carry__1
       (.CI(letra21__104_carry__0_n_0),
        .CO({letra21__104_carry__1_n_0,NLW_letra21__104_carry__1_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({letra21__420_carry__1_1,letra21__420_carry__0_1,letra21__208_carry__4_i_5_0}),
        .O({letra21__104_carry__1_n_4,letra21__104_carry__1_n_5,letra21__104_carry__1_n_6,letra21__104_carry__1_n_7}),
        .S(letra21__208_carry__4_i_5_1));
  CARRY4 letra21__104_carry__2
       (.CI(letra21__104_carry__1_n_0),
        .CO({NLW_letra21__104_carry__2_CO_UNCONNECTED[3:2],letra21__104_carry__2_i_1,NLW_letra21__104_carry__2_CO_UNCONNECTED[0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,letra21__420_carry__1_0}),
        .O({NLW_letra21__104_carry__2_O_UNCONNECTED[3:1],letra21__104_carry__2_n_7}),
        .S({1'b0,1'b0,1'b1,letra21__208_carry__5_i_5_0}));
  (* OPT_MODIFIED = "PROPCONST SWEEP" *) 
  CARRY4 letra21__130_carry
       (.CI(1'b0),
        .CO({letra21__130_carry_n_0,NLW_letra21__130_carry_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({letra21__208_carry__3_i_5_3,1'b0}),
        .O({letra21__130_carry_n_4,letra21__130_carry_n_5,letra21__130_carry_n_6,letra21__130_carry_n_7}),
        .S(letra21__208_carry__3_i_5_4));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 letra21__130_carry__0
       (.CI(letra21__130_carry_n_0),
        .CO({letra21__130_carry__0_n_0,NLW_letra21__130_carry__0_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(letra21__208_carry__4_i_5_2),
        .O({letra21__130_carry__0_n_4,letra21__130_carry__0_n_5,letra21__130_carry__0_n_6,letra21__130_carry__0_n_7}),
        .S(letra21__208_carry__4_i_5_3));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 letra21__130_carry__1
       (.CI(letra21__130_carry__0_n_0),
        .CO({letra21__130_carry__1_n_0,NLW_letra21__130_carry__1_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(letra21__208_carry__5_i_5_1),
        .O({letra21__130_carry__1_n_4,letra21__130_carry__1_n_5,letra21__130_carry__1_n_6,letra21__130_carry__1_n_7}),
        .S(letra21__208_carry__5_i_5_2));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 letra21__130_carry__2
       (.CI(letra21__130_carry__1_n_0),
        .CO({letra21__130_carry__2_n_0,NLW_letra21__130_carry__2_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({letra21_carry__2_i_6,letra21_carry__2_i_6,letra21_carry__2_i_6,letra21_carry__2_i_6}),
        .O({letra21__130_carry__2_n_4,letra21__130_carry__2_n_5,letra21__130_carry__2_n_6,letra21__130_carry__2_n_7}),
        .S({1'b1,1'b1,1'b1,letra21__208_carry__6_i_6_0}));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 letra21__130_carry__3
       (.CI(letra21__130_carry__2_n_0),
        .CO({letra21__130_carry__3_n_0,NLW_letra21__130_carry__3_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({letra21_carry__2_i_6,letra21_carry__2_i_6,letra21_carry__2_i_6,letra21_carry__2_i_6}),
        .O({letra21__130_carry__3_n_4,letra21__130_carry__3_n_5,letra21__130_carry__3_n_6,letra21__130_carry__3_n_7}),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 letra21__130_carry__4
       (.CI(letra21__130_carry__3_n_0),
        .CO({letra21__130_carry__4_n_0,NLW_letra21__130_carry__4_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({letra21_carry__2_i_6,letra21_carry__2_i_6,letra21_carry__2_i_6,letra21_carry__2_i_6}),
        .O({letra21__130_carry__4_n_4,letra21__130_carry__4_n_5,letra21__130_carry__4_n_6,letra21__130_carry__4_n_7}),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 letra21__130_carry__5
       (.CI(letra21__130_carry__4_n_0),
        .CO({letra21__130_carry__5_n_0,NLW_letra21__130_carry__5_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({letra21_carry__2_i_6,letra21_carry__2_i_6,letra21_carry__2_i_6,letra21_carry__2_i_6}),
        .O({letra21__130_carry__5_n_4,letra21__130_carry__5_n_5,letra21__130_carry__5_n_6,letra21__130_carry__5_n_7}),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 letra21__130_carry__6
       (.CI(letra21__130_carry__5_n_0),
        .CO({letra21_carry__2_0,NLW_letra21__130_carry__6_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({letra21_carry__2_i_6,letra21_carry__2_i_6,letra21_carry__2_i_6,letra21_carry__2_i_6}),
        .O(letra21_carry__2_1),
        .S({1'b1,1'b1,1'b1,1'b1}));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 letra21__208_carry
       (.CI(1'b0),
        .CO({letra21__208_carry_n_0,NLW_letra21__208_carry_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({letra21__208_carry_i_1_n_0,letra21__208_carry_i_2_n_0,letra21__208_carry_i_3_n_0,letra21__208_carry__0_0}),
        .O(NLW_letra21__208_carry_O_UNCONNECTED[3:0]),
        .S({letra21__208_carry_i_5_n_0,letra21__208_carry_i_6_n_0,letra21__208_carry_i_7_n_0,letra21__208_carry_i_8_n_0}));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 letra21__208_carry__0
       (.CI(letra21__208_carry_n_0),
        .CO({letra21__208_carry__0_n_0,NLW_letra21__208_carry__0_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({letra21__208_carry__0_i_1_n_0,letra21__208_carry__0_i_2_n_0,letra21__208_carry__0_i_3_n_0,letra21__208_carry__0_i_4_n_0}),
        .O(NLW_letra21__208_carry__0_O_UNCONNECTED[3:0]),
        .S({letra21__208_carry__0_i_5_n_0,letra21__208_carry__0_i_6_n_0,letra21__208_carry__0_i_7_n_0,letra21__208_carry__0_i_8_n_0}));
  (* HLUTNM = "lutpair7" *) 
  LUT3 #(
    .INIT(8'h28)) 
    letra21__208_carry__0_i_1
       (.I0(letra21__37_carry__0_n_7),
        .I1(letra21_carry_n_7),
        .I2(letra21_carry__2_n_7),
        .O(letra21__208_carry__0_i_1_n_0));
  (* HLUTNM = "lutpair6" *) 
  LUT2 #(
    .INIT(4'h8)) 
    letra21__208_carry__0_i_2
       (.I0(letra21__37_carry_n_4),
        .I1(letra21_carry__1_n_4),
        .O(letra21__208_carry__0_i_2_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    letra21__208_carry__0_i_3
       (.I0(letra21__37_carry_n_5),
        .I1(letra21_carry__1_n_5),
        .O(letra21__208_carry__0_i_3_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    letra21__208_carry__0_i_4
       (.I0(letra21__37_carry_n_6),
        .I1(letra21_carry__1_n_6),
        .O(letra21__208_carry__0_i_4_n_0));
  LUT6 #(
    .INIT(64'h9669699669966996)) 
    letra21__208_carry__0_i_5
       (.I0(letra21__208_carry__0_i_1_n_0),
        .I1(letra21__37_carry__0_n_6),
        .I2(letra21__74_carry_n_6),
        .I3(letra21_carry__2_n_6),
        .I4(letra21_carry__2_n_7),
        .I5(letra21_carry_n_7),
        .O(letra21__208_carry__0_i_5_n_0));
  (* HLUTNM = "lutpair7" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    letra21__208_carry__0_i_6
       (.I0(letra21__37_carry__0_n_7),
        .I1(letra21_carry_n_7),
        .I2(letra21_carry__2_n_7),
        .I3(letra21__208_carry__0_i_2_n_0),
        .O(letra21__208_carry__0_i_6_n_0));
  (* HLUTNM = "lutpair6" *) 
  LUT4 #(
    .INIT(16'h9666)) 
    letra21__208_carry__0_i_7
       (.I0(letra21__37_carry_n_4),
        .I1(letra21_carry__1_n_4),
        .I2(letra21_carry__1_n_5),
        .I3(letra21__37_carry_n_5),
        .O(letra21__208_carry__0_i_7_n_0));
  LUT4 #(
    .INIT(16'h8778)) 
    letra21__208_carry__0_i_8
       (.I0(letra21_carry__1_n_6),
        .I1(letra21__37_carry_n_6),
        .I2(letra21_carry__1_n_5),
        .I3(letra21__37_carry_n_5),
        .O(letra21__208_carry__0_i_8_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 letra21__208_carry__1
       (.CI(letra21__208_carry__0_n_0),
        .CO({letra21__208_carry__1_n_0,NLW_letra21__208_carry__1_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({letra21__208_carry__1_i_1_n_0,letra21__208_carry__1_i_2_n_0,letra21__208_carry__1_i_3_n_0,letra21__208_carry__1_i_4_n_0}),
        .O(NLW_letra21__208_carry__1_O_UNCONNECTED[3:0]),
        .S({letra21__208_carry__1_i_5_n_0,letra21__208_carry__1_i_6_n_0,letra21__208_carry__1_i_7_n_0,letra21__208_carry__1_i_8_n_0}));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 letra21__208_carry__10
       (.CI(letra21__208_carry__9_n_0),
        .CO({letra21__208_carry__10_n_0,NLW_letra21__208_carry__10_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({letra21__208_carry__10_i_1_n_0,letra21__208_carry__10_i_2_n_0,letra21__208_carry__10_i_3_n_0,letra21__208_carry__10_i_4_n_0}),
        .O({letra21__208_carry__10_n_4,letra21__208_carry__10_n_5,letra21__208_carry__10_n_6,letra21__208_carry__10_n_7}),
        .S({letra21__208_carry__10_i_5_n_0,letra21__208_carry__10_i_6_n_0,letra21__208_carry__10_i_7_n_0,letra21__208_carry__10_i_8_n_0}));
  LUT5 #(
    .INIT(32'h477D411D)) 
    letra21__208_carry__10_i_1
       (.I0(letra21__37_carry__2_i_1),
        .I1(letra21_carry__2_1[0]),
        .I2(letra21__104_carry__2_i_1),
        .I3(letra21__74_carry__2_i_3),
        .I4(letra21__130_carry__5_n_4),
        .O(letra21__208_carry__10_i_1_n_0));
  LUT5 #(
    .INIT(32'h03EB283F)) 
    letra21__208_carry__10_i_2
       (.I0(letra21__130_carry__5_n_5),
        .I1(letra21__104_carry__2_i_1),
        .I2(letra21__74_carry__2_i_3),
        .I3(letra21__37_carry__2_i_1),
        .I4(letra21__130_carry__5_n_4),
        .O(letra21__208_carry__10_i_2_n_0));
  LUT5 #(
    .INIT(32'h03EB283F)) 
    letra21__208_carry__10_i_3
       (.I0(letra21__130_carry__5_n_6),
        .I1(letra21__104_carry__2_i_1),
        .I2(letra21__74_carry__2_i_3),
        .I3(letra21__37_carry__2_i_1),
        .I4(letra21__130_carry__5_n_5),
        .O(letra21__208_carry__10_i_3_n_0));
  LUT5 #(
    .INIT(32'h11F96077)) 
    letra21__208_carry__10_i_4
       (.I0(letra21__104_carry__2_i_1),
        .I1(letra21__74_carry__2_i_3),
        .I2(letra21__130_carry__5_n_7),
        .I3(letra21__37_carry__2_i_1),
        .I4(letra21__130_carry__5_n_6),
        .O(letra21__208_carry__10_i_4_n_0));
  LUT6 #(
    .INIT(64'h566AA995A995566A)) 
    letra21__208_carry__10_i_5
       (.I0(letra21__208_carry__10_i_1_n_0),
        .I1(letra21_carry__2_1[0]),
        .I2(letra21__104_carry__2_i_1),
        .I3(letra21__74_carry__2_i_3),
        .I4(letra21__37_carry__2_i_1),
        .I5(letra21_carry__2_1[1]),
        .O(letra21__208_carry__10_i_5_n_0));
  LUT6 #(
    .INIT(64'h566AA995A995566A)) 
    letra21__208_carry__10_i_6
       (.I0(letra21__208_carry__10_i_2_n_0),
        .I1(letra21__104_carry__2_i_1),
        .I2(letra21__74_carry__2_i_3),
        .I3(letra21__130_carry__5_n_4),
        .I4(letra21__37_carry__2_i_1),
        .I5(letra21_carry__2_1[0]),
        .O(letra21__208_carry__10_i_6_n_0));
  LUT6 #(
    .INIT(64'h566AA995A995566A)) 
    letra21__208_carry__10_i_7
       (.I0(letra21__208_carry__10_i_3_n_0),
        .I1(letra21__130_carry__5_n_5),
        .I2(letra21__104_carry__2_i_1),
        .I3(letra21__74_carry__2_i_3),
        .I4(letra21__37_carry__2_i_1),
        .I5(letra21__130_carry__5_n_4),
        .O(letra21__208_carry__10_i_7_n_0));
  LUT6 #(
    .INIT(64'h566AA995A995566A)) 
    letra21__208_carry__10_i_8
       (.I0(letra21__208_carry__10_i_4_n_0),
        .I1(letra21__130_carry__5_n_6),
        .I2(letra21__104_carry__2_i_1),
        .I3(letra21__74_carry__2_i_3),
        .I4(letra21__37_carry__2_i_1),
        .I5(letra21__130_carry__5_n_5),
        .O(letra21__208_carry__10_i_8_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 letra21__208_carry__11
       (.CI(letra21__208_carry__10_n_0),
        .CO(NLW_letra21__208_carry__11_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,letra21__336_carry__5_0}),
        .O({NLW_letra21__208_carry__11_O_UNCONNECTED[3],letra21__208_carry__11_n_5,letra21__208_carry__11_n_6,letra21__208_carry__11_n_7}),
        .S({1'b0,letra21__336_carry__5_1}));
  LUT4 #(
    .INIT(16'hB190)) 
    letra21__208_carry__1_i_1
       (.I0(letra21__74_carry__0_n_7),
        .I1(letra21_carry__2_i_6),
        .I2(letra21__37_carry__1_n_7),
        .I3(letra21__74_carry_n_4),
        .O(letra21__208_carry__1_i_1_n_0));
  LUT5 #(
    .INIT(32'hEA8080EA)) 
    letra21__208_carry__1_i_2
       (.I0(letra21__37_carry__0_n_4),
        .I1(letra21__74_carry_n_5),
        .I2(letra21_carry__2_n_5),
        .I3(letra21_carry__2_i_6),
        .I4(letra21__74_carry_n_4),
        .O(letra21__208_carry__1_i_2_n_0));
  LUT5 #(
    .INIT(32'hBE282828)) 
    letra21__208_carry__1_i_3
       (.I0(letra21__37_carry__0_n_5),
        .I1(letra21__74_carry_n_5),
        .I2(letra21_carry__2_n_5),
        .I3(letra21_carry__2_n_6),
        .I4(letra21__74_carry_n_6),
        .O(letra21__208_carry__1_i_3_n_0));
  LUT5 #(
    .INIT(32'hBE282828)) 
    letra21__208_carry__1_i_4
       (.I0(letra21__37_carry__0_n_6),
        .I1(letra21__74_carry_n_6),
        .I2(letra21_carry__2_n_6),
        .I3(letra21_carry__2_n_7),
        .I4(letra21_carry_n_7),
        .O(letra21__208_carry__1_i_4_n_0));
  LUT6 #(
    .INIT(64'h6996699669969669)) 
    letra21__208_carry__1_i_5
       (.I0(letra21__208_carry__1_i_1_n_0),
        .I1(letra21__37_carry__1_n_6),
        .I2(letra21__74_carry__0_i_4[0]),
        .I3(letra21__208_carry__0_i_5_0[0]),
        .I4(letra21_carry__2_i_6),
        .I5(letra21__74_carry__0_n_7),
        .O(letra21__208_carry__1_i_5_n_0));
  LUT6 #(
    .INIT(64'hE1871E78871E78E1)) 
    letra21__208_carry__1_i_6
       (.I0(letra21__208_carry__1_i_9_n_0),
        .I1(letra21__37_carry__0_n_4),
        .I2(letra21__37_carry__1_n_7),
        .I3(letra21_carry__2_i_6),
        .I4(letra21__74_carry__0_n_7),
        .I5(letra21__74_carry_n_4),
        .O(letra21__208_carry__1_i_6_n_0));
  LUT6 #(
    .INIT(64'h6999966696666999)) 
    letra21__208_carry__1_i_7
       (.I0(letra21__208_carry__1_i_3_n_0),
        .I1(letra21__37_carry__0_n_4),
        .I2(letra21__74_carry_n_5),
        .I3(letra21_carry__2_n_5),
        .I4(letra21_carry__2_i_6),
        .I5(letra21__74_carry_n_4),
        .O(letra21__208_carry__1_i_7_n_0));
  LUT6 #(
    .INIT(64'h9669699669966996)) 
    letra21__208_carry__1_i_8
       (.I0(letra21__208_carry__1_i_4_n_0),
        .I1(letra21__37_carry__0_n_5),
        .I2(letra21__74_carry_n_5),
        .I3(letra21_carry__2_n_5),
        .I4(letra21_carry__2_n_6),
        .I5(letra21__74_carry_n_6),
        .O(letra21__208_carry__1_i_8_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    letra21__208_carry__1_i_9
       (.I0(letra21_carry__2_n_5),
        .I1(letra21__74_carry_n_5),
        .O(letra21__208_carry__1_i_9_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 letra21__208_carry__2
       (.CI(letra21__208_carry__1_n_0),
        .CO({letra21__208_carry__2_n_0,NLW_letra21__208_carry__2_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({letra21__208_carry__3_0[1],letra21__208_carry__2_i_10,letra21__208_carry__3_0[0],letra21__208_carry__2_i_4_n_0}),
        .O(NLW_letra21__208_carry__2_O_UNCONNECTED[3:0]),
        .S({letra21__208_carry__3_1,letra21__208_carry__2_i_8_n_0}));
  LUT6 #(
    .INIT(64'hBEEB3AA33AA32882)) 
    letra21__208_carry__2_i_2
       (.I0(letra21__37_carry__1_i_7[1]),
        .I1(letra21_carry__2_i_6),
        .I2(letra21__208_carry__1_i_6_0[0]),
        .I3(letra21__74_carry__0_i_4[2]),
        .I4(letra21__74_carry__0_i_4[1]),
        .I5(letra21__208_carry__2_i_6),
        .O(letra21__208_carry__2_i_10));
  LUT5 #(
    .INIT(32'h28EB2882)) 
    letra21__208_carry__2_i_4
       (.I0(letra21__37_carry__1_n_6),
        .I1(letra21__74_carry__0_i_4[0]),
        .I2(letra21__208_carry__0_i_5_0[0]),
        .I3(letra21_carry__2_i_6),
        .I4(letra21__74_carry__0_n_7),
        .O(letra21__208_carry__2_i_4_n_0));
  LUT6 #(
    .INIT(64'h9A5965A665A69A59)) 
    letra21__208_carry__2_i_8
       (.I0(letra21__208_carry__2_i_4_n_0),
        .I1(letra21__208_carry__0_i_5_0[0]),
        .I2(letra21_carry__2_i_6),
        .I3(letra21__74_carry__0_i_4[0]),
        .I4(letra21__37_carry__1_i_7[0]),
        .I5(letra21__208_carry__2_0),
        .O(letra21__208_carry__2_i_8_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 letra21__208_carry__3
       (.CI(letra21__208_carry__2_n_0),
        .CO({letra21__208_carry__3_n_0,NLW_letra21__208_carry__3_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({letra21__208_carry__3_i_1_n_0,letra21__208_carry__3_i_2_n_0,letra21__208_carry__3_i_3_n_0,letra21__208_carry__4_0}),
        .O(NLW_letra21__208_carry__3_O_UNCONNECTED[3:0]),
        .S({letra21__208_carry__3_i_5_n_0,letra21__208_carry__3_i_6_n_0,letra21__208_carry__3_i_7_n_0,letra21__208_carry__4_1}));
  LUT5 #(
    .INIT(32'hBB2B2B22)) 
    letra21__208_carry__3_i_1
       (.I0(letra21__208_carry__3_i_9_n_0),
        .I1(letra21__37_carry__2_i_1),
        .I2(letra21_carry__2_i_6),
        .I3(letra21__74_carry__1_n_4),
        .I4(letra21__104_carry_n_4),
        .O(letra21__208_carry__3_i_1_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    letra21__208_carry__3_i_10
       (.I0(letra21__104_carry__0_n_6),
        .I1(letra21__74_carry__2_n_6),
        .I2(letra21__130_carry_n_6),
        .O(letra21__208_carry__3_i_10_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'h96)) 
    letra21__208_carry__3_i_11
       (.I0(letra21__104_carry_n_4),
        .I1(letra21__74_carry__1_n_4),
        .I2(letra21_carry__2_i_6),
        .O(letra21__208_carry__3_i_11_n_0));
  (* SOFT_HLUTNM = "soft_lutpair0" *) 
  LUT3 #(
    .INIT(8'h96)) 
    letra21__208_carry__3_i_12
       (.I0(letra21__104_carry_n_5),
        .I1(letra21__74_carry__1_n_5),
        .I2(letra21_carry__2_i_6),
        .O(letra21_carry__2_2));
  LUT6 #(
    .INIT(64'h7DD7355335531441)) 
    letra21__208_carry__3_i_2
       (.I0(letra21__37_carry__2_i_1),
        .I1(letra21_carry__2_i_6),
        .I2(letra21__74_carry__1_n_4),
        .I3(letra21__104_carry_n_4),
        .I4(letra21__74_carry__1_n_5),
        .I5(letra21__104_carry_n_5),
        .O(letra21__208_carry__3_i_2_n_0));
  LUT6 #(
    .INIT(64'h7DD7355335531441)) 
    letra21__208_carry__3_i_3
       (.I0(letra21__37_carry__2_i_1),
        .I1(letra21_carry__2_i_6),
        .I2(letra21__74_carry__1_n_5),
        .I3(letra21__104_carry_n_5),
        .I4(letra21__74_carry__1_i_5[1]),
        .I5(letra21__104_carry_i_4),
        .O(letra21__208_carry__3_i_3_n_0));
  LUT6 #(
    .INIT(64'hA995566A566AA995)) 
    letra21__208_carry__3_i_5
       (.I0(letra21__208_carry__3_i_1_n_0),
        .I1(letra21__130_carry_n_7),
        .I2(letra21__74_carry__2_n_7),
        .I3(letra21__104_carry__0_n_7),
        .I4(letra21__37_carry__2_i_1),
        .I5(letra21__208_carry__3_i_10_n_0),
        .O(letra21__208_carry__3_i_5_n_0));
  LUT6 #(
    .INIT(64'hA665599A599AA665)) 
    letra21__208_carry__3_i_6
       (.I0(letra21__208_carry__3_i_2_n_0),
        .I1(letra21_carry__2_i_6),
        .I2(letra21__74_carry__1_n_4),
        .I3(letra21__104_carry_n_4),
        .I4(letra21__37_carry__2_i_1),
        .I5(letra21__208_carry__3_i_9_n_0),
        .O(letra21__208_carry__3_i_6_n_0));
  LUT6 #(
    .INIT(64'h599AA665A665599A)) 
    letra21__208_carry__3_i_7
       (.I0(letra21__208_carry__3_i_3_n_0),
        .I1(letra21_carry__2_i_6),
        .I2(letra21__74_carry__1_n_5),
        .I3(letra21__104_carry_n_5),
        .I4(letra21__37_carry__2_i_1),
        .I5(letra21__208_carry__3_i_11_n_0),
        .O(letra21__208_carry__3_i_7_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    letra21__208_carry__3_i_9
       (.I0(letra21__104_carry__0_n_7),
        .I1(letra21__74_carry__2_n_7),
        .I2(letra21__130_carry_n_7),
        .O(letra21__208_carry__3_i_9_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 letra21__208_carry__4
       (.CI(letra21__208_carry__3_n_0),
        .CO({letra21__208_carry__4_n_0,NLW_letra21__208_carry__4_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({letra21__208_carry__4_i_1_n_0,letra21__208_carry__4_i_2_n_0,letra21__208_carry__4_i_3_n_0,letra21__208_carry__4_i_4_n_0}),
        .O({letra21__208_carry__4_i_8_0,NLW_letra21__208_carry__4_O_UNCONNECTED[2:0]}),
        .S({letra21__208_carry__4_i_5_n_0,letra21__208_carry__4_i_6_n_0,letra21__208_carry__4_i_7_n_0,letra21__208_carry__4_i_8_n_0}));
  LUT6 #(
    .INIT(64'h32B380FE80FE32B3)) 
    letra21__208_carry__4_i_1
       (.I0(letra21__104_carry__0_n_4),
        .I1(letra21__74_carry__2_i_3),
        .I2(letra21__130_carry_n_4),
        .I3(letra21__37_carry__2_i_1),
        .I4(letra21__104_carry__1_n_7),
        .I5(letra21__130_carry__0_n_7),
        .O(letra21__208_carry__4_i_1_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    letra21__208_carry__4_i_10
       (.I0(letra21__104_carry__0_n_5),
        .I1(letra21__74_carry__2_n_5),
        .I2(letra21__130_carry_n_5),
        .O(letra21__208_carry__4_i_10_n_0));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'h69)) 
    letra21__208_carry__4_i_11
       (.I0(letra21__130_carry__0_n_6),
        .I1(letra21__74_carry__2_i_3),
        .I2(letra21__104_carry__1_n_6),
        .O(letra21__208_carry__4_i_11_n_0));
  (* SOFT_HLUTNM = "soft_lutpair2" *) 
  LUT3 #(
    .INIT(8'h69)) 
    letra21__208_carry__4_i_12
       (.I0(letra21__130_carry__0_n_7),
        .I1(letra21__74_carry__2_i_3),
        .I2(letra21__104_carry__1_n_7),
        .O(letra21__208_carry__4_i_12_n_0));
  LUT5 #(
    .INIT(32'hBBB2B222)) 
    letra21__208_carry__4_i_2
       (.I0(letra21__208_carry__4_i_9_n_0),
        .I1(letra21__37_carry__2_i_1),
        .I2(letra21__130_carry_n_5),
        .I3(letra21__74_carry__2_n_5),
        .I4(letra21__104_carry__0_n_5),
        .O(letra21__208_carry__4_i_2_n_0));
  LUT5 #(
    .INIT(32'hBBB2B222)) 
    letra21__208_carry__4_i_3
       (.I0(letra21__208_carry__4_i_10_n_0),
        .I1(letra21__37_carry__2_i_1),
        .I2(letra21__130_carry_n_6),
        .I3(letra21__74_carry__2_n_6),
        .I4(letra21__104_carry__0_n_6),
        .O(letra21__208_carry__4_i_3_n_0));
  LUT5 #(
    .INIT(32'hBBB2B222)) 
    letra21__208_carry__4_i_4
       (.I0(letra21__208_carry__3_i_10_n_0),
        .I1(letra21__37_carry__2_i_1),
        .I2(letra21__130_carry_n_7),
        .I3(letra21__74_carry__2_n_7),
        .I4(letra21__104_carry__0_n_7),
        .O(letra21__208_carry__4_i_4_n_0));
  LUT6 #(
    .INIT(64'h9A5965A665A69A59)) 
    letra21__208_carry__4_i_5
       (.I0(letra21__208_carry__4_i_1_n_0),
        .I1(letra21__104_carry__1_n_7),
        .I2(letra21__74_carry__2_i_3),
        .I3(letra21__130_carry__0_n_7),
        .I4(letra21__37_carry__2_i_1),
        .I5(letra21__208_carry__4_i_11_n_0),
        .O(letra21__208_carry__4_i_5_n_0));
  LUT6 #(
    .INIT(64'h9A5965A665A69A59)) 
    letra21__208_carry__4_i_6
       (.I0(letra21__208_carry__4_i_2_n_0),
        .I1(letra21__104_carry__0_n_4),
        .I2(letra21__74_carry__2_i_3),
        .I3(letra21__130_carry_n_4),
        .I4(letra21__37_carry__2_i_1),
        .I5(letra21__208_carry__4_i_12_n_0),
        .O(letra21__208_carry__4_i_6_n_0));
  LUT6 #(
    .INIT(64'hA995566A566AA995)) 
    letra21__208_carry__4_i_7
       (.I0(letra21__208_carry__4_i_3_n_0),
        .I1(letra21__130_carry_n_5),
        .I2(letra21__74_carry__2_n_5),
        .I3(letra21__104_carry__0_n_5),
        .I4(letra21__37_carry__2_i_1),
        .I5(letra21__208_carry__4_i_9_n_0),
        .O(letra21__208_carry__4_i_7_n_0));
  LUT6 #(
    .INIT(64'hA995566A566AA995)) 
    letra21__208_carry__4_i_8
       (.I0(letra21__208_carry__4_i_4_n_0),
        .I1(letra21__130_carry_n_6),
        .I2(letra21__74_carry__2_n_6),
        .I3(letra21__104_carry__0_n_6),
        .I4(letra21__37_carry__2_i_1),
        .I5(letra21__208_carry__4_i_10_n_0),
        .O(letra21__208_carry__4_i_8_n_0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'h69)) 
    letra21__208_carry__4_i_9
       (.I0(letra21__130_carry_n_4),
        .I1(letra21__74_carry__2_i_3),
        .I2(letra21__104_carry__0_n_4),
        .O(letra21__208_carry__4_i_9_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 letra21__208_carry__5
       (.CI(letra21__208_carry__4_n_0),
        .CO({letra21__208_carry__5_n_0,NLW_letra21__208_carry__5_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({letra21__208_carry__5_i_1_n_0,letra21__208_carry__5_i_2_n_0,letra21__208_carry__5_i_3_n_0,letra21__208_carry__5_i_4_n_0}),
        .O({letra21__208_carry__5_n_4,letra21__208_carry__5_n_5,letra21__208_carry__5_n_6,letra21__208_carry__5_n_7}),
        .S({letra21__208_carry__5_i_5_n_0,letra21__208_carry__5_i_6_n_0,letra21__208_carry__5_i_7_n_0,letra21__208_carry__5_i_8_n_0}));
  LUT6 #(
    .INIT(64'h32B380FE80FE32B3)) 
    letra21__208_carry__5_i_1
       (.I0(letra21__104_carry__1_n_4),
        .I1(letra21__74_carry__2_i_3),
        .I2(letra21__130_carry__0_n_4),
        .I3(letra21__37_carry__2_i_1),
        .I4(letra21__104_carry__2_n_7),
        .I5(letra21__130_carry__1_n_7),
        .O(letra21__208_carry__5_i_1_n_0));
  LUT3 #(
    .INIT(8'h69)) 
    letra21__208_carry__5_i_10
       (.I0(letra21__130_carry__1_n_7),
        .I1(letra21__74_carry__2_i_3),
        .I2(letra21__104_carry__2_n_7),
        .O(letra21__208_carry__5_i_10_n_0));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'h69)) 
    letra21__208_carry__5_i_11
       (.I0(letra21__130_carry__0_n_4),
        .I1(letra21__74_carry__2_i_3),
        .I2(letra21__104_carry__1_n_4),
        .O(letra21__208_carry__5_i_11_n_0));
  (* SOFT_HLUTNM = "soft_lutpair3" *) 
  LUT3 #(
    .INIT(8'h69)) 
    letra21__208_carry__5_i_12
       (.I0(letra21__130_carry__0_n_5),
        .I1(letra21__74_carry__2_i_3),
        .I2(letra21__104_carry__1_n_5),
        .O(letra21__208_carry__5_i_12_n_0));
  LUT6 #(
    .INIT(64'h32B380FE80FE32B3)) 
    letra21__208_carry__5_i_2
       (.I0(letra21__104_carry__1_n_5),
        .I1(letra21__74_carry__2_i_3),
        .I2(letra21__130_carry__0_n_5),
        .I3(letra21__37_carry__2_i_1),
        .I4(letra21__104_carry__1_n_4),
        .I5(letra21__130_carry__0_n_4),
        .O(letra21__208_carry__5_i_2_n_0));
  LUT6 #(
    .INIT(64'h32B380FE80FE32B3)) 
    letra21__208_carry__5_i_3
       (.I0(letra21__104_carry__1_n_6),
        .I1(letra21__74_carry__2_i_3),
        .I2(letra21__130_carry__0_n_6),
        .I3(letra21__37_carry__2_i_1),
        .I4(letra21__104_carry__1_n_5),
        .I5(letra21__130_carry__0_n_5),
        .O(letra21__208_carry__5_i_3_n_0));
  LUT6 #(
    .INIT(64'h32B380FE80FE32B3)) 
    letra21__208_carry__5_i_4
       (.I0(letra21__104_carry__1_n_7),
        .I1(letra21__74_carry__2_i_3),
        .I2(letra21__130_carry__0_n_7),
        .I3(letra21__37_carry__2_i_1),
        .I4(letra21__104_carry__1_n_6),
        .I5(letra21__130_carry__0_n_6),
        .O(letra21__208_carry__5_i_4_n_0));
  LUT6 #(
    .INIT(64'h9A5965A665A69A59)) 
    letra21__208_carry__5_i_5
       (.I0(letra21__208_carry__5_i_1_n_0),
        .I1(letra21__104_carry__2_n_7),
        .I2(letra21__74_carry__2_i_3),
        .I3(letra21__130_carry__1_n_7),
        .I4(letra21__37_carry__2_i_1),
        .I5(letra21__208_carry__5_i_9_n_0),
        .O(letra21__208_carry__5_i_5_n_0));
  LUT6 #(
    .INIT(64'h9A5965A665A69A59)) 
    letra21__208_carry__5_i_6
       (.I0(letra21__208_carry__5_i_2_n_0),
        .I1(letra21__104_carry__1_n_4),
        .I2(letra21__74_carry__2_i_3),
        .I3(letra21__130_carry__0_n_4),
        .I4(letra21__37_carry__2_i_1),
        .I5(letra21__208_carry__5_i_10_n_0),
        .O(letra21__208_carry__5_i_6_n_0));
  LUT6 #(
    .INIT(64'h9A5965A665A69A59)) 
    letra21__208_carry__5_i_7
       (.I0(letra21__208_carry__5_i_3_n_0),
        .I1(letra21__104_carry__1_n_5),
        .I2(letra21__74_carry__2_i_3),
        .I3(letra21__130_carry__0_n_5),
        .I4(letra21__37_carry__2_i_1),
        .I5(letra21__208_carry__5_i_11_n_0),
        .O(letra21__208_carry__5_i_7_n_0));
  LUT6 #(
    .INIT(64'h9A5965A665A69A59)) 
    letra21__208_carry__5_i_8
       (.I0(letra21__208_carry__5_i_4_n_0),
        .I1(letra21__104_carry__1_n_6),
        .I2(letra21__74_carry__2_i_3),
        .I3(letra21__130_carry__0_n_6),
        .I4(letra21__37_carry__2_i_1),
        .I5(letra21__208_carry__5_i_12_n_0),
        .O(letra21__208_carry__5_i_8_n_0));
  (* SOFT_HLUTNM = "soft_lutpair1" *) 
  LUT3 #(
    .INIT(8'h96)) 
    letra21__208_carry__5_i_9
       (.I0(letra21__104_carry__2_i_1),
        .I1(letra21__74_carry__2_i_3),
        .I2(letra21__130_carry__1_n_6),
        .O(letra21__208_carry__5_i_9_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 letra21__208_carry__6
       (.CI(letra21__208_carry__5_n_0),
        .CO({letra21__208_carry__6_n_0,NLW_letra21__208_carry__6_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({letra21__208_carry__6_i_1_n_0,letra21__208_carry__6_i_2_n_0,letra21__208_carry__6_i_3_n_0,letra21__208_carry__6_i_4_n_0}),
        .O({letra21__208_carry__6_n_4,letra21__208_carry__6_n_5,letra21__208_carry__6_n_6,letra21__208_carry__6_n_7}),
        .S({letra21__208_carry__6_i_5_n_0,letra21__208_carry__6_i_6_n_0,letra21__208_carry__6_i_7_n_0,letra21__208_carry__6_i_8_n_0}));
  LUT5 #(
    .INIT(32'h477D411D)) 
    letra21__208_carry__6_i_1
       (.I0(letra21__37_carry__2_i_1),
        .I1(letra21__130_carry__2_n_7),
        .I2(letra21__74_carry__2_i_3),
        .I3(letra21__104_carry__2_i_1),
        .I4(letra21__130_carry__1_n_4),
        .O(letra21__208_carry__6_i_1_n_0));
  LUT5 #(
    .INIT(32'h03EB283F)) 
    letra21__208_carry__6_i_2
       (.I0(letra21__130_carry__1_n_5),
        .I1(letra21__74_carry__2_i_3),
        .I2(letra21__104_carry__2_i_1),
        .I3(letra21__37_carry__2_i_1),
        .I4(letra21__130_carry__1_n_4),
        .O(letra21__208_carry__6_i_2_n_0));
  LUT5 #(
    .INIT(32'h03EB283F)) 
    letra21__208_carry__6_i_3
       (.I0(letra21__130_carry__1_n_6),
        .I1(letra21__74_carry__2_i_3),
        .I2(letra21__104_carry__2_i_1),
        .I3(letra21__37_carry__2_i_1),
        .I4(letra21__130_carry__1_n_5),
        .O(letra21__208_carry__6_i_3_n_0));
  LUT6 #(
    .INIT(64'h80FE32B332B380FE)) 
    letra21__208_carry__6_i_4
       (.I0(letra21__104_carry__2_n_7),
        .I1(letra21__74_carry__2_i_3),
        .I2(letra21__130_carry__1_n_7),
        .I3(letra21__37_carry__2_i_1),
        .I4(letra21__130_carry__1_n_6),
        .I5(letra21__104_carry__2_i_1),
        .O(letra21__208_carry__6_i_4_n_0));
  LUT6 #(
    .INIT(64'h566AA995A995566A)) 
    letra21__208_carry__6_i_5
       (.I0(letra21__208_carry__6_i_1_n_0),
        .I1(letra21__130_carry__2_n_7),
        .I2(letra21__74_carry__2_i_3),
        .I3(letra21__104_carry__2_i_1),
        .I4(letra21__37_carry__2_i_1),
        .I5(letra21__130_carry__2_n_6),
        .O(letra21__208_carry__6_i_5_n_0));
  LUT6 #(
    .INIT(64'h566AA995A995566A)) 
    letra21__208_carry__6_i_6
       (.I0(letra21__208_carry__6_i_2_n_0),
        .I1(letra21__130_carry__1_n_4),
        .I2(letra21__74_carry__2_i_3),
        .I3(letra21__104_carry__2_i_1),
        .I4(letra21__37_carry__2_i_1),
        .I5(letra21__130_carry__2_n_7),
        .O(letra21__208_carry__6_i_6_n_0));
  LUT6 #(
    .INIT(64'h566AA995A995566A)) 
    letra21__208_carry__6_i_7
       (.I0(letra21__208_carry__6_i_3_n_0),
        .I1(letra21__130_carry__1_n_5),
        .I2(letra21__74_carry__2_i_3),
        .I3(letra21__104_carry__2_i_1),
        .I4(letra21__37_carry__2_i_1),
        .I5(letra21__130_carry__1_n_4),
        .O(letra21__208_carry__6_i_7_n_0));
  LUT6 #(
    .INIT(64'h566AA995A995566A)) 
    letra21__208_carry__6_i_8
       (.I0(letra21__208_carry__6_i_4_n_0),
        .I1(letra21__130_carry__1_n_6),
        .I2(letra21__74_carry__2_i_3),
        .I3(letra21__104_carry__2_i_1),
        .I4(letra21__37_carry__2_i_1),
        .I5(letra21__130_carry__1_n_5),
        .O(letra21__208_carry__6_i_8_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 letra21__208_carry__7
       (.CI(letra21__208_carry__6_n_0),
        .CO({letra21__208_carry__7_n_0,NLW_letra21__208_carry__7_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({letra21__208_carry__7_i_1_n_0,letra21__208_carry__7_i_2_n_0,letra21__208_carry__7_i_3_n_0,letra21__208_carry__7_i_4_n_0}),
        .O({letra21__208_carry__7_n_4,letra21__208_carry__7_n_5,letra21__208_carry__7_n_6,letra21__208_carry__7_n_7}),
        .S({letra21__208_carry__7_i_5_n_0,letra21__208_carry__7_i_6_n_0,letra21__208_carry__7_i_7_n_0,letra21__208_carry__7_i_8_n_0}));
  LUT5 #(
    .INIT(32'h03EB283F)) 
    letra21__208_carry__7_i_1
       (.I0(letra21__130_carry__2_n_4),
        .I1(letra21__74_carry__2_i_3),
        .I2(letra21__104_carry__2_i_1),
        .I3(letra21__37_carry__2_i_1),
        .I4(letra21__130_carry__3_n_7),
        .O(letra21__208_carry__7_i_1_n_0));
  LUT5 #(
    .INIT(32'h477D411D)) 
    letra21__208_carry__7_i_2
       (.I0(letra21__37_carry__2_i_1),
        .I1(letra21__130_carry__2_n_4),
        .I2(letra21__74_carry__2_i_3),
        .I3(letra21__104_carry__2_i_1),
        .I4(letra21__130_carry__2_n_5),
        .O(letra21__208_carry__7_i_2_n_0));
  LUT5 #(
    .INIT(32'h03EB283F)) 
    letra21__208_carry__7_i_3
       (.I0(letra21__130_carry__2_n_6),
        .I1(letra21__74_carry__2_i_3),
        .I2(letra21__104_carry__2_i_1),
        .I3(letra21__37_carry__2_i_1),
        .I4(letra21__130_carry__2_n_5),
        .O(letra21__208_carry__7_i_3_n_0));
  LUT5 #(
    .INIT(32'h03EB283F)) 
    letra21__208_carry__7_i_4
       (.I0(letra21__130_carry__2_n_7),
        .I1(letra21__74_carry__2_i_3),
        .I2(letra21__104_carry__2_i_1),
        .I3(letra21__37_carry__2_i_1),
        .I4(letra21__130_carry__2_n_6),
        .O(letra21__208_carry__7_i_4_n_0));
  LUT6 #(
    .INIT(64'h566AA995A995566A)) 
    letra21__208_carry__7_i_5
       (.I0(letra21__208_carry__7_i_1_n_0),
        .I1(letra21__130_carry__3_n_7),
        .I2(letra21__74_carry__2_i_3),
        .I3(letra21__104_carry__2_i_1),
        .I4(letra21__37_carry__2_i_1),
        .I5(letra21__130_carry__3_n_6),
        .O(letra21__208_carry__7_i_5_n_0));
  LUT6 #(
    .INIT(64'h566AA995A995566A)) 
    letra21__208_carry__7_i_6
       (.I0(letra21__208_carry__7_i_2_n_0),
        .I1(letra21__130_carry__2_n_4),
        .I2(letra21__74_carry__2_i_3),
        .I3(letra21__104_carry__2_i_1),
        .I4(letra21__37_carry__2_i_1),
        .I5(letra21__130_carry__3_n_7),
        .O(letra21__208_carry__7_i_6_n_0));
  LUT6 #(
    .INIT(64'h566AA995A995566A)) 
    letra21__208_carry__7_i_7
       (.I0(letra21__208_carry__7_i_3_n_0),
        .I1(letra21__130_carry__2_n_5),
        .I2(letra21__74_carry__2_i_3),
        .I3(letra21__104_carry__2_i_1),
        .I4(letra21__37_carry__2_i_1),
        .I5(letra21__130_carry__2_n_4),
        .O(letra21__208_carry__7_i_7_n_0));
  LUT6 #(
    .INIT(64'h566AA995A995566A)) 
    letra21__208_carry__7_i_8
       (.I0(letra21__208_carry__7_i_4_n_0),
        .I1(letra21__130_carry__2_n_6),
        .I2(letra21__74_carry__2_i_3),
        .I3(letra21__104_carry__2_i_1),
        .I4(letra21__37_carry__2_i_1),
        .I5(letra21__130_carry__2_n_5),
        .O(letra21__208_carry__7_i_8_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 letra21__208_carry__8
       (.CI(letra21__208_carry__7_n_0),
        .CO({letra21__208_carry__8_n_0,NLW_letra21__208_carry__8_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({letra21__208_carry__8_i_1_n_0,letra21__208_carry__8_i_2_n_0,letra21__208_carry__8_i_3_n_0,letra21__208_carry__8_i_4_n_0}),
        .O({letra21__208_carry__8_n_4,letra21__208_carry__8_n_5,letra21__208_carry__8_n_6,letra21__208_carry__8_n_7}),
        .S({letra21__208_carry__8_i_5_n_0,letra21__208_carry__8_i_6_n_0,letra21__208_carry__8_i_7_n_0,letra21__208_carry__8_i_8_n_0}));
  LUT5 #(
    .INIT(32'h11F96077)) 
    letra21__208_carry__8_i_1
       (.I0(letra21__74_carry__2_i_3),
        .I1(letra21__104_carry__2_i_1),
        .I2(letra21__130_carry__3_n_4),
        .I3(letra21__37_carry__2_i_1),
        .I4(letra21__130_carry__4_n_7),
        .O(letra21__208_carry__8_i_1_n_0));
  LUT5 #(
    .INIT(32'h03EB283F)) 
    letra21__208_carry__8_i_2
       (.I0(letra21__130_carry__3_n_5),
        .I1(letra21__74_carry__2_i_3),
        .I2(letra21__104_carry__2_i_1),
        .I3(letra21__37_carry__2_i_1),
        .I4(letra21__130_carry__3_n_4),
        .O(letra21__208_carry__8_i_2_n_0));
  LUT5 #(
    .INIT(32'h03EB283F)) 
    letra21__208_carry__8_i_3
       (.I0(letra21__130_carry__3_n_6),
        .I1(letra21__74_carry__2_i_3),
        .I2(letra21__104_carry__2_i_1),
        .I3(letra21__37_carry__2_i_1),
        .I4(letra21__130_carry__3_n_5),
        .O(letra21__208_carry__8_i_3_n_0));
  LUT5 #(
    .INIT(32'h477D411D)) 
    letra21__208_carry__8_i_4
       (.I0(letra21__37_carry__2_i_1),
        .I1(letra21__130_carry__3_n_6),
        .I2(letra21__74_carry__2_i_3),
        .I3(letra21__104_carry__2_i_1),
        .I4(letra21__130_carry__3_n_7),
        .O(letra21__208_carry__8_i_4_n_0));
  LUT6 #(
    .INIT(64'h566AA995A995566A)) 
    letra21__208_carry__8_i_5
       (.I0(letra21__208_carry__8_i_1_n_0),
        .I1(letra21__74_carry__2_i_3),
        .I2(letra21__104_carry__2_i_1),
        .I3(letra21__130_carry__4_n_7),
        .I4(letra21__37_carry__2_i_1),
        .I5(letra21__130_carry__4_n_6),
        .O(letra21__208_carry__8_i_5_n_0));
  LUT6 #(
    .INIT(64'h566AA995A995566A)) 
    letra21__208_carry__8_i_6
       (.I0(letra21__208_carry__8_i_2_n_0),
        .I1(letra21__74_carry__2_i_3),
        .I2(letra21__104_carry__2_i_1),
        .I3(letra21__130_carry__3_n_4),
        .I4(letra21__37_carry__2_i_1),
        .I5(letra21__130_carry__4_n_7),
        .O(letra21__208_carry__8_i_6_n_0));
  LUT6 #(
    .INIT(64'h566AA995A995566A)) 
    letra21__208_carry__8_i_7
       (.I0(letra21__208_carry__8_i_3_n_0),
        .I1(letra21__130_carry__3_n_5),
        .I2(letra21__74_carry__2_i_3),
        .I3(letra21__104_carry__2_i_1),
        .I4(letra21__37_carry__2_i_1),
        .I5(letra21__130_carry__3_n_4),
        .O(letra21__208_carry__8_i_7_n_0));
  LUT6 #(
    .INIT(64'h566AA995A995566A)) 
    letra21__208_carry__8_i_8
       (.I0(letra21__208_carry__8_i_4_n_0),
        .I1(letra21__130_carry__3_n_6),
        .I2(letra21__74_carry__2_i_3),
        .I3(letra21__104_carry__2_i_1),
        .I4(letra21__37_carry__2_i_1),
        .I5(letra21__130_carry__3_n_5),
        .O(letra21__208_carry__8_i_8_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 letra21__208_carry__9
       (.CI(letra21__208_carry__8_n_0),
        .CO({letra21__208_carry__9_n_0,NLW_letra21__208_carry__9_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({letra21__208_carry__9_i_1_n_0,letra21__208_carry__9_i_2_n_0,letra21__208_carry__9_i_3_n_0,letra21__208_carry__9_i_4_n_0}),
        .O({letra21__208_carry__9_n_4,letra21__208_carry__9_n_5,letra21__208_carry__9_n_6,letra21__208_carry__9_n_7}),
        .S({letra21__208_carry__9_i_5_n_0,letra21__208_carry__9_i_6_n_0,letra21__208_carry__9_i_7_n_0,letra21__208_carry__9_i_8_n_0}));
  LUT5 #(
    .INIT(32'h11F96077)) 
    letra21__208_carry__9_i_1
       (.I0(letra21__74_carry__2_i_3),
        .I1(letra21__104_carry__2_i_1),
        .I2(letra21__130_carry__4_n_4),
        .I3(letra21__37_carry__2_i_1),
        .I4(letra21__130_carry__5_n_7),
        .O(letra21__208_carry__9_i_1_n_0));
  LUT5 #(
    .INIT(32'h03EB283F)) 
    letra21__208_carry__9_i_2
       (.I0(letra21__130_carry__4_n_5),
        .I1(letra21__74_carry__2_i_3),
        .I2(letra21__104_carry__2_i_1),
        .I3(letra21__37_carry__2_i_1),
        .I4(letra21__130_carry__4_n_4),
        .O(letra21__208_carry__9_i_2_n_0));
  LUT5 #(
    .INIT(32'h03EB283F)) 
    letra21__208_carry__9_i_3
       (.I0(letra21__130_carry__4_n_6),
        .I1(letra21__74_carry__2_i_3),
        .I2(letra21__104_carry__2_i_1),
        .I3(letra21__37_carry__2_i_1),
        .I4(letra21__130_carry__4_n_5),
        .O(letra21__208_carry__9_i_3_n_0));
  LUT5 #(
    .INIT(32'h11F96077)) 
    letra21__208_carry__9_i_4
       (.I0(letra21__74_carry__2_i_3),
        .I1(letra21__104_carry__2_i_1),
        .I2(letra21__130_carry__4_n_7),
        .I3(letra21__37_carry__2_i_1),
        .I4(letra21__130_carry__4_n_6),
        .O(letra21__208_carry__9_i_4_n_0));
  LUT6 #(
    .INIT(64'h566AA995A995566A)) 
    letra21__208_carry__9_i_5
       (.I0(letra21__208_carry__9_i_1_n_0),
        .I1(letra21__104_carry__2_i_1),
        .I2(letra21__74_carry__2_i_3),
        .I3(letra21__130_carry__5_n_7),
        .I4(letra21__37_carry__2_i_1),
        .I5(letra21__130_carry__5_n_6),
        .O(letra21__208_carry__9_i_5_n_0));
  LUT6 #(
    .INIT(64'h566AA995A995566A)) 
    letra21__208_carry__9_i_6
       (.I0(letra21__208_carry__9_i_2_n_0),
        .I1(letra21__74_carry__2_i_3),
        .I2(letra21__104_carry__2_i_1),
        .I3(letra21__130_carry__4_n_4),
        .I4(letra21__37_carry__2_i_1),
        .I5(letra21__130_carry__5_n_7),
        .O(letra21__208_carry__9_i_6_n_0));
  LUT6 #(
    .INIT(64'h566AA995A995566A)) 
    letra21__208_carry__9_i_7
       (.I0(letra21__208_carry__9_i_3_n_0),
        .I1(letra21__130_carry__4_n_5),
        .I2(letra21__74_carry__2_i_3),
        .I3(letra21__104_carry__2_i_1),
        .I4(letra21__37_carry__2_i_1),
        .I5(letra21__130_carry__4_n_4),
        .O(letra21__208_carry__9_i_7_n_0));
  LUT6 #(
    .INIT(64'h566AA995A995566A)) 
    letra21__208_carry__9_i_8
       (.I0(letra21__208_carry__9_i_4_n_0),
        .I1(letra21__130_carry__4_n_6),
        .I2(letra21__74_carry__2_i_3),
        .I3(letra21__104_carry__2_i_1),
        .I4(letra21__37_carry__2_i_1),
        .I5(letra21__130_carry__4_n_5),
        .O(letra21__208_carry__9_i_8_n_0));
  LUT2 #(
    .INIT(4'h8)) 
    letra21__208_carry_i_1
       (.I0(letra21__37_carry_n_7),
        .I1(letra21_carry__1_n_7),
        .O(letra21__208_carry_i_1_n_0));
  LUT5 #(
    .INIT(32'h02AAA800)) 
    letra21__208_carry_i_2
       (.I0(letra21_carry__0_n_4),
        .I1(letra230_out[0]),
        .I2(letra21__208_carry__0_i_5_0[0]),
        .I3(letra230_out[3]),
        .I4(letra230_out[1]),
        .O(letra21__208_carry_i_2_n_0));
  (* HLUTNM = "lutpair5" *) 
  LUT4 #(
    .INIT(16'h2888)) 
    letra21__208_carry_i_3
       (.I0(letra21_carry__0_n_5),
        .I1(letra230_out[0]),
        .I2(letra230_out[3]),
        .I3(letra21__208_carry__0_i_5_0[0]),
        .O(letra21__208_carry_i_3_n_0));
  LUT4 #(
    .INIT(16'h8778)) 
    letra21__208_carry_i_5
       (.I0(letra21_carry__1_n_7),
        .I1(letra21__37_carry_n_7),
        .I2(letra21_carry__1_n_6),
        .I3(letra21__37_carry_n_6),
        .O(letra21__208_carry_i_5_n_0));
  LUT4 #(
    .INIT(16'h8778)) 
    letra21__208_carry_i_6
       (.I0(letra21__208_carry__1_i_6_0[0]),
        .I1(letra21_carry__0_n_4),
        .I2(letra21_carry__1_n_7),
        .I3(letra21__37_carry_n_7),
        .O(letra21__208_carry_i_6_n_0));
  LUT6 #(
    .INIT(64'h6969699996969666)) 
    letra21__208_carry_i_7
       (.I0(letra21__208_carry_i_3_n_0),
        .I1(letra230_out[1]),
        .I2(letra230_out[3]),
        .I3(letra21__208_carry__0_i_5_0[0]),
        .I4(letra230_out[0]),
        .I5(letra21_carry__0_n_4),
        .O(letra21__208_carry_i_7_n_0));
  (* HLUTNM = "lutpair5" *) 
  LUT5 #(
    .INIT(32'h69669666)) 
    letra21__208_carry_i_8
       (.I0(letra21_carry__0_n_5),
        .I1(letra230_out[0]),
        .I2(letra230_out[3]),
        .I3(letra21__208_carry__0_i_5_0[0]),
        .I4(O),
        .O(letra21__208_carry_i_8_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "PROPCONST SWEEP" *) 
  CARRY4 letra21__336_carry
       (.CI(1'b0),
        .CO({letra21__336_carry_n_0,NLW_letra21__336_carry_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({letra21__208_carry__5_n_4,letra21__208_carry__5_n_5,letra21__208_carry__5_n_6,1'b0}),
        .O({letra21__208_carry__5_0,letra21__336_carry_n_7}),
        .S({letra21__336_carry_i_1_n_0,letra21__336_carry_i_2_n_0,letra21__336_carry_i_3_n_0,letra21__208_carry__5_n_7}));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 letra21__336_carry__0
       (.CI(letra21__336_carry_n_0),
        .CO({letra21__336_carry__0_n_0,NLW_letra21__336_carry__0_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({letra21__208_carry__6_n_4,letra21__208_carry__6_n_5,letra21__208_carry__6_n_6,letra21__208_carry__6_n_7}),
        .O({letra21__336_carry__0_i_4_0[2:1],letra21__336_carry__0_n_6,letra21__336_carry__0_i_4_0[0]}),
        .S({letra21__336_carry__0_i_1_n_0,letra21__336_carry__0_i_2_n_0,letra21__336_carry__0_i_3_n_0,letra21__336_carry__0_i_4_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    letra21__336_carry__0_i_1
       (.I0(letra21__208_carry__6_n_4),
        .I1(letra21__208_carry__6_n_6),
        .O(letra21__336_carry__0_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    letra21__336_carry__0_i_2
       (.I0(letra21__208_carry__6_n_5),
        .I1(letra21__208_carry__6_n_7),
        .O(letra21__336_carry__0_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    letra21__336_carry__0_i_3
       (.I0(letra21__208_carry__6_n_6),
        .I1(letra21__208_carry__5_n_4),
        .O(letra21__336_carry__0_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    letra21__336_carry__0_i_4
       (.I0(letra21__208_carry__6_n_7),
        .I1(letra21__208_carry__5_n_5),
        .O(letra21__336_carry__0_i_4_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 letra21__336_carry__1
       (.CI(letra21__336_carry__0_n_0),
        .CO({letra21__336_carry__1_n_0,NLW_letra21__336_carry__1_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({letra21__208_carry__7_n_4,letra21__208_carry__7_n_5,letra21__208_carry__7_n_6,letra21__208_carry__7_n_7}),
        .O({letra21__336_carry__1_n_4,letra21__336_carry__1_n_5,letra21__336_carry__1_n_6,letra21__336_carry__1_n_7}),
        .S({letra21__336_carry__1_i_1_n_0,letra21__336_carry__1_i_2_n_0,letra21__336_carry__1_i_3_n_0,letra21__336_carry__1_i_4_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    letra21__336_carry__1_i_1
       (.I0(letra21__208_carry__7_n_4),
        .I1(letra21__208_carry__7_n_6),
        .O(letra21__336_carry__1_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    letra21__336_carry__1_i_2
       (.I0(letra21__208_carry__7_n_5),
        .I1(letra21__208_carry__7_n_7),
        .O(letra21__336_carry__1_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    letra21__336_carry__1_i_3
       (.I0(letra21__208_carry__7_n_6),
        .I1(letra21__208_carry__6_n_4),
        .O(letra21__336_carry__1_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    letra21__336_carry__1_i_4
       (.I0(letra21__208_carry__7_n_7),
        .I1(letra21__208_carry__6_n_5),
        .O(letra21__336_carry__1_i_4_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 letra21__336_carry__2
       (.CI(letra21__336_carry__1_n_0),
        .CO({letra21__336_carry__2_n_0,NLW_letra21__336_carry__2_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({letra21__208_carry__8_n_4,letra21__208_carry__8_n_5,letra21__208_carry__8_n_6,letra21__208_carry__8_n_7}),
        .O({letra21__336_carry__2_n_4,letra21__336_carry__2_n_5,letra21__336_carry__2_n_6,letra21__336_carry__2_n_7}),
        .S({letra21__336_carry__2_i_1_n_0,letra21__336_carry__2_i_2_n_0,letra21__336_carry__2_i_3_n_0,letra21__336_carry__2_i_4_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    letra21__336_carry__2_i_1
       (.I0(letra21__208_carry__8_n_4),
        .I1(letra21__208_carry__8_n_6),
        .O(letra21__336_carry__2_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    letra21__336_carry__2_i_2
       (.I0(letra21__208_carry__8_n_5),
        .I1(letra21__208_carry__8_n_7),
        .O(letra21__336_carry__2_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    letra21__336_carry__2_i_3
       (.I0(letra21__208_carry__8_n_6),
        .I1(letra21__208_carry__7_n_4),
        .O(letra21__336_carry__2_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    letra21__336_carry__2_i_4
       (.I0(letra21__208_carry__8_n_7),
        .I1(letra21__208_carry__7_n_5),
        .O(letra21__336_carry__2_i_4_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 letra21__336_carry__3
       (.CI(letra21__336_carry__2_n_0),
        .CO({letra21__336_carry__3_n_0,NLW_letra21__336_carry__3_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({letra21__208_carry__9_n_4,letra21__208_carry__9_n_5,letra21__208_carry__9_n_6,letra21__208_carry__9_n_7}),
        .O({letra21__336_carry__3_n_4,letra21__336_carry__3_n_5,letra21__336_carry__3_n_6,letra21__336_carry__3_n_7}),
        .S({letra21__336_carry__3_i_1_n_0,letra21__336_carry__3_i_2_n_0,letra21__336_carry__3_i_3_n_0,letra21__336_carry__3_i_4_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    letra21__336_carry__3_i_1
       (.I0(letra21__208_carry__9_n_4),
        .I1(letra21__208_carry__9_n_6),
        .O(letra21__336_carry__3_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    letra21__336_carry__3_i_2
       (.I0(letra21__208_carry__9_n_5),
        .I1(letra21__208_carry__9_n_7),
        .O(letra21__336_carry__3_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    letra21__336_carry__3_i_3
       (.I0(letra21__208_carry__9_n_6),
        .I1(letra21__208_carry__8_n_4),
        .O(letra21__336_carry__3_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    letra21__336_carry__3_i_4
       (.I0(letra21__208_carry__9_n_7),
        .I1(letra21__208_carry__8_n_5),
        .O(letra21__336_carry__3_i_4_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 letra21__336_carry__4
       (.CI(letra21__336_carry__3_n_0),
        .CO({letra21__336_carry__4_n_0,NLW_letra21__336_carry__4_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({letra21__208_carry__10_n_4,letra21__208_carry__10_n_5,letra21__208_carry__10_n_6,letra21__208_carry__10_n_7}),
        .O({letra21__336_carry__4_n_4,letra21__336_carry__4_n_5,letra21__336_carry__4_n_6,letra21__336_carry__4_n_7}),
        .S({letra21__336_carry__4_i_1_n_0,letra21__336_carry__4_i_2_n_0,letra21__336_carry__4_i_3_n_0,letra21__336_carry__4_i_4_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    letra21__336_carry__4_i_1
       (.I0(letra21__208_carry__10_n_4),
        .I1(letra21__208_carry__10_n_6),
        .O(letra21__336_carry__4_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    letra21__336_carry__4_i_2
       (.I0(letra21__208_carry__10_n_5),
        .I1(letra21__208_carry__10_n_7),
        .O(letra21__336_carry__4_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    letra21__336_carry__4_i_3
       (.I0(letra21__208_carry__10_n_6),
        .I1(letra21__208_carry__9_n_4),
        .O(letra21__336_carry__4_i_3_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    letra21__336_carry__4_i_4
       (.I0(letra21__208_carry__10_n_7),
        .I1(letra21__208_carry__9_n_5),
        .O(letra21__336_carry__4_i_4_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 letra21__336_carry__5
       (.CI(letra21__336_carry__4_n_0),
        .CO({letra21__336_carry__5_n_0,NLW_letra21__336_carry__5_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,letra21__208_carry__11_n_5,letra21__208_carry__11_n_6,letra21__208_carry__11_n_7}),
        .O({letra21__336_carry__5_n_4,letra21__336_carry__5_n_5,letra21__336_carry__5_n_6,letra21__336_carry__5_n_7}),
        .S({letra21__208_carry__11_n_6,letra21__336_carry__5_i_1_n_0,letra21__336_carry__5_i_2_n_0,letra21__336_carry__5_i_3_n_0}));
  LUT2 #(
    .INIT(4'h6)) 
    letra21__336_carry__5_i_1
       (.I0(letra21__208_carry__11_n_5),
        .I1(letra21__208_carry__11_n_7),
        .O(letra21__336_carry__5_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    letra21__336_carry__5_i_2
       (.I0(letra21__208_carry__11_n_6),
        .I1(letra21__208_carry__10_n_4),
        .O(letra21__336_carry__5_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    letra21__336_carry__5_i_3
       (.I0(letra21__208_carry__11_n_7),
        .I1(letra21__208_carry__10_n_5),
        .O(letra21__336_carry__5_i_3_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 letra21__336_carry__6
       (.CI(letra21__336_carry__5_n_0),
        .CO(NLW_letra21__336_carry__6_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_letra21__336_carry__6_O_UNCONNECTED[3:1],letra21__336_carry__6_n_7}),
        .S({1'b0,1'b0,1'b0,letra21__208_carry__11_n_5}));
  LUT2 #(
    .INIT(4'h6)) 
    letra21__336_carry_i_1
       (.I0(letra21__208_carry__5_n_4),
        .I1(letra21__208_carry__5_n_6),
        .O(letra21__336_carry_i_1_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    letra21__336_carry_i_2
       (.I0(letra21__208_carry__5_n_5),
        .I1(letra21__208_carry__5_n_7),
        .O(letra21__336_carry_i_2_n_0));
  LUT2 #(
    .INIT(4'h6)) 
    letra21__336_carry_i_3
       (.I0(letra21__208_carry__5_n_6),
        .I1(letra21__208_carry__4_i_8_0),
        .O(letra21__336_carry_i_3_n_0));
  (* OPT_MODIFIED = "PROPCONST SWEEP" *) 
  CARRY4 letra21__37_carry
       (.CI(1'b0),
        .CO({letra21__37_carry_n_0,NLW_letra21__37_carry_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({letra21__208_carry__3_i_8,1'b0}),
        .O({letra21__37_carry_n_4,letra21__37_carry_n_5,letra21__37_carry_n_6,letra21__37_carry_n_7}),
        .S(letra21__208_carry_i_6_2));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 letra21__37_carry__0
       (.CI(letra21__37_carry_n_0),
        .CO({letra21__37_carry__0_n_0,NLW_letra21__37_carry__0_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(letra21__208_carry__0_i_1_0),
        .O({letra21__37_carry__0_n_4,letra21__37_carry__0_n_5,letra21__37_carry__0_n_6,letra21__37_carry__0_n_7}),
        .S(letra21__208_carry__0_i_1_1));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 letra21__37_carry__1
       (.CI(letra21__37_carry__0_n_0),
        .CO({letra21__37_carry__1_n_0,NLW_letra21__37_carry__1_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({letra21__420_carry__1_1,letra21__420_carry__0_1,letra21__208_carry__4_i_5_0}),
        .O({letra21__37_carry__1_i_7,letra21__37_carry__1_n_6,letra21__37_carry__1_n_7}),
        .S(letra21__208_carry__1_i_6_1));
  CARRY4 letra21__37_carry__2
       (.CI(letra21__37_carry__1_n_0),
        .CO({NLW_letra21__37_carry__2_CO_UNCONNECTED[3:2],letra21__37_carry__2_i_1,NLW_letra21__37_carry__2_CO_UNCONNECTED[0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,letra21__420_carry__1_0}),
        .O({NLW_letra21__37_carry__2_O_UNCONNECTED[3:1],letra21__37_carry__2_i_1_0}),
        .S({1'b0,1'b0,1'b1,letra21__208_carry__2_i_1}));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "PROPCONST SWEEP" *) 
  CARRY4 letra21__420_carry
       (.CI(1'b0),
        .CO({letra21__420_carry_n_0,NLW_letra21__420_carry_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({letra21__420_carry_i_1_n_0,letra21__420_carry_i_2_n_0,letra21__420_carry_i_3_n_0,1'b0}),
        .O(NLW_letra21__420_carry_O_UNCONNECTED[3:0]),
        .S({letra21__420_carry__0_2[1],letra21__420_carry_i_5_n_0,letra21__420_carry_i_6_n_0,letra21__420_carry__0_2[0]}));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 letra21__420_carry__0
       (.CI(letra21__420_carry_n_0),
        .CO({letra21__420_carry__0_n_0,NLW_letra21__420_carry__0_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({letra21__420_carry__0_i_1_n_0,letra21__420_carry__0_i_2_n_0,letra21__420_carry__0_i_3_n_0,letra21__420_carry__0_i_4_n_0}),
        .O(NLW_letra21__420_carry__0_O_UNCONNECTED[3:0]),
        .S({letra21__420_carry__0_i_5_n_0,letra21__420_carry__0_i_6_n_0,letra21__420_carry__1_2}));
  LUT2 #(
    .INIT(4'h2)) 
    letra21__420_carry__0_i_1
       (.I0(letra21__336_carry__0_n_6),
        .I1(letra21__420_carry__0_1),
        .O(letra21__420_carry__0_i_1_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    letra21__420_carry__0_i_2
       (.I0(letra21__336_carry__0_i_4_0[0]),
        .I1(letra21__420_carry__0_5),
        .O(letra21__420_carry__0_i_2_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    letra21__420_carry__0_i_3
       (.I0(letra21__208_carry__5_0[2]),
        .I1(letra21__420_carry__0_4),
        .O(letra21__420_carry__0_i_3_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    letra21__420_carry__0_i_4
       (.I0(letra21__208_carry__5_0[1]),
        .I1(letra21__420_carry__0_3),
        .O(letra21__420_carry__0_i_4_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    letra21__420_carry__0_i_5
       (.I0(letra21__420_carry__0_i_1_n_0),
        .I1(letra21__420_carry__0_0),
        .I2(letra21__336_carry__0_i_4_0[1]),
        .O(letra21__420_carry__0_i_5_n_0));
  LUT4 #(
    .INIT(16'h9699)) 
    letra21__420_carry__0_i_6
       (.I0(letra21__336_carry__0_n_6),
        .I1(letra21__420_carry__0_1),
        .I2(letra21__420_carry__0_5),
        .I3(letra21__336_carry__0_i_4_0[0]),
        .O(letra21__420_carry__0_i_6_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 letra21__420_carry__1
       (.CI(letra21__420_carry__0_n_0),
        .CO({letra21__420_carry__1_n_0,NLW_letra21__420_carry__1_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({letra21__336_carry__1_n_6,letra21__336_carry__1_n_7,letra21__420_carry__1_i_1_n_0,letra21__420_carry__1_i_2_n_0}),
        .O(NLW_letra21__420_carry__1_O_UNCONNECTED[3:0]),
        .S({letra21__420_carry__1_i_3_n_0,letra21__420_carry__1_i_4_n_0,letra21__420_carry__1_i_5_n_0,letra21__420_carry__2_0}));
  LUT2 #(
    .INIT(4'h2)) 
    letra21__420_carry__1_i_1
       (.I0(letra21__336_carry__0_i_4_0[2]),
        .I1(letra21__420_carry__1_0),
        .O(letra21__420_carry__1_i_1_n_0));
  LUT2 #(
    .INIT(4'h2)) 
    letra21__420_carry__1_i_2
       (.I0(letra21__336_carry__0_i_4_0[1]),
        .I1(letra21__420_carry__1_1),
        .O(letra21__420_carry__1_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    letra21__420_carry__1_i_3
       (.I0(letra21__336_carry__1_n_6),
        .I1(letra21__336_carry__1_n_5),
        .O(letra21__420_carry__1_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    letra21__420_carry__1_i_4
       (.I0(letra21__336_carry__1_n_7),
        .I1(letra21__336_carry__1_n_6),
        .O(letra21__420_carry__1_i_4_n_0));
  LUT3 #(
    .INIT(8'h4B)) 
    letra21__420_carry__1_i_5
       (.I0(letra21__420_carry__1_0),
        .I1(letra21__336_carry__0_i_4_0[2]),
        .I2(letra21__336_carry__1_n_7),
        .O(letra21__420_carry__1_i_5_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 letra21__420_carry__2
       (.CI(letra21__420_carry__1_n_0),
        .CO({letra21__420_carry__2_n_0,NLW_letra21__420_carry__2_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({letra21__336_carry__2_n_6,letra21__336_carry__2_n_7,letra21__336_carry__1_n_4,letra21__336_carry__1_n_5}),
        .O(NLW_letra21__420_carry__2_O_UNCONNECTED[3:0]),
        .S({letra21__420_carry__2_i_1_n_0,letra21__420_carry__2_i_2_n_0,letra21__420_carry__2_i_3_n_0,letra21__420_carry__2_i_4_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    letra21__420_carry__2_i_1
       (.I0(letra21__336_carry__2_n_6),
        .I1(letra21__336_carry__2_n_5),
        .O(letra21__420_carry__2_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    letra21__420_carry__2_i_2
       (.I0(letra21__336_carry__2_n_7),
        .I1(letra21__336_carry__2_n_6),
        .O(letra21__420_carry__2_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    letra21__420_carry__2_i_3
       (.I0(letra21__336_carry__1_n_4),
        .I1(letra21__336_carry__2_n_7),
        .O(letra21__420_carry__2_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    letra21__420_carry__2_i_4
       (.I0(letra21__336_carry__1_n_5),
        .I1(letra21__336_carry__1_n_4),
        .O(letra21__420_carry__2_i_4_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 letra21__420_carry__3
       (.CI(letra21__420_carry__2_n_0),
        .CO({letra21__420_carry__3_n_0,NLW_letra21__420_carry__3_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({letra21__336_carry__3_n_6,letra21__336_carry__3_n_7,letra21__336_carry__2_n_4,letra21__336_carry__2_n_5}),
        .O(NLW_letra21__420_carry__3_O_UNCONNECTED[3:0]),
        .S({letra21__420_carry__3_i_1_n_0,letra21__420_carry__3_i_2_n_0,letra21__420_carry__3_i_3_n_0,letra21__420_carry__3_i_4_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    letra21__420_carry__3_i_1
       (.I0(letra21__336_carry__3_n_6),
        .I1(letra21__336_carry__3_n_5),
        .O(letra21__420_carry__3_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    letra21__420_carry__3_i_2
       (.I0(letra21__336_carry__3_n_7),
        .I1(letra21__336_carry__3_n_6),
        .O(letra21__420_carry__3_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    letra21__420_carry__3_i_3
       (.I0(letra21__336_carry__2_n_4),
        .I1(letra21__336_carry__3_n_7),
        .O(letra21__420_carry__3_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    letra21__420_carry__3_i_4
       (.I0(letra21__336_carry__2_n_5),
        .I1(letra21__336_carry__2_n_4),
        .O(letra21__420_carry__3_i_4_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 letra21__420_carry__4
       (.CI(letra21__420_carry__3_n_0),
        .CO({letra21__420_carry__4_n_0,NLW_letra21__420_carry__4_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({letra21__336_carry__4_n_6,letra21__336_carry__4_n_7,letra21__336_carry__3_n_4,letra21__336_carry__3_n_5}),
        .O(NLW_letra21__420_carry__4_O_UNCONNECTED[3:0]),
        .S({letra21__420_carry__4_i_1_n_0,letra21__420_carry__4_i_2_n_0,letra21__420_carry__4_i_3_n_0,letra21__420_carry__4_i_4_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    letra21__420_carry__4_i_1
       (.I0(letra21__336_carry__4_n_6),
        .I1(letra21__336_carry__4_n_5),
        .O(letra21__420_carry__4_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    letra21__420_carry__4_i_2
       (.I0(letra21__336_carry__4_n_7),
        .I1(letra21__336_carry__4_n_6),
        .O(letra21__420_carry__4_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    letra21__420_carry__4_i_3
       (.I0(letra21__336_carry__3_n_4),
        .I1(letra21__336_carry__4_n_7),
        .O(letra21__420_carry__4_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    letra21__420_carry__4_i_4
       (.I0(letra21__336_carry__3_n_5),
        .I1(letra21__336_carry__3_n_4),
        .O(letra21__420_carry__4_i_4_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 letra21__420_carry__5
       (.CI(letra21__420_carry__4_n_0),
        .CO({letra21__420_carry__5_n_0,NLW_letra21__420_carry__5_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({letra21__336_carry__5_n_6,letra21__336_carry__5_n_7,letra21__336_carry__4_n_4,letra21__336_carry__4_n_5}),
        .O(NLW_letra21__420_carry__5_O_UNCONNECTED[3:0]),
        .S({letra21__420_carry__5_i_1_n_0,letra21__420_carry__5_i_2_n_0,letra21__420_carry__5_i_3_n_0,letra21__420_carry__5_i_4_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    letra21__420_carry__5_i_1
       (.I0(letra21__336_carry__5_n_6),
        .I1(letra21__336_carry__5_n_5),
        .O(letra21__420_carry__5_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    letra21__420_carry__5_i_2
       (.I0(letra21__336_carry__5_n_7),
        .I1(letra21__336_carry__5_n_6),
        .O(letra21__420_carry__5_i_2_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    letra21__420_carry__5_i_3
       (.I0(letra21__336_carry__4_n_4),
        .I1(letra21__336_carry__5_n_7),
        .O(letra21__420_carry__5_i_3_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    letra21__420_carry__5_i_4
       (.I0(letra21__336_carry__4_n_5),
        .I1(letra21__336_carry__4_n_4),
        .O(letra21__420_carry__5_i_4_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 letra21__420_carry__6
       (.CI(letra21__420_carry__5_n_0),
        .CO({NLW_letra21__420_carry__6_CO_UNCONNECTED[3:2],letra21__420_carry__6_n_2,NLW_letra21__420_carry__6_CO_UNCONNECTED[0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,letra21__336_carry__5_n_4,letra21__336_carry__5_n_5}),
        .O(NLW_letra21__420_carry__6_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,letra21__420_carry__6_i_1_n_0,letra21__420_carry__6_i_2_n_0}));
  LUT2 #(
    .INIT(4'h9)) 
    letra21__420_carry__6_i_1
       (.I0(letra21__336_carry__5_n_4),
        .I1(letra21__336_carry__6_n_7),
        .O(letra21__420_carry__6_i_1_n_0));
  LUT2 #(
    .INIT(4'h9)) 
    letra21__420_carry__6_i_2
       (.I0(letra21__336_carry__5_n_5),
        .I1(letra21__336_carry__5_n_4),
        .O(letra21__420_carry__6_i_2_n_0));
  LUT6 #(
    .INIT(64'hFFFEAAAAAAABFFFF)) 
    letra21__420_carry_i_1
       (.I0(letra21__208_carry__5_0[0]),
        .I1(letra21__208_carry__0_i_5_0[0]),
        .I2(letra230_out[0]),
        .I3(letra230_out[1]),
        .I4(letra230_out[3]),
        .I5(letra230_out[2]),
        .O(letra21__420_carry_i_1_n_0));
  LUT5 #(
    .INIT(32'hA80002AA)) 
    letra21__420_carry_i_2
       (.I0(letra21__336_carry_n_7),
        .I1(letra230_out[0]),
        .I2(letra21__208_carry__0_i_5_0[0]),
        .I3(letra230_out[3]),
        .I4(letra230_out[1]),
        .O(letra21__420_carry_i_2_n_0));
  LUT4 #(
    .INIT(16'hEBBB)) 
    letra21__420_carry_i_3
       (.I0(letra21__208_carry__4_i_8_0),
        .I1(letra230_out[0]),
        .I2(letra230_out[3]),
        .I3(letra21__208_carry__0_i_5_0[0]),
        .O(letra21__420_carry_i_3_n_0));
  LUT3 #(
    .INIT(8'h96)) 
    letra21__420_carry_i_5
       (.I0(letra21__420_carry_i_2_n_0),
        .I1(letra21__420_carry_0),
        .I2(letra21__208_carry__5_0[0]),
        .O(letra21__420_carry_i_5_n_0));
  LUT6 #(
    .INIT(64'hC696933339696CCC)) 
    letra21__420_carry_i_6
       (.I0(letra21__208_carry__4_i_8_0),
        .I1(letra230_out[1]),
        .I2(letra230_out[3]),
        .I3(letra21__208_carry__0_i_5_0[0]),
        .I4(letra230_out[0]),
        .I5(letra21__336_carry_n_7),
        .O(letra21__420_carry_i_6_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 letra21__479_carry
       (.CI(1'b0),
        .CO(NLW_letra21__479_carry_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b1}),
        .O({letra21__479_carry_n_4,letra21__479_carry_n_5,letra21__479_carry_n_6,letra21__479_carry_n_7}),
        .S({letra21__208_carry__5_n_5,letra21__208_carry__5_n_6,letra21__208_carry__5_n_7,letra21__479_carry_i_1_n_0}));
  LUT1 #(
    .INIT(2'h1)) 
    letra21__479_carry_i_1
       (.I0(letra21__208_carry__4_i_8_0),
        .O(letra21__479_carry_i_1_n_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 letra21__74_carry
       (.CI(1'b0),
        .CO({letra21__74_carry_n_0,NLW_letra21__74_carry_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({letra21__208_carry__0_i_5_3,letra21__208_carry__0_i_5_0[0],1'b0,1'b1}),
        .O({letra21__74_carry_n_4,letra21__74_carry_n_5,letra21__74_carry_n_6,NLW_letra21__74_carry_O_UNCONNECTED[0]}),
        .S({letra21__208_carry__0_i_5_4,letra21__208_carry__0_i_5_0[0]}));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 letra21__74_carry__0
       (.CI(letra21__74_carry_n_0),
        .CO({letra21__74_carry__0_n_0,NLW_letra21__74_carry__0_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(letra21__208_carry__1_i_6_0),
        .O({letra21__74_carry__0_i_4,letra21__74_carry__0_n_7}),
        .S(letra21__208_carry__1_i_6_2));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 letra21__74_carry__1
       (.CI(letra21__74_carry__0_n_0),
        .CO({letra21__74_carry__1_n_0,NLW_letra21__74_carry__1_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({letra21__208_carry__2_i_11,letra21__208_carry_i_6_0[2:0]}),
        .O({letra21__74_carry__1_n_4,letra21__74_carry__1_n_5,letra21__74_carry__1_i_5}),
        .S(letra21__208_carry__2_i_11_0));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 letra21__74_carry__2
       (.CI(letra21__74_carry__1_n_0),
        .CO({letra21__74_carry__2_i_3,NLW_letra21__74_carry__2_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,letra21__420_carry__1_0,letra21__420_carry__1_1,letra21__420_carry__0_0}),
        .O({NLW_letra21__74_carry__2_O_UNCONNECTED[3],letra21__74_carry__2_n_5,letra21__74_carry__2_n_6,letra21__74_carry__2_n_7}),
        .S({1'b1,letra21__208_carry__3_i_5_0}));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 letra21_carry
       (.CI(1'b0),
        .CO({letra21_carry_n_0,NLW_letra21_carry_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({letra21__208_carry__0_i_5_0,1'b0,1'b1}),
        .O({NLW_letra21_carry_O_UNCONNECTED[3:1],letra21_carry_n_7}),
        .S({letra21__208_carry__0_i_5_1,letra21__208_carry__0_i_5_0[0]}));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 letra21_carry__0
       (.CI(letra21_carry_n_0),
        .CO({letra21_carry__0_n_0,NLW_letra21_carry__0_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(letra21__208_carry__1_i_6_0),
        .O({letra21_carry__0_n_4,letra21_carry__0_n_5,O,NLW_letra21_carry__0_O_UNCONNECTED[0]}),
        .S(letra21__208_carry_i_4));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 letra21_carry__1
       (.CI(letra21_carry__0_n_0),
        .CO({letra21_carry__1_n_0,NLW_letra21_carry__1_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI(letra21__208_carry_i_6_0),
        .O({letra21_carry__1_n_4,letra21_carry__1_n_5,letra21_carry__1_n_6,letra21_carry__1_n_7}),
        .S(letra21__208_carry_i_6_1));
  LUT1 #(
    .INIT(2'h1)) 
    letra21_carry__1_i_13
       (.I0(CO),
        .O(letra10__1_carry__0_2[1]));
  LUT3 #(
    .INIT(8'h9A)) 
    letra21_carry__1_i_14
       (.I0(Q[3]),
        .I1(CO),
        .I2(Q[4]),
        .O(letra10__1_carry__0_2[0]));
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 letra21_carry__2
       (.CI(letra21_carry__1_n_0),
        .CO({letra21_carry__2_i_6,NLW_letra21_carry__2_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,letra21__420_carry__1_0,letra21__420_carry__1_1,letra21__420_carry__0_0}),
        .O({NLW_letra21_carry__2_O_UNCONNECTED[3],letra21_carry__2_n_5,letra21_carry__2_n_6,letra21_carry__2_n_7}),
        .S({1'b1,letra21__208_carry__0_i_5_2}));
  LUT3 #(
    .INIT(8'h59)) 
    letra21_carry_i_6
       (.I0(Q[2]),
        .I1(CO),
        .I2(Q[4]),
        .O(\salida_reg[7] [1]));
  LUT3 #(
    .INIT(8'h59)) 
    letra21_carry_i_7
       (.I0(Q[1]),
        .I1(CO),
        .I2(Q[4]),
        .O(\salida_reg[7] [0]));
endmodule

module Maquina_refrescos
   (bebida_lista,
    \FSM_sequential_state_reg[2]_0 ,
    \ticks_reg[12] ,
    S,
    DI,
    \ticks_reg[7] ,
    \ticks_reg[3] ,
    LEDs_OBUF,
    \FSM_sequential_state_reg[2]_1 ,
    \ticks_reg[12]_0 ,
    letra21__37_carry__2,
    letra21_carry_i_5_0,
    Q,
    \salida_reg[0]_0 ,
    letra21__208_carry__2_i_9_0,
    \salida_reg[7]_0 ,
    \salida_reg[2]_0 ,
    g0_b0__6_i_3_0,
    g0_b0__1_i_14_0,
    letra21__37_carry__1_i_1_0,
    letra21_carry__1_i_10_0,
    letra21_carry_i_5_1,
    letra21_carry__1_i_10_1,
    letra21_carry__0_i_11_0,
    letra21_carry__1_i_11_0,
    letra21_carry__2,
    g0_b0__1_i_13_0,
    letra21_carry__2_i_1_0,
    letra21_carry__2_0,
    letra21_carry__1_i_9_0,
    letra21_carry__2_i_1_1,
    letra21_carry__1_i_9_1,
    letra21_carry__2_i_1_2,
    letra21_carry_i_5_2,
    letra21_carry_i_5_3,
    letra21_carry__2_1,
    letra21_carry_i_5_4,
    letra21__130_carry__6,
    letra21__104_carry__2,
    letra21_carry__0,
    \salida_reg[7]_1 ,
    g0_b0_i_7_0,
    \salida_reg[3]_0 ,
    letra21_carry__1_i_9_2,
    letra21_carry_i_5_5,
    letra21_carry_i_5_6,
    letra21_carry__2_i_1_3,
    letra21_carry__2_i_1_4,
    letra21_carry_i_5_7,
    letra21_carry__2_i_1_5,
    \salida_reg[7]_2 ,
    letra21_carry__0_i_1_0,
    letra21__37_carry__1_i_1_1,
    letra21__104_carry,
    letra21_carry__0_i_12_0,
    letra21_carry__0_i_10_0,
    letra21_carry__2_i_1_6,
    letra21_carry__0_i_12_1,
    letra21__130_carry__0_i_1_0,
    letra21_carry__2_2,
    letra21_carry__2_3,
    letra21__208_carry__3_i_12,
    letra21__336_carry,
    letra21__336_carry__0,
    letra21__336_carry__0_0,
    letra21_carry__0_i_12_2,
    letra21__37_carry__1_i_1_2,
    letra21_carry__0_i_12_3,
    letra21_carry__0_i_11_1,
    letra21_carry__2_i_1_7,
    display_number_OBUF,
    \display_reg[0] ,
    \display_reg[0]_0 ,
    letra21__37_carry__1_i_1_3,
    g0_b7__3_i_4_0,
    g0_b6__3_i_1_0,
    \FSM_sequential_state_reg[2]_2 ,
    \FSM_sequential_state_reg[0]_0 ,
    selector_IBUF,
    ticks_reg,
    out,
    \bebida_reg[0]_0 ,
    letra21__130_carry__1,
    letra21__208_carry__2_i_7_0,
    letra21__208_carry__3,
    letra21__208_carry__2,
    letra21__208_carry__11,
    letra21__208_carry__3_0,
    g0_b0__2_0,
    g0_b0__2_1,
    g0_b0__2_i_4_0,
    CO,
    letra21__208_carry__2_0,
    letra21__208_carry__11_0,
    letra21__208_carry__11_1,
    letra21__208_carry__11_2,
    O,
    g0_b0__1_0,
    g0_b0__1_1,
    letra21__208_carry_i_3,
    letra21__74_carry__1_i_3_0,
    letra21__208_carry__11_i_3_0,
    letra21__208_carry__2_1,
    letra21__208_carry__3_1,
    letra21__420_carry,
    letra21__420_carry__0,
    letra21__420_carry__1,
    \display_number[0] ,
    g0_b7__2_0,
    g0_b7__2_1,
    g0_b7__2_2,
    \FSM_sequential_state_reg[2]_3 ,
    \FSM_sequential_state_reg[2]_4 ,
    \FSM_sequential_state_reg[2]_5 ,
    CLK_P_BUFG,
    RESET_IBUF,
    \display_number_OBUF[7]_inst_i_4_0 ,
    \display_number_OBUF[7]_inst_i_4_1 );
  output bebida_lista;
  output [0:0]\FSM_sequential_state_reg[2]_0 ;
  output \ticks_reg[12] ;
  output [0:0]S;
  output [1:0]DI;
  output [0:0]\ticks_reg[7] ;
  output [0:0]\ticks_reg[3] ;
  output [4:0]LEDs_OBUF;
  output \FSM_sequential_state_reg[2]_1 ;
  output [0:0]\ticks_reg[12]_0 ;
  output [1:0]letra21__37_carry__2;
  output [3:0]letra21_carry_i_5_0;
  output [5:0]Q;
  output \salida_reg[0]_0 ;
  output [0:0]letra21__208_carry__2_i_9_0;
  output [1:0]\salida_reg[7]_0 ;
  output \salida_reg[2]_0 ;
  output g0_b0__6_i_3_0;
  output [3:0]g0_b0__1_i_14_0;
  output [3:0]letra21__37_carry__1_i_1_0;
  output letra21_carry__1_i_10_0;
  output letra21_carry_i_5_1;
  output letra21_carry__1_i_10_1;
  output [2:0]letra21_carry__0_i_11_0;
  output letra21_carry__1_i_11_0;
  output [3:0]letra21_carry__2;
  output g0_b0__1_i_13_0;
  output [3:0]letra21_carry__2_i_1_0;
  output [3:0]letra21_carry__2_0;
  output letra21_carry__1_i_9_0;
  output [0:0]letra21_carry__2_i_1_1;
  output letra21_carry__1_i_9_1;
  output [0:0]letra21_carry__2_i_1_2;
  output [2:0]letra21_carry_i_5_2;
  output letra21_carry_i_5_3;
  output letra21_carry__2_1;
  output [2:0]letra21_carry_i_5_4;
  output [1:0]letra21__130_carry__6;
  output [2:0]letra21__104_carry__2;
  output [0:0]letra21_carry__0;
  output [3:0]\salida_reg[7]_1 ;
  output g0_b0_i_7_0;
  output [0:0]\salida_reg[3]_0 ;
  output [1:0]letra21_carry__1_i_9_2;
  output [2:0]letra21_carry_i_5_5;
  output [0:0]letra21_carry_i_5_6;
  output [2:0]letra21_carry__2_i_1_3;
  output [0:0]letra21_carry__2_i_1_4;
  output [0:0]letra21_carry_i_5_7;
  output [2:0]letra21_carry__2_i_1_5;
  output [1:0]\salida_reg[7]_2 ;
  output [3:0]letra21_carry__0_i_1_0;
  output [3:0]letra21__37_carry__1_i_1_1;
  output [2:0]letra21__104_carry;
  output [3:0]letra21_carry__0_i_12_0;
  output [3:0]letra21_carry__0_i_10_0;
  output [3:0]letra21_carry__2_i_1_6;
  output [3:0]letra21_carry__0_i_12_1;
  output [3:0]letra21__130_carry__0_i_1_0;
  output [3:0]letra21_carry__2_2;
  output [0:0]letra21_carry__2_3;
  output [0:0]letra21__208_carry__3_i_12;
  output [1:0]letra21__336_carry;
  output [1:0]letra21__336_carry__0;
  output [0:0]letra21__336_carry__0_0;
  output [3:0]letra21_carry__0_i_12_2;
  output [3:0]letra21__37_carry__1_i_1_2;
  output [3:0]letra21_carry__0_i_12_3;
  output [3:0]letra21_carry__0_i_11_1;
  output [3:0]letra21_carry__2_i_1_7;
  output [6:0]display_number_OBUF;
  output \display_reg[0] ;
  output \display_reg[0]_0 ;
  output [0:0]letra21__37_carry__1_i_1_3;
  output g0_b7__3_i_4_0;
  output g0_b6__3_i_1_0;
  output [2:0]\FSM_sequential_state_reg[2]_2 ;
  input \FSM_sequential_state_reg[0]_0 ;
  input [2:0]selector_IBUF;
  input [6:0]ticks_reg;
  input [7:0]out;
  input \bebida_reg[0]_0 ;
  input [0:0]letra21__130_carry__1;
  input [2:0]letra21__208_carry__2_i_7_0;
  input [1:0]letra21__208_carry__3;
  input [0:0]letra21__208_carry__2;
  input [0:0]letra21__208_carry__11;
  input [0:0]letra21__208_carry__3_0;
  input g0_b0__2_0;
  input g0_b0__2_1;
  input g0_b0__2_i_4_0;
  input [0:0]CO;
  input [1:0]letra21__208_carry__2_0;
  input [3:0]letra21__208_carry__11_0;
  input [0:0]letra21__208_carry__11_1;
  input [0:0]letra21__208_carry__11_2;
  input [0:0]O;
  input g0_b0__1_0;
  input g0_b0__1_1;
  input [1:0]letra21__208_carry_i_3;
  input [1:0]letra21__74_carry__1_i_3_0;
  input [0:0]letra21__208_carry__11_i_3_0;
  input [0:0]letra21__208_carry__2_1;
  input letra21__208_carry__3_1;
  input [0:0]letra21__420_carry;
  input [2:0]letra21__420_carry__0;
  input [2:0]letra21__420_carry__1;
  input [2:0]\display_number[0] ;
  input g0_b7__2_0;
  input g0_b7__2_1;
  input g0_b7__2_2;
  input \FSM_sequential_state_reg[2]_3 ;
  input \FSM_sequential_state_reg[2]_4 ;
  input \FSM_sequential_state_reg[2]_5 ;
  input CLK_P_BUFG;
  input RESET_IBUF;
  input \display_number_OBUF[7]_inst_i_4_0 ;
  input \display_number_OBUF[7]_inst_i_4_1 ;

  wire CLK_P_BUFG;
  wire [0:0]CO;
  wire [1:0]DI;
  wire \FSM_sequential_state[0]_i_1_n_0 ;
  wire \FSM_sequential_state[0]_i_2_n_0 ;
  wire \FSM_sequential_state[1]_i_1_n_0 ;
  wire \FSM_sequential_state[2]_i_1_n_0 ;
  wire \FSM_sequential_state[2]_i_2_n_0 ;
  wire \FSM_sequential_state[2]_i_4_n_0 ;
  wire \FSM_sequential_state[2]_i_9_n_0 ;
  wire \FSM_sequential_state_reg[0]_0 ;
  wire [0:0]\FSM_sequential_state_reg[2]_0 ;
  wire \FSM_sequential_state_reg[2]_1 ;
  wire [2:0]\FSM_sequential_state_reg[2]_2 ;
  wire \FSM_sequential_state_reg[2]_3 ;
  wire \FSM_sequential_state_reg[2]_4 ;
  wire \FSM_sequential_state_reg[2]_5 ;
  wire [4:0]LEDs_OBUF;
  wire [0:0]O;
  wire [5:0]Q;
  wire RESET_IBUF;
  wire [0:0]S;
  wire [7:0]bebida;
  wire bebida__0;
  wire \bebida_elegida_reg[0]_i_1_n_0 ;
  wire \bebida_elegida_reg[1]_i_1_n_0 ;
  wire \bebida_elegida_reg[2]_i_1_n_0 ;
  wire \bebida_elegida_reg[2]_i_2_n_0 ;
  wire bebida_lista;
  wire bebida_lista_reg_i_1_n_0;
  wire bebida_lista_reg_i_2_n_0;
  wire \bebida_reg[0]_0 ;
  wire \bebida_reg[0]_i_1_n_0 ;
  wire \bebida_reg[1]_i_1_n_0 ;
  wire \bebida_reg[7]_i_1_n_0 ;
  wire \bebida_reg[7]_i_4_n_0 ;
  wire [4:0]decoderBCD_display_exit_S4;
  wire [0:0]decoderBCD_display_exit_S5;
  wire [0:0]decoderBCD_display_exit_S6;
  wire [1:1]decoderBCD_display_exit_S7;
  wire [2:0]\display_number[0] ;
  wire [6:0]display_number_OBUF;
  wire \display_number_OBUF[0]_inst_i_2_n_0 ;
  wire \display_number_OBUF[0]_inst_i_3_n_0 ;
  wire \display_number_OBUF[0]_inst_i_4_n_0 ;
  wire \display_number_OBUF[0]_inst_i_5_n_0 ;
  wire \display_number_OBUF[1]_inst_i_2_n_0 ;
  wire \display_number_OBUF[1]_inst_i_3_n_0 ;
  wire \display_number_OBUF[1]_inst_i_4_n_0 ;
  wire \display_number_OBUF[1]_inst_i_5_n_0 ;
  wire \display_number_OBUF[2]_inst_i_2_n_0 ;
  wire \display_number_OBUF[2]_inst_i_3_n_0 ;
  wire \display_number_OBUF[2]_inst_i_4_n_0 ;
  wire \display_number_OBUF[2]_inst_i_5_n_0 ;
  wire \display_number_OBUF[3]_inst_i_2_n_0 ;
  wire \display_number_OBUF[3]_inst_i_3_n_0 ;
  wire \display_number_OBUF[3]_inst_i_4_n_0 ;
  wire \display_number_OBUF[3]_inst_i_5_n_0 ;
  wire \display_number_OBUF[4]_inst_i_2_n_0 ;
  wire \display_number_OBUF[4]_inst_i_3_n_0 ;
  wire \display_number_OBUF[4]_inst_i_4_n_0 ;
  wire \display_number_OBUF[4]_inst_i_5_n_0 ;
  wire \display_number_OBUF[5]_inst_i_2_n_0 ;
  wire \display_number_OBUF[5]_inst_i_3_n_0 ;
  wire \display_number_OBUF[5]_inst_i_4_n_0 ;
  wire \display_number_OBUF[5]_inst_i_5_n_0 ;
  wire \display_number_OBUF[6]_inst_i_2_n_0 ;
  wire \display_number_OBUF[6]_inst_i_3_n_0 ;
  wire \display_number_OBUF[6]_inst_i_4_n_0 ;
  wire \display_number_OBUF[6]_inst_i_5_n_0 ;
  wire \display_number_OBUF[7]_inst_i_4_0 ;
  wire \display_number_OBUF[7]_inst_i_4_1 ;
  wire \display_reg[0] ;
  wire \display_reg[0]_0 ;
  wire [6:1]fsm_BCD_decoder;
  wire g0_b0__0_i_6_n_0;
  wire g0_b0__0_i_7_n_0;
  wire g0_b0__0_i_8_n_0;
  wire g0_b0__0_n_0;
  wire g0_b0__1_0;
  wire g0_b0__1_1;
  wire g0_b0__1_i_13_0;
  wire [3:0]g0_b0__1_i_14_0;
  wire g0_b0__1_i_14_n_0;
  wire g0_b0__1_i_2_n_0;
  wire g0_b0__1_i_3_n_0;
  wire g0_b0__1_i_5_n_0;
  wire g0_b0__1_n_0;
  wire g0_b0__2_0;
  wire g0_b0__2_1;
  wire g0_b0__2_i_13_n_0;
  wire g0_b0__2_i_1_n_0;
  wire g0_b0__2_i_2_n_0;
  wire g0_b0__2_i_3_n_0;
  wire g0_b0__2_i_4_0;
  wire g0_b0__2_i_4_n_0;
  wire g0_b0__2_i_6_n_0;
  wire g0_b0__2_i_8_n_0;
  wire g0_b0__2_n_0;
  wire g0_b0__3_n_0;
  wire g0_b0__5_n_0;
  wire g0_b0__6_i_3_0;
  wire g0_b0__6_i_3_n_0;
  wire g0_b0__6_n_0;
  wire g0_b0_i_1_n_0;
  wire g0_b0_i_2_n_0;
  wire g0_b0_i_3_n_0;
  wire g0_b0_i_4_n_0;
  wire g0_b0_i_5_n_0;
  wire g0_b0_i_7_0;
  wire g0_b0_i_7_n_0;
  wire g0_b0_n_0;
  wire g0_b1__0_n_0;
  wire g0_b1__1_n_0;
  wire g0_b1__2_n_0;
  wire g0_b1__3_n_0;
  wire g0_b1__4_n_0;
  wire g0_b1__5_n_0;
  wire g0_b1__6_n_0;
  wire g0_b1_n_0;
  wire g0_b2__0_n_0;
  wire g0_b2__1_n_0;
  wire g0_b2__2_n_0;
  wire g0_b2__3_n_0;
  wire g0_b2__4_n_0;
  wire g0_b2__5_n_0;
  wire g0_b2__6_n_0;
  wire g0_b2_n_0;
  wire g0_b3__0_n_0;
  wire g0_b3__1_n_0;
  wire g0_b3__2_n_0;
  wire g0_b3__3_n_0;
  wire g0_b3__4_n_0;
  wire g0_b3__5_n_0;
  wire g0_b3__6_n_0;
  wire g0_b3_n_0;
  wire g0_b4__0_n_0;
  wire g0_b4__1_n_0;
  wire g0_b4__2_n_0;
  wire g0_b4__3_n_0;
  wire g0_b4__4_n_0;
  wire g0_b4__5_n_0;
  wire g0_b4__6_n_0;
  wire g0_b4_n_0;
  wire g0_b5__0_n_0;
  wire g0_b5__1_n_0;
  wire g0_b5__2_n_0;
  wire g0_b5__3_n_0;
  wire g0_b5__4_n_0;
  wire g0_b5__5_n_0;
  wire g0_b5__6_n_0;
  wire g0_b5_n_0;
  wire g0_b6__0_n_0;
  wire g0_b6__1_n_0;
  wire g0_b6__2_n_0;
  wire g0_b6__3_i_1_0;
  wire g0_b6__3_i_2_n_0;
  wire g0_b6__3_i_3_n_0;
  wire g0_b6__3_n_0;
  wire g0_b6__4_n_0;
  wire g0_b6__5_n_0;
  wire g0_b6__6_n_0;
  wire g0_b6_n_0;
  wire g0_b7__0_n_0;
  wire g0_b7__1_n_0;
  wire g0_b7__2_0;
  wire g0_b7__2_1;
  wire g0_b7__2_2;
  wire g0_b7__2_n_0;
  wire g0_b7__3_i_4_0;
  wire g0_b7__3_i_4_n_0;
  wire g0_b7_n_0;
  wire [2:0]letra21__104_carry;
  wire [2:0]letra21__104_carry__2;
  wire [3:0]letra21__130_carry__0_i_1_0;
  wire [0:0]letra21__130_carry__1;
  wire [1:0]letra21__130_carry__6;
  wire [0:0]letra21__208_carry__11;
  wire [3:0]letra21__208_carry__11_0;
  wire [0:0]letra21__208_carry__11_1;
  wire [0:0]letra21__208_carry__11_2;
  wire [0:0]letra21__208_carry__11_i_3_0;
  wire letra21__208_carry__11_i_6_n_3;
  wire [0:0]letra21__208_carry__2;
  wire [1:0]letra21__208_carry__2_0;
  wire [0:0]letra21__208_carry__2_1;
  wire letra21__208_carry__2_i_11_n_0;
  wire letra21__208_carry__2_i_12_n_0;
  wire letra21__208_carry__2_i_13_n_0;
  wire [2:0]letra21__208_carry__2_i_7_0;
  wire [0:0]letra21__208_carry__2_i_9_0;
  wire letra21__208_carry__2_i_9_n_0;
  wire [1:0]letra21__208_carry__3;
  wire [0:0]letra21__208_carry__3_0;
  wire letra21__208_carry__3_1;
  wire [0:0]letra21__208_carry__3_i_12;
  wire [1:0]letra21__208_carry_i_3;
  wire [1:0]letra21__336_carry;
  wire [1:0]letra21__336_carry__0;
  wire [0:0]letra21__336_carry__0_0;
  wire [3:0]letra21__37_carry__1_i_1_0;
  wire [3:0]letra21__37_carry__1_i_1_1;
  wire [3:0]letra21__37_carry__1_i_1_2;
  wire [0:0]letra21__37_carry__1_i_1_3;
  wire [1:0]letra21__37_carry__2;
  wire [0:0]letra21__420_carry;
  wire [2:0]letra21__420_carry__0;
  wire [2:0]letra21__420_carry__1;
  wire [1:0]letra21__74_carry__1_i_3_0;
  wire [0:0]letra21_carry__0;
  wire [3:0]letra21_carry__0_i_10_0;
  wire [2:0]letra21_carry__0_i_11_0;
  wire [3:0]letra21_carry__0_i_11_1;
  wire [3:0]letra21_carry__0_i_12_0;
  wire [3:0]letra21_carry__0_i_12_1;
  wire [3:0]letra21_carry__0_i_12_2;
  wire [3:0]letra21_carry__0_i_12_3;
  wire [3:0]letra21_carry__0_i_1_0;
  wire letra21_carry__1_i_10_0;
  wire letra21_carry__1_i_10_1;
  wire letra21_carry__1_i_10_n_0;
  wire letra21_carry__1_i_11_0;
  wire letra21_carry__1_i_11_n_0;
  wire letra21_carry__1_i_12_n_0;
  wire letra21_carry__1_i_9_0;
  wire letra21_carry__1_i_9_1;
  wire [1:0]letra21_carry__1_i_9_2;
  wire letra21_carry__1_i_9_n_0;
  wire [3:0]letra21_carry__2;
  wire [3:0]letra21_carry__2_0;
  wire letra21_carry__2_1;
  wire [3:0]letra21_carry__2_2;
  wire [0:0]letra21_carry__2_3;
  wire [3:0]letra21_carry__2_i_1_0;
  wire [0:0]letra21_carry__2_i_1_1;
  wire [0:0]letra21_carry__2_i_1_2;
  wire [2:0]letra21_carry__2_i_1_3;
  wire [0:0]letra21_carry__2_i_1_4;
  wire [2:0]letra21_carry__2_i_1_5;
  wire [3:0]letra21_carry__2_i_1_6;
  wire [3:0]letra21_carry__2_i_1_7;
  wire [3:0]letra21_carry_i_5_0;
  wire letra21_carry_i_5_1;
  wire [2:0]letra21_carry_i_5_2;
  wire letra21_carry_i_5_3;
  wire [2:0]letra21_carry_i_5_4;
  wire [2:0]letra21_carry_i_5_5;
  wire [0:0]letra21_carry_i_5_6;
  wire [0:0]letra21_carry_i_5_7;
  wire letra21_carry_i_5_n_0;
  wire letra21_carry_i_8_n_0;
  wire [8:4]letra230_out;
  wire [7:0]out;
  wire \salida_reg[0]_0 ;
  wire \salida_reg[0]_i_1_n_0 ;
  wire \salida_reg[1]_i_1_n_0 ;
  wire \salida_reg[2]_0 ;
  wire \salida_reg[2]_i_1_n_0 ;
  wire [0:0]\salida_reg[3]_0 ;
  wire \salida_reg[3]_i_1_n_0 ;
  wire \salida_reg[4]_i_1_n_0 ;
  wire \salida_reg[5]_i_1_n_0 ;
  wire \salida_reg[6]_i_1_n_0 ;
  wire [1:0]\salida_reg[7]_0 ;
  wire [3:0]\salida_reg[7]_1 ;
  wire [1:0]\salida_reg[7]_2 ;
  wire \salida_reg[7]_i_1_n_0 ;
  wire \salida_reg[7]_i_2_n_0 ;
  wire \salida_reg[7]_i_3_n_0 ;
  wire [5:0]sel;
  wire [2:0]selector_IBUF;
  wire [1:0]state;
  wire [6:0]ticks_reg;
  wire \ticks_reg[12] ;
  wire [0:0]\ticks_reg[12]_0 ;
  wire [0:0]\ticks_reg[3] ;
  wire [0:0]\ticks_reg[7] ;
  wire [3:0]NLW_g0_b0__1_i_13_CO_UNCONNECTED;
  wire [3:1]NLW_g0_b0__1_i_13_O_UNCONNECTED;
  wire [3:1]NLW_letra21__208_carry__11_i_6_CO_UNCONNECTED;
  wire [3:0]NLW_letra21__208_carry__11_i_6_O_UNCONNECTED;
  wire [2:0]NLW_letra21_carry__1_i_9_CO_UNCONNECTED;
  wire [2:0]NLW_letra21_carry_i_5_CO_UNCONNECTED;

  LUT6 #(
    .INIT(64'h0F000F0FE0EEE0E0)) 
    \FSM_sequential_state[0]_i_1 
       (.I0(\FSM_sequential_state_reg[2]_3 ),
        .I1(\FSM_sequential_state[0]_i_2_n_0 ),
        .I2(\FSM_sequential_state[2]_i_4_n_0 ),
        .I3(\FSM_sequential_state_reg[2]_4 ),
        .I4(\FSM_sequential_state_reg[2]_5 ),
        .I5(state[0]),
        .O(\FSM_sequential_state[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \FSM_sequential_state[0]_i_2 
       (.I0(state[1]),
        .I1(\FSM_sequential_state_reg[2]_0 ),
        .O(\FSM_sequential_state[0]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h1F111F1F20222020)) 
    \FSM_sequential_state[1]_i_1 
       (.I0(state[0]),
        .I1(\FSM_sequential_state_reg[2]_0 ),
        .I2(\FSM_sequential_state[2]_i_4_n_0 ),
        .I3(\FSM_sequential_state_reg[2]_4 ),
        .I4(\FSM_sequential_state_reg[2]_5 ),
        .I5(state[1]),
        .O(\FSM_sequential_state[1]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h2F222F2F20222020)) 
    \FSM_sequential_state[2]_i_1 
       (.I0(\FSM_sequential_state[2]_i_2_n_0 ),
        .I1(\FSM_sequential_state_reg[2]_3 ),
        .I2(\FSM_sequential_state[2]_i_4_n_0 ),
        .I3(\FSM_sequential_state_reg[2]_4 ),
        .I4(\FSM_sequential_state_reg[2]_5 ),
        .I5(\FSM_sequential_state_reg[2]_0 ),
        .O(\FSM_sequential_state[2]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT5 #(
    .INIT(32'hFFFFF8FF)) 
    \FSM_sequential_state[2]_i_10 
       (.I0(\FSM_sequential_state_reg[2]_0 ),
        .I1(state[1]),
        .I2(ticks_reg[5]),
        .I3(state[0]),
        .I4(ticks_reg[4]),
        .O(\FSM_sequential_state_reg[2]_1 ));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT2 #(
    .INIT(4'h1)) 
    \FSM_sequential_state[2]_i_2 
       (.I0(state[0]),
        .I1(state[1]),
        .O(\FSM_sequential_state[2]_i_2_n_0 ));
  LUT6 #(
    .INIT(64'h000033333330EEEE)) 
    \FSM_sequential_state[2]_i_4 
       (.I0(\FSM_sequential_state_reg[0]_0 ),
        .I1(state[0]),
        .I2(selector_IBUF[0]),
        .I3(\FSM_sequential_state[2]_i_9_n_0 ),
        .I4(state[1]),
        .I5(\FSM_sequential_state_reg[2]_0 ),
        .O(\FSM_sequential_state[2]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT2 #(
    .INIT(4'hE)) 
    \FSM_sequential_state[2]_i_9 
       (.I0(selector_IBUF[1]),
        .I1(selector_IBUF[2]),
        .O(\FSM_sequential_state[2]_i_9_n_0 ));
  (* FSM_ENCODED_STATES = "s0_esp:000,s3_sal:011,s4_err:101,s5_sel:010,s2_exceso:100,s1_lleno:001" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_sequential_state_reg[0] 
       (.C(CLK_P_BUFG),
        .CE(1'b1),
        .CLR(RESET_IBUF),
        .D(\FSM_sequential_state[0]_i_1_n_0 ),
        .Q(state[0]));
  (* FSM_ENCODED_STATES = "s0_esp:000,s3_sal:011,s4_err:101,s5_sel:010,s2_exceso:100,s1_lleno:001" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_sequential_state_reg[1] 
       (.C(CLK_P_BUFG),
        .CE(1'b1),
        .CLR(RESET_IBUF),
        .D(\FSM_sequential_state[1]_i_1_n_0 ),
        .Q(state[1]));
  (* FSM_ENCODED_STATES = "s0_esp:000,s3_sal:011,s4_err:101,s5_sel:010,s2_exceso:100,s1_lleno:001" *) 
  FDCE #(
    .INIT(1'b0)) 
    \FSM_sequential_state_reg[2] 
       (.C(CLK_P_BUFG),
        .CE(1'b1),
        .CLR(RESET_IBUF),
        .D(\FSM_sequential_state[2]_i_1_n_0 ),
        .Q(\FSM_sequential_state_reg[2]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT3 #(
    .INIT(8'h08)) 
    LED_error_OBUF_inst_i_1
       (.I0(state[0]),
        .I1(\FSM_sequential_state_reg[2]_0 ),
        .I2(state[1]),
        .O(LEDs_OBUF[4]));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT3 #(
    .INIT(8'h01)) 
    \LEDs_OBUF[0]_inst_i_1 
       (.I0(state[1]),
        .I1(state[0]),
        .I2(\FSM_sequential_state_reg[2]_0 ),
        .O(LEDs_OBUF[0]));
  (* SOFT_HLUTNM = "soft_lutpair17" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \LEDs_OBUF[1]_inst_i_1 
       (.I0(state[0]),
        .I1(\FSM_sequential_state_reg[2]_0 ),
        .I2(state[1]),
        .O(LEDs_OBUF[1]));
  (* SOFT_HLUTNM = "soft_lutpair40" *) 
  LUT3 #(
    .INIT(8'h04)) 
    \LEDs_OBUF[2]_inst_i_1 
       (.I0(state[0]),
        .I1(\FSM_sequential_state_reg[2]_0 ),
        .I2(state[1]),
        .O(LEDs_OBUF[2]));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bebida_elegida_reg[0] 
       (.CLR(1'b0),
        .D(\bebida_elegida_reg[0]_i_1_n_0 ),
        .G(\bebida_elegida_reg[2]_i_2_n_0 ),
        .GE(1'b1),
        .Q(\FSM_sequential_state_reg[2]_2 [0]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT3 #(
    .INIT(8'h20)) 
    \bebida_elegida_reg[0]_i_1 
       (.I0(selector_IBUF[0]),
        .I1(\FSM_sequential_state_reg[2]_0 ),
        .I2(state[1]),
        .O(\bebida_elegida_reg[0]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bebida_elegida_reg[1] 
       (.CLR(1'b0),
        .D(\bebida_elegida_reg[1]_i_1_n_0 ),
        .G(\bebida_elegida_reg[2]_i_2_n_0 ),
        .GE(1'b1),
        .Q(\FSM_sequential_state_reg[2]_2 [1]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT3 #(
    .INIT(8'h20)) 
    \bebida_elegida_reg[1]_i_1 
       (.I0(selector_IBUF[1]),
        .I1(\FSM_sequential_state_reg[2]_0 ),
        .I2(state[1]),
        .O(\bebida_elegida_reg[1]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bebida_elegida_reg[2] 
       (.CLR(1'b0),
        .D(\bebida_elegida_reg[2]_i_1_n_0 ),
        .G(\bebida_elegida_reg[2]_i_2_n_0 ),
        .GE(1'b1),
        .Q(\FSM_sequential_state_reg[2]_2 [2]));
  (* SOFT_HLUTNM = "soft_lutpair32" *) 
  LUT3 #(
    .INIT(8'h20)) 
    \bebida_elegida_reg[2]_i_1 
       (.I0(selector_IBUF[2]),
        .I1(\FSM_sequential_state_reg[2]_0 ),
        .I2(state[1]),
        .O(\bebida_elegida_reg[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'h00FF000000FF16FF)) 
    \bebida_elegida_reg[2]_i_2 
       (.I0(selector_IBUF[2]),
        .I1(selector_IBUF[1]),
        .I2(selector_IBUF[0]),
        .I3(state[1]),
        .I4(\FSM_sequential_state_reg[2]_0 ),
        .I5(state[0]),
        .O(\bebida_elegida_reg[2]_i_2_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    bebida_lista_reg
       (.CLR(1'b0),
        .D(bebida_lista_reg_i_1_n_0),
        .G(bebida_lista_reg_i_2_n_0),
        .GE(1'b1),
        .Q(bebida_lista));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT3 #(
    .INIT(8'h06)) 
    bebida_lista_reg_i_1
       (.I0(\FSM_sequential_state_reg[2]_0 ),
        .I1(state[1]),
        .I2(state[0]),
        .O(bebida_lista_reg_i_1_n_0));
  LUT6 #(
    .INIT(64'h6565656765676765)) 
    bebida_lista_reg_i_2
       (.I0(state[1]),
        .I1(\FSM_sequential_state_reg[2]_0 ),
        .I2(state[0]),
        .I3(selector_IBUF[0]),
        .I4(selector_IBUF[1]),
        .I5(selector_IBUF[2]),
        .O(bebida_lista_reg_i_2_n_0));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bebida_reg[0] 
       (.CLR(1'b0),
        .D(\bebida_reg[0]_i_1_n_0 ),
        .G(bebida__0),
        .GE(1'b1),
        .Q(bebida[0]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT5 #(
    .INIT(32'h0000FB00)) 
    \bebida_reg[0]_i_1 
       (.I0(selector_IBUF[0]),
        .I1(selector_IBUF[1]),
        .I2(selector_IBUF[2]),
        .I3(state[1]),
        .I4(\FSM_sequential_state_reg[2]_0 ),
        .O(\bebida_reg[0]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bebida_reg[1] 
       (.CLR(1'b0),
        .D(\bebida_reg[1]_i_1_n_0 ),
        .G(bebida__0),
        .GE(1'b1),
        .Q(bebida[1]));
  (* SOFT_HLUTNM = "soft_lutpair14" *) 
  LUT5 #(
    .INIT(32'h44404444)) 
    \bebida_reg[1]_i_1 
       (.I0(\FSM_sequential_state_reg[2]_0 ),
        .I1(state[1]),
        .I2(selector_IBUF[1]),
        .I3(selector_IBUF[2]),
        .I4(selector_IBUF[0]),
        .O(\bebida_reg[1]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \bebida_reg[7] 
       (.CLR(1'b0),
        .D(\bebida_reg[7]_i_1_n_0 ),
        .G(bebida__0),
        .GE(1'b1),
        .Q(bebida[7]));
  (* SOFT_HLUTNM = "soft_lutpair33" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \bebida_reg[7]_i_1 
       (.I0(state[1]),
        .I1(\FSM_sequential_state_reg[2]_0 ),
        .O(\bebida_reg[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair15" *) 
  LUT5 #(
    .INIT(32'h000000E2)) 
    \bebida_reg[7]_i_2 
       (.I0(\bebida_reg[0]_0 ),
        .I1(state[1]),
        .I2(\bebida_reg[7]_i_4_n_0 ),
        .I3(\FSM_sequential_state_reg[2]_0 ),
        .I4(state[0]),
        .O(bebida__0));
  (* SOFT_HLUTNM = "soft_lutpair34" *) 
  LUT3 #(
    .INIT(8'h16)) 
    \bebida_reg[7]_i_4 
       (.I0(selector_IBUF[2]),
        .I1(selector_IBUF[1]),
        .I2(selector_IBUF[0]),
        .O(\bebida_reg[7]_i_4_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT3 #(
    .INIT(8'h40)) 
    bebida_saliente_OBUF_inst_i_1
       (.I0(\FSM_sequential_state_reg[2]_0 ),
        .I1(state[1]),
        .I2(state[0]),
        .O(LEDs_OBUF[3]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \display_number_OBUF[0]_inst_i_1 
       (.I0(\display_number_OBUF[0]_inst_i_2_n_0 ),
        .I1(\display_number_OBUF[0]_inst_i_3_n_0 ),
        .I2(\display_number[0] [2]),
        .I3(\display_number_OBUF[0]_inst_i_4_n_0 ),
        .I4(\display_number[0] [1]),
        .I5(\display_number_OBUF[0]_inst_i_5_n_0 ),
        .O(display_number_OBUF[0]));
  MUXF7 \display_number_OBUF[0]_inst_i_2 
       (.I0(g0_b0__6_n_0),
        .I1(decoderBCD_display_exit_S5),
        .O(\display_number_OBUF[0]_inst_i_2_n_0 ),
        .S(\display_number[0] [0]));
  MUXF7 \display_number_OBUF[0]_inst_i_3 
       (.I0(g0_b0__5_n_0),
        .I1(g0_b0__3_n_0),
        .O(\display_number_OBUF[0]_inst_i_3_n_0 ),
        .S(\display_number[0] [0]));
  MUXF7 \display_number_OBUF[0]_inst_i_4 
       (.I0(g0_b0__1_n_0),
        .I1(g0_b0__2_n_0),
        .O(\display_number_OBUF[0]_inst_i_4_n_0 ),
        .S(\display_number[0] [0]));
  MUXF7 \display_number_OBUF[0]_inst_i_5 
       (.I0(g0_b0_n_0),
        .I1(g0_b0__0_n_0),
        .O(\display_number_OBUF[0]_inst_i_5_n_0 ),
        .S(\display_number[0] [0]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \display_number_OBUF[1]_inst_i_1 
       (.I0(\display_number_OBUF[1]_inst_i_2_n_0 ),
        .I1(\display_number_OBUF[1]_inst_i_3_n_0 ),
        .I2(\display_number[0] [2]),
        .I3(\display_number_OBUF[1]_inst_i_4_n_0 ),
        .I4(\display_number[0] [1]),
        .I5(\display_number_OBUF[1]_inst_i_5_n_0 ),
        .O(display_number_OBUF[1]));
  MUXF7 \display_number_OBUF[1]_inst_i_2 
       (.I0(g0_b1__6_n_0),
        .I1(g0_b1__4_n_0),
        .O(\display_number_OBUF[1]_inst_i_2_n_0 ),
        .S(\display_number[0] [0]));
  MUXF7 \display_number_OBUF[1]_inst_i_3 
       (.I0(g0_b1__5_n_0),
        .I1(g0_b1__3_n_0),
        .O(\display_number_OBUF[1]_inst_i_3_n_0 ),
        .S(\display_number[0] [0]));
  MUXF7 \display_number_OBUF[1]_inst_i_4 
       (.I0(g0_b1__1_n_0),
        .I1(g0_b1__2_n_0),
        .O(\display_number_OBUF[1]_inst_i_4_n_0 ),
        .S(\display_number[0] [0]));
  MUXF7 \display_number_OBUF[1]_inst_i_5 
       (.I0(g0_b1_n_0),
        .I1(g0_b1__0_n_0),
        .O(\display_number_OBUF[1]_inst_i_5_n_0 ),
        .S(\display_number[0] [0]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \display_number_OBUF[2]_inst_i_1 
       (.I0(\display_number_OBUF[2]_inst_i_2_n_0 ),
        .I1(\display_number_OBUF[2]_inst_i_3_n_0 ),
        .I2(\display_number[0] [2]),
        .I3(\display_number_OBUF[2]_inst_i_4_n_0 ),
        .I4(\display_number[0] [1]),
        .I5(\display_number_OBUF[2]_inst_i_5_n_0 ),
        .O(display_number_OBUF[2]));
  MUXF7 \display_number_OBUF[2]_inst_i_2 
       (.I0(g0_b2__6_n_0),
        .I1(g0_b2__4_n_0),
        .O(\display_number_OBUF[2]_inst_i_2_n_0 ),
        .S(\display_number[0] [0]));
  MUXF7 \display_number_OBUF[2]_inst_i_3 
       (.I0(g0_b2__5_n_0),
        .I1(g0_b2__3_n_0),
        .O(\display_number_OBUF[2]_inst_i_3_n_0 ),
        .S(\display_number[0] [0]));
  MUXF7 \display_number_OBUF[2]_inst_i_4 
       (.I0(g0_b2__1_n_0),
        .I1(g0_b2__2_n_0),
        .O(\display_number_OBUF[2]_inst_i_4_n_0 ),
        .S(\display_number[0] [0]));
  MUXF7 \display_number_OBUF[2]_inst_i_5 
       (.I0(g0_b2_n_0),
        .I1(g0_b2__0_n_0),
        .O(\display_number_OBUF[2]_inst_i_5_n_0 ),
        .S(\display_number[0] [0]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \display_number_OBUF[3]_inst_i_1 
       (.I0(\display_number_OBUF[3]_inst_i_2_n_0 ),
        .I1(\display_number_OBUF[3]_inst_i_3_n_0 ),
        .I2(\display_number[0] [2]),
        .I3(\display_number_OBUF[3]_inst_i_4_n_0 ),
        .I4(\display_number[0] [1]),
        .I5(\display_number_OBUF[3]_inst_i_5_n_0 ),
        .O(display_number_OBUF[3]));
  MUXF7 \display_number_OBUF[3]_inst_i_2 
       (.I0(g0_b3__6_n_0),
        .I1(g0_b3__4_n_0),
        .O(\display_number_OBUF[3]_inst_i_2_n_0 ),
        .S(\display_number[0] [0]));
  MUXF7 \display_number_OBUF[3]_inst_i_3 
       (.I0(g0_b3__5_n_0),
        .I1(g0_b3__3_n_0),
        .O(\display_number_OBUF[3]_inst_i_3_n_0 ),
        .S(\display_number[0] [0]));
  MUXF7 \display_number_OBUF[3]_inst_i_4 
       (.I0(g0_b3__1_n_0),
        .I1(g0_b3__2_n_0),
        .O(\display_number_OBUF[3]_inst_i_4_n_0 ),
        .S(\display_number[0] [0]));
  MUXF7 \display_number_OBUF[3]_inst_i_5 
       (.I0(g0_b3_n_0),
        .I1(g0_b3__0_n_0),
        .O(\display_number_OBUF[3]_inst_i_5_n_0 ),
        .S(\display_number[0] [0]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \display_number_OBUF[4]_inst_i_1 
       (.I0(\display_number_OBUF[4]_inst_i_2_n_0 ),
        .I1(\display_number_OBUF[4]_inst_i_3_n_0 ),
        .I2(\display_number[0] [2]),
        .I3(\display_number_OBUF[4]_inst_i_4_n_0 ),
        .I4(\display_number[0] [1]),
        .I5(\display_number_OBUF[4]_inst_i_5_n_0 ),
        .O(display_number_OBUF[4]));
  MUXF7 \display_number_OBUF[4]_inst_i_2 
       (.I0(g0_b4__6_n_0),
        .I1(g0_b4__4_n_0),
        .O(\display_number_OBUF[4]_inst_i_2_n_0 ),
        .S(\display_number[0] [0]));
  MUXF7 \display_number_OBUF[4]_inst_i_3 
       (.I0(g0_b4__5_n_0),
        .I1(g0_b4__3_n_0),
        .O(\display_number_OBUF[4]_inst_i_3_n_0 ),
        .S(\display_number[0] [0]));
  MUXF7 \display_number_OBUF[4]_inst_i_4 
       (.I0(g0_b4__1_n_0),
        .I1(g0_b4__2_n_0),
        .O(\display_number_OBUF[4]_inst_i_4_n_0 ),
        .S(\display_number[0] [0]));
  MUXF7 \display_number_OBUF[4]_inst_i_5 
       (.I0(g0_b4_n_0),
        .I1(g0_b4__0_n_0),
        .O(\display_number_OBUF[4]_inst_i_5_n_0 ),
        .S(\display_number[0] [0]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \display_number_OBUF[5]_inst_i_1 
       (.I0(\display_number_OBUF[5]_inst_i_2_n_0 ),
        .I1(\display_number_OBUF[5]_inst_i_3_n_0 ),
        .I2(\display_number[0] [2]),
        .I3(\display_number_OBUF[5]_inst_i_4_n_0 ),
        .I4(\display_number[0] [1]),
        .I5(\display_number_OBUF[5]_inst_i_5_n_0 ),
        .O(display_number_OBUF[5]));
  MUXF7 \display_number_OBUF[5]_inst_i_2 
       (.I0(g0_b5__6_n_0),
        .I1(g0_b5__4_n_0),
        .O(\display_number_OBUF[5]_inst_i_2_n_0 ),
        .S(\display_number[0] [0]));
  MUXF7 \display_number_OBUF[5]_inst_i_3 
       (.I0(g0_b5__5_n_0),
        .I1(g0_b5__3_n_0),
        .O(\display_number_OBUF[5]_inst_i_3_n_0 ),
        .S(\display_number[0] [0]));
  MUXF7 \display_number_OBUF[5]_inst_i_4 
       (.I0(g0_b5__1_n_0),
        .I1(g0_b5__2_n_0),
        .O(\display_number_OBUF[5]_inst_i_4_n_0 ),
        .S(\display_number[0] [0]));
  MUXF7 \display_number_OBUF[5]_inst_i_5 
       (.I0(g0_b5_n_0),
        .I1(g0_b5__0_n_0),
        .O(\display_number_OBUF[5]_inst_i_5_n_0 ),
        .S(\display_number[0] [0]));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \display_number_OBUF[6]_inst_i_1 
       (.I0(\display_number_OBUF[6]_inst_i_2_n_0 ),
        .I1(\display_number_OBUF[6]_inst_i_3_n_0 ),
        .I2(\display_number[0] [2]),
        .I3(\display_number_OBUF[6]_inst_i_4_n_0 ),
        .I4(\display_number[0] [1]),
        .I5(\display_number_OBUF[6]_inst_i_5_n_0 ),
        .O(display_number_OBUF[6]));
  MUXF7 \display_number_OBUF[6]_inst_i_2 
       (.I0(g0_b6__6_n_0),
        .I1(g0_b6__4_n_0),
        .O(\display_number_OBUF[6]_inst_i_2_n_0 ),
        .S(\display_number[0] [0]));
  MUXF7 \display_number_OBUF[6]_inst_i_3 
       (.I0(g0_b6__5_n_0),
        .I1(g0_b6__3_n_0),
        .O(\display_number_OBUF[6]_inst_i_3_n_0 ),
        .S(\display_number[0] [0]));
  MUXF7 \display_number_OBUF[6]_inst_i_4 
       (.I0(g0_b6__1_n_0),
        .I1(g0_b6__2_n_0),
        .O(\display_number_OBUF[6]_inst_i_4_n_0 ),
        .S(\display_number[0] [0]));
  MUXF7 \display_number_OBUF[6]_inst_i_5 
       (.I0(g0_b6_n_0),
        .I1(g0_b6__0_n_0),
        .O(\display_number_OBUF[6]_inst_i_5_n_0 ),
        .S(\display_number[0] [0]));
  MUXF7 \display_number_OBUF[7]_inst_i_4 
       (.I0(g0_b7__1_n_0),
        .I1(g0_b7__2_n_0),
        .O(\display_reg[0]_0 ),
        .S(\display_number[0] [0]));
  MUXF7 \display_number_OBUF[7]_inst_i_5 
       (.I0(g0_b7_n_0),
        .I1(g0_b7__0_n_0),
        .O(\display_reg[0] ),
        .S(\display_number[0] [0]));
  LUT6 #(
    .INIT(64'hFFFFFFF6E8BA2812)) 
    g0_b0
       (.I0(g0_b0_i_1_n_0),
        .I1(g0_b0_i_2_n_0),
        .I2(g0_b0_i_3_n_0),
        .I3(g0_b0_i_4_n_0),
        .I4(g0_b0_i_5_n_0),
        .I5(sel[5]),
        .O(g0_b0_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFF6E8BA2812)) 
    g0_b0__0
       (.I0(sel[0]),
        .I1(sel[1]),
        .I2(sel[2]),
        .I3(sel[3]),
        .I4(sel[4]),
        .I5(sel[5]),
        .O(g0_b0__0_n_0));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT3 #(
    .INIT(8'hBA)) 
    g0_b0__0_i_1
       (.I0(g0_b0__0_i_6_n_0),
        .I1(CO),
        .I2(g0_b0__6_i_3_0),
        .O(sel[0]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT5 #(
    .INIT(32'h00001102)) 
    g0_b0__0_i_2
       (.I0(fsm_BCD_decoder[1]),
        .I1(g0_b0_i_7_n_0),
        .I2(Q[0]),
        .I3(Q[1]),
        .I4(g0_b0__6_i_3_0),
        .O(sel[1]));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT5 #(
    .INIT(32'h0000FECD)) 
    g0_b0__0_i_3
       (.I0(Q[0]),
        .I1(g0_b0_i_7_n_0),
        .I2(fsm_BCD_decoder[1]),
        .I3(Q[1]),
        .I4(g0_b0__6_i_3_0),
        .O(sel[2]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT4 #(
    .INIT(16'h0036)) 
    g0_b0__0_i_4
       (.I0(Q[0]),
        .I1(fsm_BCD_decoder[1]),
        .I2(Q[1]),
        .I3(g0_b0_i_7_n_0),
        .O(sel[3]));
  (* SOFT_HLUTNM = "soft_lutpair28" *) 
  LUT4 #(
    .INIT(16'h0510)) 
    g0_b0__0_i_5
       (.I0(g0_b0_i_7_n_0),
        .I1(fsm_BCD_decoder[1]),
        .I2(Q[1]),
        .I3(Q[0]),
        .O(sel[4]));
  LUT6 #(
    .INIT(64'h00000000FFFFFF7F)) 
    g0_b0__0_i_6
       (.I0(Q[2]),
        .I1(Q[5]),
        .I2(fsm_BCD_decoder[6]),
        .I3(g0_b0__0_i_7_n_0),
        .I4(Q[4]),
        .I5(g0_b0__0_i_8_n_0),
        .O(g0_b0__0_i_6_n_0));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT4 #(
    .INIT(16'hFFDF)) 
    g0_b0__0_i_7
       (.I0(Q[0]),
        .I1(fsm_BCD_decoder[1]),
        .I2(Q[1]),
        .I3(Q[3]),
        .O(g0_b0__0_i_7_n_0));
  (* SOFT_HLUTNM = "soft_lutpair23" *) 
  LUT5 #(
    .INIT(32'hAABBAABA)) 
    g0_b0__0_i_8
       (.I0(g0_b0__6_i_3_0),
        .I1(Q[1]),
        .I2(fsm_BCD_decoder[1]),
        .I3(g0_b0_i_7_n_0),
        .I4(Q[0]),
        .O(g0_b0__0_i_8_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFF6E8BA2812)) 
    g0_b0__1
       (.I0(\display_number_OBUF[7]_inst_i_4_0 ),
        .I1(g0_b0__1_i_2_n_0),
        .I2(g0_b0__1_i_3_n_0),
        .I3(\display_number_OBUF[7]_inst_i_4_1 ),
        .I4(g0_b0__1_i_5_n_0),
        .I5(sel[5]),
        .O(g0_b0__1_n_0));
  (* ADDER_THRESHOLD = "35" *) 
  CARRY4 g0_b0__1_i_13
       (.CI(letra21_carry__1_i_9_n_0),
        .CO(NLW_g0_b0__1_i_13_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_g0_b0__1_i_13_O_UNCONNECTED[3:1],g0_b0__1_i_14_0[3]}),
        .S({1'b0,1'b0,1'b0,g0_b0__1_i_14_n_0}));
  LUT2 #(
    .INIT(4'hB)) 
    g0_b0__1_i_14
       (.I0(Q[5]),
        .I1(CO),
        .O(g0_b0__1_i_14_n_0));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT3 #(
    .INIT(8'h2A)) 
    g0_b0__1_i_2
       (.I0(decoderBCD_display_exit_S4[1]),
        .I1(g0_b0__1_0),
        .I2(g0_b0__6_i_3_0),
        .O(g0_b0__1_i_2_n_0));
  LUT6 #(
    .INIT(64'h0000FFEFFFEFFFEF)) 
    g0_b0__1_i_3
       (.I0(g0_b0_i_7_n_0),
        .I1(fsm_BCD_decoder[1]),
        .I2(Q[1]),
        .I3(Q[0]),
        .I4(g0_b0__1_1),
        .I5(g0_b0__6_i_3_0),
        .O(g0_b0__1_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT4 #(
    .INIT(16'h0406)) 
    g0_b0__1_i_5
       (.I0(Q[1]),
        .I1(fsm_BCD_decoder[1]),
        .I2(g0_b0_i_7_n_0),
        .I3(Q[0]),
        .O(g0_b0__1_i_5_n_0));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT3 #(
    .INIT(8'h04)) 
    g0_b0__1_i_6
       (.I0(g0_b0_i_7_n_0),
        .I1(Q[0]),
        .I2(Q[1]),
        .O(\salida_reg[2]_0 ));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT4 #(
    .INIT(16'hFF8B)) 
    g0_b0__1_i_9
       (.I0(fsm_BCD_decoder[1]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(g0_b0_i_7_n_0),
        .O(g0_b0_i_7_0));
  LUT6 #(
    .INIT(64'hFFFFFFF6E8BA2812)) 
    g0_b0__2
       (.I0(g0_b0__2_i_1_n_0),
        .I1(g0_b0__2_i_2_n_0),
        .I2(g0_b0__2_i_3_n_0),
        .I3(g0_b0__2_i_4_n_0),
        .I4(sel[1]),
        .I5(sel[5]),
        .O(g0_b0__2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair37" *) 
  LUT3 #(
    .INIT(8'h0D)) 
    g0_b0__2_i_1
       (.I0(g0_b0__6_i_3_0),
        .I1(Q[0]),
        .I2(sel[4]),
        .O(g0_b0__2_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT2 #(
    .INIT(4'h6)) 
    g0_b0__2_i_13
       (.I0(Q[5]),
        .I1(CO),
        .O(g0_b0__2_i_13_n_0));
  LUT6 #(
    .INIT(64'h484848484B7B4848)) 
    g0_b0__2_i_2
       (.I0(g0_b0__2_0),
        .I1(g0_b0__6_i_3_0),
        .I2(fsm_BCD_decoder[1]),
        .I3(Q[1]),
        .I4(Q[0]),
        .I5(g0_b0_i_7_n_0),
        .O(g0_b0__2_i_2_n_0));
  LUT6 #(
    .INIT(64'h0040440444444444)) 
    g0_b0__2_i_3
       (.I0(\salida_reg[2]_0 ),
        .I1(g0_b0__2_i_6_n_0),
        .I2(g0_b0__2_0),
        .I3(fsm_BCD_decoder[1]),
        .I4(g0_b0__2_1),
        .I5(g0_b0__6_i_3_0),
        .O(g0_b0__2_i_3_n_0));
  LUT6 #(
    .INIT(64'h1441411455555555)) 
    g0_b0__2_i_4
       (.I0(sel[5]),
        .I1(g0_b0__2_i_8_n_0),
        .I2(g0_b7__2_0),
        .I3(g0_b7__2_1),
        .I4(g0_b7__2_2),
        .I5(g0_b0__6_i_3_0),
        .O(g0_b0__2_i_4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair39" *) 
  LUT3 #(
    .INIT(8'hEF)) 
    g0_b0__2_i_6
       (.I0(g0_b0_i_7_n_0),
        .I1(fsm_BCD_decoder[1]),
        .I2(Q[1]),
        .O(g0_b0__2_i_6_n_0));
  LUT6 #(
    .INIT(64'h82283C3C2882C3C3)) 
    g0_b0__2_i_8
       (.I0(fsm_BCD_decoder[1]),
        .I1(g0_b0__2_i_13_n_0),
        .I2(g0_b0__2_i_4_0),
        .I3(g0_b0__1_i_14_0[3]),
        .I4(g0_b0__2_0),
        .I5(Q[1]),
        .O(g0_b0__2_i_8_n_0));
  LUT4 #(
    .INIT(16'hDE4C)) 
    g0_b0__3
       (.I0(sel[5]),
        .I1(decoderBCD_display_exit_S5),
        .I2(g0_b6__3_i_2_n_0),
        .I3(g0_b6__3_i_3_n_0),
        .O(g0_b0__3_n_0));
  LUT6 #(
    .INIT(64'hFFFFFF7DDEC80648)) 
    g0_b0__5
       (.I0(sel[5]),
        .I1(decoderBCD_display_exit_S4[0]),
        .I2(decoderBCD_display_exit_S4[1]),
        .I3(decoderBCD_display_exit_S4[2]),
        .I4(decoderBCD_display_exit_S4[4]),
        .I5(g0_b7__3_i_4_n_0),
        .O(g0_b0__5_n_0));
  LUT6 #(
    .INIT(64'hFFFF7FF7D13050D3)) 
    g0_b0__6
       (.I0(sel[2]),
        .I1(g0_b0__6_i_3_0),
        .I2(decoderBCD_display_exit_S6),
        .I3(g0_b6__3_i_2_n_0),
        .I4(decoderBCD_display_exit_S4[1]),
        .I5(decoderBCD_display_exit_S5),
        .O(g0_b0__6_n_0));
  (* SOFT_HLUTNM = "soft_lutpair30" *) 
  LUT4 #(
    .INIT(16'h1555)) 
    g0_b0__6_i_1
       (.I0(Q[5]),
        .I1(Q[4]),
        .I2(fsm_BCD_decoder[6]),
        .I3(g0_b0__6_i_3_n_0),
        .O(g0_b0__6_i_3_0));
  (* SOFT_HLUTNM = "soft_lutpair29" *) 
  LUT4 #(
    .INIT(16'hFFDF)) 
    g0_b0__6_i_2
       (.I0(Q[1]),
        .I1(g0_b0_i_7_n_0),
        .I2(Q[0]),
        .I3(fsm_BCD_decoder[1]),
        .O(decoderBCD_display_exit_S6));
  (* SOFT_HLUTNM = "soft_lutpair24" *) 
  LUT5 #(
    .INIT(32'hFFFEEEEE)) 
    g0_b0__6_i_3
       (.I0(Q[3]),
        .I1(Q[2]),
        .I2(Q[0]),
        .I3(fsm_BCD_decoder[1]),
        .I4(Q[1]),
        .O(g0_b0__6_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT5 #(
    .INIT(32'h55445455)) 
    g0_b0_i_1
       (.I0(g0_b0__6_i_3_0),
        .I1(g0_b0_i_7_n_0),
        .I2(fsm_BCD_decoder[1]),
        .I3(Q[1]),
        .I4(Q[0]),
        .O(g0_b0_i_1_n_0));
  LUT3 #(
    .INIT(8'h06)) 
    g0_b0_i_2
       (.I0(fsm_BCD_decoder[1]),
        .I1(Q[1]),
        .I2(g0_b0_i_7_n_0),
        .O(g0_b0_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair21" *) 
  LUT5 #(
    .INIT(32'h0000FBBF)) 
    g0_b0_i_3
       (.I0(g0_b0_i_7_n_0),
        .I1(Q[0]),
        .I2(Q[1]),
        .I3(fsm_BCD_decoder[1]),
        .I4(g0_b0__6_i_3_0),
        .O(g0_b0_i_3_n_0));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT4 #(
    .INIT(16'h005E)) 
    g0_b0_i_4
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(fsm_BCD_decoder[1]),
        .I3(g0_b0_i_7_n_0),
        .O(g0_b0_i_4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT4 #(
    .INIT(16'h0400)) 
    g0_b0_i_5
       (.I0(fsm_BCD_decoder[1]),
        .I1(Q[0]),
        .I2(g0_b0_i_7_n_0),
        .I3(Q[1]),
        .O(g0_b0_i_5_n_0));
  LUT5 #(
    .INIT(32'h0000EECD)) 
    g0_b0_i_6
       (.I0(fsm_BCD_decoder[1]),
        .I1(g0_b0_i_7_n_0),
        .I2(Q[0]),
        .I3(Q[1]),
        .I4(g0_b0__6_i_3_0),
        .O(sel[5]));
  LUT5 #(
    .INIT(32'hFFFFFF7F)) 
    g0_b0_i_7
       (.I0(fsm_BCD_decoder[6]),
        .I1(Q[5]),
        .I2(Q[2]),
        .I3(Q[4]),
        .I4(Q[3]),
        .O(g0_b0_i_7_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFB78A4D860)) 
    g0_b1
       (.I0(g0_b0_i_1_n_0),
        .I1(g0_b0_i_2_n_0),
        .I2(g0_b0_i_3_n_0),
        .I3(g0_b0_i_4_n_0),
        .I4(g0_b0_i_5_n_0),
        .I5(sel[5]),
        .O(g0_b1_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFB78A4D860)) 
    g0_b1__0
       (.I0(sel[0]),
        .I1(sel[1]),
        .I2(sel[2]),
        .I3(sel[3]),
        .I4(sel[4]),
        .I5(sel[5]),
        .O(g0_b1__0_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFB78A4D860)) 
    g0_b1__1
       (.I0(\display_number_OBUF[7]_inst_i_4_0 ),
        .I1(g0_b0__1_i_2_n_0),
        .I2(g0_b0__1_i_3_n_0),
        .I3(\display_number_OBUF[7]_inst_i_4_1 ),
        .I4(g0_b0__1_i_5_n_0),
        .I5(sel[5]),
        .O(g0_b1__1_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFB78A4D860)) 
    g0_b1__2
       (.I0(g0_b0__2_i_1_n_0),
        .I1(g0_b0__2_i_2_n_0),
        .I2(g0_b0__2_i_3_n_0),
        .I3(g0_b0__2_i_4_n_0),
        .I4(sel[1]),
        .I5(sel[5]),
        .O(g0_b1__2_n_0));
  LUT4 #(
    .INIT(16'hFDCC)) 
    g0_b1__3
       (.I0(sel[5]),
        .I1(decoderBCD_display_exit_S5),
        .I2(g0_b6__3_i_2_n_0),
        .I3(g0_b6__3_i_3_n_0),
        .O(g0_b1__3_n_0));
  LUT4 #(
    .INIT(16'hECCC)) 
    g0_b1__4
       (.I0(sel[5]),
        .I1(decoderBCD_display_exit_S5),
        .I2(decoderBCD_display_exit_S7),
        .I3(decoderBCD_display_exit_S4[1]),
        .O(g0_b1__4_n_0));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT4 #(
    .INIT(16'h0036)) 
    g0_b1__4_i_1
       (.I0(Q[0]),
        .I1(Q[1]),
        .I2(fsm_BCD_decoder[1]),
        .I3(g0_b0_i_7_n_0),
        .O(decoderBCD_display_exit_S7));
  (* SOFT_HLUTNM = "soft_lutpair38" *) 
  LUT2 #(
    .INIT(4'hE)) 
    g0_b1__4_i_2
       (.I0(g0_b0__6_i_3_0),
        .I1(g0_b0_i_2_n_0),
        .O(decoderBCD_display_exit_S4[1]));
  LUT6 #(
    .INIT(64'hFFFFFFDF9D607940)) 
    g0_b1__5
       (.I0(sel[5]),
        .I1(decoderBCD_display_exit_S4[0]),
        .I2(decoderBCD_display_exit_S4[1]),
        .I3(decoderBCD_display_exit_S4[2]),
        .I4(decoderBCD_display_exit_S4[4]),
        .I5(g0_b7__3_i_4_n_0),
        .O(g0_b1__5_n_0));
  LUT6 #(
    .INIT(64'hFFFFF7FFE3131630)) 
    g0_b1__6
       (.I0(sel[2]),
        .I1(g0_b0__6_i_3_0),
        .I2(decoderBCD_display_exit_S6),
        .I3(g0_b6__3_i_2_n_0),
        .I4(decoderBCD_display_exit_S4[1]),
        .I5(decoderBCD_display_exit_S5),
        .O(g0_b1__6_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFA2A34D004)) 
    g0_b2
       (.I0(g0_b0_i_1_n_0),
        .I1(g0_b0_i_2_n_0),
        .I2(g0_b0_i_3_n_0),
        .I3(g0_b0_i_4_n_0),
        .I4(g0_b0_i_5_n_0),
        .I5(sel[5]),
        .O(g0_b2_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFA2A34D004)) 
    g0_b2__0
       (.I0(sel[0]),
        .I1(sel[1]),
        .I2(sel[2]),
        .I3(sel[3]),
        .I4(sel[4]),
        .I5(sel[5]),
        .O(g0_b2__0_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFA2A34D004)) 
    g0_b2__1
       (.I0(\display_number_OBUF[7]_inst_i_4_0 ),
        .I1(g0_b0__1_i_2_n_0),
        .I2(g0_b0__1_i_3_n_0),
        .I3(\display_number_OBUF[7]_inst_i_4_1 ),
        .I4(g0_b0__1_i_5_n_0),
        .I5(sel[5]),
        .O(g0_b2__1_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFA2A34D004)) 
    g0_b2__2
       (.I0(g0_b0__2_i_1_n_0),
        .I1(g0_b0__2_i_2_n_0),
        .I2(g0_b0__2_i_3_n_0),
        .I3(g0_b0__2_i_4_n_0),
        .I4(sel[1]),
        .I5(sel[5]),
        .O(g0_b2__2_n_0));
  LUT4 #(
    .INIT(16'hCCEC)) 
    g0_b2__3
       (.I0(sel[5]),
        .I1(decoderBCD_display_exit_S5),
        .I2(g0_b6__3_i_2_n_0),
        .I3(g0_b6__3_i_3_n_0),
        .O(g0_b2__3_n_0));
  LUT3 #(
    .INIT(8'hEC)) 
    g0_b2__4
       (.I0(sel[5]),
        .I1(decoderBCD_display_exit_S5),
        .I2(decoderBCD_display_exit_S7),
        .O(g0_b2__4_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFDD0E645120)) 
    g0_b2__5
       (.I0(sel[5]),
        .I1(decoderBCD_display_exit_S4[0]),
        .I2(decoderBCD_display_exit_S4[1]),
        .I3(decoderBCD_display_exit_S4[2]),
        .I4(decoderBCD_display_exit_S4[4]),
        .I5(g0_b7__3_i_4_n_0),
        .O(g0_b2__5_n_0));
  LUT6 #(
    .INIT(64'hFFFFF7F762520C11)) 
    g0_b2__6
       (.I0(sel[2]),
        .I1(g0_b0__6_i_3_0),
        .I2(decoderBCD_display_exit_S6),
        .I3(g0_b6__3_i_2_n_0),
        .I4(decoderBCD_display_exit_S4[1]),
        .I5(decoderBCD_display_exit_S5),
        .O(g0_b2__6_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFD20EC28692)) 
    g0_b3
       (.I0(g0_b0_i_1_n_0),
        .I1(g0_b0_i_2_n_0),
        .I2(g0_b0_i_3_n_0),
        .I3(g0_b0_i_4_n_0),
        .I4(g0_b0_i_5_n_0),
        .I5(sel[5]),
        .O(g0_b3_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFD20EC28692)) 
    g0_b3__0
       (.I0(sel[0]),
        .I1(sel[1]),
        .I2(sel[2]),
        .I3(sel[3]),
        .I4(sel[4]),
        .I5(sel[5]),
        .O(g0_b3__0_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFD20EC28692)) 
    g0_b3__1
       (.I0(\display_number_OBUF[7]_inst_i_4_0 ),
        .I1(g0_b0__1_i_2_n_0),
        .I2(g0_b0__1_i_3_n_0),
        .I3(\display_number_OBUF[7]_inst_i_4_1 ),
        .I4(g0_b0__1_i_5_n_0),
        .I5(sel[5]),
        .O(g0_b3__1_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFD20EC28692)) 
    g0_b3__2
       (.I0(g0_b0__2_i_1_n_0),
        .I1(g0_b0__2_i_2_n_0),
        .I2(g0_b0__2_i_3_n_0),
        .I3(g0_b0__2_i_4_n_0),
        .I4(sel[1]),
        .I5(sel[5]),
        .O(g0_b3__2_n_0));
  LUT4 #(
    .INIT(16'hC65C)) 
    g0_b3__3
       (.I0(sel[5]),
        .I1(decoderBCD_display_exit_S5),
        .I2(g0_b6__3_i_2_n_0),
        .I3(g0_b6__3_i_3_n_0),
        .O(g0_b3__3_n_0));
  LUT4 #(
    .INIT(16'hDCD4)) 
    g0_b3__4
       (.I0(sel[5]),
        .I1(decoderBCD_display_exit_S5),
        .I2(decoderBCD_display_exit_S7),
        .I3(decoderBCD_display_exit_S4[1]),
        .O(g0_b3__4_n_0));
  LUT6 #(
    .INIT(64'hFFFFF75DA05CC21C)) 
    g0_b3__5
       (.I0(sel[5]),
        .I1(decoderBCD_display_exit_S4[0]),
        .I2(decoderBCD_display_exit_S4[1]),
        .I3(decoderBCD_display_exit_S4[2]),
        .I4(decoderBCD_display_exit_S4[4]),
        .I5(g0_b7__3_i_4_n_0),
        .O(g0_b3__5_n_0));
  LUT6 #(
    .INIT(64'hFFFF77D76CC031C2)) 
    g0_b3__6
       (.I0(sel[2]),
        .I1(g0_b0__6_i_3_0),
        .I2(decoderBCD_display_exit_S6),
        .I3(g0_b6__3_i_2_n_0),
        .I4(decoderBCD_display_exit_S4[1]),
        .I5(decoderBCD_display_exit_S5),
        .O(g0_b3__6_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFC140102BA)) 
    g0_b4
       (.I0(g0_b0_i_1_n_0),
        .I1(g0_b0_i_2_n_0),
        .I2(g0_b0_i_3_n_0),
        .I3(g0_b0_i_4_n_0),
        .I4(g0_b0_i_5_n_0),
        .I5(sel[5]),
        .O(g0_b4_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFC140102BA)) 
    g0_b4__0
       (.I0(sel[0]),
        .I1(sel[1]),
        .I2(sel[2]),
        .I3(sel[3]),
        .I4(sel[4]),
        .I5(sel[5]),
        .O(g0_b4__0_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFC140102BA)) 
    g0_b4__1
       (.I0(\display_number_OBUF[7]_inst_i_4_0 ),
        .I1(g0_b0__1_i_2_n_0),
        .I2(g0_b0__1_i_3_n_0),
        .I3(\display_number_OBUF[7]_inst_i_4_1 ),
        .I4(g0_b0__1_i_5_n_0),
        .I5(sel[5]),
        .O(g0_b4__1_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFFC140102BA)) 
    g0_b4__2
       (.I0(g0_b0__2_i_1_n_0),
        .I1(g0_b0__2_i_2_n_0),
        .I2(g0_b0__2_i_3_n_0),
        .I3(g0_b0__2_i_4_n_0),
        .I4(sel[1]),
        .I5(sel[5]),
        .O(g0_b4__2_n_0));
  LUT4 #(
    .INIT(16'hCFD4)) 
    g0_b4__3
       (.I0(sel[5]),
        .I1(decoderBCD_display_exit_S5),
        .I2(g0_b6__3_i_2_n_0),
        .I3(g0_b6__3_i_3_n_0),
        .O(g0_b4__3_n_0));
  LUT4 #(
    .INIT(16'hDECC)) 
    g0_b4__4
       (.I0(sel[5]),
        .I1(decoderBCD_display_exit_S5),
        .I2(decoderBCD_display_exit_S7),
        .I3(decoderBCD_display_exit_S4[1]),
        .O(g0_b4__4_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFF501128A8C)) 
    g0_b4__5
       (.I0(sel[5]),
        .I1(decoderBCD_display_exit_S4[0]),
        .I2(decoderBCD_display_exit_S4[1]),
        .I3(decoderBCD_display_exit_S4[2]),
        .I4(decoderBCD_display_exit_S4[4]),
        .I5(g0_b7__3_i_4_n_0),
        .O(g0_b4__5_n_0));
  LUT6 #(
    .INIT(64'hFFFFFF770481A0A6)) 
    g0_b4__6
       (.I0(sel[2]),
        .I1(g0_b0__6_i_3_0),
        .I2(decoderBCD_display_exit_S6),
        .I3(g0_b6__3_i_2_n_0),
        .I4(decoderBCD_display_exit_S4[1]),
        .I5(decoderBCD_display_exit_S5),
        .O(g0_b4__6_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFF9488C208E)) 
    g0_b5
       (.I0(g0_b0_i_1_n_0),
        .I1(g0_b0_i_2_n_0),
        .I2(g0_b0_i_3_n_0),
        .I3(g0_b0_i_4_n_0),
        .I4(g0_b0_i_5_n_0),
        .I5(sel[5]),
        .O(g0_b5_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFF9488C208E)) 
    g0_b5__0
       (.I0(sel[0]),
        .I1(sel[1]),
        .I2(sel[2]),
        .I3(sel[3]),
        .I4(sel[4]),
        .I5(sel[5]),
        .O(g0_b5__0_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFF9488C208E)) 
    g0_b5__1
       (.I0(\display_number_OBUF[7]_inst_i_4_0 ),
        .I1(g0_b0__1_i_2_n_0),
        .I2(g0_b0__1_i_3_n_0),
        .I3(\display_number_OBUF[7]_inst_i_4_1 ),
        .I4(g0_b0__1_i_5_n_0),
        .I5(sel[5]),
        .O(g0_b5__1_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFF9488C208E)) 
    g0_b5__2
       (.I0(g0_b0__2_i_1_n_0),
        .I1(g0_b0__2_i_2_n_0),
        .I2(g0_b0__2_i_3_n_0),
        .I3(g0_b0__2_i_4_n_0),
        .I4(sel[1]),
        .I5(sel[5]),
        .O(g0_b5__2_n_0));
  LUT4 #(
    .INIT(16'hDCE4)) 
    g0_b5__3
       (.I0(sel[5]),
        .I1(decoderBCD_display_exit_S5),
        .I2(g0_b6__3_i_2_n_0),
        .I3(g0_b6__3_i_3_n_0),
        .O(g0_b5__3_n_0));
  LUT3 #(
    .INIT(8'hEC)) 
    g0_b5__4
       (.I0(sel[5]),
        .I1(decoderBCD_display_exit_S5),
        .I2(decoderBCD_display_exit_S7),
        .O(g0_b5__4_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFD790E084A8)) 
    g0_b5__5
       (.I0(sel[5]),
        .I1(decoderBCD_display_exit_S4[0]),
        .I2(decoderBCD_display_exit_S4[1]),
        .I3(decoderBCD_display_exit_S4[2]),
        .I4(decoderBCD_display_exit_S4[4]),
        .I5(g0_b7__3_i_4_n_0),
        .O(g0_b5__5_n_0));
  LUT6 #(
    .INIT(64'hFFFFF77F4120FC80)) 
    g0_b5__6
       (.I0(sel[2]),
        .I1(g0_b0__6_i_3_0),
        .I2(decoderBCD_display_exit_S6),
        .I3(g0_b6__3_i_2_n_0),
        .I4(decoderBCD_display_exit_S4[1]),
        .I5(decoderBCD_display_exit_S5),
        .O(g0_b5__6_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFE1C16C1083)) 
    g0_b6
       (.I0(g0_b0_i_1_n_0),
        .I1(g0_b0_i_2_n_0),
        .I2(g0_b0_i_3_n_0),
        .I3(g0_b0_i_4_n_0),
        .I4(g0_b0_i_5_n_0),
        .I5(sel[5]),
        .O(g0_b6_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFE1C16C1083)) 
    g0_b6__0
       (.I0(sel[0]),
        .I1(sel[1]),
        .I2(sel[2]),
        .I3(sel[3]),
        .I4(sel[4]),
        .I5(sel[5]),
        .O(g0_b6__0_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFE1C16C1083)) 
    g0_b6__1
       (.I0(\display_number_OBUF[7]_inst_i_4_0 ),
        .I1(g0_b0__1_i_2_n_0),
        .I2(g0_b0__1_i_3_n_0),
        .I3(\display_number_OBUF[7]_inst_i_4_1 ),
        .I4(g0_b0__1_i_5_n_0),
        .I5(sel[5]),
        .O(g0_b6__1_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFE1C16C1083)) 
    g0_b6__2
       (.I0(g0_b0__2_i_1_n_0),
        .I1(g0_b0__2_i_2_n_0),
        .I2(g0_b0__2_i_3_n_0),
        .I3(g0_b0__2_i_4_n_0),
        .I4(sel[1]),
        .I5(sel[5]),
        .O(g0_b6__2_n_0));
  LUT4 #(
    .INIT(16'hDC47)) 
    g0_b6__3
       (.I0(sel[5]),
        .I1(decoderBCD_display_exit_S5),
        .I2(g0_b6__3_i_2_n_0),
        .I3(g0_b6__3_i_3_n_0),
        .O(g0_b6__3_n_0));
  LUT4 #(
    .INIT(16'h5545)) 
    g0_b6__3_i_1
       (.I0(g0_b0__6_i_3_0),
        .I1(fsm_BCD_decoder[1]),
        .I2(Q[0]),
        .I3(g0_b0_i_7_n_0),
        .O(decoderBCD_display_exit_S5));
  (* SOFT_HLUTNM = "soft_lutpair25" *) 
  LUT5 #(
    .INIT(32'hAABAAABE)) 
    g0_b6__3_i_2
       (.I0(g0_b0__6_i_3_0),
        .I1(Q[1]),
        .I2(fsm_BCD_decoder[1]),
        .I3(g0_b0_i_7_n_0),
        .I4(Q[0]),
        .O(g0_b6__3_i_2_n_0));
  (* SOFT_HLUTNM = "soft_lutpair31" *) 
  LUT4 #(
    .INIT(16'hFFEF)) 
    g0_b6__3_i_3
       (.I0(fsm_BCD_decoder[1]),
        .I1(Q[1]),
        .I2(Q[0]),
        .I3(g0_b0_i_7_n_0),
        .O(g0_b6__3_i_3_n_0));
  LUT4 #(
    .INIT(16'hEDCE)) 
    g0_b6__4
       (.I0(sel[5]),
        .I1(decoderBCD_display_exit_S5),
        .I2(decoderBCD_display_exit_S7),
        .I3(decoderBCD_display_exit_S4[1]),
        .O(g0_b6__4_n_0));
  LUT6 #(
    .INIT(64'hFFFFFD5778A1810A)) 
    g0_b6__5
       (.I0(sel[5]),
        .I1(decoderBCD_display_exit_S4[0]),
        .I2(decoderBCD_display_exit_S4[1]),
        .I3(decoderBCD_display_exit_S4[2]),
        .I4(decoderBCD_display_exit_S4[4]),
        .I5(g0_b7__3_i_4_n_0),
        .O(g0_b6__5_n_0));
  LUT6 #(
    .INIT(64'hFFFF777D11066598)) 
    g0_b6__6
       (.I0(sel[2]),
        .I1(g0_b0__6_i_3_0),
        .I2(decoderBCD_display_exit_S6),
        .I3(g0_b6__3_i_2_n_0),
        .I4(decoderBCD_display_exit_S4[1]),
        .I5(decoderBCD_display_exit_S5),
        .O(g0_b6__6_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFBFFFFFFFFF)) 
    g0_b7
       (.I0(g0_b0_i_1_n_0),
        .I1(g0_b0_i_2_n_0),
        .I2(g0_b0_i_3_n_0),
        .I3(g0_b0_i_4_n_0),
        .I4(g0_b0_i_5_n_0),
        .I5(sel[5]),
        .O(g0_b7_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFBFFFFFFFFF)) 
    g0_b7__0
       (.I0(sel[0]),
        .I1(sel[1]),
        .I2(sel[2]),
        .I3(sel[3]),
        .I4(sel[4]),
        .I5(sel[5]),
        .O(g0_b7__0_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFBFFFFFFFFF)) 
    g0_b7__1
       (.I0(\display_number_OBUF[7]_inst_i_4_0 ),
        .I1(g0_b0__1_i_2_n_0),
        .I2(g0_b0__1_i_3_n_0),
        .I3(\display_number_OBUF[7]_inst_i_4_1 ),
        .I4(g0_b0__1_i_5_n_0),
        .I5(sel[5]),
        .O(g0_b7__1_n_0));
  LUT6 #(
    .INIT(64'hFFFFFFBFFFFFFFFF)) 
    g0_b7__2
       (.I0(g0_b0__2_i_1_n_0),
        .I1(g0_b0__2_i_2_n_0),
        .I2(g0_b0__2_i_3_n_0),
        .I3(g0_b0__2_i_4_n_0),
        .I4(sel[1]),
        .I5(sel[5]),
        .O(g0_b7__2_n_0));
  LUT6 #(
    .INIT(64'hFFFFDFFFFFFFFFFF)) 
    g0_b7__3
       (.I0(sel[5]),
        .I1(decoderBCD_display_exit_S4[0]),
        .I2(decoderBCD_display_exit_S4[1]),
        .I3(decoderBCD_display_exit_S4[2]),
        .I4(decoderBCD_display_exit_S4[4]),
        .I5(g0_b7__3_i_4_n_0),
        .O(g0_b7__3_i_4_0));
  (* SOFT_HLUTNM = "soft_lutpair22" *) 
  LUT5 #(
    .INIT(32'hAAAAAEAA)) 
    g0_b7__3_i_1
       (.I0(g0_b0__0_i_6_n_0),
        .I1(fsm_BCD_decoder[1]),
        .I2(g0_b0_i_7_n_0),
        .I3(Q[0]),
        .I4(Q[1]),
        .O(decoderBCD_display_exit_S4[0]));
  (* SOFT_HLUTNM = "soft_lutpair20" *) 
  LUT5 #(
    .INIT(32'hFFFFFFEB)) 
    g0_b7__3_i_2
       (.I0(g0_b0__6_i_3_0),
        .I1(Q[1]),
        .I2(fsm_BCD_decoder[1]),
        .I3(g0_b0_i_7_n_0),
        .I4(Q[0]),
        .O(decoderBCD_display_exit_S4[2]));
  (* SOFT_HLUTNM = "soft_lutpair27" *) 
  LUT4 #(
    .INIT(16'h0120)) 
    g0_b7__3_i_3
       (.I0(fsm_BCD_decoder[1]),
        .I1(g0_b0_i_7_n_0),
        .I2(Q[0]),
        .I3(Q[1]),
        .O(decoderBCD_display_exit_S4[4]));
  LUT2 #(
    .INIT(4'h1)) 
    g0_b7__3_i_4
       (.I0(g0_b0__6_i_3_0),
        .I1(sel[3]),
        .O(g0_b7__3_i_4_n_0));
  LUT6 #(
    .INIT(64'hFFFFFDFFFFFFFFFF)) 
    g0_b7__4
       (.I0(sel[2]),
        .I1(g0_b0__6_i_3_0),
        .I2(decoderBCD_display_exit_S6),
        .I3(g0_b6__3_i_2_n_0),
        .I4(decoderBCD_display_exit_S4[1]),
        .I5(decoderBCD_display_exit_S5),
        .O(g0_b6__3_i_1_0));
  LUT1 #(
    .INIT(2'h1)) 
    letra10__1_carry__0_i_1
       (.I0(Q[5]),
        .O(\salida_reg[7]_0 [1]));
  LUT2 #(
    .INIT(4'hB)) 
    letra10__1_carry__0_i_2
       (.I0(Q[5]),
        .I1(Q[4]),
        .O(\salida_reg[7]_0 [0]));
  LUT2 #(
    .INIT(4'hE)) 
    letra10__1_carry__0_i_3
       (.I0(fsm_BCD_decoder[6]),
        .I1(Q[5]),
        .O(\salida_reg[7]_2 [1]));
  LUT3 #(
    .INIT(8'hC9)) 
    letra10__1_carry__0_i_4
       (.I0(Q[4]),
        .I1(fsm_BCD_decoder[6]),
        .I2(Q[5]),
        .O(\salida_reg[7]_2 [0]));
  LUT1 #(
    .INIT(2'h1)) 
    letra10__1_carry_i_1
       (.I0(Q[2]),
        .O(\salida_reg[3]_0 ));
  LUT2 #(
    .INIT(4'h6)) 
    letra10__1_carry_i_2
       (.I0(Q[4]),
        .I1(Q[5]),
        .O(\salida_reg[7]_1 [3]));
  LUT1 #(
    .INIT(2'h1)) 
    letra10__1_carry_i_3
       (.I0(Q[3]),
        .O(\salida_reg[7]_1 [2]));
  LUT3 #(
    .INIT(8'hD2)) 
    letra10__1_carry_i_4
       (.I0(Q[1]),
        .I1(Q[5]),
        .I2(Q[2]),
        .O(\salida_reg[7]_1 [1]));
  LUT2 #(
    .INIT(4'h6)) 
    letra10__1_carry_i_5
       (.I0(Q[1]),
        .I1(Q[5]),
        .O(\salida_reg[7]_1 [0]));
  LUT2 #(
    .INIT(4'h9)) 
    letra21__104_carry__0_i_1
       (.I0(letra21_carry__1_i_11_0),
        .I1(letra21_carry__1_i_10_0),
        .O(letra21__37_carry__1_i_1_3));
  (* HLUTNM = "lutpair1" *) 
  LUT5 #(
    .INIT(32'h69696669)) 
    letra21__104_carry__0_i_2
       (.I0(letra21_carry__1_i_11_0),
        .I1(letra21_carry__1_i_10_0),
        .I2(letra21_carry__1_i_9_1),
        .I3(letra21_carry_i_5_1),
        .I4(letra21_carry__1_i_10_1),
        .O(letra21_carry__0_i_10_0[3]));
  LUT6 #(
    .INIT(64'hB24DB2B2B2B2B24D)) 
    letra21__104_carry__0_i_3
       (.I0(\salida_reg[0]_0 ),
        .I1(letra21_carry__1_i_11_0),
        .I2(g0_b0__1_i_13_0),
        .I3(letra21_carry__1_i_9_1),
        .I4(letra21_carry_i_5_1),
        .I5(letra21_carry__1_i_10_1),
        .O(letra21_carry__0_i_10_0[2]));
  LUT4 #(
    .INIT(16'h9669)) 
    letra21__104_carry__0_i_4
       (.I0(letra21__37_carry__1_i_1_0[1]),
        .I1(\salida_reg[0]_0 ),
        .I2(letra21_carry__1_i_11_0),
        .I3(g0_b0__1_i_13_0),
        .O(letra21_carry__0_i_10_0[1]));
  LUT4 #(
    .INIT(16'h9669)) 
    letra21__104_carry__0_i_5
       (.I0(letra21__37_carry__1_i_1_0[0]),
        .I1(letra21_carry__1_i_10_0),
        .I2(letra21_carry_i_5_1),
        .I3(letra21_carry_i_5_0[0]),
        .O(letra21_carry__0_i_10_0[0]));
  LUT2 #(
    .INIT(4'h9)) 
    letra21__104_carry__1_i_1
       (.I0(g0_b0__1_i_13_0),
        .I1(letra21_carry__1_i_9_1),
        .O(letra21_carry__2_i_1_6[3]));
  LUT2 #(
    .INIT(4'h6)) 
    letra21__104_carry__1_i_2
       (.I0(letra21_carry__1_i_10_0),
        .I1(letra21_carry__1_i_9_0),
        .O(letra21_carry__2_i_1_6[2]));
  LUT4 #(
    .INIT(16'h444B)) 
    letra21__104_carry__1_i_3
       (.I0(g0_b0__1_i_13_0),
        .I1(letra21_carry__1_i_10_1),
        .I2(letra21_carry__1_i_9_1),
        .I3(letra21_carry__1_i_10_0),
        .O(letra21_carry__2_i_1_6[1]));
  LUT2 #(
    .INIT(4'h6)) 
    letra21__104_carry__1_i_4
       (.I0(letra21_carry__1_i_9_2[0]),
        .I1(letra21_carry__2_i_1_0[2]),
        .O(letra21_carry__2_i_1_6[0]));
  LUT1 #(
    .INIT(2'h1)) 
    letra21__104_carry__2_i_1
       (.I0(letra21_carry__1_i_9_1),
        .O(letra21_carry__2_i_1_2));
  LUT6 #(
    .INIT(64'h6996969696696969)) 
    letra21__104_carry_i_1
       (.I0(letra21_carry__0_i_11_0[2]),
        .I1(letra21_carry__1_i_10_1),
        .I2(g0_b0__1_i_14_0[0]),
        .I3(g0_b0__1_i_14_0[3]),
        .I4(Q[0]),
        .I5(\salida_reg[0]_0 ),
        .O(letra21_carry__0_i_12_0[3]));
  LUT6 #(
    .INIT(64'h6669969696699969)) 
    letra21__104_carry_i_2
       (.I0(g0_b0__1_i_14_0[1]),
        .I1(letra21_carry__1_i_11_0),
        .I2(Q[0]),
        .I3(g0_b0__1_i_14_0[3]),
        .I4(g0_b0__1_i_14_0[0]),
        .I5(letra21_carry_i_5_1),
        .O(letra21_carry__0_i_12_0[2]));
  LUT5 #(
    .INIT(32'h956A6A95)) 
    letra21__104_carry_i_3
       (.I0(letra21_carry__0_i_11_0[0]),
        .I1(Q[0]),
        .I2(g0_b0__1_i_14_0[3]),
        .I3(g0_b0__1_i_14_0[0]),
        .I4(letra21_carry_i_5_1),
        .O(letra21_carry__0_i_12_0[1]));
  (* HLUTNM = "lutpair0" *) 
  LUT5 #(
    .INIT(32'h9696969C)) 
    letra21__104_carry_i_4
       (.I0(g0_b0__1_i_14_0[3]),
        .I1(g0_b0__1_i_14_0[2]),
        .I2(Q[0]),
        .I3(g0_b0__1_i_14_0[0]),
        .I4(g0_b0__1_i_14_0[1]),
        .O(letra21_carry__0_i_12_0[0]));
  (* HLUTNM = "lutpair2" *) 
  LUT3 #(
    .INIT(8'h2B)) 
    letra21__130_carry__0_i_1
       (.I0(letra21_carry_i_5_1),
        .I1(letra21_carry__1_i_10_1),
        .I2(letra21__130_carry__1),
        .O(letra21_carry__2[3]));
  LUT3 #(
    .INIT(8'h2B)) 
    letra21__130_carry__0_i_2
       (.I0(\salida_reg[0]_0 ),
        .I1(letra21__130_carry__1),
        .I2(letra21_carry__1_i_11_0),
        .O(letra21_carry__2[2]));
  LUT6 #(
    .INIT(64'h1117777777711111)) 
    letra21__130_carry__0_i_3
       (.I0(letra21_carry_i_5_1),
        .I1(letra21__130_carry__1),
        .I2(g0_b0__1_i_14_0[0]),
        .I3(Q[0]),
        .I4(g0_b0__1_i_14_0[3]),
        .I5(g0_b0__1_i_14_0[1]),
        .O(letra21_carry__2[1]));
  LUT6 #(
    .INIT(64'h660000CCFECC67FF)) 
    letra21__130_carry__0_i_4
       (.I0(Q[0]),
        .I1(g0_b0__1_i_14_0[0]),
        .I2(g0_b0__1_i_14_0[1]),
        .I3(g0_b0__1_i_14_0[3]),
        .I4(g0_b0__1_i_14_0[2]),
        .I5(letra21__130_carry__1),
        .O(letra21_carry__2[0]));
  (* HLUTNM = "lutpair3" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    letra21__130_carry__0_i_5
       (.I0(letra21_carry__1_i_11_0),
        .I1(letra21_carry__1_i_10_0),
        .I2(letra21__130_carry__1),
        .I3(letra21_carry__2[3]),
        .O(letra21__130_carry__0_i_1_0[3]));
  (* HLUTNM = "lutpair2" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    letra21__130_carry__0_i_6
       (.I0(letra21_carry_i_5_1),
        .I1(letra21_carry__1_i_10_1),
        .I2(letra21__130_carry__1),
        .I3(letra21_carry__2[2]),
        .O(letra21__130_carry__0_i_1_0[2]));
  LUT4 #(
    .INIT(16'h6996)) 
    letra21__130_carry__0_i_7
       (.I0(\salida_reg[0]_0 ),
        .I1(letra21__130_carry__1),
        .I2(letra21_carry__1_i_11_0),
        .I3(letra21_carry__2[1]),
        .O(letra21__130_carry__0_i_1_0[1]));
  LUT4 #(
    .INIT(16'h6996)) 
    letra21__130_carry__0_i_8
       (.I0(letra21_carry__2[0]),
        .I1(letra21__130_carry__1),
        .I2(letra21_carry_i_5_0[0]),
        .I3(letra21_carry_i_5_1),
        .O(letra21__130_carry__0_i_1_0[0]));
  LUT2 #(
    .INIT(4'hB)) 
    letra21__130_carry__1_i_1
       (.I0(g0_b0__1_i_13_0),
        .I1(letra21__130_carry__1),
        .O(letra21_carry__2_0[3]));
  (* HLUTNM = "lutpair4" *) 
  LUT3 #(
    .INIT(8'hAB)) 
    letra21__130_carry__1_i_2
       (.I0(letra21_carry__1_i_10_0),
        .I1(letra21_carry__1_i_9_1),
        .I2(letra21__130_carry__1),
        .O(letra21_carry__2_0[2]));
  LUT4 #(
    .INIT(16'h0CEE)) 
    letra21__130_carry__1_i_3
       (.I0(letra21_carry__1_i_9_0),
        .I1(letra21_carry__1_i_10_1),
        .I2(g0_b0__1_i_13_0),
        .I3(letra21__130_carry__1),
        .O(letra21_carry__2_0[1]));
  (* HLUTNM = "lutpair3" *) 
  LUT3 #(
    .INIT(8'h2B)) 
    letra21__130_carry__1_i_4
       (.I0(letra21_carry__1_i_11_0),
        .I1(letra21_carry__1_i_10_0),
        .I2(letra21__130_carry__1),
        .O(letra21_carry__2_0[0]));
  LUT3 #(
    .INIT(8'h93)) 
    letra21__130_carry__1_i_5
       (.I0(g0_b0__1_i_13_0),
        .I1(letra21_carry__1_i_9_1),
        .I2(letra21__130_carry__1),
        .O(letra21_carry__2_2[3]));
  LUT3 #(
    .INIT(8'h69)) 
    letra21__130_carry__1_i_6
       (.I0(letra21_carry__2_0[2]),
        .I1(letra21_carry__1_i_9_0),
        .I2(letra21__130_carry__1),
        .O(letra21_carry__2_2[2]));
  (* HLUTNM = "lutpair4" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    letra21__130_carry__1_i_7
       (.I0(letra21_carry__1_i_10_0),
        .I1(letra21_carry__1_i_9_1),
        .I2(letra21__130_carry__1),
        .I3(letra21_carry__2_0[1]),
        .O(letra21_carry__2_2[1]));
  LUT3 #(
    .INIT(8'h69)) 
    letra21__130_carry__1_i_8
       (.I0(letra21_carry__2_0[0]),
        .I1(letra21_carry__2_i_1_0[2]),
        .I2(letra21__130_carry__1),
        .O(letra21_carry__2_2[0]));
  LUT2 #(
    .INIT(4'h7)) 
    letra21__130_carry__2_i_1
       (.I0(letra21_carry__1_i_9_1),
        .I1(letra21__130_carry__1),
        .O(letra21_carry__2_3));
  LUT5 #(
    .INIT(32'hB22BA32B)) 
    letra21__130_carry_i_1
       (.I0(Q[0]),
        .I1(letra21__130_carry__1),
        .I2(g0_b0__1_i_14_0[1]),
        .I3(g0_b0__1_i_14_0[3]),
        .I4(g0_b0__1_i_14_0[0]),
        .O(letra21_carry_i_5_4[2]));
  LUT5 #(
    .INIT(32'h6965969A)) 
    letra21__130_carry_i_2
       (.I0(g0_b0__1_i_14_0[1]),
        .I1(g0_b0__1_i_14_0[3]),
        .I2(Q[0]),
        .I3(g0_b0__1_i_14_0[0]),
        .I4(letra21__130_carry__1),
        .O(letra21_carry_i_5_4[1]));
  LUT4 #(
    .INIT(16'h9666)) 
    letra21__130_carry_i_3
       (.I0(letra21__130_carry__1),
        .I1(g0_b0__1_i_14_0[0]),
        .I2(g0_b0__1_i_14_0[3]),
        .I3(Q[0]),
        .O(letra21_carry_i_5_4[0]));
  LUT6 #(
    .INIT(64'h9669696969969696)) 
    letra21__130_carry_i_4
       (.I0(letra21_carry_i_5_4[2]),
        .I1(letra21__130_carry__1),
        .I2(g0_b0__1_i_14_0[0]),
        .I3(g0_b0__1_i_14_0[3]),
        .I4(Q[0]),
        .I5(\salida_reg[0]_0 ),
        .O(letra21_carry__0_i_12_1[3]));
  LUT5 #(
    .INIT(32'h4B962DA5)) 
    letra21__130_carry_i_5
       (.I0(Q[0]),
        .I1(letra21__130_carry__1),
        .I2(g0_b0__1_i_14_0[1]),
        .I3(g0_b0__1_i_14_0[3]),
        .I4(g0_b0__1_i_14_0[0]),
        .O(letra21_carry__0_i_12_1[2]));
  LUT4 #(
    .INIT(16'hD287)) 
    letra21__130_carry_i_6
       (.I0(Q[0]),
        .I1(g0_b0__1_i_14_0[3]),
        .I2(g0_b0__1_i_14_0[0]),
        .I3(letra21__130_carry__1),
        .O(letra21_carry__0_i_12_1[1]));
  LUT2 #(
    .INIT(4'h9)) 
    letra21__130_carry_i_7
       (.I0(Q[0]),
        .I1(letra21__130_carry__1),
        .O(letra21_carry__0_i_12_1[0]));
  LUT5 #(
    .INIT(32'h573D4315)) 
    letra21__208_carry__11_i_1
       (.I0(letra21__208_carry__11),
        .I1(letra21__208_carry__11_1),
        .I2(letra21__208_carry__11_2),
        .I3(letra21__208_carry__11_0[2]),
        .I4(letra21__208_carry__11_0[1]),
        .O(letra21__130_carry__6[1]));
  LUT5 #(
    .INIT(32'h03EB283F)) 
    letra21__208_carry__11_i_2
       (.I0(letra21__208_carry__11_0[0]),
        .I1(letra21__208_carry__11_1),
        .I2(letra21__208_carry__11_2),
        .I3(letra21__208_carry__11),
        .I4(letra21__208_carry__11_0[1]),
        .O(letra21__130_carry__6[0]));
  LUT6 #(
    .INIT(64'h555A566A566A5AAA)) 
    letra21__208_carry__11_i_3
       (.I0(letra21__208_carry__11_i_6_n_3),
        .I1(letra21__208_carry__11_0[2]),
        .I2(letra21__208_carry__11),
        .I3(letra21__208_carry__11_0[3]),
        .I4(letra21__208_carry__11_2),
        .I5(letra21__208_carry__11_1),
        .O(letra21__104_carry__2[2]));
  LUT6 #(
    .INIT(64'h6669999669999666)) 
    letra21__208_carry__11_i_4
       (.I0(letra21__130_carry__6[1]),
        .I1(letra21__208_carry__11),
        .I2(letra21__208_carry__11_1),
        .I3(letra21__208_carry__11_2),
        .I4(letra21__208_carry__11_0[3]),
        .I5(letra21__208_carry__11_0[2]),
        .O(letra21__104_carry__2[1]));
  LUT6 #(
    .INIT(64'h566AA995A995566A)) 
    letra21__208_carry__11_i_5
       (.I0(letra21__130_carry__6[0]),
        .I1(letra21__208_carry__11_1),
        .I2(letra21__208_carry__11_2),
        .I3(letra21__208_carry__11_0[1]),
        .I4(letra21__208_carry__11),
        .I5(letra21__208_carry__11_0[2]),
        .O(letra21__104_carry__2[0]));
  CARRY4 letra21__208_carry__11_i_6
       (.CI(letra21__208_carry__11_i_3_0),
        .CO({NLW_letra21__208_carry__11_i_6_CO_UNCONNECTED[3:1],letra21__208_carry__11_i_6_n_3}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O(NLW_letra21__208_carry__11_i_6_O_UNCONNECTED[3:0]),
        .S({1'b0,1'b0,1'b0,1'b1}));
  LUT6 #(
    .INIT(64'hB3FEFEB332808032)) 
    letra21__208_carry__2_i_1
       (.I0(letra21_carry_i_5_0[0]),
        .I1(letra21__130_carry__1),
        .I2(letra21__208_carry__2_i_7_0[2]),
        .I3(letra21__208_carry__3[0]),
        .I4(letra21__208_carry__2_i_9_n_0),
        .I5(letra21__208_carry__2),
        .O(letra21__37_carry__2[1]));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT3 #(
    .INIT(8'h78)) 
    letra21__208_carry__2_i_10
       (.I0(Q[0]),
        .I1(g0_b0__1_i_14_0[3]),
        .I2(g0_b0__1_i_14_0[0]),
        .O(letra21_carry_i_5_3));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT4 #(
    .INIT(16'hB22B)) 
    letra21__208_carry__2_i_11
       (.I0(letra21__130_carry__1),
        .I1(letra21__208_carry__3[0]),
        .I2(\salida_reg[0]_0 ),
        .I3(Q[0]),
        .O(letra21__208_carry__2_i_11_n_0));
  (* SOFT_HLUTNM = "soft_lutpair26" *) 
  LUT4 #(
    .INIT(16'h6996)) 
    letra21__208_carry__2_i_12
       (.I0(Q[0]),
        .I1(\salida_reg[0]_0 ),
        .I2(letra21__208_carry__3[0]),
        .I3(letra21__130_carry__1),
        .O(letra21__208_carry__2_i_12_n_0));
  LUT6 #(
    .INIT(64'h6969699996969666)) 
    letra21__208_carry__2_i_13
       (.I0(letra21__208_carry__2_i_7_0[2]),
        .I1(g0_b0__1_i_14_0[1]),
        .I2(g0_b0__1_i_14_0[3]),
        .I3(Q[0]),
        .I4(g0_b0__1_i_14_0[0]),
        .I5(letra21__130_carry__1),
        .O(letra21__208_carry__2_i_13_n_0));
  (* SOFT_HLUTNM = "soft_lutpair19" *) 
  LUT5 #(
    .INIT(32'h6A95956A)) 
    letra21__208_carry__2_i_14
       (.I0(letra21__208_carry__2_i_7_0[1]),
        .I1(Q[0]),
        .I2(g0_b0__1_i_14_0[3]),
        .I3(g0_b0__1_i_14_0[0]),
        .I4(letra21__130_carry__1),
        .O(letra21_carry__2_1));
  LUT6 #(
    .INIT(64'hB3FEFEB332808032)) 
    letra21__208_carry__2_i_3
       (.I0(Q[0]),
        .I1(letra21__130_carry__1),
        .I2(letra21__208_carry__2_i_7_0[0]),
        .I3(letra21_carry_i_5_3),
        .I4(letra21__208_carry__2_i_7_0[1]),
        .I5(letra21__208_carry__2_0[0]),
        .O(letra21__37_carry__2[0]));
  LUT6 #(
    .INIT(64'h9669699669969669)) 
    letra21__208_carry__2_i_5
       (.I0(letra21__37_carry__2[1]),
        .I1(letra21__208_carry__2_i_11_n_0),
        .I2(letra21__208_carry__11),
        .I3(letra21__130_carry__1),
        .I4(letra21__208_carry__3[1]),
        .I5(letra21__208_carry__3_0),
        .O(letra21__104_carry[2]));
  LUT6 #(
    .INIT(64'h9A5965A665A69A59)) 
    letra21__208_carry__2_i_6
       (.I0(letra21__208_carry__2_1),
        .I1(letra21_carry_i_5_0[0]),
        .I2(letra21__130_carry__1),
        .I3(letra21__208_carry__2_i_7_0[2]),
        .I4(letra21__208_carry__2),
        .I5(letra21__208_carry__2_i_12_n_0),
        .O(letra21__104_carry[1]));
  LUT6 #(
    .INIT(64'hA665599A599AA665)) 
    letra21__208_carry__2_i_7
       (.I0(letra21__37_carry__2[0]),
        .I1(letra21__130_carry__1),
        .I2(letra21__208_carry__2_i_7_0[1]),
        .I3(letra21_carry_i_5_3),
        .I4(letra21__208_carry__2_0[1]),
        .I5(letra21__208_carry__2_i_13_n_0),
        .O(letra21__104_carry[0]));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT5 #(
    .INIT(32'hF00F1FE0)) 
    letra21__208_carry__2_i_9
       (.I0(g0_b0__1_i_14_0[0]),
        .I1(g0_b0__1_i_14_0[1]),
        .I2(g0_b0__1_i_14_0[3]),
        .I3(g0_b0__1_i_14_0[2]),
        .I4(Q[0]),
        .O(letra21__208_carry__2_i_9_n_0));
  LUT6 #(
    .INIT(64'h7DD7355335531441)) 
    letra21__208_carry__3_i_4
       (.I0(letra21__208_carry__11),
        .I1(letra21__130_carry__1),
        .I2(letra21__208_carry__3[1]),
        .I3(letra21__208_carry__3_0),
        .I4(letra21__208_carry__3[0]),
        .I5(letra21__208_carry__2_i_9_n_0),
        .O(letra21__208_carry__2_i_9_0));
  LUT6 #(
    .INIT(64'h599AA665A665599A)) 
    letra21__208_carry__3_i_8
       (.I0(letra21__208_carry__2_i_9_0),
        .I1(letra21__130_carry__1),
        .I2(letra21__208_carry__3[1]),
        .I3(letra21__208_carry__3_0),
        .I4(letra21__208_carry__11),
        .I5(letra21__208_carry__3_1),
        .O(letra21__208_carry__3_i_12));
  LUT2 #(
    .INIT(4'h8)) 
    letra21__208_carry_i_4
       (.I0(Q[0]),
        .I1(O),
        .O(letra21_carry__0));
  LUT2 #(
    .INIT(4'h9)) 
    letra21__37_carry__0_i_1
       (.I0(letra21_carry__1_i_11_0),
        .I1(letra21_carry__1_i_10_0),
        .O(letra21__37_carry__1_i_1_0[3]));
  LUT3 #(
    .INIT(8'hB2)) 
    letra21__37_carry__0_i_2
       (.I0(g0_b0__1_i_13_0),
        .I1(letra21_carry__1_i_11_0),
        .I2(\salida_reg[0]_0 ),
        .O(letra21__37_carry__1_i_1_0[2]));
  LUT6 #(
    .INIT(64'h222BBBBBBBB22222)) 
    letra21__37_carry__0_i_3
       (.I0(letra21_carry__1_i_10_0),
        .I1(letra21_carry_i_5_1),
        .I2(g0_b0__1_i_14_0[0]),
        .I3(Q[0]),
        .I4(g0_b0__1_i_14_0[3]),
        .I5(g0_b0__1_i_14_0[1]),
        .O(letra21__37_carry__1_i_1_0[1]));
  LUT6 #(
    .INIT(64'hBEBCA0A0282AFAFA)) 
    letra21__37_carry__0_i_4
       (.I0(letra21_carry__1_i_10_1),
        .I1(Q[0]),
        .I2(g0_b0__1_i_14_0[0]),
        .I3(g0_b0__1_i_14_0[1]),
        .I4(g0_b0__1_i_14_0[3]),
        .I5(g0_b0__1_i_14_0[2]),
        .O(letra21__37_carry__1_i_1_0[0]));
  LUT5 #(
    .INIT(32'h51AEAE51)) 
    letra21__37_carry__0_i_5
       (.I0(letra21_carry__1_i_9_1),
        .I1(letra21_carry_i_5_1),
        .I2(letra21_carry__1_i_10_1),
        .I3(letra21_carry__1_i_10_0),
        .I4(letra21_carry__1_i_11_0),
        .O(letra21_carry__0_i_11_1[3]));
  LUT6 #(
    .INIT(64'hB24DB2B2B2B2B24D)) 
    letra21__37_carry__0_i_6
       (.I0(\salida_reg[0]_0 ),
        .I1(letra21_carry__1_i_11_0),
        .I2(g0_b0__1_i_13_0),
        .I3(letra21_carry__1_i_9_1),
        .I4(letra21_carry_i_5_1),
        .I5(letra21_carry__1_i_10_1),
        .O(letra21_carry__0_i_11_1[2]));
  LUT4 #(
    .INIT(16'h9669)) 
    letra21__37_carry__0_i_7
       (.I0(letra21__37_carry__1_i_1_0[1]),
        .I1(\salida_reg[0]_0 ),
        .I2(letra21_carry__1_i_11_0),
        .I3(g0_b0__1_i_13_0),
        .O(letra21_carry__0_i_11_1[1]));
  LUT4 #(
    .INIT(16'h9669)) 
    letra21__37_carry__0_i_8
       (.I0(letra21__37_carry__1_i_1_0[0]),
        .I1(letra21_carry__1_i_10_0),
        .I2(letra21_carry_i_5_1),
        .I3(letra21_carry_i_5_0[0]),
        .O(letra21_carry__0_i_11_1[0]));
  LUT4 #(
    .INIT(16'h666A)) 
    letra21__37_carry__1_i_1
       (.I0(letra230_out[7]),
        .I1(g0_b0__1_i_14_0[3]),
        .I2(letra230_out[6]),
        .I3(letra21_carry__1_i_10_n_0),
        .O(letra21_carry__1_i_10_0));
  LUT4 #(
    .INIT(16'h2844)) 
    letra21__37_carry__1_i_2
       (.I0(g0_b0__1_i_14_0[3]),
        .I1(letra230_out[6]),
        .I2(letra21_carry__1_i_10_n_0),
        .I3(letra230_out[8]),
        .O(letra21_carry__1_i_9_2[1]));
  (* HLUTNM = "lutpair1" *) 
  LUT2 #(
    .INIT(4'h2)) 
    letra21__37_carry__1_i_3
       (.I0(letra21_carry__1_i_11_0),
        .I1(letra21_carry__1_i_10_0),
        .O(letra21_carry__1_i_9_2[0]));
  LUT2 #(
    .INIT(4'h9)) 
    letra21__37_carry__1_i_4
       (.I0(g0_b0__1_i_13_0),
        .I1(letra21_carry__1_i_9_1),
        .O(letra21_carry__2_i_1_7[3]));
  LUT2 #(
    .INIT(4'h6)) 
    letra21__37_carry__1_i_5
       (.I0(letra21_carry__1_i_10_0),
        .I1(letra21_carry__1_i_9_0),
        .O(letra21_carry__2_i_1_7[2]));
  LUT4 #(
    .INIT(16'h444B)) 
    letra21__37_carry__1_i_6
       (.I0(g0_b0__1_i_13_0),
        .I1(letra21_carry__1_i_10_1),
        .I2(letra21_carry__1_i_9_1),
        .I3(letra21_carry__1_i_10_0),
        .O(letra21_carry__2_i_1_7[1]));
  LUT2 #(
    .INIT(4'h6)) 
    letra21__37_carry__1_i_7
       (.I0(letra21_carry__1_i_9_2[0]),
        .I1(letra21_carry__2_i_1_0[2]),
        .O(letra21_carry__2_i_1_7[0]));
  LUT1 #(
    .INIT(2'h1)) 
    letra21__37_carry__2_i_1
       (.I0(letra21_carry__1_i_9_1),
        .O(letra21_carry__2_i_1_4));
  LUT5 #(
    .INIT(32'hFF87C300)) 
    letra21__37_carry_i_1
       (.I0(g0_b0__1_i_14_0[0]),
        .I1(g0_b0__1_i_14_0[3]),
        .I2(g0_b0__1_i_14_0[1]),
        .I3(Q[0]),
        .I4(letra21_carry__1_i_11_0),
        .O(letra21_carry__0_i_11_0[2]));
  LUT5 #(
    .INIT(32'h96699699)) 
    letra21__37_carry_i_2
       (.I0(letra21_carry__1_i_11_0),
        .I1(g0_b0__1_i_14_0[1]),
        .I2(g0_b0__1_i_14_0[3]),
        .I3(Q[0]),
        .I4(g0_b0__1_i_14_0[0]),
        .O(letra21_carry__0_i_11_0[1]));
  (* HLUTNM = "lutpair0" *) 
  LUT3 #(
    .INIT(8'h6F)) 
    letra21__37_carry_i_3
       (.I0(g0_b0__1_i_14_0[3]),
        .I1(g0_b0__1_i_14_0[2]),
        .I2(Q[0]),
        .O(letra21_carry__0_i_11_0[0]));
  LUT6 #(
    .INIT(64'h6996969696696969)) 
    letra21__37_carry_i_4
       (.I0(letra21_carry__0_i_11_0[2]),
        .I1(letra21_carry__1_i_10_1),
        .I2(g0_b0__1_i_14_0[0]),
        .I3(g0_b0__1_i_14_0[3]),
        .I4(Q[0]),
        .I5(\salida_reg[0]_0 ),
        .O(letra21_carry__0_i_12_3[3]));
  LUT6 #(
    .INIT(64'h6669969696699969)) 
    letra21__37_carry_i_5
       (.I0(g0_b0__1_i_14_0[1]),
        .I1(letra21_carry__1_i_11_0),
        .I2(Q[0]),
        .I3(g0_b0__1_i_14_0[3]),
        .I4(g0_b0__1_i_14_0[0]),
        .I5(letra21_carry_i_5_1),
        .O(letra21_carry__0_i_12_3[2]));
  LUT5 #(
    .INIT(32'h956A6A95)) 
    letra21__37_carry_i_6
       (.I0(letra21_carry__0_i_11_0[0]),
        .I1(Q[0]),
        .I2(g0_b0__1_i_14_0[3]),
        .I3(g0_b0__1_i_14_0[0]),
        .I4(letra21_carry_i_5_1),
        .O(letra21_carry__0_i_12_3[1]));
  LUT5 #(
    .INIT(32'hF00F1FE0)) 
    letra21__37_carry_i_7
       (.I0(g0_b0__1_i_14_0[0]),
        .I1(g0_b0__1_i_14_0[1]),
        .I2(g0_b0__1_i_14_0[3]),
        .I3(g0_b0__1_i_14_0[2]),
        .I4(Q[0]),
        .O(letra21_carry__0_i_12_3[0]));
  LUT4 #(
    .INIT(16'hB44B)) 
    letra21__420_carry__0_i_7
       (.I0(letra21_carry__1_i_11_0),
        .I1(letra21__420_carry__0[2]),
        .I2(letra21_carry__1_i_10_1),
        .I3(letra21__420_carry__1[0]),
        .O(letra21__336_carry__0[1]));
  LUT4 #(
    .INIT(16'hB44B)) 
    letra21__420_carry__0_i_8
       (.I0(letra21_carry_i_5_1),
        .I1(letra21__420_carry__0[1]),
        .I2(letra21_carry__1_i_11_0),
        .I3(letra21__420_carry__0[2]),
        .O(letra21__336_carry__0[0]));
  LUT4 #(
    .INIT(16'hB44B)) 
    letra21__420_carry__1_i_6
       (.I0(g0_b0__1_i_13_0),
        .I1(letra21__420_carry__1[1]),
        .I2(letra21_carry__1_i_9_1),
        .I3(letra21__420_carry__1[2]),
        .O(letra21__336_carry__0_0));
  LUT4 #(
    .INIT(16'h2DD2)) 
    letra21__420_carry_i_4
       (.I0(\salida_reg[0]_0 ),
        .I1(letra21__420_carry__0[0]),
        .I2(letra21_carry_i_5_1),
        .I3(letra21__420_carry__0[1]),
        .O(letra21__336_carry[1]));
  LUT4 #(
    .INIT(16'h956A)) 
    letra21__420_carry_i_7
       (.I0(g0_b0__1_i_14_0[0]),
        .I1(g0_b0__1_i_14_0[3]),
        .I2(Q[0]),
        .I3(letra21__420_carry),
        .O(letra21__336_carry[0]));
  LUT4 #(
    .INIT(16'h9669)) 
    letra21__74_carry__0_i_1
       (.I0(letra21_carry__1_i_11_0),
        .I1(letra21_carry__1_i_10_0),
        .I2(\salida_reg[0]_0 ),
        .I3(letra21_carry_i_5_0[3]),
        .O(letra21_carry__0_i_1_0[3]));
  LUT4 #(
    .INIT(16'h9669)) 
    letra21__74_carry__0_i_2
       (.I0(letra21_carry_i_5_0[2]),
        .I1(letra21_carry__1_i_10_1),
        .I2(letra21_carry_i_5_1),
        .I3(letra21_carry_i_5_0[0]),
        .O(letra21_carry__0_i_1_0[2]));
  LUT6 #(
    .INIT(64'h966969965AA55AA5)) 
    letra21__74_carry__0_i_3
       (.I0(g0_b0__1_i_14_0[0]),
        .I1(g0_b0__1_i_14_0[3]),
        .I2(\salida_reg[0]_0 ),
        .I3(letra21_carry__1_i_11_0),
        .I4(letra21_carry_i_5_1),
        .I5(Q[0]),
        .O(letra21_carry__0_i_1_0[1]));
  LUT3 #(
    .INIT(8'h69)) 
    letra21__74_carry__0_i_4
       (.I0(letra21_carry_i_5_1),
        .I1(Q[0]),
        .I2(letra21_carry_i_5_0[0]),
        .O(letra21_carry__0_i_1_0[0]));
  LUT2 #(
    .INIT(4'h1)) 
    letra21__74_carry__1_i_1
       (.I0(letra21_carry__1_i_10_0),
        .I1(letra21_carry__1_i_9_1),
        .O(letra21_carry__2_i_1_1));
  LUT4 #(
    .INIT(16'hEEE1)) 
    letra21__74_carry__1_i_2
       (.I0(g0_b0__1_i_13_0),
        .I1(letra21_carry__1_i_10_1),
        .I2(letra21_carry__1_i_9_1),
        .I3(letra21_carry__1_i_10_0),
        .O(letra21__37_carry__1_i_1_1[3]));
  LUT6 #(
    .INIT(64'h965A5A565A5A69A5)) 
    letra21__74_carry__1_i_3
       (.I0(letra230_out[8]),
        .I1(g0_b0__1_i_14_0[3]),
        .I2(letra230_out[6]),
        .I3(letra21_carry__1_i_11_n_0),
        .I4(letra230_out[5]),
        .I5(letra230_out[7]),
        .O(letra21__37_carry__1_i_1_1[2]));
  LUT4 #(
    .INIT(16'h9AA9)) 
    letra21__74_carry__1_i_4
       (.I0(letra21_carry__2_i_1_0[1]),
        .I1(letra21_carry__1_i_9_1),
        .I2(letra21_carry__1_i_11_0),
        .I3(letra21_carry__1_i_10_0),
        .O(letra21__37_carry__1_i_1_1[1]));
  LUT4 #(
    .INIT(16'h9669)) 
    letra21__74_carry__1_i_5
       (.I0(letra21_carry__2_i_1_0[0]),
        .I1(g0_b0__1_i_13_0),
        .I2(letra21_carry_i_5_1),
        .I3(letra21_carry__1_i_10_1),
        .O(letra21__37_carry__1_i_1_1[0]));
  LUT1 #(
    .INIT(2'h1)) 
    letra21__74_carry__2_i_1
       (.I0(letra21_carry__1_i_9_1),
        .O(letra21_carry__2_i_1_5[2]));
  LUT2 #(
    .INIT(4'h9)) 
    letra21__74_carry__2_i_2
       (.I0(g0_b0__1_i_13_0),
        .I1(letra21_carry__1_i_9_1),
        .O(letra21_carry__2_i_1_5[1]));
  LUT3 #(
    .INIT(8'h1E)) 
    letra21__74_carry__2_i_3
       (.I0(letra21_carry__1_i_9_1),
        .I1(letra21_carry__1_i_10_0),
        .I2(letra21_carry__1_i_9_0),
        .O(letra21_carry__2_i_1_5[0]));
  LUT3 #(
    .INIT(8'h78)) 
    letra21__74_carry_i_1
       (.I0(Q[0]),
        .I1(g0_b0__1_i_14_0[3]),
        .I2(g0_b0__1_i_14_0[0]),
        .O(letra21_carry_i_5_7));
  (* HLUTNM = "lutpair8" *) 
  LUT5 #(
    .INIT(32'hF3080CF7)) 
    letra21__74_carry_i_2
       (.I0(g0_b0__1_i_14_0[1]),
        .I1(g0_b0__1_i_14_0[3]),
        .I2(Q[0]),
        .I3(g0_b0__1_i_14_0[0]),
        .I4(g0_b0__1_i_14_0[2]),
        .O(letra21_carry_i_5_2[2]));
  LUT4 #(
    .INIT(16'h2CD3)) 
    letra21__74_carry_i_3
       (.I0(g0_b0__1_i_14_0[0]),
        .I1(Q[0]),
        .I2(g0_b0__1_i_14_0[3]),
        .I3(g0_b0__1_i_14_0[1]),
        .O(letra21_carry_i_5_2[1]));
  LUT3 #(
    .INIT(8'h95)) 
    letra21__74_carry_i_4
       (.I0(g0_b0__1_i_14_0[0]),
        .I1(g0_b0__1_i_14_0[3]),
        .I2(Q[0]),
        .O(letra21_carry_i_5_2[0]));
  LUT6 #(
    .INIT(64'h2BB22BB22BB2B2B2)) 
    letra21_carry__0_i_1
       (.I0(letra21_carry_i_5_1),
        .I1(letra21_carry__1_i_10_1),
        .I2(g0_b0__1_i_14_0[1]),
        .I3(g0_b0__1_i_14_0[3]),
        .I4(Q[0]),
        .I5(g0_b0__1_i_14_0[0]),
        .O(letra21_carry_i_5_0[3]));
  LUT3 #(
    .INIT(8'h6A)) 
    letra21_carry__0_i_10
       (.I0(letra230_out[6]),
        .I1(g0_b0__1_i_14_0[3]),
        .I2(letra21_carry__1_i_10_n_0),
        .O(letra21_carry__1_i_10_1));
  LUT3 #(
    .INIT(8'h6A)) 
    letra21_carry__0_i_11
       (.I0(letra230_out[5]),
        .I1(g0_b0__1_i_14_0[3]),
        .I2(letra21_carry__1_i_11_n_0),
        .O(letra21_carry__1_i_11_0));
  (* SOFT_HLUTNM = "soft_lutpair18" *) 
  LUT5 #(
    .INIT(32'h6666666A)) 
    letra21_carry__0_i_12
       (.I0(g0_b0__1_i_14_0[2]),
        .I1(g0_b0__1_i_14_0[3]),
        .I2(g0_b0__1_i_14_0[1]),
        .I3(g0_b0__1_i_14_0[0]),
        .I4(Q[0]),
        .O(\salida_reg[0]_0 ));
  LUT6 #(
    .INIT(64'h307B307F48FC48F8)) 
    letra21_carry__0_i_2
       (.I0(Q[0]),
        .I1(g0_b0__1_i_14_0[3]),
        .I2(g0_b0__1_i_14_0[0]),
        .I3(letra21_carry__1_i_11_0),
        .I4(g0_b0__1_i_14_0[1]),
        .I5(g0_b0__1_i_14_0[2]),
        .O(letra21_carry_i_5_0[2]));
  LUT6 #(
    .INIT(64'h6666999996966999)) 
    letra21_carry__0_i_3
       (.I0(letra21_carry__1_i_11_0),
        .I1(g0_b0__1_i_14_0[2]),
        .I2(g0_b0__1_i_14_0[3]),
        .I3(g0_b0__1_i_14_0[1]),
        .I4(g0_b0__1_i_14_0[0]),
        .I5(Q[0]),
        .O(letra21_carry_i_5_0[1]));
  (* HLUTNM = "lutpair8" *) 
  LUT4 #(
    .INIT(16'h666A)) 
    letra21_carry__0_i_4
       (.I0(g0_b0__1_i_14_0[1]),
        .I1(g0_b0__1_i_14_0[3]),
        .I2(Q[0]),
        .I3(g0_b0__1_i_14_0[0]),
        .O(letra21_carry_i_5_0[0]));
  LUT4 #(
    .INIT(16'h9669)) 
    letra21_carry__0_i_5
       (.I0(letra21_carry_i_5_0[3]),
        .I1(letra21_carry__1_i_11_0),
        .I2(letra21_carry__1_i_10_0),
        .I3(\salida_reg[0]_0 ),
        .O(letra21_carry__0_i_12_2[3]));
  LUT4 #(
    .INIT(16'h9669)) 
    letra21_carry__0_i_6
       (.I0(letra21_carry_i_5_0[2]),
        .I1(letra21_carry__1_i_10_1),
        .I2(letra21_carry_i_5_1),
        .I3(letra21_carry_i_5_0[0]),
        .O(letra21_carry__0_i_12_2[2]));
  LUT6 #(
    .INIT(64'h966969965AA55AA5)) 
    letra21_carry__0_i_7
       (.I0(g0_b0__1_i_14_0[0]),
        .I1(g0_b0__1_i_14_0[3]),
        .I2(\salida_reg[0]_0 ),
        .I3(letra21_carry__1_i_11_0),
        .I4(letra21_carry_i_5_1),
        .I5(Q[0]),
        .O(letra21_carry__0_i_12_2[1]));
  LUT3 #(
    .INIT(8'h69)) 
    letra21_carry__0_i_8
       (.I0(letra21_carry_i_5_1),
        .I1(Q[0]),
        .I2(letra21_carry_i_5_0[0]),
        .O(letra21_carry__0_i_12_2[0]));
  LUT6 #(
    .INIT(64'h666666666666666A)) 
    letra21_carry__0_i_9
       (.I0(letra230_out[4]),
        .I1(g0_b0__1_i_14_0[3]),
        .I2(g0_b0__1_i_14_0[2]),
        .I3(Q[0]),
        .I4(g0_b0__1_i_14_0[0]),
        .I5(g0_b0__1_i_14_0[1]),
        .O(letra21_carry_i_5_1));
  LUT2 #(
    .INIT(4'h1)) 
    letra21_carry__1_i_1
       (.I0(letra21_carry__1_i_10_0),
        .I1(letra21_carry__1_i_9_1),
        .O(letra21_carry__2_i_1_0[3]));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    letra21_carry__1_i_10
       (.I0(letra230_out[5]),
        .I1(g0_b0__1_i_14_0[2]),
        .I2(Q[0]),
        .I3(g0_b0__1_i_14_0[0]),
        .I4(g0_b0__1_i_14_0[1]),
        .I5(letra230_out[4]),
        .O(letra21_carry__1_i_10_n_0));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    letra21_carry__1_i_11
       (.I0(letra230_out[4]),
        .I1(g0_b0__1_i_14_0[1]),
        .I2(g0_b0__1_i_14_0[0]),
        .I3(Q[0]),
        .I4(g0_b0__1_i_14_0[2]),
        .O(letra21_carry__1_i_11_n_0));
  LUT2 #(
    .INIT(4'hB)) 
    letra21_carry__1_i_12
       (.I0(Q[5]),
        .I1(CO),
        .O(letra21_carry__1_i_12_n_0));
  LUT5 #(
    .INIT(32'hC2F03D0F)) 
    letra21_carry__1_i_2
       (.I0(letra230_out[7]),
        .I1(letra21_carry__1_i_10_n_0),
        .I2(letra230_out[6]),
        .I3(g0_b0__1_i_14_0[3]),
        .I4(letra230_out[8]),
        .O(letra21_carry__2_i_1_0[2]));
  LUT6 #(
    .INIT(64'hEEEC7D7F28284444)) 
    letra21_carry__1_i_3
       (.I0(g0_b0__1_i_14_0[3]),
        .I1(letra230_out[6]),
        .I2(letra21_carry__1_i_10_n_0),
        .I3(letra230_out[7]),
        .I4(letra230_out[8]),
        .I5(letra21_carry_i_5_1),
        .O(letra21_carry__2_i_1_0[1]));
  LUT3 #(
    .INIT(8'hB2)) 
    letra21_carry__1_i_4
       (.I0(letra21_carry__1_i_11_0),
        .I1(letra21_carry__1_i_10_0),
        .I2(\salida_reg[0]_0 ),
        .O(letra21_carry__2_i_1_0[0]));
  LUT4 #(
    .INIT(16'hEEE1)) 
    letra21_carry__1_i_5
       (.I0(g0_b0__1_i_13_0),
        .I1(letra21_carry__1_i_10_1),
        .I2(letra21_carry__1_i_9_1),
        .I3(letra21_carry__1_i_10_0),
        .O(letra21__37_carry__1_i_1_2[3]));
  LUT6 #(
    .INIT(64'h965A5A565A5A69A5)) 
    letra21_carry__1_i_6
       (.I0(letra230_out[8]),
        .I1(g0_b0__1_i_14_0[3]),
        .I2(letra230_out[6]),
        .I3(letra21_carry__1_i_11_n_0),
        .I4(letra230_out[5]),
        .I5(letra230_out[7]),
        .O(letra21__37_carry__1_i_1_2[2]));
  LUT4 #(
    .INIT(16'h9AA9)) 
    letra21_carry__1_i_7
       (.I0(letra21_carry__2_i_1_0[1]),
        .I1(letra21_carry__1_i_9_1),
        .I2(letra21_carry__1_i_11_0),
        .I3(letra21_carry__1_i_10_0),
        .O(letra21__37_carry__1_i_1_2[1]));
  LUT4 #(
    .INIT(16'h9669)) 
    letra21_carry__1_i_8
       (.I0(letra21_carry__2_i_1_0[0]),
        .I1(g0_b0__1_i_13_0),
        .I2(letra21_carry_i_5_1),
        .I3(letra21_carry__1_i_10_1),
        .O(letra21__37_carry__1_i_1_2[0]));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 letra21_carry__1_i_9
       (.CI(letra21_carry_i_5_n_0),
        .CO({letra21_carry__1_i_9_n_0,NLW_letra21_carry__1_i_9_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,Q[5],fsm_BCD_decoder[6],Q[4]}),
        .O(letra230_out[8:5]),
        .S({letra21_carry__1_i_12_n_0,letra21__74_carry__1_i_3_0[1],fsm_BCD_decoder[6],letra21__74_carry__1_i_3_0[0]}));
  LUT5 #(
    .INIT(32'h00000100)) 
    letra21_carry__2_i_1
       (.I0(letra230_out[6]),
        .I1(letra21_carry__1_i_10_n_0),
        .I2(letra230_out[7]),
        .I3(g0_b0__1_i_14_0[3]),
        .I4(letra230_out[8]),
        .O(letra21_carry__1_i_9_1));
  LUT5 #(
    .INIT(32'h5556AAAA)) 
    letra21_carry__2_i_2
       (.I0(letra230_out[8]),
        .I1(letra230_out[7]),
        .I2(letra21_carry__1_i_10_n_0),
        .I3(letra230_out[6]),
        .I4(g0_b0__1_i_14_0[3]),
        .O(g0_b0__1_i_13_0));
  LUT5 #(
    .INIT(32'h99999995)) 
    letra21_carry__2_i_3
       (.I0(letra230_out[8]),
        .I1(g0_b0__1_i_14_0[3]),
        .I2(letra230_out[6]),
        .I3(letra21_carry__1_i_10_n_0),
        .I4(letra230_out[7]),
        .O(letra21_carry__1_i_9_0));
  LUT1 #(
    .INIT(2'h1)) 
    letra21_carry__2_i_4
       (.I0(letra21_carry__1_i_9_1),
        .O(letra21_carry__2_i_1_3[2]));
  LUT2 #(
    .INIT(4'h9)) 
    letra21_carry__2_i_5
       (.I0(g0_b0__1_i_13_0),
        .I1(letra21_carry__1_i_9_1),
        .O(letra21_carry__2_i_1_3[1]));
  LUT3 #(
    .INIT(8'h1E)) 
    letra21_carry__2_i_6
       (.I0(letra21_carry__1_i_9_1),
        .I1(letra21_carry__1_i_10_0),
        .I2(letra21_carry__1_i_9_0),
        .O(letra21_carry__2_i_1_3[0]));
  LUT3 #(
    .INIT(8'h78)) 
    letra21_carry_i_1
       (.I0(Q[0]),
        .I1(g0_b0__1_i_14_0[3]),
        .I2(g0_b0__1_i_14_0[0]),
        .O(letra21_carry_i_5_6));
  LUT5 #(
    .INIT(32'h98CC6733)) 
    letra21_carry_i_2
       (.I0(Q[0]),
        .I1(g0_b0__1_i_14_0[0]),
        .I2(g0_b0__1_i_14_0[1]),
        .I3(g0_b0__1_i_14_0[3]),
        .I4(g0_b0__1_i_14_0[2]),
        .O(letra21_carry_i_5_5[2]));
  LUT4 #(
    .INIT(16'h2CD3)) 
    letra21_carry_i_3
       (.I0(g0_b0__1_i_14_0[0]),
        .I1(Q[0]),
        .I2(g0_b0__1_i_14_0[3]),
        .I3(g0_b0__1_i_14_0[1]),
        .O(letra21_carry_i_5_5[1]));
  LUT3 #(
    .INIT(8'h95)) 
    letra21_carry_i_4
       (.I0(g0_b0__1_i_14_0[0]),
        .I1(g0_b0__1_i_14_0[3]),
        .I2(Q[0]),
        .O(letra21_carry_i_5_5[0]));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "PROPCONST SWEEP" *) 
  CARRY4 letra21_carry_i_5
       (.CI(1'b0),
        .CO({letra21_carry_i_5_n_0,NLW_letra21_carry_i_5_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({Q[3:1],1'b0}),
        .O({letra230_out[4],g0_b0__1_i_14_0[2:0]}),
        .S({letra21__208_carry_i_3,letra21_carry_i_8_n_0,fsm_BCD_decoder[1]}));
  LUT3 #(
    .INIT(8'h69)) 
    letra21_carry_i_8
       (.I0(Q[1]),
        .I1(CO),
        .I2(Q[5]),
        .O(letra21_carry_i_8_n_0));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \salida_reg[0] 
       (.CLR(1'b0),
        .D(\salida_reg[0]_i_1_n_0 ),
        .G(\salida_reg[7]_i_2_n_0 ),
        .GE(1'b1),
        .Q(Q[0]));
  LUT6 #(
    .INIT(64'h00000000F1FFF100)) 
    \salida_reg[0]_i_1 
       (.I0(\bebida_reg[7]_i_4_n_0 ),
        .I1(state[0]),
        .I2(bebida[0]),
        .I3(state[1]),
        .I4(out[0]),
        .I5(\FSM_sequential_state_reg[2]_0 ),
        .O(\salida_reg[0]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \salida_reg[1] 
       (.CLR(1'b0),
        .D(\salida_reg[1]_i_1_n_0 ),
        .G(\salida_reg[7]_i_2_n_0 ),
        .GE(1'b1),
        .Q(fsm_BCD_decoder[1]));
  LUT6 #(
    .INIT(64'h0C0A0C0A0C0A000A)) 
    \salida_reg[1]_i_1 
       (.I0(out[1]),
        .I1(bebida[1]),
        .I2(\FSM_sequential_state_reg[2]_0 ),
        .I3(state[1]),
        .I4(state[0]),
        .I5(\bebida_reg[7]_i_4_n_0 ),
        .O(\salida_reg[1]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \salida_reg[2] 
       (.CLR(1'b0),
        .D(\salida_reg[2]_i_1_n_0 ),
        .G(\salida_reg[7]_i_2_n_0 ),
        .GE(1'b1),
        .Q(Q[1]));
  (* SOFT_HLUTNM = "soft_lutpair16" *) 
  LUT5 #(
    .INIT(32'h0F1F0F10)) 
    \salida_reg[2]_i_1 
       (.I0(state[0]),
        .I1(\bebida_reg[7]_i_4_n_0 ),
        .I2(state[1]),
        .I3(\FSM_sequential_state_reg[2]_0 ),
        .I4(out[2]),
        .O(\salida_reg[2]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \salida_reg[3] 
       (.CLR(1'b0),
        .D(\salida_reg[3]_i_1_n_0 ),
        .G(\salida_reg[7]_i_2_n_0 ),
        .GE(1'b1),
        .Q(Q[2]));
  LUT6 #(
    .INIT(64'h00FFF1FF00FFF100)) 
    \salida_reg[3]_i_1 
       (.I0(\bebida_reg[7]_i_4_n_0 ),
        .I1(state[0]),
        .I2(bebida[7]),
        .I3(state[1]),
        .I4(\FSM_sequential_state_reg[2]_0 ),
        .I5(out[3]),
        .O(\salida_reg[3]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \salida_reg[4] 
       (.CLR(1'b0),
        .D(\salida_reg[4]_i_1_n_0 ),
        .G(\salida_reg[7]_i_2_n_0 ),
        .GE(1'b1),
        .Q(Q[3]));
  (* SOFT_HLUTNM = "soft_lutpair35" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \salida_reg[4]_i_1 
       (.I0(out[4]),
        .I1(\FSM_sequential_state_reg[2]_0 ),
        .I2(state[1]),
        .O(\salida_reg[4]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \salida_reg[5] 
       (.CLR(1'b0),
        .D(\salida_reg[5]_i_1_n_0 ),
        .G(\salida_reg[7]_i_2_n_0 ),
        .GE(1'b1),
        .Q(Q[4]));
  (* SOFT_HLUTNM = "soft_lutpair36" *) 
  LUT3 #(
    .INIT(8'h02)) 
    \salida_reg[5]_i_1 
       (.I0(out[5]),
        .I1(\FSM_sequential_state_reg[2]_0 ),
        .I2(state[1]),
        .O(\salida_reg[5]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \salida_reg[6] 
       (.CLR(1'b0),
        .D(\salida_reg[6]_i_1_n_0 ),
        .G(\salida_reg[7]_i_2_n_0 ),
        .GE(1'b1),
        .Q(fsm_BCD_decoder[6]));
  LUT6 #(
    .INIT(64'h00FFF1FF00FFF100)) 
    \salida_reg[6]_i_1 
       (.I0(\bebida_reg[7]_i_4_n_0 ),
        .I1(state[0]),
        .I2(bebida[7]),
        .I3(state[1]),
        .I4(\FSM_sequential_state_reg[2]_0 ),
        .I5(out[6]),
        .O(\salida_reg[6]_i_1_n_0 ));
  (* XILINX_LEGACY_PRIM = "LD" *) 
  LDCE #(
    .INIT(1'b0)) 
    \salida_reg[7] 
       (.CLR(1'b0),
        .D(\salida_reg[7]_i_1_n_0 ),
        .G(\salida_reg[7]_i_2_n_0 ),
        .GE(1'b1),
        .Q(Q[5]));
  LUT6 #(
    .INIT(64'h00FFF1FF00FFF100)) 
    \salida_reg[7]_i_1 
       (.I0(\bebida_reg[7]_i_4_n_0 ),
        .I1(state[0]),
        .I2(bebida[7]),
        .I3(state[1]),
        .I4(\FSM_sequential_state_reg[2]_0 ),
        .I5(out[7]),
        .O(\salida_reg[7]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair13" *) 
  LUT5 #(
    .INIT(32'h03F007FF)) 
    \salida_reg[7]_i_2 
       (.I0(\bebida_reg[7]_i_4_n_0 ),
        .I1(\salida_reg[7]_i_3_n_0 ),
        .I2(\FSM_sequential_state_reg[2]_0 ),
        .I3(state[1]),
        .I4(state[0]),
        .O(\salida_reg[7]_i_2_n_0 ));
  LUT3 #(
    .INIT(8'h01)) 
    \salida_reg[7]_i_3 
       (.I0(bebida[7]),
        .I1(bebida[1]),
        .I2(bebida[0]),
        .O(\salida_reg[7]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'hE)) 
    \ticks[0]_i_3 
       (.I0(bebida_lista),
        .I1(ticks_reg[4]),
        .O(\ticks_reg[12] ));
  LUT2 #(
    .INIT(4'hE)) 
    \ticks[0]_i_5 
       (.I0(bebida_lista),
        .I1(ticks_reg[0]),
        .O(\ticks_reg[3] ));
  LUT2 #(
    .INIT(4'hE)) 
    \ticks[12]_i_5 
       (.I0(bebida_lista),
        .I1(ticks_reg[4]),
        .O(\ticks_reg[12]_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \ticks[28]_i_5 
       (.I0(bebida_lista),
        .I1(ticks_reg[6]),
        .O(S));
  LUT2 #(
    .INIT(4'hE)) 
    \ticks[4]_i_2 
       (.I0(bebida_lista),
        .I1(ticks_reg[1]),
        .O(\ticks_reg[7] ));
  LUT2 #(
    .INIT(4'hE)) 
    \ticks[8]_i_4 
       (.I0(bebida_lista),
        .I1(ticks_reg[3]),
        .O(DI[1]));
  LUT2 #(
    .INIT(4'hE)) 
    \ticks[8]_i_5 
       (.I0(bebida_lista),
        .I1(ticks_reg[2]),
        .O(DI[0]));
endmodule

module SYNCHRNZR
   (\sreg_reg[0]_0 ,
    CLK_P_BUFG,
    monedas_IBUF);
  output \sreg_reg[0]_0 ;
  input CLK_P_BUFG;
  input [0:0]monedas_IBUF;

  wire CLK_P_BUFG;
  wire [0:0]monedas_IBUF;
  wire \sreg_reg[0]_0 ;
  wire \sreg_reg_n_0_[0] ;

  (* srl_bus_name = "\synchronizers " *) 
  (* srl_name = "\synchronizers[0].inst_synchronizer_i/SYNC_OUT_reg_srl2 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    SYNC_OUT_reg_srl2
       (.A0(1'b1),
        .A1(1'b0),
        .A2(1'b0),
        .A3(1'b0),
        .CE(1'b1),
        .CLK(CLK_P_BUFG),
        .D(\sreg_reg_n_0_[0] ),
        .Q(\sreg_reg[0]_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \sreg_reg[0] 
       (.C(CLK_P_BUFG),
        .CE(1'b1),
        .D(monedas_IBUF),
        .Q(\sreg_reg_n_0_[0] ),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "SYNCHRNZR" *) 
module SYNCHRNZR_1
   (\sreg_reg[0]_0 ,
    CLK_P_BUFG,
    monedas_IBUF);
  output \sreg_reg[0]_0 ;
  input CLK_P_BUFG;
  input [0:0]monedas_IBUF;

  wire CLK_P_BUFG;
  wire [0:0]monedas_IBUF;
  wire \sreg_reg[0]_0 ;
  wire \sreg_reg_n_0_[0] ;

  (* srl_bus_name = "\synchronizers " *) 
  (* srl_name = "\synchronizers[1].inst_synchronizer_i/SYNC_OUT_reg_srl2 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    SYNC_OUT_reg_srl2
       (.A0(1'b1),
        .A1(1'b0),
        .A2(1'b0),
        .A3(1'b0),
        .CE(1'b1),
        .CLK(CLK_P_BUFG),
        .D(\sreg_reg_n_0_[0] ),
        .Q(\sreg_reg[0]_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \sreg_reg[0] 
       (.C(CLK_P_BUFG),
        .CE(1'b1),
        .D(monedas_IBUF),
        .Q(\sreg_reg_n_0_[0] ),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "SYNCHRNZR" *) 
module SYNCHRNZR_3
   (\sreg_reg[0]_0 ,
    CLK_P_BUFG,
    monedas_IBUF);
  output \sreg_reg[0]_0 ;
  input CLK_P_BUFG;
  input [0:0]monedas_IBUF;

  wire CLK_P_BUFG;
  wire [0:0]monedas_IBUF;
  wire \sreg_reg[0]_0 ;
  wire \sreg_reg_n_0_[0] ;

  (* srl_bus_name = "\synchronizers " *) 
  (* srl_name = "\synchronizers[2].inst_synchronizer_i/SYNC_OUT_reg_srl2 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    SYNC_OUT_reg_srl2
       (.A0(1'b1),
        .A1(1'b0),
        .A2(1'b0),
        .A3(1'b0),
        .CE(1'b1),
        .CLK(CLK_P_BUFG),
        .D(\sreg_reg_n_0_[0] ),
        .Q(\sreg_reg[0]_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \sreg_reg[0] 
       (.C(CLK_P_BUFG),
        .CE(1'b1),
        .D(monedas_IBUF),
        .Q(\sreg_reg_n_0_[0] ),
        .R(1'b0));
endmodule

(* ORIG_REF_NAME = "SYNCHRNZR" *) 
module SYNCHRNZR_5
   (\sreg_reg[0]_0 ,
    CLK_P_BUFG,
    monedas_IBUF);
  output \sreg_reg[0]_0 ;
  input CLK_P_BUFG;
  input [0:0]monedas_IBUF;

  wire CLK_P_BUFG;
  wire [0:0]monedas_IBUF;
  wire \sreg_reg[0]_0 ;
  wire \sreg_reg_n_0_[0] ;

  (* srl_bus_name = "\synchronizers " *) 
  (* srl_name = "\synchronizers[3].inst_synchronizer_i/SYNC_OUT_reg_srl2 " *) 
  SRL16E #(
    .INIT(16'h0000)) 
    SYNC_OUT_reg_srl2
       (.A0(1'b1),
        .A1(1'b0),
        .A2(1'b0),
        .A3(1'b0),
        .CE(1'b1),
        .CLK(CLK_P_BUFG),
        .D(\sreg_reg_n_0_[0] ),
        .Q(\sreg_reg[0]_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \sreg_reg[0] 
       (.C(CLK_P_BUFG),
        .CE(1'b1),
        .D(monedas_IBUF),
        .Q(\sreg_reg_n_0_[0] ),
        .R(1'b0));
endmodule

module debouncer
   (Q2,
    Q1,
    Q3,
    Q3_reg_0,
    S,
    CLK_P_BUFG,
    Q1_reg_0,
    out);
  output Q2;
  output Q1;
  output Q3;
  output Q3_reg_0;
  output [0:0]S;
  input CLK_P_BUFG;
  input Q1_reg_0;
  input [0:0]out;

  wire CLK_P_BUFG;
  wire Q1;
  wire Q1_reg_0;
  wire Q2;
  wire Q3;
  wire Q3_reg_0;
  wire [0:0]S;
  wire [0:0]out;

  FDRE #(
    .INIT(1'b0)) 
    Q1_reg
       (.C(CLK_P_BUFG),
        .CE(1'b1),
        .D(Q1_reg_0),
        .Q(Q1),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    Q2_reg
       (.C(CLK_P_BUFG),
        .CE(1'b1),
        .D(Q1),
        .Q(Q2),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    Q3_reg
       (.C(CLK_P_BUFG),
        .CE(1'b1),
        .D(Q2),
        .Q(Q3),
        .R(1'b0));
  LUT3 #(
    .INIT(8'hBF)) 
    \cuenta[7]_i_5 
       (.I0(Q3),
        .I1(Q2),
        .I2(Q1),
        .O(Q3_reg_0));
  LUT4 #(
    .INIT(16'hAA6A)) 
    plusOp_carry_i_1
       (.I0(out),
        .I1(Q1),
        .I2(Q2),
        .I3(Q3),
        .O(S));
endmodule

(* ORIG_REF_NAME = "debouncer" *) 
module debouncer_0
   (Q2,
    Q1,
    Q3,
    Q1_reg_0,
    Q3_reg_0,
    CLK_P_BUFG,
    Q1_reg_1,
    Q1_0,
    Q2_1,
    Q3_2);
  output Q2;
  output Q1;
  output Q3;
  output Q1_reg_0;
  output Q3_reg_0;
  input CLK_P_BUFG;
  input Q1_reg_1;
  input Q1_0;
  input Q2_1;
  input Q3_2;

  wire CLK_P_BUFG;
  wire Q1;
  wire Q1_0;
  wire Q1_reg_0;
  wire Q1_reg_1;
  wire Q2;
  wire Q2_1;
  wire Q3;
  wire Q3_2;
  wire Q3_reg_0;

  FDRE #(
    .INIT(1'b0)) 
    Q1_reg
       (.C(CLK_P_BUFG),
        .CE(1'b1),
        .D(Q1_reg_1),
        .Q(Q1),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    Q2_reg
       (.C(CLK_P_BUFG),
        .CE(1'b1),
        .D(Q1),
        .Q(Q2),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    Q3_reg
       (.C(CLK_P_BUFG),
        .CE(1'b1),
        .D(Q2),
        .Q(Q3),
        .R(1'b0));
  LUT6 #(
    .INIT(64'h08080808FF080808)) 
    \cuenta[7]_i_4 
       (.I0(Q1),
        .I1(Q2),
        .I2(Q3),
        .I3(Q1_0),
        .I4(Q2_1),
        .I5(Q3_2),
        .O(Q1_reg_0));
  LUT3 #(
    .INIT(8'hBF)) 
    plusOp_carry_i_4
       (.I0(Q3),
        .I1(Q2),
        .I2(Q1),
        .O(Q3_reg_0));
endmodule

(* ORIG_REF_NAME = "debouncer" *) 
module debouncer_2
   (Q2,
    Q1,
    Q3,
    S,
    CLK_P_BUFG,
    Q1_reg_0,
    out,
    \cuenta_reg[3] ,
    \cuenta_reg[3]_0 );
  output Q2;
  output Q1;
  output Q3;
  output [0:0]S;
  input CLK_P_BUFG;
  input Q1_reg_0;
  input [0:0]out;
  input \cuenta_reg[3] ;
  input \cuenta_reg[3]_0 ;

  wire CLK_P_BUFG;
  wire Q1;
  wire Q1_reg_0;
  wire Q2;
  wire Q3;
  wire [0:0]S;
  wire \cuenta_reg[3] ;
  wire \cuenta_reg[3]_0 ;
  wire [0:0]out;

  FDRE #(
    .INIT(1'b0)) 
    Q1_reg
       (.C(CLK_P_BUFG),
        .CE(1'b1),
        .D(Q1_reg_0),
        .Q(Q1),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    Q2_reg
       (.C(CLK_P_BUFG),
        .CE(1'b1),
        .D(Q1),
        .Q(Q2),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    Q3_reg
       (.C(CLK_P_BUFG),
        .CE(1'b1),
        .D(Q2),
        .Q(Q3),
        .R(1'b0));
  LUT6 #(
    .INIT(64'hAAAA6AAA55555555)) 
    plusOp_carry_i_3
       (.I0(out),
        .I1(\cuenta_reg[3] ),
        .I2(Q1),
        .I3(Q2),
        .I4(Q3),
        .I5(\cuenta_reg[3]_0 ),
        .O(S));
endmodule

(* ORIG_REF_NAME = "debouncer" *) 
module debouncer_4
   (E,
    CLK_P_BUFG,
    Q1_reg_0,
    \cuenta_reg[7] ,
    \cuenta_reg[7]_0 );
  output [0:0]E;
  input CLK_P_BUFG;
  input Q1_reg_0;
  input \cuenta_reg[7] ;
  input \cuenta_reg[7]_0 ;

  wire CLK_P_BUFG;
  wire [0:0]E;
  wire Q1;
  wire Q1_reg_0;
  wire Q2;
  wire Q3;
  wire \cuenta_reg[7] ;
  wire \cuenta_reg[7]_0 ;

  FDRE #(
    .INIT(1'b0)) 
    Q1_reg
       (.C(CLK_P_BUFG),
        .CE(1'b1),
        .D(Q1_reg_0),
        .Q(Q1),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    Q2_reg
       (.C(CLK_P_BUFG),
        .CE(1'b1),
        .D(Q1),
        .Q(Q2),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    Q3_reg
       (.C(CLK_P_BUFG),
        .CE(1'b1),
        .D(Q2),
        .Q(Q3),
        .R(1'b0));
  LUT5 #(
    .INIT(32'hFF08FFFF)) 
    \cuenta[7]_i_2 
       (.I0(Q1),
        .I1(Q2),
        .I2(Q3),
        .I3(\cuenta_reg[7] ),
        .I4(\cuenta_reg[7]_0 ),
        .O(E));
endmodule

module display_exit
   (Q,
    display_number_OBUF,
    display_selection_OBUF,
    \display_number[7] ,
    \display_number[7]_0 ,
    \display_number_OBUF[7]_inst_i_1_0 ,
    \display_number_OBUF[7]_inst_i_1_1 ,
    CLK);
  output [2:0]Q;
  output [0:0]display_number_OBUF;
  output [7:0]display_selection_OBUF;
  input \display_number[7] ;
  input \display_number[7]_0 ;
  input \display_number_OBUF[7]_inst_i_1_0 ;
  input \display_number_OBUF[7]_inst_i_1_1 ;
  input CLK;

  wire CLK;
  wire [2:0]Q;
  wire \display[0]_i_1_n_0 ;
  wire \display[1]_i_1_n_0 ;
  wire \display[2]_i_1_n_0 ;
  wire \display_number[7] ;
  wire \display_number[7]_0 ;
  wire [0:0]display_number_OBUF;
  wire \display_number_OBUF[7]_inst_i_1_0 ;
  wire \display_number_OBUF[7]_inst_i_1_1 ;
  wire \display_number_OBUF[7]_inst_i_2_n_0 ;
  wire \display_number_OBUF[7]_inst_i_3_n_0 ;
  wire [7:0]display_selection_OBUF;

  LUT1 #(
    .INIT(2'h1)) 
    \display[0]_i_1 
       (.I0(Q[0]),
        .O(\display[0]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT2 #(
    .INIT(4'h6)) 
    \display[1]_i_1 
       (.I0(Q[1]),
        .I1(Q[0]),
        .O(\display[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'h78)) 
    \display[2]_i_1 
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(Q[2]),
        .O(\display[2]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hAFA0CFCFAFA0C0C0)) 
    \display_number_OBUF[7]_inst_i_1 
       (.I0(\display_number_OBUF[7]_inst_i_2_n_0 ),
        .I1(\display_number_OBUF[7]_inst_i_3_n_0 ),
        .I2(Q[2]),
        .I3(\display_number[7] ),
        .I4(Q[1]),
        .I5(\display_number[7]_0 ),
        .O(display_number_OBUF));
  MUXF7 \display_number_OBUF[7]_inst_i_2 
       (.I0(\display_number_OBUF[7]_inst_i_1_1 ),
        .I1(1'b1),
        .O(\display_number_OBUF[7]_inst_i_2_n_0 ),
        .S(Q[0]));
  MUXF7 \display_number_OBUF[7]_inst_i_3 
       (.I0(\display_number_OBUF[7]_inst_i_1_0 ),
        .I1(1'b1),
        .O(\display_number_OBUF[7]_inst_i_3_n_0 ),
        .S(Q[0]));
  FDRE #(
    .INIT(1'b0)) 
    \display_reg[0] 
       (.C(CLK),
        .CE(1'b1),
        .D(\display[0]_i_1_n_0 ),
        .Q(Q[0]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \display_reg[1] 
       (.C(CLK),
        .CE(1'b1),
        .D(\display[1]_i_1_n_0 ),
        .Q(Q[1]),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \display_reg[2] 
       (.C(CLK),
        .CE(1'b1),
        .D(\display[2]_i_1_n_0 ),
        .Q(Q[2]),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair8" *) 
  LUT3 #(
    .INIT(8'h7F)) 
    \display_selection_OBUF[0]_inst_i_1 
       (.I0(Q[2]),
        .I1(Q[0]),
        .I2(Q[1]),
        .O(display_selection_OBUF[0]));
  (* SOFT_HLUTNM = "soft_lutpair4" *) 
  LUT3 #(
    .INIT(8'hF7)) 
    \display_selection_OBUF[1]_inst_i_1 
       (.I0(Q[1]),
        .I1(Q[2]),
        .I2(Q[0]),
        .O(display_selection_OBUF[1]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'hF7)) 
    \display_selection_OBUF[2]_inst_i_1 
       (.I0(Q[2]),
        .I1(Q[0]),
        .I2(Q[1]),
        .O(display_selection_OBUF[2]));
  (* SOFT_HLUTNM = "soft_lutpair5" *) 
  LUT3 #(
    .INIT(8'hEF)) 
    \display_selection_OBUF[3]_inst_i_1 
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(Q[2]),
        .O(display_selection_OBUF[3]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hF7)) 
    \display_selection_OBUF[4]_inst_i_1 
       (.I0(Q[1]),
        .I1(Q[0]),
        .I2(Q[2]),
        .O(display_selection_OBUF[4]));
  (* SOFT_HLUTNM = "soft_lutpair6" *) 
  LUT3 #(
    .INIT(8'hEF)) 
    \display_selection_OBUF[5]_inst_i_1 
       (.I0(Q[2]),
        .I1(Q[0]),
        .I2(Q[1]),
        .O(display_selection_OBUF[5]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hEF)) 
    \display_selection_OBUF[6]_inst_i_1 
       (.I0(Q[1]),
        .I1(Q[2]),
        .I2(Q[0]),
        .O(display_selection_OBUF[6]));
  (* SOFT_HLUTNM = "soft_lutpair7" *) 
  LUT3 #(
    .INIT(8'hFE)) 
    \display_selection_OBUF[7]_inst_i_1 
       (.I0(Q[1]),
        .I1(Q[2]),
        .I2(Q[0]),
        .O(display_selection_OBUF[7]));
endmodule

module fdivider_200Hz
   (CLK,
    CLK_IBUF_BUFG,
    RESET_IBUF);
  output CLK;
  input CLK_IBUF_BUFG;
  input RESET_IBUF;

  wire CE_OUT_i_1_n_0;
  wire CE_OUT_i_2_n_0;
  wire CLK;
  wire CLK_IBUF_BUFG;
  wire RESET_IBUF;
  wire \count[1]_i_1_n_0 ;
  wire \count[3]_i_2_n_0 ;
  wire \count[5]_i_1_n_0 ;
  wire [7:0]count_reg;
  wire [7:0]p_0_in;

  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT5 #(
    .INIT(32'h00000001)) 
    CE_OUT_i_1
       (.I0(CE_OUT_i_2_n_0),
        .I1(count_reg[6]),
        .I2(count_reg[7]),
        .I3(count_reg[4]),
        .I4(count_reg[5]),
        .O(CE_OUT_i_1_n_0));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT4 #(
    .INIT(16'hFFFE)) 
    CE_OUT_i_2
       (.I0(count_reg[3]),
        .I1(count_reg[0]),
        .I2(count_reg[1]),
        .I3(count_reg[2]),
        .O(CE_OUT_i_2_n_0));
  FDCE #(
    .INIT(1'b0)) 
    CE_OUT_reg
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .CLR(RESET_IBUF),
        .D(CE_OUT_i_1_n_0),
        .Q(CLK));
  LUT1 #(
    .INIT(2'h1)) 
    \count[0]_i_1 
       (.I0(count_reg[0]),
        .O(p_0_in[0]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT2 #(
    .INIT(4'h9)) 
    \count[1]_i_1 
       (.I0(count_reg[0]),
        .I1(count_reg[1]),
        .O(\count[1]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair11" *) 
  LUT3 #(
    .INIT(8'hE1)) 
    \count[2]_i_1 
       (.I0(count_reg[0]),
        .I1(count_reg[1]),
        .I2(count_reg[2]),
        .O(p_0_in[2]));
  LUT6 #(
    .INIT(64'h0000FFFEFFFF0000)) 
    \count[3]_i_1 
       (.I0(count_reg[5]),
        .I1(count_reg[4]),
        .I2(count_reg[7]),
        .I3(count_reg[6]),
        .I4(count_reg[3]),
        .I5(\count[3]_i_2_n_0 ),
        .O(p_0_in[3]));
  (* SOFT_HLUTNM = "soft_lutpair12" *) 
  LUT3 #(
    .INIT(8'h01)) 
    \count[3]_i_2 
       (.I0(count_reg[2]),
        .I1(count_reg[1]),
        .I2(count_reg[0]),
        .O(\count[3]_i_2_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair10" *) 
  LUT5 #(
    .INIT(32'hFF0000FE)) 
    \count[4]_i_1 
       (.I0(count_reg[5]),
        .I1(count_reg[7]),
        .I2(count_reg[6]),
        .I3(CE_OUT_i_2_n_0),
        .I4(count_reg[4]),
        .O(p_0_in[4]));
  LUT5 #(
    .INIT(32'hFFF0000E)) 
    \count[5]_i_1 
       (.I0(count_reg[7]),
        .I1(count_reg[6]),
        .I2(count_reg[4]),
        .I3(CE_OUT_i_2_n_0),
        .I4(count_reg[5]),
        .O(\count[5]_i_1_n_0 ));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT4 #(
    .INIT(16'hAAA9)) 
    \count[6]_i_1 
       (.I0(count_reg[6]),
        .I1(count_reg[4]),
        .I2(CE_OUT_i_2_n_0),
        .I3(count_reg[5]),
        .O(p_0_in[6]));
  (* SOFT_HLUTNM = "soft_lutpair9" *) 
  LUT5 #(
    .INIT(32'hAAAAAAA9)) 
    \count[7]_i_1 
       (.I0(count_reg[7]),
        .I1(count_reg[6]),
        .I2(count_reg[5]),
        .I3(CE_OUT_i_2_n_0),
        .I4(count_reg[4]),
        .O(p_0_in[7]));
  FDPE #(
    .INIT(1'b1)) 
    \count_reg[0] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(p_0_in[0]),
        .PRE(RESET_IBUF),
        .Q(count_reg[0]));
  FDPE #(
    .INIT(1'b1)) 
    \count_reg[1] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(\count[1]_i_1_n_0 ),
        .PRE(RESET_IBUF),
        .Q(count_reg[1]));
  FDPE #(
    .INIT(1'b1)) 
    \count_reg[2] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(p_0_in[2]),
        .PRE(RESET_IBUF),
        .Q(count_reg[2]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[3] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .CLR(RESET_IBUF),
        .D(p_0_in[3]),
        .Q(count_reg[3]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[4] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .CLR(RESET_IBUF),
        .D(p_0_in[4]),
        .Q(count_reg[4]));
  FDCE #(
    .INIT(1'b0)) 
    \count_reg[5] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .CLR(RESET_IBUF),
        .D(\count[5]_i_1_n_0 ),
        .Q(count_reg[5]));
  FDPE #(
    .INIT(1'b1)) 
    \count_reg[6] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(p_0_in[6]),
        .PRE(RESET_IBUF),
        .Q(count_reg[6]));
  FDPE #(
    .INIT(1'b1)) 
    \count_reg[7] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(p_0_in[7]),
        .PRE(RESET_IBUF),
        .Q(count_reg[7]));
endmodule

module prescaler
   (CLK_P,
    CLK_IBUF_BUFG);
  output CLK_P;
  input CLK_IBUF_BUFG;

  wire CLK_IBUF_BUFG;
  wire CLK_P;
  wire clk_out_i;
  wire clk_out_i_i_1_n_0;
  wire [0:0]cuenta;
  wire \cuenta[0]_i_2_n_0 ;
  wire \cuenta[0]_i_3_n_0 ;
  wire \cuenta[0]_i_4_n_0 ;
  wire \cuenta[0]_i_5_n_0 ;
  wire \cuenta_reg_n_0_[0] ;
  wire \cuenta_reg_n_0_[10] ;
  wire \cuenta_reg_n_0_[11] ;
  wire \cuenta_reg_n_0_[12] ;
  wire \cuenta_reg_n_0_[13] ;
  wire \cuenta_reg_n_0_[14] ;
  wire \cuenta_reg_n_0_[15] ;
  wire \cuenta_reg_n_0_[1] ;
  wire \cuenta_reg_n_0_[2] ;
  wire \cuenta_reg_n_0_[3] ;
  wire \cuenta_reg_n_0_[4] ;
  wire \cuenta_reg_n_0_[5] ;
  wire \cuenta_reg_n_0_[6] ;
  wire \cuenta_reg_n_0_[7] ;
  wire \cuenta_reg_n_0_[8] ;
  wire \cuenta_reg_n_0_[9] ;
  wire plusOp_carry__0_n_0;
  wire plusOp_carry__0_n_4;
  wire plusOp_carry__0_n_5;
  wire plusOp_carry__0_n_6;
  wire plusOp_carry__0_n_7;
  wire plusOp_carry__1_n_0;
  wire plusOp_carry__1_n_4;
  wire plusOp_carry__1_n_5;
  wire plusOp_carry__1_n_6;
  wire plusOp_carry__1_n_7;
  wire plusOp_carry__2_n_5;
  wire plusOp_carry__2_n_6;
  wire plusOp_carry__2_n_7;
  wire plusOp_carry_n_0;
  wire plusOp_carry_n_4;
  wire plusOp_carry_n_5;
  wire plusOp_carry_n_6;
  wire plusOp_carry_n_7;
  wire [2:0]NLW_plusOp_carry_CO_UNCONNECTED;
  wire [2:0]NLW_plusOp_carry__0_CO_UNCONNECTED;
  wire [2:0]NLW_plusOp_carry__1_CO_UNCONNECTED;
  wire [3:0]NLW_plusOp_carry__2_CO_UNCONNECTED;
  wire [3:3]NLW_plusOp_carry__2_O_UNCONNECTED;

  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT3 #(
    .INIT(8'hE1)) 
    clk_out_i_i_1
       (.I0(\cuenta[0]_i_2_n_0 ),
        .I1(\cuenta_reg_n_0_[0] ),
        .I2(CLK_P),
        .O(clk_out_i_i_1_n_0));
  FDRE #(
    .INIT(1'b0)) 
    clk_out_i_reg
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(clk_out_i_i_1_n_0),
        .Q(CLK_P),
        .R(1'b0));
  (* SOFT_HLUTNM = "soft_lutpair41" *) 
  LUT2 #(
    .INIT(4'h2)) 
    \cuenta[0]_i_1 
       (.I0(\cuenta[0]_i_2_n_0 ),
        .I1(\cuenta_reg_n_0_[0] ),
        .O(cuenta));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFEFFF)) 
    \cuenta[0]_i_2 
       (.I0(\cuenta[0]_i_3_n_0 ),
        .I1(\cuenta[0]_i_4_n_0 ),
        .I2(\cuenta_reg_n_0_[15] ),
        .I3(\cuenta_reg_n_0_[14] ),
        .I4(\cuenta_reg_n_0_[1] ),
        .I5(\cuenta[0]_i_5_n_0 ),
        .O(\cuenta[0]_i_2_n_0 ));
  LUT4 #(
    .INIT(16'hDFFF)) 
    \cuenta[0]_i_3 
       (.I0(\cuenta_reg_n_0_[6] ),
        .I1(\cuenta_reg_n_0_[7] ),
        .I2(\cuenta_reg_n_0_[9] ),
        .I3(\cuenta_reg_n_0_[8] ),
        .O(\cuenta[0]_i_3_n_0 ));
  LUT4 #(
    .INIT(16'hFFEF)) 
    \cuenta[0]_i_4 
       (.I0(\cuenta_reg_n_0_[3] ),
        .I1(\cuenta_reg_n_0_[2] ),
        .I2(\cuenta_reg_n_0_[4] ),
        .I3(\cuenta_reg_n_0_[5] ),
        .O(\cuenta[0]_i_4_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \cuenta[0]_i_5 
       (.I0(\cuenta_reg_n_0_[11] ),
        .I1(\cuenta_reg_n_0_[10] ),
        .I2(\cuenta_reg_n_0_[13] ),
        .I3(\cuenta_reg_n_0_[12] ),
        .O(\cuenta[0]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \cuenta[15]_i_1 
       (.I0(\cuenta[0]_i_2_n_0 ),
        .I1(\cuenta_reg_n_0_[0] ),
        .O(clk_out_i));
  FDRE #(
    .INIT(1'b0)) 
    \cuenta_reg[0] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(cuenta),
        .Q(\cuenta_reg_n_0_[0] ),
        .R(1'b0));
  FDRE #(
    .INIT(1'b0)) 
    \cuenta_reg[10] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(plusOp_carry__1_n_6),
        .Q(\cuenta_reg_n_0_[10] ),
        .R(clk_out_i));
  FDRE #(
    .INIT(1'b0)) 
    \cuenta_reg[11] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(plusOp_carry__1_n_5),
        .Q(\cuenta_reg_n_0_[11] ),
        .R(clk_out_i));
  FDRE #(
    .INIT(1'b0)) 
    \cuenta_reg[12] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(plusOp_carry__1_n_4),
        .Q(\cuenta_reg_n_0_[12] ),
        .R(clk_out_i));
  FDRE #(
    .INIT(1'b0)) 
    \cuenta_reg[13] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(plusOp_carry__2_n_7),
        .Q(\cuenta_reg_n_0_[13] ),
        .R(clk_out_i));
  FDRE #(
    .INIT(1'b0)) 
    \cuenta_reg[14] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(plusOp_carry__2_n_6),
        .Q(\cuenta_reg_n_0_[14] ),
        .R(clk_out_i));
  FDRE #(
    .INIT(1'b0)) 
    \cuenta_reg[15] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(plusOp_carry__2_n_5),
        .Q(\cuenta_reg_n_0_[15] ),
        .R(clk_out_i));
  FDRE #(
    .INIT(1'b0)) 
    \cuenta_reg[1] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(plusOp_carry_n_7),
        .Q(\cuenta_reg_n_0_[1] ),
        .R(clk_out_i));
  FDRE #(
    .INIT(1'b0)) 
    \cuenta_reg[2] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(plusOp_carry_n_6),
        .Q(\cuenta_reg_n_0_[2] ),
        .R(clk_out_i));
  FDRE #(
    .INIT(1'b0)) 
    \cuenta_reg[3] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(plusOp_carry_n_5),
        .Q(\cuenta_reg_n_0_[3] ),
        .R(clk_out_i));
  FDRE #(
    .INIT(1'b0)) 
    \cuenta_reg[4] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(plusOp_carry_n_4),
        .Q(\cuenta_reg_n_0_[4] ),
        .R(clk_out_i));
  FDRE #(
    .INIT(1'b0)) 
    \cuenta_reg[5] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(plusOp_carry__0_n_7),
        .Q(\cuenta_reg_n_0_[5] ),
        .R(clk_out_i));
  FDRE #(
    .INIT(1'b0)) 
    \cuenta_reg[6] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(plusOp_carry__0_n_6),
        .Q(\cuenta_reg_n_0_[6] ),
        .R(clk_out_i));
  FDRE #(
    .INIT(1'b0)) 
    \cuenta_reg[7] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(plusOp_carry__0_n_5),
        .Q(\cuenta_reg_n_0_[7] ),
        .R(clk_out_i));
  FDRE #(
    .INIT(1'b0)) 
    \cuenta_reg[8] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(plusOp_carry__0_n_4),
        .Q(\cuenta_reg_n_0_[8] ),
        .R(clk_out_i));
  FDRE #(
    .INIT(1'b0)) 
    \cuenta_reg[9] 
       (.C(CLK_IBUF_BUFG),
        .CE(1'b1),
        .D(plusOp_carry__1_n_7),
        .Q(\cuenta_reg_n_0_[9] ),
        .R(clk_out_i));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 plusOp_carry
       (.CI(1'b0),
        .CO({plusOp_carry_n_0,NLW_plusOp_carry_CO_UNCONNECTED[2:0]}),
        .CYINIT(\cuenta_reg_n_0_[0] ),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({plusOp_carry_n_4,plusOp_carry_n_5,plusOp_carry_n_6,plusOp_carry_n_7}),
        .S({\cuenta_reg_n_0_[4] ,\cuenta_reg_n_0_[3] ,\cuenta_reg_n_0_[2] ,\cuenta_reg_n_0_[1] }));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 plusOp_carry__0
       (.CI(plusOp_carry_n_0),
        .CO({plusOp_carry__0_n_0,NLW_plusOp_carry__0_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({plusOp_carry__0_n_4,plusOp_carry__0_n_5,plusOp_carry__0_n_6,plusOp_carry__0_n_7}),
        .S({\cuenta_reg_n_0_[8] ,\cuenta_reg_n_0_[7] ,\cuenta_reg_n_0_[6] ,\cuenta_reg_n_0_[5] }));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 plusOp_carry__1
       (.CI(plusOp_carry__0_n_0),
        .CO({plusOp_carry__1_n_0,NLW_plusOp_carry__1_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({plusOp_carry__1_n_4,plusOp_carry__1_n_5,plusOp_carry__1_n_6,plusOp_carry__1_n_7}),
        .S({\cuenta_reg_n_0_[12] ,\cuenta_reg_n_0_[11] ,\cuenta_reg_n_0_[10] ,\cuenta_reg_n_0_[9] }));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 plusOp_carry__2
       (.CI(plusOp_carry__1_n_0),
        .CO(NLW_plusOp_carry__2_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,1'b0,1'b0,1'b0}),
        .O({NLW_plusOp_carry__2_O_UNCONNECTED[3],plusOp_carry__2_n_5,plusOp_carry__2_n_6,plusOp_carry__2_n_7}),
        .S({1'b0,\cuenta_reg_n_0_[15] ,\cuenta_reg_n_0_[14] ,\cuenta_reg_n_0_[13] }));
endmodule

module regis_dinero
   (out,
    \cuenta_reg[2]_0 ,
    \cuenta_reg[0]_0 ,
    \cuenta_reg[2]_1 ,
    S,
    RESET_IBUF,
    state,
    E,
    CLK_P_BUFG,
    \cuenta_reg[3]_0 ,
    Q1,
    Q2,
    Q3,
    \cuenta_reg[3]_1 ,
    \cuenta_reg[7]_0 ,
    Q3_0,
    Q2_1,
    Q1_2,
    Q3_3,
    Q2_4,
    Q1_5);
  output [7:0]out;
  output \cuenta_reg[2]_0 ;
  output \cuenta_reg[0]_0 ;
  output \cuenta_reg[2]_1 ;
  input [1:0]S;
  input RESET_IBUF;
  input [0:0]state;
  input [0:0]E;
  input CLK_P_BUFG;
  input \cuenta_reg[3]_0 ;
  input Q1;
  input Q2;
  input Q3;
  input \cuenta_reg[3]_1 ;
  input \cuenta_reg[7]_0 ;
  input Q3_0;
  input Q2_1;
  input Q1_2;
  input Q3_3;
  input Q2_4;
  input Q1_5;

  wire CLK_P_BUFG;
  wire [0:0]E;
  wire \FSM_sequential_state[2]_i_7_n_0 ;
  wire Q1;
  wire Q1_2;
  wire Q1_5;
  wire Q2;
  wire Q2_1;
  wire Q2_4;
  wire Q3;
  wire Q3_0;
  wire Q3_3;
  wire RESET_IBUF;
  wire [1:0]S;
  wire [7:0]cuenta;
  wire \cuenta[7]_i_1_n_0 ;
  wire \cuenta[7]_i_3_n_0 ;
  wire \cuenta_reg[0]_0 ;
  wire \cuenta_reg[2]_0 ;
  wire \cuenta_reg[2]_1 ;
  wire \cuenta_reg[3]_0 ;
  wire \cuenta_reg[3]_1 ;
  wire \cuenta_reg[7]_0 ;
  wire [7:0]out;
  wire plusOp_carry__0_i_1_n_0;
  wire plusOp_carry__0_i_2_n_0;
  wire plusOp_carry__0_i_3_n_0;
  wire plusOp_carry_i_2_n_0;
  wire plusOp_carry_n_0;
  wire [0:0]state;
  wire [2:0]NLW_plusOp_carry_CO_UNCONNECTED;
  wire [3:0]NLW_plusOp_carry__0_CO_UNCONNECTED;

  LUT6 #(
    .INIT(64'h0000000010000000)) 
    \FSM_sequential_state[2]_i_3 
       (.I0(\FSM_sequential_state[2]_i_7_n_0 ),
        .I1(out[0]),
        .I2(out[6]),
        .I3(out[5]),
        .I4(out[2]),
        .I5(state),
        .O(\cuenta_reg[0]_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \FSM_sequential_state[2]_i_7 
       (.I0(out[1]),
        .I1(out[3]),
        .I2(out[4]),
        .I3(out[7]),
        .O(\FSM_sequential_state[2]_i_7_n_0 ));
  LUT6 #(
    .INIT(64'h00FEFF000300FF00)) 
    \FSM_sequential_state[2]_i_8 
       (.I0(out[2]),
        .I1(out[3]),
        .I2(out[4]),
        .I3(out[7]),
        .I4(out[6]),
        .I5(out[5]),
        .O(\cuenta_reg[2]_0 ));
  LUT5 #(
    .INIT(32'h00000080)) 
    \bebida_reg[7]_i_3 
       (.I0(out[2]),
        .I1(out[5]),
        .I2(out[6]),
        .I3(out[0]),
        .I4(\FSM_sequential_state[2]_i_7_n_0 ),
        .O(\cuenta_reg[2]_1 ));
  LUT2 #(
    .INIT(4'hB)) 
    \cuenta[7]_i_1 
       (.I0(RESET_IBUF),
        .I1(\cuenta[7]_i_3_n_0 ),
        .O(\cuenta[7]_i_1_n_0 ));
  LUT6 #(
    .INIT(64'hF7F7F7F7F7F7F7FF)) 
    \cuenta[7]_i_3 
       (.I0(out[5]),
        .I1(out[6]),
        .I2(out[7]),
        .I3(out[4]),
        .I4(out[3]),
        .I5(out[2]),
        .O(\cuenta[7]_i_3_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \cuenta_reg[0] 
       (.C(CLK_P_BUFG),
        .CE(E),
        .D(cuenta[0]),
        .Q(out[0]),
        .R(\cuenta[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \cuenta_reg[1] 
       (.C(CLK_P_BUFG),
        .CE(E),
        .D(cuenta[1]),
        .Q(out[1]),
        .R(\cuenta[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \cuenta_reg[2] 
       (.C(CLK_P_BUFG),
        .CE(E),
        .D(cuenta[2]),
        .Q(out[2]),
        .R(\cuenta[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \cuenta_reg[3] 
       (.C(CLK_P_BUFG),
        .CE(E),
        .D(cuenta[3]),
        .Q(out[3]),
        .R(\cuenta[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \cuenta_reg[4] 
       (.C(CLK_P_BUFG),
        .CE(E),
        .D(cuenta[4]),
        .Q(out[4]),
        .R(\cuenta[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \cuenta_reg[5] 
       (.C(CLK_P_BUFG),
        .CE(E),
        .D(cuenta[5]),
        .Q(out[5]),
        .R(\cuenta[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \cuenta_reg[6] 
       (.C(CLK_P_BUFG),
        .CE(E),
        .D(cuenta[6]),
        .Q(out[6]),
        .R(\cuenta[7]_i_1_n_0 ));
  FDRE #(
    .INIT(1'b0)) 
    \cuenta_reg[7] 
       (.C(CLK_P_BUFG),
        .CE(E),
        .D(cuenta[7]),
        .Q(out[7]),
        .R(\cuenta[7]_i_1_n_0 ));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "PROPCONST SWEEP" *) 
  CARRY4 plusOp_carry
       (.CI(1'b0),
        .CO({plusOp_carry_n_0,NLW_plusOp_carry_CO_UNCONNECTED[2:0]}),
        .CYINIT(1'b0),
        .DI({out[3:1],1'b0}),
        .O(cuenta[3:0]),
        .S({S[1],plusOp_carry_i_2_n_0,S[0],out[0]}));
  (* ADDER_THRESHOLD = "35" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 plusOp_carry__0
       (.CI(plusOp_carry_n_0),
        .CO(NLW_plusOp_carry__0_CO_UNCONNECTED[3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,out[6:4]}),
        .O(cuenta[7:4]),
        .S({out[7],plusOp_carry__0_i_1_n_0,plusOp_carry__0_i_2_n_0,plusOp_carry__0_i_3_n_0}));
  LUT5 #(
    .INIT(32'h9A999999)) 
    plusOp_carry__0_i_1
       (.I0(out[6]),
        .I1(\cuenta_reg[7]_0 ),
        .I2(Q3_0),
        .I3(Q2_1),
        .I4(Q1_2),
        .O(plusOp_carry__0_i_1_n_0));
  LUT5 #(
    .INIT(32'h6555AAAA)) 
    plusOp_carry__0_i_2
       (.I0(out[5]),
        .I1(Q3_3),
        .I2(Q2_4),
        .I3(Q1_5),
        .I4(\cuenta_reg[3]_1 ),
        .O(plusOp_carry__0_i_2_n_0));
  LUT5 #(
    .INIT(32'h6A666666)) 
    plusOp_carry__0_i_3
       (.I0(out[4]),
        .I1(\cuenta_reg[7]_0 ),
        .I2(Q3_0),
        .I3(Q2_1),
        .I4(Q1_2),
        .O(plusOp_carry__0_i_3_n_0));
  LUT6 #(
    .INIT(64'h55559555AAAAAAAA)) 
    plusOp_carry_i_2
       (.I0(out[2]),
        .I1(\cuenta_reg[3]_0 ),
        .I2(Q1),
        .I3(Q2),
        .I4(Q3),
        .I5(\cuenta_reg[3]_1 ),
        .O(plusOp_carry_i_2_n_0));
endmodule

module timer
   (\ticks_reg[31]_0 ,
    \ticks_reg[24]_0 ,
    \ticks_reg[10]_0 ,
    CLK_P_BUFG,
    RESET_IBUF,
    \FSM_sequential_state_reg[2] ,
    \ticks_reg[0]_0 ,
    bebida_lista,
    \ticks_reg[3]_0 ,
    \ticks_reg[7]_0 ,
    DI,
    \ticks_reg[15]_0 ,
    S);
  output [6:0]\ticks_reg[31]_0 ;
  output \ticks_reg[24]_0 ;
  output \ticks_reg[10]_0 ;
  input CLK_P_BUFG;
  input RESET_IBUF;
  input \FSM_sequential_state_reg[2] ;
  input \ticks_reg[0]_0 ;
  input bebida_lista;
  input [0:0]\ticks_reg[3]_0 ;
  input [0:0]\ticks_reg[7]_0 ;
  input [1:0]DI;
  input [0:0]\ticks_reg[15]_0 ;
  input [0:0]S;

  wire CLK_P_BUFG;
  wire [1:0]DI;
  wire \FSM_sequential_state[2]_i_11_n_0 ;
  wire \FSM_sequential_state[2]_i_12_n_0 ;
  wire \FSM_sequential_state[2]_i_13_n_0 ;
  wire \FSM_sequential_state[2]_i_14_n_0 ;
  wire \FSM_sequential_state[2]_i_15_n_0 ;
  wire \FSM_sequential_state_reg[2] ;
  wire RESET_IBUF;
  wire [0:0]S;
  wire bebida_lista;
  wire \ticks[0]_i_10_n_0 ;
  wire \ticks[0]_i_11_n_0 ;
  wire \ticks[0]_i_12_n_0 ;
  wire \ticks[0]_i_1_n_0 ;
  wire \ticks[0]_i_4_n_0 ;
  wire \ticks[0]_i_6_n_0 ;
  wire \ticks[0]_i_7_n_0 ;
  wire \ticks[0]_i_8_n_0 ;
  wire \ticks[0]_i_9_n_0 ;
  wire \ticks[12]_i_2_n_0 ;
  wire \ticks[12]_i_3_n_0 ;
  wire \ticks[12]_i_4_n_0 ;
  wire \ticks[12]_i_6_n_0 ;
  wire \ticks[12]_i_7_n_0 ;
  wire \ticks[12]_i_8_n_0 ;
  wire \ticks[12]_i_9_n_0 ;
  wire \ticks[16]_i_2_n_0 ;
  wire \ticks[16]_i_3_n_0 ;
  wire \ticks[16]_i_4_n_0 ;
  wire \ticks[16]_i_5_n_0 ;
  wire \ticks[16]_i_6_n_0 ;
  wire \ticks[16]_i_7_n_0 ;
  wire \ticks[16]_i_8_n_0 ;
  wire \ticks[16]_i_9_n_0 ;
  wire \ticks[20]_i_2_n_0 ;
  wire \ticks[20]_i_3_n_0 ;
  wire \ticks[20]_i_4_n_0 ;
  wire \ticks[20]_i_5_n_0 ;
  wire \ticks[20]_i_6_n_0 ;
  wire \ticks[20]_i_7_n_0 ;
  wire \ticks[20]_i_8_n_0 ;
  wire \ticks[20]_i_9_n_0 ;
  wire \ticks[24]_i_2_n_0 ;
  wire \ticks[24]_i_3_n_0 ;
  wire \ticks[24]_i_4_n_0 ;
  wire \ticks[24]_i_5_n_0 ;
  wire \ticks[24]_i_6_n_0 ;
  wire \ticks[24]_i_7_n_0 ;
  wire \ticks[24]_i_8_n_0 ;
  wire \ticks[24]_i_9_n_0 ;
  wire \ticks[28]_i_2_n_0 ;
  wire \ticks[28]_i_3_n_0 ;
  wire \ticks[28]_i_4_n_0 ;
  wire \ticks[28]_i_6_n_0 ;
  wire \ticks[28]_i_7_n_0 ;
  wire \ticks[28]_i_8_n_0 ;
  wire \ticks[4]_i_3_n_0 ;
  wire \ticks[4]_i_4_n_0 ;
  wire \ticks[4]_i_5_n_0 ;
  wire \ticks[4]_i_6_n_0 ;
  wire \ticks[4]_i_7_n_0 ;
  wire \ticks[4]_i_8_n_0 ;
  wire \ticks[4]_i_9_n_0 ;
  wire \ticks[8]_i_2_n_0 ;
  wire \ticks[8]_i_3_n_0 ;
  wire \ticks[8]_i_6_n_0 ;
  wire \ticks[8]_i_7_n_0 ;
  wire \ticks[8]_i_8_n_0 ;
  wire \ticks[8]_i_9_n_0 ;
  wire [30:0]ticks_reg;
  wire \ticks_reg[0]_0 ;
  wire \ticks_reg[0]_i_2_n_0 ;
  wire \ticks_reg[0]_i_2_n_4 ;
  wire \ticks_reg[0]_i_2_n_5 ;
  wire \ticks_reg[0]_i_2_n_6 ;
  wire \ticks_reg[0]_i_2_n_7 ;
  wire \ticks_reg[10]_0 ;
  wire \ticks_reg[12]_i_1_n_0 ;
  wire \ticks_reg[12]_i_1_n_4 ;
  wire \ticks_reg[12]_i_1_n_5 ;
  wire \ticks_reg[12]_i_1_n_6 ;
  wire \ticks_reg[12]_i_1_n_7 ;
  wire [0:0]\ticks_reg[15]_0 ;
  wire \ticks_reg[16]_i_1_n_0 ;
  wire \ticks_reg[16]_i_1_n_4 ;
  wire \ticks_reg[16]_i_1_n_5 ;
  wire \ticks_reg[16]_i_1_n_6 ;
  wire \ticks_reg[16]_i_1_n_7 ;
  wire \ticks_reg[20]_i_1_n_0 ;
  wire \ticks_reg[20]_i_1_n_4 ;
  wire \ticks_reg[20]_i_1_n_5 ;
  wire \ticks_reg[20]_i_1_n_6 ;
  wire \ticks_reg[20]_i_1_n_7 ;
  wire \ticks_reg[24]_0 ;
  wire \ticks_reg[24]_i_1_n_0 ;
  wire \ticks_reg[24]_i_1_n_4 ;
  wire \ticks_reg[24]_i_1_n_5 ;
  wire \ticks_reg[24]_i_1_n_6 ;
  wire \ticks_reg[24]_i_1_n_7 ;
  wire \ticks_reg[28]_i_1_n_4 ;
  wire \ticks_reg[28]_i_1_n_5 ;
  wire \ticks_reg[28]_i_1_n_6 ;
  wire \ticks_reg[28]_i_1_n_7 ;
  wire [6:0]\ticks_reg[31]_0 ;
  wire [0:0]\ticks_reg[3]_0 ;
  wire \ticks_reg[4]_i_1_n_0 ;
  wire \ticks_reg[4]_i_1_n_4 ;
  wire \ticks_reg[4]_i_1_n_5 ;
  wire \ticks_reg[4]_i_1_n_6 ;
  wire \ticks_reg[4]_i_1_n_7 ;
  wire [0:0]\ticks_reg[7]_0 ;
  wire \ticks_reg[8]_i_1_n_0 ;
  wire \ticks_reg[8]_i_1_n_4 ;
  wire \ticks_reg[8]_i_1_n_5 ;
  wire \ticks_reg[8]_i_1_n_6 ;
  wire \ticks_reg[8]_i_1_n_7 ;
  wire [2:0]\NLW_ticks_reg[0]_i_2_CO_UNCONNECTED ;
  wire [2:0]\NLW_ticks_reg[12]_i_1_CO_UNCONNECTED ;
  wire [2:0]\NLW_ticks_reg[16]_i_1_CO_UNCONNECTED ;
  wire [2:0]\NLW_ticks_reg[20]_i_1_CO_UNCONNECTED ;
  wire [2:0]\NLW_ticks_reg[24]_i_1_CO_UNCONNECTED ;
  wire [3:0]\NLW_ticks_reg[28]_i_1_CO_UNCONNECTED ;
  wire [2:0]\NLW_ticks_reg[4]_i_1_CO_UNCONNECTED ;
  wire [2:0]\NLW_ticks_reg[8]_i_1_CO_UNCONNECTED ;

  LUT4 #(
    .INIT(16'hFFFE)) 
    \FSM_sequential_state[2]_i_11 
       (.I0(ticks_reg[6]),
        .I1(\ticks_reg[31]_0 [3]),
        .I2(ticks_reg[2]),
        .I3(\ticks_reg[31]_0 [0]),
        .O(\FSM_sequential_state[2]_i_11_n_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFE)) 
    \FSM_sequential_state[2]_i_12 
       (.I0(\FSM_sequential_state[2]_i_13_n_0 ),
        .I1(ticks_reg[4]),
        .I2(ticks_reg[5]),
        .I3(ticks_reg[0]),
        .I4(ticks_reg[1]),
        .I5(\FSM_sequential_state[2]_i_14_n_0 ),
        .O(\FSM_sequential_state[2]_i_12_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \FSM_sequential_state[2]_i_13 
       (.I0(ticks_reg[28]),
        .I1(ticks_reg[29]),
        .I2(ticks_reg[17]),
        .I3(ticks_reg[27]),
        .O(\FSM_sequential_state[2]_i_13_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \FSM_sequential_state[2]_i_14 
       (.I0(ticks_reg[23]),
        .I1(ticks_reg[19]),
        .I2(ticks_reg[21]),
        .I3(ticks_reg[18]),
        .I4(\FSM_sequential_state[2]_i_15_n_0 ),
        .O(\FSM_sequential_state[2]_i_14_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \FSM_sequential_state[2]_i_15 
       (.I0(\ticks_reg[31]_0 [1]),
        .I1(\ticks_reg[31]_0 [2]),
        .I2(ticks_reg[20]),
        .I3(ticks_reg[22]),
        .O(\FSM_sequential_state[2]_i_15_n_0 ));
  LUT5 #(
    .INIT(32'hFFFFFFFE)) 
    \FSM_sequential_state[2]_i_5 
       (.I0(\FSM_sequential_state_reg[2] ),
        .I1(\ticks[0]_i_4_n_0 ),
        .I2(ticks_reg[24]),
        .I3(ticks_reg[25]),
        .I4(\ticks_reg[31]_0 [5]),
        .O(\ticks_reg[24]_0 ));
  LUT6 #(
    .INIT(64'h0000000000000001)) 
    \FSM_sequential_state[2]_i_6 
       (.I0(\FSM_sequential_state[2]_i_11_n_0 ),
        .I1(ticks_reg[10]),
        .I2(ticks_reg[11]),
        .I3(ticks_reg[15]),
        .I4(ticks_reg[16]),
        .I5(\FSM_sequential_state[2]_i_12_n_0 ),
        .O(\ticks_reg[10]_0 ));
  LUT6 #(
    .INIT(64'hFFFFFFFFFFFFFFFD)) 
    \ticks[0]_i_1 
       (.I0(\ticks_reg[10]_0 ),
        .I1(\ticks_reg[0]_0 ),
        .I2(\ticks[0]_i_4_n_0 ),
        .I3(ticks_reg[24]),
        .I4(ticks_reg[25]),
        .I5(\ticks_reg[31]_0 [5]),
        .O(\ticks[0]_i_1_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \ticks[0]_i_10 
       (.I0(ticks_reg[2]),
        .I1(bebida_lista),
        .O(\ticks[0]_i_10_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \ticks[0]_i_11 
       (.I0(ticks_reg[1]),
        .I1(bebida_lista),
        .O(\ticks[0]_i_11_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \ticks[0]_i_12 
       (.I0(ticks_reg[0]),
        .I1(bebida_lista),
        .O(\ticks[0]_i_12_n_0 ));
  LUT4 #(
    .INIT(16'hFFFE)) 
    \ticks[0]_i_4 
       (.I0(ticks_reg[13]),
        .I1(ticks_reg[14]),
        .I2(ticks_reg[30]),
        .I3(\ticks_reg[31]_0 [6]),
        .O(\ticks[0]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \ticks[0]_i_6 
       (.I0(ticks_reg[2]),
        .I1(bebida_lista),
        .O(\ticks[0]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \ticks[0]_i_7 
       (.I0(ticks_reg[1]),
        .I1(bebida_lista),
        .O(\ticks[0]_i_7_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \ticks[0]_i_8 
       (.I0(ticks_reg[0]),
        .I1(bebida_lista),
        .O(\ticks[0]_i_8_n_0 ));
  LUT2 #(
    .INIT(4'hD)) 
    \ticks[0]_i_9 
       (.I0(\ticks_reg[31]_0 [0]),
        .I1(bebida_lista),
        .O(\ticks[0]_i_9_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \ticks[12]_i_2 
       (.I0(ticks_reg[15]),
        .I1(bebida_lista),
        .O(\ticks[12]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \ticks[12]_i_3 
       (.I0(ticks_reg[14]),
        .I1(bebida_lista),
        .O(\ticks[12]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \ticks[12]_i_4 
       (.I0(ticks_reg[13]),
        .I1(bebida_lista),
        .O(\ticks[12]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \ticks[12]_i_6 
       (.I0(ticks_reg[15]),
        .I1(bebida_lista),
        .O(\ticks[12]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \ticks[12]_i_7 
       (.I0(ticks_reg[14]),
        .I1(bebida_lista),
        .O(\ticks[12]_i_7_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \ticks[12]_i_8 
       (.I0(ticks_reg[13]),
        .I1(bebida_lista),
        .O(\ticks[12]_i_8_n_0 ));
  LUT2 #(
    .INIT(4'hD)) 
    \ticks[12]_i_9 
       (.I0(\ticks_reg[31]_0 [4]),
        .I1(bebida_lista),
        .O(\ticks[12]_i_9_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \ticks[16]_i_2 
       (.I0(ticks_reg[19]),
        .I1(bebida_lista),
        .O(\ticks[16]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \ticks[16]_i_3 
       (.I0(ticks_reg[18]),
        .I1(bebida_lista),
        .O(\ticks[16]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \ticks[16]_i_4 
       (.I0(ticks_reg[17]),
        .I1(bebida_lista),
        .O(\ticks[16]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \ticks[16]_i_5 
       (.I0(ticks_reg[16]),
        .I1(bebida_lista),
        .O(\ticks[16]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \ticks[16]_i_6 
       (.I0(ticks_reg[19]),
        .I1(bebida_lista),
        .O(\ticks[16]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \ticks[16]_i_7 
       (.I0(ticks_reg[18]),
        .I1(bebida_lista),
        .O(\ticks[16]_i_7_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \ticks[16]_i_8 
       (.I0(ticks_reg[17]),
        .I1(bebida_lista),
        .O(\ticks[16]_i_8_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \ticks[16]_i_9 
       (.I0(ticks_reg[16]),
        .I1(bebida_lista),
        .O(\ticks[16]_i_9_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \ticks[20]_i_2 
       (.I0(ticks_reg[23]),
        .I1(bebida_lista),
        .O(\ticks[20]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \ticks[20]_i_3 
       (.I0(ticks_reg[22]),
        .I1(bebida_lista),
        .O(\ticks[20]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \ticks[20]_i_4 
       (.I0(ticks_reg[21]),
        .I1(bebida_lista),
        .O(\ticks[20]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \ticks[20]_i_5 
       (.I0(ticks_reg[20]),
        .I1(bebida_lista),
        .O(\ticks[20]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \ticks[20]_i_6 
       (.I0(ticks_reg[23]),
        .I1(bebida_lista),
        .O(\ticks[20]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \ticks[20]_i_7 
       (.I0(ticks_reg[22]),
        .I1(bebida_lista),
        .O(\ticks[20]_i_7_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \ticks[20]_i_8 
       (.I0(ticks_reg[21]),
        .I1(bebida_lista),
        .O(\ticks[20]_i_8_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \ticks[20]_i_9 
       (.I0(ticks_reg[20]),
        .I1(bebida_lista),
        .O(\ticks[20]_i_9_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \ticks[24]_i_2 
       (.I0(ticks_reg[27]),
        .I1(bebida_lista),
        .O(\ticks[24]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \ticks[24]_i_3 
       (.I0(\ticks_reg[31]_0 [5]),
        .I1(bebida_lista),
        .O(\ticks[24]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \ticks[24]_i_4 
       (.I0(ticks_reg[25]),
        .I1(bebida_lista),
        .O(\ticks[24]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \ticks[24]_i_5 
       (.I0(ticks_reg[24]),
        .I1(bebida_lista),
        .O(\ticks[24]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \ticks[24]_i_6 
       (.I0(ticks_reg[27]),
        .I1(bebida_lista),
        .O(\ticks[24]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \ticks[24]_i_7 
       (.I0(\ticks_reg[31]_0 [5]),
        .I1(bebida_lista),
        .O(\ticks[24]_i_7_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \ticks[24]_i_8 
       (.I0(ticks_reg[25]),
        .I1(bebida_lista),
        .O(\ticks[24]_i_8_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \ticks[24]_i_9 
       (.I0(ticks_reg[24]),
        .I1(bebida_lista),
        .O(\ticks[24]_i_9_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \ticks[28]_i_2 
       (.I0(ticks_reg[30]),
        .I1(bebida_lista),
        .O(\ticks[28]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \ticks[28]_i_3 
       (.I0(ticks_reg[29]),
        .I1(bebida_lista),
        .O(\ticks[28]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \ticks[28]_i_4 
       (.I0(ticks_reg[28]),
        .I1(bebida_lista),
        .O(\ticks[28]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \ticks[28]_i_6 
       (.I0(ticks_reg[30]),
        .I1(bebida_lista),
        .O(\ticks[28]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \ticks[28]_i_7 
       (.I0(ticks_reg[29]),
        .I1(bebida_lista),
        .O(\ticks[28]_i_7_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \ticks[28]_i_8 
       (.I0(ticks_reg[28]),
        .I1(bebida_lista),
        .O(\ticks[28]_i_8_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \ticks[4]_i_3 
       (.I0(ticks_reg[6]),
        .I1(bebida_lista),
        .O(\ticks[4]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \ticks[4]_i_4 
       (.I0(ticks_reg[5]),
        .I1(bebida_lista),
        .O(\ticks[4]_i_4_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \ticks[4]_i_5 
       (.I0(ticks_reg[4]),
        .I1(bebida_lista),
        .O(\ticks[4]_i_5_n_0 ));
  LUT2 #(
    .INIT(4'hD)) 
    \ticks[4]_i_6 
       (.I0(\ticks_reg[31]_0 [1]),
        .I1(bebida_lista),
        .O(\ticks[4]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \ticks[4]_i_7 
       (.I0(ticks_reg[6]),
        .I1(bebida_lista),
        .O(\ticks[4]_i_7_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \ticks[4]_i_8 
       (.I0(ticks_reg[5]),
        .I1(bebida_lista),
        .O(\ticks[4]_i_8_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \ticks[4]_i_9 
       (.I0(ticks_reg[4]),
        .I1(bebida_lista),
        .O(\ticks[4]_i_9_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \ticks[8]_i_2 
       (.I0(ticks_reg[11]),
        .I1(bebida_lista),
        .O(\ticks[8]_i_2_n_0 ));
  LUT2 #(
    .INIT(4'h2)) 
    \ticks[8]_i_3 
       (.I0(ticks_reg[10]),
        .I1(bebida_lista),
        .O(\ticks[8]_i_3_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \ticks[8]_i_6 
       (.I0(ticks_reg[11]),
        .I1(bebida_lista),
        .O(\ticks[8]_i_6_n_0 ));
  LUT2 #(
    .INIT(4'h1)) 
    \ticks[8]_i_7 
       (.I0(ticks_reg[10]),
        .I1(bebida_lista),
        .O(\ticks[8]_i_7_n_0 ));
  LUT2 #(
    .INIT(4'hD)) 
    \ticks[8]_i_8 
       (.I0(\ticks_reg[31]_0 [3]),
        .I1(bebida_lista),
        .O(\ticks[8]_i_8_n_0 ));
  LUT2 #(
    .INIT(4'hD)) 
    \ticks[8]_i_9 
       (.I0(\ticks_reg[31]_0 [2]),
        .I1(bebida_lista),
        .O(\ticks[8]_i_9_n_0 ));
  FDCE #(
    .INIT(1'b0)) 
    \ticks_reg[0] 
       (.C(CLK_P_BUFG),
        .CE(\ticks[0]_i_1_n_0 ),
        .CLR(RESET_IBUF),
        .D(\ticks_reg[0]_i_2_n_7 ),
        .Q(ticks_reg[0]));
  (* ADDER_THRESHOLD = "11" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \ticks_reg[0]_i_2 
       (.CI(1'b0),
        .CO({\ticks_reg[0]_i_2_n_0 ,\NLW_ticks_reg[0]_i_2_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({\ticks_reg[3]_0 ,\ticks[0]_i_6_n_0 ,\ticks[0]_i_7_n_0 ,\ticks[0]_i_8_n_0 }),
        .O({\ticks_reg[0]_i_2_n_4 ,\ticks_reg[0]_i_2_n_5 ,\ticks_reg[0]_i_2_n_6 ,\ticks_reg[0]_i_2_n_7 }),
        .S({\ticks[0]_i_9_n_0 ,\ticks[0]_i_10_n_0 ,\ticks[0]_i_11_n_0 ,\ticks[0]_i_12_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \ticks_reg[10] 
       (.C(CLK_P_BUFG),
        .CE(\ticks[0]_i_1_n_0 ),
        .CLR(RESET_IBUF),
        .D(\ticks_reg[8]_i_1_n_5 ),
        .Q(ticks_reg[10]));
  FDCE #(
    .INIT(1'b0)) 
    \ticks_reg[11] 
       (.C(CLK_P_BUFG),
        .CE(\ticks[0]_i_1_n_0 ),
        .CLR(RESET_IBUF),
        .D(\ticks_reg[8]_i_1_n_4 ),
        .Q(ticks_reg[11]));
  FDCE #(
    .INIT(1'b0)) 
    \ticks_reg[12] 
       (.C(CLK_P_BUFG),
        .CE(\ticks[0]_i_1_n_0 ),
        .CLR(RESET_IBUF),
        .D(\ticks_reg[12]_i_1_n_7 ),
        .Q(\ticks_reg[31]_0 [4]));
  (* ADDER_THRESHOLD = "11" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \ticks_reg[12]_i_1 
       (.CI(\ticks_reg[8]_i_1_n_0 ),
        .CO({\ticks_reg[12]_i_1_n_0 ,\NLW_ticks_reg[12]_i_1_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({\ticks[12]_i_2_n_0 ,\ticks[12]_i_3_n_0 ,\ticks[12]_i_4_n_0 ,\ticks_reg[15]_0 }),
        .O({\ticks_reg[12]_i_1_n_4 ,\ticks_reg[12]_i_1_n_5 ,\ticks_reg[12]_i_1_n_6 ,\ticks_reg[12]_i_1_n_7 }),
        .S({\ticks[12]_i_6_n_0 ,\ticks[12]_i_7_n_0 ,\ticks[12]_i_8_n_0 ,\ticks[12]_i_9_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \ticks_reg[13] 
       (.C(CLK_P_BUFG),
        .CE(\ticks[0]_i_1_n_0 ),
        .CLR(RESET_IBUF),
        .D(\ticks_reg[12]_i_1_n_6 ),
        .Q(ticks_reg[13]));
  FDCE #(
    .INIT(1'b0)) 
    \ticks_reg[14] 
       (.C(CLK_P_BUFG),
        .CE(\ticks[0]_i_1_n_0 ),
        .CLR(RESET_IBUF),
        .D(\ticks_reg[12]_i_1_n_5 ),
        .Q(ticks_reg[14]));
  FDCE #(
    .INIT(1'b0)) 
    \ticks_reg[15] 
       (.C(CLK_P_BUFG),
        .CE(\ticks[0]_i_1_n_0 ),
        .CLR(RESET_IBUF),
        .D(\ticks_reg[12]_i_1_n_4 ),
        .Q(ticks_reg[15]));
  FDCE #(
    .INIT(1'b0)) 
    \ticks_reg[16] 
       (.C(CLK_P_BUFG),
        .CE(\ticks[0]_i_1_n_0 ),
        .CLR(RESET_IBUF),
        .D(\ticks_reg[16]_i_1_n_7 ),
        .Q(ticks_reg[16]));
  (* ADDER_THRESHOLD = "11" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \ticks_reg[16]_i_1 
       (.CI(\ticks_reg[12]_i_1_n_0 ),
        .CO({\ticks_reg[16]_i_1_n_0 ,\NLW_ticks_reg[16]_i_1_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({\ticks[16]_i_2_n_0 ,\ticks[16]_i_3_n_0 ,\ticks[16]_i_4_n_0 ,\ticks[16]_i_5_n_0 }),
        .O({\ticks_reg[16]_i_1_n_4 ,\ticks_reg[16]_i_1_n_5 ,\ticks_reg[16]_i_1_n_6 ,\ticks_reg[16]_i_1_n_7 }),
        .S({\ticks[16]_i_6_n_0 ,\ticks[16]_i_7_n_0 ,\ticks[16]_i_8_n_0 ,\ticks[16]_i_9_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \ticks_reg[17] 
       (.C(CLK_P_BUFG),
        .CE(\ticks[0]_i_1_n_0 ),
        .CLR(RESET_IBUF),
        .D(\ticks_reg[16]_i_1_n_6 ),
        .Q(ticks_reg[17]));
  FDCE #(
    .INIT(1'b0)) 
    \ticks_reg[18] 
       (.C(CLK_P_BUFG),
        .CE(\ticks[0]_i_1_n_0 ),
        .CLR(RESET_IBUF),
        .D(\ticks_reg[16]_i_1_n_5 ),
        .Q(ticks_reg[18]));
  FDCE #(
    .INIT(1'b0)) 
    \ticks_reg[19] 
       (.C(CLK_P_BUFG),
        .CE(\ticks[0]_i_1_n_0 ),
        .CLR(RESET_IBUF),
        .D(\ticks_reg[16]_i_1_n_4 ),
        .Q(ticks_reg[19]));
  FDCE #(
    .INIT(1'b0)) 
    \ticks_reg[1] 
       (.C(CLK_P_BUFG),
        .CE(\ticks[0]_i_1_n_0 ),
        .CLR(RESET_IBUF),
        .D(\ticks_reg[0]_i_2_n_6 ),
        .Q(ticks_reg[1]));
  FDCE #(
    .INIT(1'b0)) 
    \ticks_reg[20] 
       (.C(CLK_P_BUFG),
        .CE(\ticks[0]_i_1_n_0 ),
        .CLR(RESET_IBUF),
        .D(\ticks_reg[20]_i_1_n_7 ),
        .Q(ticks_reg[20]));
  (* ADDER_THRESHOLD = "11" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \ticks_reg[20]_i_1 
       (.CI(\ticks_reg[16]_i_1_n_0 ),
        .CO({\ticks_reg[20]_i_1_n_0 ,\NLW_ticks_reg[20]_i_1_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({\ticks[20]_i_2_n_0 ,\ticks[20]_i_3_n_0 ,\ticks[20]_i_4_n_0 ,\ticks[20]_i_5_n_0 }),
        .O({\ticks_reg[20]_i_1_n_4 ,\ticks_reg[20]_i_1_n_5 ,\ticks_reg[20]_i_1_n_6 ,\ticks_reg[20]_i_1_n_7 }),
        .S({\ticks[20]_i_6_n_0 ,\ticks[20]_i_7_n_0 ,\ticks[20]_i_8_n_0 ,\ticks[20]_i_9_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \ticks_reg[21] 
       (.C(CLK_P_BUFG),
        .CE(\ticks[0]_i_1_n_0 ),
        .CLR(RESET_IBUF),
        .D(\ticks_reg[20]_i_1_n_6 ),
        .Q(ticks_reg[21]));
  FDCE #(
    .INIT(1'b0)) 
    \ticks_reg[22] 
       (.C(CLK_P_BUFG),
        .CE(\ticks[0]_i_1_n_0 ),
        .CLR(RESET_IBUF),
        .D(\ticks_reg[20]_i_1_n_5 ),
        .Q(ticks_reg[22]));
  FDCE #(
    .INIT(1'b0)) 
    \ticks_reg[23] 
       (.C(CLK_P_BUFG),
        .CE(\ticks[0]_i_1_n_0 ),
        .CLR(RESET_IBUF),
        .D(\ticks_reg[20]_i_1_n_4 ),
        .Q(ticks_reg[23]));
  FDCE #(
    .INIT(1'b0)) 
    \ticks_reg[24] 
       (.C(CLK_P_BUFG),
        .CE(\ticks[0]_i_1_n_0 ),
        .CLR(RESET_IBUF),
        .D(\ticks_reg[24]_i_1_n_7 ),
        .Q(ticks_reg[24]));
  (* ADDER_THRESHOLD = "11" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \ticks_reg[24]_i_1 
       (.CI(\ticks_reg[20]_i_1_n_0 ),
        .CO({\ticks_reg[24]_i_1_n_0 ,\NLW_ticks_reg[24]_i_1_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({\ticks[24]_i_2_n_0 ,\ticks[24]_i_3_n_0 ,\ticks[24]_i_4_n_0 ,\ticks[24]_i_5_n_0 }),
        .O({\ticks_reg[24]_i_1_n_4 ,\ticks_reg[24]_i_1_n_5 ,\ticks_reg[24]_i_1_n_6 ,\ticks_reg[24]_i_1_n_7 }),
        .S({\ticks[24]_i_6_n_0 ,\ticks[24]_i_7_n_0 ,\ticks[24]_i_8_n_0 ,\ticks[24]_i_9_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \ticks_reg[25] 
       (.C(CLK_P_BUFG),
        .CE(\ticks[0]_i_1_n_0 ),
        .CLR(RESET_IBUF),
        .D(\ticks_reg[24]_i_1_n_6 ),
        .Q(ticks_reg[25]));
  FDCE #(
    .INIT(1'b0)) 
    \ticks_reg[26] 
       (.C(CLK_P_BUFG),
        .CE(\ticks[0]_i_1_n_0 ),
        .CLR(RESET_IBUF),
        .D(\ticks_reg[24]_i_1_n_5 ),
        .Q(\ticks_reg[31]_0 [5]));
  FDCE #(
    .INIT(1'b0)) 
    \ticks_reg[27] 
       (.C(CLK_P_BUFG),
        .CE(\ticks[0]_i_1_n_0 ),
        .CLR(RESET_IBUF),
        .D(\ticks_reg[24]_i_1_n_4 ),
        .Q(ticks_reg[27]));
  FDCE #(
    .INIT(1'b0)) 
    \ticks_reg[28] 
       (.C(CLK_P_BUFG),
        .CE(\ticks[0]_i_1_n_0 ),
        .CLR(RESET_IBUF),
        .D(\ticks_reg[28]_i_1_n_7 ),
        .Q(ticks_reg[28]));
  (* ADDER_THRESHOLD = "11" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \ticks_reg[28]_i_1 
       (.CI(\ticks_reg[24]_i_1_n_0 ),
        .CO(\NLW_ticks_reg[28]_i_1_CO_UNCONNECTED [3:0]),
        .CYINIT(1'b0),
        .DI({1'b0,\ticks[28]_i_2_n_0 ,\ticks[28]_i_3_n_0 ,\ticks[28]_i_4_n_0 }),
        .O({\ticks_reg[28]_i_1_n_4 ,\ticks_reg[28]_i_1_n_5 ,\ticks_reg[28]_i_1_n_6 ,\ticks_reg[28]_i_1_n_7 }),
        .S({S,\ticks[28]_i_6_n_0 ,\ticks[28]_i_7_n_0 ,\ticks[28]_i_8_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \ticks_reg[29] 
       (.C(CLK_P_BUFG),
        .CE(\ticks[0]_i_1_n_0 ),
        .CLR(RESET_IBUF),
        .D(\ticks_reg[28]_i_1_n_6 ),
        .Q(ticks_reg[29]));
  FDCE #(
    .INIT(1'b0)) 
    \ticks_reg[2] 
       (.C(CLK_P_BUFG),
        .CE(\ticks[0]_i_1_n_0 ),
        .CLR(RESET_IBUF),
        .D(\ticks_reg[0]_i_2_n_5 ),
        .Q(ticks_reg[2]));
  FDCE #(
    .INIT(1'b0)) 
    \ticks_reg[30] 
       (.C(CLK_P_BUFG),
        .CE(\ticks[0]_i_1_n_0 ),
        .CLR(RESET_IBUF),
        .D(\ticks_reg[28]_i_1_n_5 ),
        .Q(ticks_reg[30]));
  FDCE #(
    .INIT(1'b0)) 
    \ticks_reg[31] 
       (.C(CLK_P_BUFG),
        .CE(\ticks[0]_i_1_n_0 ),
        .CLR(RESET_IBUF),
        .D(\ticks_reg[28]_i_1_n_4 ),
        .Q(\ticks_reg[31]_0 [6]));
  FDCE #(
    .INIT(1'b0)) 
    \ticks_reg[3] 
       (.C(CLK_P_BUFG),
        .CE(\ticks[0]_i_1_n_0 ),
        .CLR(RESET_IBUF),
        .D(\ticks_reg[0]_i_2_n_4 ),
        .Q(\ticks_reg[31]_0 [0]));
  FDCE #(
    .INIT(1'b0)) 
    \ticks_reg[4] 
       (.C(CLK_P_BUFG),
        .CE(\ticks[0]_i_1_n_0 ),
        .CLR(RESET_IBUF),
        .D(\ticks_reg[4]_i_1_n_7 ),
        .Q(ticks_reg[4]));
  (* ADDER_THRESHOLD = "11" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \ticks_reg[4]_i_1 
       (.CI(\ticks_reg[0]_i_2_n_0 ),
        .CO({\ticks_reg[4]_i_1_n_0 ,\NLW_ticks_reg[4]_i_1_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({\ticks_reg[7]_0 ,\ticks[4]_i_3_n_0 ,\ticks[4]_i_4_n_0 ,\ticks[4]_i_5_n_0 }),
        .O({\ticks_reg[4]_i_1_n_4 ,\ticks_reg[4]_i_1_n_5 ,\ticks_reg[4]_i_1_n_6 ,\ticks_reg[4]_i_1_n_7 }),
        .S({\ticks[4]_i_6_n_0 ,\ticks[4]_i_7_n_0 ,\ticks[4]_i_8_n_0 ,\ticks[4]_i_9_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \ticks_reg[5] 
       (.C(CLK_P_BUFG),
        .CE(\ticks[0]_i_1_n_0 ),
        .CLR(RESET_IBUF),
        .D(\ticks_reg[4]_i_1_n_6 ),
        .Q(ticks_reg[5]));
  FDCE #(
    .INIT(1'b0)) 
    \ticks_reg[6] 
       (.C(CLK_P_BUFG),
        .CE(\ticks[0]_i_1_n_0 ),
        .CLR(RESET_IBUF),
        .D(\ticks_reg[4]_i_1_n_5 ),
        .Q(ticks_reg[6]));
  FDCE #(
    .INIT(1'b0)) 
    \ticks_reg[7] 
       (.C(CLK_P_BUFG),
        .CE(\ticks[0]_i_1_n_0 ),
        .CLR(RESET_IBUF),
        .D(\ticks_reg[4]_i_1_n_4 ),
        .Q(\ticks_reg[31]_0 [1]));
  FDCE #(
    .INIT(1'b0)) 
    \ticks_reg[8] 
       (.C(CLK_P_BUFG),
        .CE(\ticks[0]_i_1_n_0 ),
        .CLR(RESET_IBUF),
        .D(\ticks_reg[8]_i_1_n_7 ),
        .Q(\ticks_reg[31]_0 [2]));
  (* ADDER_THRESHOLD = "11" *) 
  (* OPT_MODIFIED = "SWEEP" *) 
  CARRY4 \ticks_reg[8]_i_1 
       (.CI(\ticks_reg[4]_i_1_n_0 ),
        .CO({\ticks_reg[8]_i_1_n_0 ,\NLW_ticks_reg[8]_i_1_CO_UNCONNECTED [2:0]}),
        .CYINIT(1'b0),
        .DI({\ticks[8]_i_2_n_0 ,\ticks[8]_i_3_n_0 ,DI}),
        .O({\ticks_reg[8]_i_1_n_4 ,\ticks_reg[8]_i_1_n_5 ,\ticks_reg[8]_i_1_n_6 ,\ticks_reg[8]_i_1_n_7 }),
        .S({\ticks[8]_i_6_n_0 ,\ticks[8]_i_7_n_0 ,\ticks[8]_i_8_n_0 ,\ticks[8]_i_9_n_0 }));
  FDCE #(
    .INIT(1'b0)) 
    \ticks_reg[9] 
       (.C(CLK_P_BUFG),
        .CE(\ticks[0]_i_1_n_0 ),
        .CLR(RESET_IBUF),
        .D(\ticks_reg[8]_i_1_n_6 ),
        .Q(\ticks_reg[31]_0 [3]));
endmodule

(* CLKIN_FREQ = "100000000" *) (* CLKOUT_FREQ = "1000" *) (* DLY = "5" *) 
(* ECO_CHECKSUM = "2c7610fd" *) 
(* NotValidForBitStream *)
module top
   (RESET,
    CLK,
    monedas,
    selector,
    display_number,
    display_selection,
    bebida_saliente,
    LED_error,
    LEDs,
    bebida_elegida);
  input RESET;
  input CLK;
  input [3:0]monedas;
  input [2:0]selector;
  output [7:0]display_number;
  output [7:0]display_selection;
  output bebida_saliente;
  output LED_error;
  output [4:0]LEDs;
  output [2:0]bebida_elegida;

  wire CE_OUT;
  wire CLK;
  wire CLK_IBUF;
  wire CLK_IBUF_BUFG;
  wire CLK_P;
  wire CLK_P_BUFG;
  wire LED_error;
  wire [4:0]LEDs;
  wire [4:0]LEDs_OBUF;
  wire Q1;
  wire Q1_1;
  wire Q1_4;
  wire Q2;
  wire Q2_2;
  wire Q2_5;
  wire Q3;
  wire Q3_0;
  wire Q3_3;
  wire RESET;
  wire RESET_IBUF;
  wire [2:0]bebida_elegida;
  wire [2:0]bebida_elegida_OBUF;
  wire bebida_lista;
  wire bebida_saliente;
  wire cuenta;
  wire [7:0]cuenta_reg;
  wire [2:0]display;
  wire [7:0]display_number;
  wire [7:0]display_number_OBUF;
  wire [7:0]display_selection;
  wire [7:0]display_selection_OBUF;
  wire [7:0]fsm_BCD_decoder;
  wire inst_coin_counter_n_10;
  wire inst_coin_counter_n_8;
  wire inst_coin_counter_n_9;
  wire inst_decoder_display_exit_n_0;
  wire inst_decoder_display_exit_n_1;
  wire inst_decoder_display_exit_n_10;
  wire inst_decoder_display_exit_n_11;
  wire inst_decoder_display_exit_n_12;
  wire inst_decoder_display_exit_n_13;
  wire inst_decoder_display_exit_n_14;
  wire inst_decoder_display_exit_n_15;
  wire inst_decoder_display_exit_n_16;
  wire inst_decoder_display_exit_n_17;
  wire inst_decoder_display_exit_n_18;
  wire inst_decoder_display_exit_n_19;
  wire inst_decoder_display_exit_n_2;
  wire inst_decoder_display_exit_n_20;
  wire inst_decoder_display_exit_n_21;
  wire inst_decoder_display_exit_n_22;
  wire inst_decoder_display_exit_n_23;
  wire inst_decoder_display_exit_n_24;
  wire inst_decoder_display_exit_n_25;
  wire inst_decoder_display_exit_n_26;
  wire inst_decoder_display_exit_n_27;
  wire inst_decoder_display_exit_n_28;
  wire inst_decoder_display_exit_n_29;
  wire inst_decoder_display_exit_n_3;
  wire inst_decoder_display_exit_n_30;
  wire inst_decoder_display_exit_n_31;
  wire inst_decoder_display_exit_n_32;
  wire inst_decoder_display_exit_n_33;
  wire inst_decoder_display_exit_n_34;
  wire inst_decoder_display_exit_n_35;
  wire inst_decoder_display_exit_n_36;
  wire inst_decoder_display_exit_n_37;
  wire inst_decoder_display_exit_n_38;
  wire inst_decoder_display_exit_n_39;
  wire inst_decoder_display_exit_n_4;
  wire inst_decoder_display_exit_n_40;
  wire inst_decoder_display_exit_n_41;
  wire inst_decoder_display_exit_n_42;
  wire inst_decoder_display_exit_n_5;
  wire inst_decoder_display_exit_n_6;
  wire inst_decoder_display_exit_n_7;
  wire inst_decoder_display_exit_n_8;
  wire inst_decoder_display_exit_n_9;
  wire inst_fsm_n_100;
  wire inst_fsm_n_101;
  wire inst_fsm_n_102;
  wire inst_fsm_n_103;
  wire inst_fsm_n_104;
  wire inst_fsm_n_105;
  wire inst_fsm_n_106;
  wire inst_fsm_n_107;
  wire inst_fsm_n_108;
  wire inst_fsm_n_109;
  wire inst_fsm_n_110;
  wire inst_fsm_n_111;
  wire inst_fsm_n_112;
  wire inst_fsm_n_113;
  wire inst_fsm_n_114;
  wire inst_fsm_n_115;
  wire inst_fsm_n_116;
  wire inst_fsm_n_117;
  wire inst_fsm_n_118;
  wire inst_fsm_n_119;
  wire inst_fsm_n_120;
  wire inst_fsm_n_121;
  wire inst_fsm_n_122;
  wire inst_fsm_n_123;
  wire inst_fsm_n_124;
  wire inst_fsm_n_125;
  wire inst_fsm_n_126;
  wire inst_fsm_n_127;
  wire inst_fsm_n_128;
  wire inst_fsm_n_129;
  wire inst_fsm_n_13;
  wire inst_fsm_n_130;
  wire inst_fsm_n_131;
  wire inst_fsm_n_132;
  wire inst_fsm_n_133;
  wire inst_fsm_n_134;
  wire inst_fsm_n_135;
  wire inst_fsm_n_136;
  wire inst_fsm_n_137;
  wire inst_fsm_n_138;
  wire inst_fsm_n_139;
  wire inst_fsm_n_14;
  wire inst_fsm_n_140;
  wire inst_fsm_n_141;
  wire inst_fsm_n_142;
  wire inst_fsm_n_143;
  wire inst_fsm_n_144;
  wire inst_fsm_n_145;
  wire inst_fsm_n_146;
  wire inst_fsm_n_147;
  wire inst_fsm_n_148;
  wire inst_fsm_n_149;
  wire inst_fsm_n_15;
  wire inst_fsm_n_150;
  wire inst_fsm_n_151;
  wire inst_fsm_n_152;
  wire inst_fsm_n_153;
  wire inst_fsm_n_154;
  wire inst_fsm_n_155;
  wire inst_fsm_n_156;
  wire inst_fsm_n_157;
  wire inst_fsm_n_158;
  wire inst_fsm_n_159;
  wire inst_fsm_n_16;
  wire inst_fsm_n_160;
  wire inst_fsm_n_161;
  wire inst_fsm_n_162;
  wire inst_fsm_n_17;
  wire inst_fsm_n_170;
  wire inst_fsm_n_171;
  wire inst_fsm_n_172;
  wire inst_fsm_n_173;
  wire inst_fsm_n_174;
  wire inst_fsm_n_18;
  wire inst_fsm_n_19;
  wire inst_fsm_n_2;
  wire inst_fsm_n_20;
  wire inst_fsm_n_27;
  wire inst_fsm_n_28;
  wire inst_fsm_n_29;
  wire inst_fsm_n_3;
  wire inst_fsm_n_30;
  wire inst_fsm_n_31;
  wire inst_fsm_n_32;
  wire inst_fsm_n_37;
  wire inst_fsm_n_38;
  wire inst_fsm_n_39;
  wire inst_fsm_n_4;
  wire inst_fsm_n_40;
  wire inst_fsm_n_41;
  wire inst_fsm_n_42;
  wire inst_fsm_n_43;
  wire inst_fsm_n_44;
  wire inst_fsm_n_45;
  wire inst_fsm_n_46;
  wire inst_fsm_n_47;
  wire inst_fsm_n_48;
  wire inst_fsm_n_49;
  wire inst_fsm_n_5;
  wire inst_fsm_n_50;
  wire inst_fsm_n_51;
  wire inst_fsm_n_52;
  wire inst_fsm_n_53;
  wire inst_fsm_n_54;
  wire inst_fsm_n_55;
  wire inst_fsm_n_56;
  wire inst_fsm_n_57;
  wire inst_fsm_n_58;
  wire inst_fsm_n_59;
  wire inst_fsm_n_6;
  wire inst_fsm_n_60;
  wire inst_fsm_n_61;
  wire inst_fsm_n_62;
  wire inst_fsm_n_63;
  wire inst_fsm_n_64;
  wire inst_fsm_n_65;
  wire inst_fsm_n_66;
  wire inst_fsm_n_67;
  wire inst_fsm_n_68;
  wire inst_fsm_n_69;
  wire inst_fsm_n_7;
  wire inst_fsm_n_70;
  wire inst_fsm_n_71;
  wire inst_fsm_n_72;
  wire inst_fsm_n_73;
  wire inst_fsm_n_74;
  wire inst_fsm_n_75;
  wire inst_fsm_n_76;
  wire inst_fsm_n_77;
  wire inst_fsm_n_78;
  wire inst_fsm_n_79;
  wire inst_fsm_n_80;
  wire inst_fsm_n_81;
  wire inst_fsm_n_82;
  wire inst_fsm_n_83;
  wire inst_fsm_n_84;
  wire inst_fsm_n_85;
  wire inst_fsm_n_86;
  wire inst_fsm_n_87;
  wire inst_fsm_n_88;
  wire inst_fsm_n_89;
  wire inst_fsm_n_90;
  wire inst_fsm_n_91;
  wire inst_fsm_n_92;
  wire inst_fsm_n_93;
  wire inst_fsm_n_94;
  wire inst_fsm_n_95;
  wire inst_fsm_n_96;
  wire inst_fsm_n_97;
  wire inst_fsm_n_98;
  wire inst_fsm_n_99;
  wire inst_timer_n_7;
  wire inst_timer_n_8;
  wire [9:1]letra230_out;
  wire [3:0]monedas;
  wire [3:0]monedas_IBUF;
  wire [2:0]selector;
  wire [2:0]selector_IBUF;
  wire [2:2]state;
  wire \synchronizers[0].inst_debouncer_i_n_3 ;
  wire \synchronizers[0].inst_debouncer_i_n_4 ;
  wire \synchronizers[0].inst_synchronizer_i_n_0 ;
  wire \synchronizers[1].inst_debouncer_i_n_3 ;
  wire \synchronizers[1].inst_debouncer_i_n_4 ;
  wire \synchronizers[1].inst_synchronizer_i_n_0 ;
  wire \synchronizers[2].inst_debouncer_i_n_3 ;
  wire \synchronizers[2].inst_synchronizer_i_n_0 ;
  wire \synchronizers[3].inst_synchronizer_i_n_0 ;
  wire [31:3]ticks_reg;

initial begin
 $sdf_annotate("timer_tb_time_impl.sdf",,,,"tool_control");
end
  BUFG CLK_IBUF_BUFG_inst
       (.I(CLK_IBUF),
        .O(CLK_IBUF_BUFG));
  IBUF CLK_IBUF_inst
       (.I(CLK),
        .O(CLK_IBUF));
  BUFG CLK_P_BUFG_inst
       (.I(CLK_P),
        .O(CLK_P_BUFG));
  OBUF LED_error_OBUF_inst
       (.I(LEDs_OBUF[4]),
        .O(LED_error));
  OBUF \LEDs_OBUF[0]_inst 
       (.I(LEDs_OBUF[0]),
        .O(LEDs[0]));
  OBUF \LEDs_OBUF[1]_inst 
       (.I(LEDs_OBUF[1]),
        .O(LEDs[1]));
  OBUF \LEDs_OBUF[2]_inst 
       (.I(LEDs_OBUF[2]),
        .O(LEDs[2]));
  OBUF \LEDs_OBUF[3]_inst 
       (.I(LEDs_OBUF[3]),
        .O(LEDs[3]));
  OBUF \LEDs_OBUF[4]_inst 
       (.I(LEDs_OBUF[4]),
        .O(LEDs[4]));
  IBUF RESET_IBUF_inst
       (.I(RESET),
        .O(RESET_IBUF));
  OBUF \bebida_elegida_OBUF[0]_inst 
       (.I(bebida_elegida_OBUF[0]),
        .O(bebida_elegida[0]));
  OBUF \bebida_elegida_OBUF[1]_inst 
       (.I(bebida_elegida_OBUF[1]),
        .O(bebida_elegida[1]));
  OBUF \bebida_elegida_OBUF[2]_inst 
       (.I(bebida_elegida_OBUF[2]),
        .O(bebida_elegida[2]));
  OBUF bebida_saliente_OBUF_inst
       (.I(LEDs_OBUF[3]),
        .O(bebida_saliente));
  OBUF \display_number_OBUF[0]_inst 
       (.I(display_number_OBUF[0]),
        .O(display_number[0]));
  OBUF \display_number_OBUF[1]_inst 
       (.I(display_number_OBUF[1]),
        .O(display_number[1]));
  OBUF \display_number_OBUF[2]_inst 
       (.I(display_number_OBUF[2]),
        .O(display_number[2]));
  OBUF \display_number_OBUF[3]_inst 
       (.I(display_number_OBUF[3]),
        .O(display_number[3]));
  OBUF \display_number_OBUF[4]_inst 
       (.I(display_number_OBUF[4]),
        .O(display_number[4]));
  OBUF \display_number_OBUF[5]_inst 
       (.I(display_number_OBUF[5]),
        .O(display_number[5]));
  OBUF \display_number_OBUF[6]_inst 
       (.I(display_number_OBUF[6]),
        .O(display_number[6]));
  OBUF \display_number_OBUF[7]_inst 
       (.I(display_number_OBUF[7]),
        .O(display_number[7]));
  OBUF \display_selection_OBUF[0]_inst 
       (.I(display_selection_OBUF[0]),
        .O(display_selection[0]));
  OBUF \display_selection_OBUF[1]_inst 
       (.I(display_selection_OBUF[1]),
        .O(display_selection[1]));
  OBUF \display_selection_OBUF[2]_inst 
       (.I(display_selection_OBUF[2]),
        .O(display_selection[2]));
  OBUF \display_selection_OBUF[3]_inst 
       (.I(display_selection_OBUF[3]),
        .O(display_selection[3]));
  OBUF \display_selection_OBUF[4]_inst 
       (.I(display_selection_OBUF[4]),
        .O(display_selection[4]));
  OBUF \display_selection_OBUF[5]_inst 
       (.I(display_selection_OBUF[5]),
        .O(display_selection[5]));
  OBUF \display_selection_OBUF[6]_inst 
       (.I(display_selection_OBUF[6]),
        .O(display_selection[6]));
  OBUF \display_selection_OBUF[7]_inst 
       (.I(display_selection_OBUF[7]),
        .O(display_selection[7]));
  regis_dinero inst_coin_counter
       (.CLK_P_BUFG(CLK_P_BUFG),
        .E(cuenta),
        .Q1(Q1_4),
        .Q1_2(Q1),
        .Q1_5(Q1_1),
        .Q2(Q2_5),
        .Q2_1(Q2),
        .Q2_4(Q2_2),
        .Q3(Q3_3),
        .Q3_0(Q3),
        .Q3_3(Q3_0),
        .RESET_IBUF(RESET_IBUF),
        .S({\synchronizers[0].inst_debouncer_i_n_4 ,\synchronizers[2].inst_debouncer_i_n_3 }),
        .\cuenta_reg[0]_0 (inst_coin_counter_n_9),
        .\cuenta_reg[2]_0 (inst_coin_counter_n_8),
        .\cuenta_reg[2]_1 (inst_coin_counter_n_10),
        .\cuenta_reg[3]_0 (\synchronizers[1].inst_debouncer_i_n_4 ),
        .\cuenta_reg[3]_1 (\synchronizers[0].inst_debouncer_i_n_3 ),
        .\cuenta_reg[7]_0 (\synchronizers[1].inst_debouncer_i_n_3 ),
        .out(cuenta_reg),
        .state(state));
  BCD_decoder inst_decoder_display_exit
       (.CO(inst_decoder_display_exit_n_0),
        .DI(inst_fsm_n_84),
        .O(inst_decoder_display_exit_n_1),
        .Q({fsm_BCD_decoder[7],fsm_BCD_decoder[5:2]}),
        .S({inst_fsm_n_79,inst_fsm_n_80,inst_fsm_n_81,inst_fsm_n_82}),
        .g0_b0__1_i_12_0(inst_decoder_display_exit_n_36),
        .g0_b0__1_i_13(inst_decoder_display_exit_n_33),
        .g0_b0__1_i_6(inst_decoder_display_exit_n_34),
        .g0_b7__1(inst_fsm_n_83),
        .g0_b7__1_0(inst_fsm_n_32),
        .g0_b7__1_1(inst_fsm_n_31),
        .letra10__1_carry__0_0(inst_decoder_display_exit_n_28),
        .letra10__1_carry__0_1(inst_decoder_display_exit_n_31),
        .letra10__1_carry__0_2({inst_decoder_display_exit_n_39,inst_decoder_display_exit_n_40}),
        .letra21__104_carry__2_i_1(inst_decoder_display_exit_n_14),
        .letra21__104_carry_i_4(inst_decoder_display_exit_n_13),
        .letra21__208_carry__0_0(inst_fsm_n_78),
        .letra21__208_carry__0_i_1_0({inst_fsm_n_37,inst_fsm_n_38,inst_fsm_n_39,inst_fsm_n_40}),
        .letra21__208_carry__0_i_1_1({inst_fsm_n_155,inst_fsm_n_156,inst_fsm_n_157,inst_fsm_n_158}),
        .letra21__208_carry__0_i_5_0({inst_fsm_n_90,fsm_BCD_decoder[0]}),
        .letra21__208_carry__0_i_5_1({inst_fsm_n_87,inst_fsm_n_88,inst_fsm_n_89}),
        .letra21__208_carry__0_i_5_2({inst_fsm_n_91,inst_fsm_n_92,inst_fsm_n_93}),
        .letra21__208_carry__0_i_5_3(inst_fsm_n_95),
        .letra21__208_carry__0_i_5_4({inst_fsm_n_65,inst_fsm_n_66,inst_fsm_n_67}),
        .letra21__208_carry__1_i_6_0({inst_fsm_n_17,inst_fsm_n_18,inst_fsm_n_19,inst_fsm_n_20}),
        .letra21__208_carry__1_i_6_1({inst_fsm_n_159,inst_fsm_n_160,inst_fsm_n_161,inst_fsm_n_162}),
        .letra21__208_carry__1_i_6_2({inst_fsm_n_101,inst_fsm_n_102,inst_fsm_n_103,inst_fsm_n_104}),
        .letra21__208_carry__2_0(inst_fsm_n_69),
        .letra21__208_carry__2_i_1(inst_fsm_n_94),
        .letra21__208_carry__2_i_10(inst_decoder_display_exit_n_20),
        .letra21__208_carry__2_i_11(inst_fsm_n_62),
        .letra21__208_carry__2_i_11_0({inst_fsm_n_105,inst_fsm_n_106,inst_fsm_n_107,inst_fsm_n_108}),
        .letra21__208_carry__2_i_6(inst_fsm_n_68),
        .letra21__208_carry__3_0({inst_fsm_n_15,inst_fsm_n_16}),
        .letra21__208_carry__3_1({inst_fsm_n_109,inst_fsm_n_110,inst_fsm_n_111}),
        .letra21__208_carry__3_i_5_0({inst_fsm_n_96,inst_fsm_n_97,inst_fsm_n_98}),
        .letra21__208_carry__3_i_5_1(inst_fsm_n_172),
        .letra21__208_carry__3_i_5_2({inst_fsm_n_116,inst_fsm_n_117,inst_fsm_n_118,inst_fsm_n_119}),
        .letra21__208_carry__3_i_5_3({inst_fsm_n_70,inst_fsm_n_71,inst_fsm_n_72}),
        .letra21__208_carry__3_i_5_4({inst_fsm_n_124,inst_fsm_n_125,inst_fsm_n_126,inst_fsm_n_127}),
        .letra21__208_carry__3_i_8({inst_fsm_n_44,inst_fsm_n_45,inst_fsm_n_46}),
        .letra21__208_carry__3_i_8_0({inst_fsm_n_112,inst_fsm_n_113,inst_fsm_n_114,inst_fsm_n_115}),
        .letra21__208_carry__4_0(inst_fsm_n_28),
        .letra21__208_carry__4_1(inst_fsm_n_137),
        .letra21__208_carry__4_i_5_0({inst_fsm_n_85,inst_fsm_n_86}),
        .letra21__208_carry__4_i_5_1({inst_fsm_n_120,inst_fsm_n_121,inst_fsm_n_122,inst_fsm_n_123}),
        .letra21__208_carry__4_i_5_2({inst_fsm_n_48,inst_fsm_n_49,inst_fsm_n_50,inst_fsm_n_51}),
        .letra21__208_carry__4_i_5_3({inst_fsm_n_128,inst_fsm_n_129,inst_fsm_n_130,inst_fsm_n_131}),
        .letra21__208_carry__4_i_8_0(inst_decoder_display_exit_n_21),
        .letra21__208_carry__5_0({inst_decoder_display_exit_n_22,inst_decoder_display_exit_n_23,inst_decoder_display_exit_n_24}),
        .letra21__208_carry__5_i_5_0(inst_fsm_n_64),
        .letra21__208_carry__5_i_5_1({inst_fsm_n_57,inst_fsm_n_58,inst_fsm_n_59,inst_fsm_n_60}),
        .letra21__208_carry__5_i_5_2({inst_fsm_n_132,inst_fsm_n_133,inst_fsm_n_134,inst_fsm_n_135}),
        .letra21__208_carry__6_i_6_0(inst_fsm_n_136),
        .letra21__208_carry_i_4({inst_fsm_n_143,inst_fsm_n_144,inst_fsm_n_145,inst_fsm_n_146}),
        .letra21__208_carry_i_6_0({inst_fsm_n_53,inst_fsm_n_54,inst_fsm_n_55,inst_fsm_n_56}),
        .letra21__208_carry_i_6_1({inst_fsm_n_147,inst_fsm_n_148,inst_fsm_n_149,inst_fsm_n_150}),
        .letra21__208_carry_i_6_2({inst_fsm_n_151,inst_fsm_n_152,inst_fsm_n_153,inst_fsm_n_154}),
        .letra21__336_carry__0_i_4_0({inst_decoder_display_exit_n_25,inst_decoder_display_exit_n_26,inst_decoder_display_exit_n_27}),
        .letra21__336_carry__5_0({inst_fsm_n_73,inst_fsm_n_74}),
        .letra21__336_carry__5_1({inst_fsm_n_75,inst_fsm_n_76,inst_fsm_n_77}),
        .letra21__37_carry__1_i_7({inst_decoder_display_exit_n_3,inst_decoder_display_exit_n_4}),
        .letra21__37_carry__2_i_1(inst_decoder_display_exit_n_5),
        .letra21__37_carry__2_i_1_0(inst_decoder_display_exit_n_6),
        .letra21__420_carry_0(inst_fsm_n_27),
        .letra21__420_carry__0_0(inst_fsm_n_61),
        .letra21__420_carry__0_1(inst_fsm_n_41),
        .letra21__420_carry__0_2({inst_fsm_n_138,inst_fsm_n_139}),
        .letra21__420_carry__0_3(inst_fsm_n_42),
        .letra21__420_carry__0_4(inst_fsm_n_47),
        .letra21__420_carry__0_5(inst_fsm_n_43),
        .letra21__420_carry__1_0(inst_fsm_n_63),
        .letra21__420_carry__1_1(inst_fsm_n_52),
        .letra21__420_carry__1_2({inst_fsm_n_140,inst_fsm_n_141}),
        .letra21__420_carry__2_0(inst_fsm_n_142),
        .letra21__479_carry_0(inst_decoder_display_exit_n_29),
        .letra21__479_carry_1(inst_decoder_display_exit_n_30),
        .letra21__479_carry_2(inst_decoder_display_exit_n_37),
        .letra21__74_carry__0_i_4({inst_decoder_display_exit_n_7,inst_decoder_display_exit_n_8,inst_decoder_display_exit_n_9}),
        .letra21__74_carry__1_i_5({inst_decoder_display_exit_n_10,inst_decoder_display_exit_n_11}),
        .letra21__74_carry__2_i_3(inst_decoder_display_exit_n_12),
        .letra21_carry__1_i_12({inst_fsm_n_29,inst_fsm_n_30}),
        .letra21_carry__1_i_12_0({inst_fsm_n_99,inst_fsm_n_100}),
        .letra21_carry__2_0(inst_decoder_display_exit_n_15),
        .letra21_carry__2_1({inst_decoder_display_exit_n_16,inst_decoder_display_exit_n_17,inst_decoder_display_exit_n_18,inst_decoder_display_exit_n_19}),
        .letra21_carry__2_2(inst_decoder_display_exit_n_32),
        .letra21_carry__2_i_6(inst_decoder_display_exit_n_2),
        .letra230_out({letra230_out[9],letra230_out[3:1]}),
        .\salida_reg[2] (inst_decoder_display_exit_n_38),
        .\salida_reg[3] (inst_decoder_display_exit_n_35),
        .\salida_reg[7] ({inst_decoder_display_exit_n_41,inst_decoder_display_exit_n_42}));
  display_exit inst_display_exit
       (.CLK(CE_OUT),
        .Q(display),
        .\display_number[7] (inst_fsm_n_171),
        .\display_number[7]_0 (inst_fsm_n_170),
        .display_number_OBUF(display_number_OBUF[7]),
        .\display_number_OBUF[7]_inst_i_1_0 (inst_fsm_n_173),
        .\display_number_OBUF[7]_inst_i_1_1 (inst_fsm_n_174),
        .display_selection_OBUF(display_selection_OBUF));
  fdivider_200Hz inst_fdivider_200Hz
       (.CLK(CE_OUT),
        .CLK_IBUF_BUFG(CLK_IBUF_BUFG),
        .RESET_IBUF(RESET_IBUF));
  Maquina_refrescos inst_fsm
       (.CLK_P_BUFG(CLK_P_BUFG),
        .CO(inst_decoder_display_exit_n_0),
        .DI({inst_fsm_n_4,inst_fsm_n_5}),
        .\FSM_sequential_state_reg[0]_0 (inst_coin_counter_n_8),
        .\FSM_sequential_state_reg[2]_0 (state),
        .\FSM_sequential_state_reg[2]_1 (inst_fsm_n_13),
        .\FSM_sequential_state_reg[2]_2 (bebida_elegida_OBUF),
        .\FSM_sequential_state_reg[2]_3 (inst_coin_counter_n_9),
        .\FSM_sequential_state_reg[2]_4 (inst_timer_n_7),
        .\FSM_sequential_state_reg[2]_5 (inst_timer_n_8),
        .LEDs_OBUF(LEDs_OBUF),
        .O(inst_decoder_display_exit_n_1),
        .Q({fsm_BCD_decoder[7],fsm_BCD_decoder[5:2],fsm_BCD_decoder[0]}),
        .RESET_IBUF(RESET_IBUF),
        .S(inst_fsm_n_3),
        .bebida_lista(bebida_lista),
        .\bebida_reg[0]_0 (inst_coin_counter_n_10),
        .\display_number[0] (display),
        .display_number_OBUF(display_number_OBUF[6:0]),
        .\display_number_OBUF[7]_inst_i_4_0 (inst_decoder_display_exit_n_34),
        .\display_number_OBUF[7]_inst_i_4_1 (inst_decoder_display_exit_n_33),
        .\display_reg[0] (inst_fsm_n_170),
        .\display_reg[0]_0 (inst_fsm_n_171),
        .g0_b0__1_0(inst_decoder_display_exit_n_37),
        .g0_b0__1_1(inst_decoder_display_exit_n_36),
        .g0_b0__1_i_13_0(inst_fsm_n_52),
        .g0_b0__1_i_14_0({letra230_out[9],letra230_out[3:1]}),
        .g0_b0__2_0(inst_decoder_display_exit_n_29),
        .g0_b0__2_1(inst_decoder_display_exit_n_28),
        .g0_b0__2_i_4_0(inst_decoder_display_exit_n_30),
        .g0_b0__6_i_3_0(inst_fsm_n_32),
        .g0_b0_i_7_0(inst_fsm_n_83),
        .g0_b6__3_i_1_0(inst_fsm_n_174),
        .g0_b7__2_0(inst_decoder_display_exit_n_38),
        .g0_b7__2_1(inst_decoder_display_exit_n_31),
        .g0_b7__2_2(inst_decoder_display_exit_n_35),
        .g0_b7__3_i_4_0(inst_fsm_n_173),
        .letra21__104_carry({inst_fsm_n_109,inst_fsm_n_110,inst_fsm_n_111}),
        .letra21__104_carry__2({inst_fsm_n_75,inst_fsm_n_76,inst_fsm_n_77}),
        .letra21__130_carry__0_i_1_0({inst_fsm_n_128,inst_fsm_n_129,inst_fsm_n_130,inst_fsm_n_131}),
        .letra21__130_carry__1(inst_decoder_display_exit_n_2),
        .letra21__130_carry__6({inst_fsm_n_73,inst_fsm_n_74}),
        .letra21__208_carry__11(inst_decoder_display_exit_n_5),
        .letra21__208_carry__11_0({inst_decoder_display_exit_n_16,inst_decoder_display_exit_n_17,inst_decoder_display_exit_n_18,inst_decoder_display_exit_n_19}),
        .letra21__208_carry__11_1(inst_decoder_display_exit_n_14),
        .letra21__208_carry__11_2(inst_decoder_display_exit_n_12),
        .letra21__208_carry__11_i_3_0(inst_decoder_display_exit_n_15),
        .letra21__208_carry__2(inst_decoder_display_exit_n_6),
        .letra21__208_carry__2_0({inst_decoder_display_exit_n_3,inst_decoder_display_exit_n_4}),
        .letra21__208_carry__2_1(inst_decoder_display_exit_n_20),
        .letra21__208_carry__2_i_7_0({inst_decoder_display_exit_n_7,inst_decoder_display_exit_n_8,inst_decoder_display_exit_n_9}),
        .letra21__208_carry__2_i_9_0(inst_fsm_n_28),
        .letra21__208_carry__3({inst_decoder_display_exit_n_10,inst_decoder_display_exit_n_11}),
        .letra21__208_carry__3_0(inst_decoder_display_exit_n_13),
        .letra21__208_carry__3_1(inst_decoder_display_exit_n_32),
        .letra21__208_carry__3_i_12(inst_fsm_n_137),
        .letra21__208_carry_i_3({inst_decoder_display_exit_n_41,inst_decoder_display_exit_n_42}),
        .letra21__336_carry({inst_fsm_n_138,inst_fsm_n_139}),
        .letra21__336_carry__0({inst_fsm_n_140,inst_fsm_n_141}),
        .letra21__336_carry__0_0(inst_fsm_n_142),
        .letra21__37_carry__1_i_1_0({inst_fsm_n_37,inst_fsm_n_38,inst_fsm_n_39,inst_fsm_n_40}),
        .letra21__37_carry__1_i_1_1({inst_fsm_n_105,inst_fsm_n_106,inst_fsm_n_107,inst_fsm_n_108}),
        .letra21__37_carry__1_i_1_2({inst_fsm_n_147,inst_fsm_n_148,inst_fsm_n_149,inst_fsm_n_150}),
        .letra21__37_carry__1_i_1_3(inst_fsm_n_172),
        .letra21__37_carry__2({inst_fsm_n_15,inst_fsm_n_16}),
        .letra21__420_carry(inst_decoder_display_exit_n_21),
        .letra21__420_carry__0({inst_decoder_display_exit_n_22,inst_decoder_display_exit_n_23,inst_decoder_display_exit_n_24}),
        .letra21__420_carry__1({inst_decoder_display_exit_n_25,inst_decoder_display_exit_n_26,inst_decoder_display_exit_n_27}),
        .letra21__74_carry__1_i_3_0({inst_decoder_display_exit_n_39,inst_decoder_display_exit_n_40}),
        .letra21_carry__0(inst_fsm_n_78),
        .letra21_carry__0_i_10_0({inst_fsm_n_116,inst_fsm_n_117,inst_fsm_n_118,inst_fsm_n_119}),
        .letra21_carry__0_i_11_0({inst_fsm_n_44,inst_fsm_n_45,inst_fsm_n_46}),
        .letra21_carry__0_i_11_1({inst_fsm_n_155,inst_fsm_n_156,inst_fsm_n_157,inst_fsm_n_158}),
        .letra21_carry__0_i_12_0({inst_fsm_n_112,inst_fsm_n_113,inst_fsm_n_114,inst_fsm_n_115}),
        .letra21_carry__0_i_12_1({inst_fsm_n_124,inst_fsm_n_125,inst_fsm_n_126,inst_fsm_n_127}),
        .letra21_carry__0_i_12_2({inst_fsm_n_143,inst_fsm_n_144,inst_fsm_n_145,inst_fsm_n_146}),
        .letra21_carry__0_i_12_3({inst_fsm_n_151,inst_fsm_n_152,inst_fsm_n_153,inst_fsm_n_154}),
        .letra21_carry__0_i_1_0({inst_fsm_n_101,inst_fsm_n_102,inst_fsm_n_103,inst_fsm_n_104}),
        .letra21_carry__1_i_10_0(inst_fsm_n_41),
        .letra21_carry__1_i_10_1(inst_fsm_n_43),
        .letra21_carry__1_i_11_0(inst_fsm_n_47),
        .letra21_carry__1_i_9_0(inst_fsm_n_61),
        .letra21_carry__1_i_9_1(inst_fsm_n_63),
        .letra21_carry__1_i_9_2({inst_fsm_n_85,inst_fsm_n_86}),
        .letra21_carry__2({inst_fsm_n_48,inst_fsm_n_49,inst_fsm_n_50,inst_fsm_n_51}),
        .letra21_carry__2_0({inst_fsm_n_57,inst_fsm_n_58,inst_fsm_n_59,inst_fsm_n_60}),
        .letra21_carry__2_1(inst_fsm_n_69),
        .letra21_carry__2_2({inst_fsm_n_132,inst_fsm_n_133,inst_fsm_n_134,inst_fsm_n_135}),
        .letra21_carry__2_3(inst_fsm_n_136),
        .letra21_carry__2_i_1_0({inst_fsm_n_53,inst_fsm_n_54,inst_fsm_n_55,inst_fsm_n_56}),
        .letra21_carry__2_i_1_1(inst_fsm_n_62),
        .letra21_carry__2_i_1_2(inst_fsm_n_64),
        .letra21_carry__2_i_1_3({inst_fsm_n_91,inst_fsm_n_92,inst_fsm_n_93}),
        .letra21_carry__2_i_1_4(inst_fsm_n_94),
        .letra21_carry__2_i_1_5({inst_fsm_n_96,inst_fsm_n_97,inst_fsm_n_98}),
        .letra21_carry__2_i_1_6({inst_fsm_n_120,inst_fsm_n_121,inst_fsm_n_122,inst_fsm_n_123}),
        .letra21_carry__2_i_1_7({inst_fsm_n_159,inst_fsm_n_160,inst_fsm_n_161,inst_fsm_n_162}),
        .letra21_carry_i_5_0({inst_fsm_n_17,inst_fsm_n_18,inst_fsm_n_19,inst_fsm_n_20}),
        .letra21_carry_i_5_1(inst_fsm_n_42),
        .letra21_carry_i_5_2({inst_fsm_n_65,inst_fsm_n_66,inst_fsm_n_67}),
        .letra21_carry_i_5_3(inst_fsm_n_68),
        .letra21_carry_i_5_4({inst_fsm_n_70,inst_fsm_n_71,inst_fsm_n_72}),
        .letra21_carry_i_5_5({inst_fsm_n_87,inst_fsm_n_88,inst_fsm_n_89}),
        .letra21_carry_i_5_6(inst_fsm_n_90),
        .letra21_carry_i_5_7(inst_fsm_n_95),
        .out(cuenta_reg),
        .\salida_reg[0]_0 (inst_fsm_n_27),
        .\salida_reg[2]_0 (inst_fsm_n_31),
        .\salida_reg[3]_0 (inst_fsm_n_84),
        .\salida_reg[7]_0 ({inst_fsm_n_29,inst_fsm_n_30}),
        .\salida_reg[7]_1 ({inst_fsm_n_79,inst_fsm_n_80,inst_fsm_n_81,inst_fsm_n_82}),
        .\salida_reg[7]_2 ({inst_fsm_n_99,inst_fsm_n_100}),
        .selector_IBUF(selector_IBUF),
        .ticks_reg({ticks_reg[31],ticks_reg[26],ticks_reg[12],ticks_reg[9:7],ticks_reg[3]}),
        .\ticks_reg[12] (inst_fsm_n_2),
        .\ticks_reg[12]_0 (inst_fsm_n_14),
        .\ticks_reg[3] (inst_fsm_n_7),
        .\ticks_reg[7] (inst_fsm_n_6));
  prescaler inst_prescaler
       (.CLK_IBUF_BUFG(CLK_IBUF_BUFG),
        .CLK_P(CLK_P));
  timer inst_timer
       (.CLK_P_BUFG(CLK_P_BUFG),
        .DI({inst_fsm_n_4,inst_fsm_n_5}),
        .\FSM_sequential_state_reg[2] (inst_fsm_n_13),
        .RESET_IBUF(RESET_IBUF),
        .S(inst_fsm_n_3),
        .bebida_lista(bebida_lista),
        .\ticks_reg[0]_0 (inst_fsm_n_2),
        .\ticks_reg[10]_0 (inst_timer_n_8),
        .\ticks_reg[15]_0 (inst_fsm_n_14),
        .\ticks_reg[24]_0 (inst_timer_n_7),
        .\ticks_reg[31]_0 ({ticks_reg[31],ticks_reg[26],ticks_reg[12],ticks_reg[9:7],ticks_reg[3]}),
        .\ticks_reg[3]_0 (inst_fsm_n_7),
        .\ticks_reg[7]_0 (inst_fsm_n_6));
  IBUF \monedas_IBUF[0]_inst 
       (.I(monedas[0]),
        .O(monedas_IBUF[0]));
  IBUF \monedas_IBUF[1]_inst 
       (.I(monedas[1]),
        .O(monedas_IBUF[1]));
  IBUF \monedas_IBUF[2]_inst 
       (.I(monedas[2]),
        .O(monedas_IBUF[2]));
  IBUF \monedas_IBUF[3]_inst 
       (.I(monedas[3]),
        .O(monedas_IBUF[3]));
  IBUF \selector_IBUF[0]_inst 
       (.I(selector[0]),
        .O(selector_IBUF[0]));
  IBUF \selector_IBUF[1]_inst 
       (.I(selector[1]),
        .O(selector_IBUF[1]));
  IBUF \selector_IBUF[2]_inst 
       (.I(selector[2]),
        .O(selector_IBUF[2]));
  debouncer \synchronizers[0].inst_debouncer_i 
       (.CLK_P_BUFG(CLK_P_BUFG),
        .Q1(Q1),
        .Q1_reg_0(\synchronizers[0].inst_synchronizer_i_n_0 ),
        .Q2(Q2),
        .Q3(Q3),
        .Q3_reg_0(\synchronizers[0].inst_debouncer_i_n_3 ),
        .S(\synchronizers[0].inst_debouncer_i_n_4 ),
        .out(cuenta_reg[3]));
  SYNCHRNZR \synchronizers[0].inst_synchronizer_i 
       (.CLK_P_BUFG(CLK_P_BUFG),
        .monedas_IBUF(monedas_IBUF[0]),
        .\sreg_reg[0]_0 (\synchronizers[0].inst_synchronizer_i_n_0 ));
  debouncer_0 \synchronizers[1].inst_debouncer_i 
       (.CLK_P_BUFG(CLK_P_BUFG),
        .Q1(Q1_1),
        .Q1_0(Q1_4),
        .Q1_reg_0(\synchronizers[1].inst_debouncer_i_n_3 ),
        .Q1_reg_1(\synchronizers[1].inst_synchronizer_i_n_0 ),
        .Q2(Q2_2),
        .Q2_1(Q2_5),
        .Q3(Q3_0),
        .Q3_2(Q3_3),
        .Q3_reg_0(\synchronizers[1].inst_debouncer_i_n_4 ));
  SYNCHRNZR_1 \synchronizers[1].inst_synchronizer_i 
       (.CLK_P_BUFG(CLK_P_BUFG),
        .monedas_IBUF(monedas_IBUF[1]),
        .\sreg_reg[0]_0 (\synchronizers[1].inst_synchronizer_i_n_0 ));
  debouncer_2 \synchronizers[2].inst_debouncer_i 
       (.CLK_P_BUFG(CLK_P_BUFG),
        .Q1(Q1_4),
        .Q1_reg_0(\synchronizers[2].inst_synchronizer_i_n_0 ),
        .Q2(Q2_5),
        .Q3(Q3_3),
        .S(\synchronizers[2].inst_debouncer_i_n_3 ),
        .\cuenta_reg[3] (\synchronizers[1].inst_debouncer_i_n_4 ),
        .\cuenta_reg[3]_0 (\synchronizers[0].inst_debouncer_i_n_3 ),
        .out(cuenta_reg[1]));
  SYNCHRNZR_3 \synchronizers[2].inst_synchronizer_i 
       (.CLK_P_BUFG(CLK_P_BUFG),
        .monedas_IBUF(monedas_IBUF[2]),
        .\sreg_reg[0]_0 (\synchronizers[2].inst_synchronizer_i_n_0 ));
  debouncer_4 \synchronizers[3].inst_debouncer_i 
       (.CLK_P_BUFG(CLK_P_BUFG),
        .E(cuenta),
        .Q1_reg_0(\synchronizers[3].inst_synchronizer_i_n_0 ),
        .\cuenta_reg[7] (\synchronizers[1].inst_debouncer_i_n_3 ),
        .\cuenta_reg[7]_0 (\synchronizers[0].inst_debouncer_i_n_3 ));
  SYNCHRNZR_5 \synchronizers[3].inst_synchronizer_i 
       (.CLK_P_BUFG(CLK_P_BUFG),
        .monedas_IBUF(monedas_IBUF[3]),
        .\sreg_reg[0]_0 (\synchronizers[3].inst_synchronizer_i_n_0 ));
endmodule
`ifndef GLBL
`define GLBL
`timescale  1 ps / 1 ps

module glbl ();

    parameter ROC_WIDTH = 100000;
    parameter TOC_WIDTH = 0;
    parameter GRES_WIDTH = 10000;
    parameter GRES_START = 10000;

//--------   STARTUP Globals --------------
    wire GSR;
    wire GTS;
    wire GWE;
    wire PRLD;
    wire GRESTORE;
    tri1 p_up_tmp;
    tri (weak1, strong0) PLL_LOCKG = p_up_tmp;

    wire PROGB_GLBL;
    wire CCLKO_GLBL;
    wire FCSBO_GLBL;
    wire [3:0] DO_GLBL;
    wire [3:0] DI_GLBL;
   
    reg GSR_int;
    reg GTS_int;
    reg PRLD_int;
    reg GRESTORE_int;

//--------   JTAG Globals --------------
    wire JTAG_TDO_GLBL;
    wire JTAG_TCK_GLBL;
    wire JTAG_TDI_GLBL;
    wire JTAG_TMS_GLBL;
    wire JTAG_TRST_GLBL;

    reg JTAG_CAPTURE_GLBL;
    reg JTAG_RESET_GLBL;
    reg JTAG_SHIFT_GLBL;
    reg JTAG_UPDATE_GLBL;
    reg JTAG_RUNTEST_GLBL;

    reg JTAG_SEL1_GLBL = 0;
    reg JTAG_SEL2_GLBL = 0 ;
    reg JTAG_SEL3_GLBL = 0;
    reg JTAG_SEL4_GLBL = 0;

    reg JTAG_USER_TDO1_GLBL = 1'bz;
    reg JTAG_USER_TDO2_GLBL = 1'bz;
    reg JTAG_USER_TDO3_GLBL = 1'bz;
    reg JTAG_USER_TDO4_GLBL = 1'bz;

    assign (strong1, weak0) GSR = GSR_int;
    assign (strong1, weak0) GTS = GTS_int;
    assign (weak1, weak0) PRLD = PRLD_int;
    assign (strong1, weak0) GRESTORE = GRESTORE_int;

    initial begin
	GSR_int = 1'b1;
	PRLD_int = 1'b1;
	#(ROC_WIDTH)
	GSR_int = 1'b0;
	PRLD_int = 1'b0;
    end

    initial begin
	GTS_int = 1'b1;
	#(TOC_WIDTH)
	GTS_int = 1'b0;
    end

    initial begin 
	GRESTORE_int = 1'b0;
	#(GRES_START);
	GRESTORE_int = 1'b1;
	#(GRES_WIDTH);
	GRESTORE_int = 1'b0;
    end

endmodule
`endif

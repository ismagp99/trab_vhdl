library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.numeric_std.all;
use IEEE.std_logic_signed.all;

entity regis_dinero is
  port (
   	clk           : in  std_logic;
    reset         : in  std_logic;
    monedas	      : in  std_logic_vector(3 downto 0); -- [100,50,20,10]
    dinero_actual : out std_logic_vector(7 downto 0); -- Cantidad de dinero que hay actualmente en la m�quina
	estado_dinero : out std_logic_vector(1 downto 0)  -- 00 si no se llega al euro, 01 euro exacto, 10 importe excedido
  );
end entity;
    
architecture behavioral of regis_dinero is

  signal cuenta   : std_logic_vector(7 downto 0) := "00000000"; -- Cuenta interna del dinero acumulado en la m�quina
  constant precio : std_logic_vector(7 downto 0) := X"64"; -- Precio del refresco (1�)
  signal lleno    : std_logic := '0'; -- Se�al que indica que el importe establecido se ha alcanzado
    
begin

  comparator: process (cuenta)
  begin
	lleno <= '0';
	estado_dinero <= "00";
	    
	if cuenta = precio then
      estado_dinero <= "01";
	  lleno <= '1';
           
	elsif cuenta < precio then
       estado_dinero <= "00";
	   lleno <= '0';
           
    elsif cuenta > precio then
       estado_dinero <= "10";
	   lleno <= '1';
	   
	end if;
  end process;

  counter: process (clk, reset, monedas, lleno, cuenta)
  begin
	if rising_edge(clk) then
      if reset = '1' or lleno = '1' then
       	cuenta <= "00000000";
	  elsif monedas(0) = '1' then -- 10 cents
		cuenta <= cuenta + 10;
	  elsif monedas(1) = '1' then -- 20 cents
		cuenta <= cuenta + 20;
	  elsif monedas(2) = '1' then -- 50 cents
		cuenta <= cuenta + 50;
      elsif monedas(3) = '1' then -- 1 �
		cuenta <= cuenta + 100;
	  end if;
	  
	end if;
  end process;
  dinero_actual <= cuenta;
end architecture;
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
 
entity timer is
  generic(
    clk_frec   : integer := 1e3; -- Frecuencia reloj interno 1kHz
    delay	   : integer := 5    -- Temporizador de 5 segs      
  );
  port(
    clk          : in std_logic;
    reset        : in std_logic;
    bebida_lista : in std_logic; -- Importe alcanzado y bebida lista para salir
    fin_espera	 : out std_logic -- Salida indicadora de fin de la espera
  );
end entity;
 
architecture behavioral of timer is 
signal ticks : integer; -- Se�al para contar hacia atr�s en cada flanco positivo de reloj 
begin
  fin_espera <= '1' when ticks = 0 else '0'; --Cuando la se�al llegue a 0 finaliza la espera    
  process(clk, reset, bebida_lista) is
  begin
    if reset = '1' then
      ticks <=  0;
    elsif rising_edge(clk) then
      if bebida_lista = '1' then
       	ticks <= delay * clk_frec; --Cuando la bebida est� lista se cargan los ticks con el tiempo que tarda en caer la bebida
      elsif ticks /= 0 then
        ticks <= ticks - 1; -- Mientras queden ticks seguir� restando el temporizador
 	  end if;          
    end if;        
  end process; 
end architecture;


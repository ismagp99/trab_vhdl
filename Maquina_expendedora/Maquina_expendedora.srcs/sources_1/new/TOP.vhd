LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_arith.ALL;
USE ieee.std_logic_unsigned.ALL;

entity top is
  generic (
    CLKIN_FREQ  : positive := 100e6; -- Frecuencia reloj interno 100MHz
    CLKOUT_FREQ : positive := 1e3;   -- Frecuencia salida prescaler 1kHz
    DLY         : integer  := 5      -- Tiempo de espera fsm 5s
  );
  PORT ( 
    RESET             : in  std_logic;
    CLK               : in  std_logic;
    monedas           : in  std_logic_vector(3 downto 0); -- [100,50,20,10]
    selector          : in  std_logic_vector(2 downto 0); -- 001 cocacola, 010 fanta, 100 agua   
    display_number    : out std_logic_vector(7 downto 0); -- Display por el que sale el dato
    display_selection : out std_logic_vector(7 downto 0);  -- Selector del display donde ir� la salida 
    bebida_saliente   : out std_logic; -- Se�al indicadora de bebida saliente
    LED_error         : out std_logic; -- LED que indica error por exceso de importe
    LEDs              : out std_logic_vector(4 downto 0); -- LEDs que indican el estado en el que se encuentra la m�quina
    bebida_elegida    : out std_logic_vector(2 downto 0) -- LEDs que indican la bebida saliente 001 cocacola, 010 fanta, 100 agua
  );
end entity;

architecture dataflow of top is
    
  --Se�ales internas
  signal SYNC            : std_logic_vector(3 downto 0); -- Se�al tras salir del sincronizador
  signal DBNCD           : std_logic_vector(3 downto 0); -- Se�al tras salir del debouncer
  
  signal CLK_P           : std_logic; -- Reloj tras prescaler
  signal bebida_lista    : std_logic; -- Importe alcanzado y bebida lista para salir
  signal fin_espera      : std_logic; -- Indicadora de fin del temporizador de salida de producto
  --signal RESET           : std_logic;
  signal dinero_actual   : std_logic_vector(7 downto 0); -- Cantidad de dinero que hay actualmente en la m�quina
  signal estado_dinero   : std_logic_vector(1 downto 0); -- 00 si no se llega al euro, 01 euro exacto, 10 importe excedido
  signal fsm_BCD_decoder : std_logic_vector(7 downto 0);
  signal clk_salida_200Hz: std_logic; -- Salida reloj para display
  
  signal decoderBCD_display_exit_S0 : std_logic_vector(5 downto 0);
  signal decoderBCD_display_exit_S1 : std_logic_vector(5 downto 0);
  signal decoderBCD_display_exit_S2 : std_logic_vector(5 downto 0);
  signal decoderBCD_display_exit_S3 : std_logic_vector(5 downto 0);
  signal decoderBCD_display_exit_S4 : std_logic_vector(5 downto 0);
  signal decoderBCD_display_exit_S5 : std_logic_vector(5 downto 0);
  signal decoderBCD_display_exit_S6 : std_logic_vector(5 downto 0);
  signal decoderBCD_display_exit_S7 : std_logic_vector(5 downto 0);
  
  signal BCD_to_7seg_s0  : std_logic_vector(7 downto 0);
  signal BCD_to_7seg_s1  : std_logic_vector(7 downto 0);
  signal BCD_to_7seg_s2  : std_logic_vector(7 downto 0);
  signal BCD_to_7seg_s3  : std_logic_vector(7 downto 0);
  signal BCD_to_7seg_s4  : std_logic_vector(7 downto 0);
  signal BCD_to_7seg_s5  : std_logic_vector(7 downto 0);
  signal BCD_to_7seg_s6  : std_logic_vector(7 downto 0);
  signal BCD_to_7seg_s7  : std_logic_vector(7 downto 0);

  component prescaler is
    generic(
      fin  : positive := CLKIN_FREQ;
      fout : positive := CLKOUT_FREQ
    );
    port (
      clk_in  : in  std_logic;
      clk_out : out std_logic
    );
  end component;
    
  component SYNCHRNZR is
    port (
      CLK      : in std_logic;
      ASYNC_IN : in std_logic;
      SYNC_OUT : out std_logic
    );
  end component;

  component debouncer is
    Port (
      clk     : in std_logic;
      deb_in  : in std_logic;
      deb_out : out std_logic
    );
  end component;

  component regis_dinero is
    port (
      clk           : in  std_logic;
      reset         : in  std_logic;
      monedas       : in  std_logic_vector(3 downto 0); --[100,50,20,10]
      dinero_actual : out std_logic_vector(7 downto 0); -- Cantidad de dinero que hay actualmente en la m?quina
      estado_dinero : out std_logic_vector(1 downto 0) -- 00 si no se llega al euro, 01 euro exacto, 10 importe excedido
    );
  end component;

  component timer is
    generic(
      clk_frec : integer := CLKOUT_FREQ;
      delay    : integer := DLY   
    );
    port(
      clk          : in std_logic; 
      reset        : in std_logic;
      bebida_lista : in std_logic; -- Importe alcanzado y bebida lista para salir
      fin_espera   : out std_logic -- Salida indicadora de fin de la espera
    );
  end component;

  component Maquina_refrescos is
    port (
      RESET           : in  std_logic;
      CLK             : in  std_logic;
      fin_espera      : in  std_logic; -- Indicadora de fin del temporizador de salida de producto
      estado_dinero   : in  std_logic_vector(1 downto 0); -- 00 si no se llega al euro, 01 euro exacto, 10 importe excedido
      dinero_actual   : in  std_logic_vector(7 downto 0); -- Dinero en c?ntimos que hay introducido
      selector        : in  std_logic_vector(2 downto 0); -- 001 cocacola, 010 fanta, 100 agua
      bebida_lista    : out std_logic; -- Importe alcanzado y bebida lista para salir
      bebida_saliente : out std_logic; -- Se?al indicadora de bebida saliente
      LED_error       : out std_logic; -- LED que indica error por exceso de importe
      LEDs            : out std_logic_vector(4 downto 0); -- LEDs que indican el estado en el que se encuentra la m?quina
      bebida_elegida  : out std_logic_vector(2 downto 0); -- LEDs que indican la bebida saliente 001 cocacola, 010 fanta, 100 agua
      salida          : out std_logic_vector(7 downto 0) -- Salida que va al display que puede ser cantidad de dinero o producto elegido
    );
  end component;
  
  component BCD_decoder is
    port (
      entrada                        : in  std_logic_vector(7 downto 0); -- N�mero que llega al decodificador para salir en BCD
      S0, S1, S2, S3, S4, S5, S6, S7 : out std_logic_vector(5 downto 0)  -- Cada una de las cifras en BCD que se pasan al display
    );
  end component;
  
  component BCD_to_7seg is
    port (
      entrada : in  std_logic_vector(5 downto 0); -- Cifra de entrada codificada en BCD
      salida  : out std_logic_vector(7 downto 0)  -- Salida por display
    );
  end component;
  
  component display_exit is
    Port ( 
      CLK                            : in  std_logic;
      s0, s1, s2, s3, s4 ,s5, s6 ,s7 : in  std_logic_vector(7 downto 0);  -- Cada una de las cifras en BCD que se pasan al display
      display_number                 : out std_logic_vector (7 downto 0); -- Display por el que sale el dato
      display_selection              : out std_logic_vector (7 downto 0)  -- Selector del display donde ir� la salida
    );
  end component;
  
  component fdivider_200Hz is
    generic (
      module: positive := 200 -- Default value is 200 Hz
    );
    port (
      reset : in  std_logic;
      CLK   : in  std_logic;
      --CE_IN : in  std_logic;
      CE_OUT: out std_logic
    );
  end component;
   
begin
  
  inst_prescaler: prescaler
    PORT MAP(
      clk_in  => clk,
      clk_out => clk_p
    );
    
  synchronizers: for i in monedas'range generate
    inst_synchronizer_i: SYNCHRNZR
      port map (
        clk      => clk_p,
        async_in => monedas(i),
        sync_out => sync(i)
      );
    
    inst_debouncer_i: debouncer
      port map (
        clk     => clk_p,
        deb_in  => sync(i),
        deb_out => dbncd(i)
      );
  end generate;
    
  inst_coin_counter: regis_dinero
    port map (
      clk           => clk_p,
      reset         => reset,
      monedas       => dbncd,
      dinero_actual => dinero_actual,
      estado_dinero => estado_dinero
    );
    
  inst_timer: timer
    port map (
      clk          => clk_p,
      reset        => reset,
      bebida_lista => bebida_lista,
      fin_espera   => fin_espera
    );
    
  inst_fdivider_200Hz: fdivider_200Hz
    port map (
      reset  => reset,
      CLK    => CLK,
      --CE_IN  => clk_entrada_200Hz,
      CE_OUT => clk_salida_200Hz
    );
    
  inst_fsm: Maquina_refrescos
    port map (
      reset           => reset,
      clk             => clk_p,
      estado_dinero   => estado_dinero,
      dinero_actual   => dinero_actual,
      fin_espera      => fin_espera,
      selector        => selector,
      bebida_lista    => bebida_lista,
      bebida_saliente => bebida_saliente,
      LED_error       => LED_error,
      LEDs            => LEDs,
      bebida_elegida  => bebida_elegida,
      salida          => fsm_BCD_decoder
    );
    
    inst_decoder_display_exit: BCD_decoder port map(
    entrada => fsm_BCD_decoder,
    S0      => decoderBCD_display_exit_S0,
    S1      => decoderBCD_display_exit_S1,
    S2      => decoderBCD_display_exit_S2,
    S3      => decoderBCD_display_exit_S3,
    S4      => decoderBCD_display_exit_S4,
    S5      => decoderBCD_display_exit_S5,
    S6      => decoderBCD_display_exit_S6,
    S7      => decoderBCD_display_exit_S7
  );
  
  inst_bcd_to_7seg_0: BCD_to_7seg port map(
    entrada => decoderBCD_display_exit_S0,
    salida  => BCD_to_7seg_s0
  );    
  inst_bcd_to_7seg_1: BCD_to_7seg port map(
    entrada => decoderBCD_display_exit_S1,
    salida  => BCD_to_7seg_s1
  );    
  inst_bcd_to_7seg_2: BCD_to_7seg port map(
    entrada => decoderBCD_display_exit_S2,
    salida  => BCD_to_7seg_s2
  );    
  inst_bcd_to_7seg_3: BCD_to_7seg port map(
    entrada => decoderBCD_display_exit_S3,
    salida  => BCD_to_7seg_s3
  );    
  inst_bcd_to_7seg_4: BCD_to_7seg port map(
    entrada => decoderBCD_display_exit_S4,
    salida  => BCD_to_7seg_s4
  );    
  inst_bcd_to_7seg_5: BCD_to_7seg port map(
    entrada => decoderBCD_display_exit_S5,
    salida  => BCD_to_7seg_s5
  );    
  inst_bcd_to_7seg_6: BCD_to_7seg port map(
    entrada => decoderBCD_display_exit_S6,
    salida  => BCD_to_7seg_s6
  );    
  inst_bcd_to_7seg_7: BCD_to_7seg port map(
    entrada => decoderBCD_display_exit_S7,
    salida  => BCD_to_7seg_s7
  );

  inst_display_exit: display_exit port map(
    CLK               => clk_salida_200Hz,
    s0                => BCD_to_7seg_s0,
    s1                => BCD_to_7seg_s1,
    s2                => BCD_to_7seg_s2,
    s3                => BCD_to_7seg_s3,
    s4                => BCD_to_7seg_s4,
    s5                => BCD_to_7seg_s5,
    s6                => BCD_to_7seg_s6,
    s7                => BCD_to_7seg_s7,
    display_number    => display_number,
    display_selection => display_selection
  );

end architecture;
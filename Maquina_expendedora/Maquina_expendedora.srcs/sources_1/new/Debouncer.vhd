library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity debouncer is
  Port (
    clk     : in std_logic;
    deb_in  : in std_logic;
    deb_out : out std_logic
  );
end entity;

architecture Behavioral of debouncer is
    signal Q1, Q2, Q3 : std_logic;
begin
    process(clk)
    begin
       if (clk'event and clk = '1') then
            Q1 <= deb_in;
            Q2 <= Q1;
            Q3 <= Q2;
       end if;
    end process;
    
    deb_out <= Q1 and Q2 and (not Q3);

end architecture;

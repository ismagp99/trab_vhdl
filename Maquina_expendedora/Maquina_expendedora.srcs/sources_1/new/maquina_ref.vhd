library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity Maquina_refrescos is
  port (
    reset           : in  std_logic;
    CLK             : in  std_logic;
    fin_espera      : in  std_logic; -- Indicadora de fin del temporizador de salida de producto
    estado_dinero   : in  std_logic_vector(1 downto 0); -- 00 si no se llega al euro, 01 euro exacto, 10 importe excedido
    dinero_actual   : in  std_logic_vector(7 downto 0); -- Dinero en c?ntimos que hay introducido
    selector        : in  std_logic_vector(2 downto 0); -- 001 cocacola, 010 fanta, 100 agua
    bebida_lista    : out std_logic; -- Importe alcanzado y bebida lista para salir
    bebida_saliente : out std_logic; -- Se?al indicadora de bebida saliente
    LED_error       : out std_logic; -- LED que indica error por exceso de importe
    LEDs            : out std_logic_vector(4 downto 0); -- LEDs que indican el estado en el que se encuentra la m?quina
    bebida_elegida  : out std_logic_vector(2 downto 0); -- LEDs que indican la bebida saliente 001 cocacola, 010 fanta, 100 agua
    salida          : out std_logic_vector(7 downto 0) -- Salida que va al display que puede ser cantidad de dinero o producto elegido
  );
end entity;

architecture behavioral of Maquina_refrescos is

  type state_t is (S0_esp, S1_lleno, S2_exceso, S3_sal, S4_err, s5_sel); -- S0 a la espera de dinero, s1 importe correcto, s2 exceso de importe, s3 salida de bebida, s4 estado de error
  signal state      : state_t;
  signal next_state : state_t;
  signal bebida     : std_logic_vector(7 downto 0) := "00000000";
  
begin

  state_register: process (reset, CLK)
  begin
    if reset = '1' then
      state <= S0_esp;
    elsif CLK'event and CLK = '1' then
      state <= next_state;    
    end if;
  end process;
  
  nextstate_decod: process (estado_dinero, state, fin_espera, selector)
  begin
    next_state <= state;
    case state is
      when S0_esp =>
        if(estado_dinero = "01") then -- Se llega al importe exacto     
          bebida <= "00000000";
          next_state <= S1_lleno;
        elsif(estado_dinero = "10") then -- Se excede el importe establecido      
          next_state <= S2_exceso;
        elsif(dinero_actual < "11001000" and dinero_actual > "01100100") then
          next_state <= s2_exceso;
        end if;
      when S1_lleno =>
        next_state <= s5_sel;
      when S2_exceso =>
        next_state <= S4_err;
      when S3_sal =>
        if fin_espera = '1' then -- Al finalizar el temporizador suponemos que la bebida ha ca?do y la m?quina vuelve al inicio
          next_state <= S0_esp;
        end if;
      when S4_err =>
        if fin_espera = '1' then -- El error por exceso de importe tiene el mismo temporizador durante el cual se devuelven las monedas
          next_state <= S0_esp;
        end if;
      when s5_sel =>
        if(selector = "001")then
          bebida <= "11001001"; -- Salida 201 correspondiente a la cocacola
        elsif(selector = "010")then
          bebida <= "11001010"; -- Salida 202 correspondiente a la fanta
        elsif(selector = "100")then
          bebida <= "11001011"; -- Salida 203 correspondiente al agua
        end if;
        if (selector > "000") then
          next_state <= S3_sal;
        end if;
      when others =>
        next_state <= S0_esp;
    end case;
  end process;

  output_decod: process (state, dinero_actual)
  begin
    case state is
      when S0_esp =>
        bebida_saliente <= '0';
        LEDs            <= "00001";
        LED_error       <= '0';
        bebida_lista    <= '0';
        bebida_elegida  <= "000";
        salida          <= dinero_actual;
      when S1_lleno => 
        bebida_saliente <= '0';
        LEDs            <= "00010";
        LED_error       <= '0';
        --salida          <= "11001101"; -- La salida es 205, "men?" de selecci?n de bebida;
      when S2_exceso => 
        bebida_saliente <= '0';
        LEDs            <= "00100";
        LED_error       <= '0';
        bebida_lista    <= '1'; -- Aqu? indicamos esto porque el temporizador del error es el mismo que el de la salida de bebidas
        bebida_elegida  <= "000";
        salida          <= "11001100"; -- La salida es 204, que es el error por exceso de importe
      when S3_sal =>
        bebida_saliente <= '1';
        LEDs            <= "01000";
        LED_error       <= '0';
        bebida_lista    <= '0';
        if(bebida > "00000000") then
          salida <= bebida;
        end if;
      when S4_err =>
        bebida_saliente <= '0';
        LEDs            <= "10000";
        LED_error       <= '1';
        bebida_lista    <= '0';
        bebida_elegida  <= "000";
        salida          <= "11001100"; -- La salida es 204, que es el error por exceso de importe
      when s5_sel =>
        bebida_saliente <= '0';
        LEDs            <= "00000";
        LED_error       <= '0';
        if (selector = "001") then
          bebida_elegida <= "001";
          bebida_lista   <= '1';
          if(bebida > "00000000") then
            salida <= bebida;
          end if;
        elsif (selector = "010") then
          bebida_elegida <= "010";
          bebida_lista   <= '1'; 
          if(bebida > "00000000") then
            salida <= bebida;
          end if;
        elsif (selector = "100") then
          bebida_elegida <= "100";
          bebida_lista   <= '1';
          if(bebida > "00000000") then
            salida <= bebida;
          end if;
          else
            salida <= "11001101"; -- La salida es 205, "men?" de selecci?n de bebida;
        end if;
      when others => 
        bebida_saliente <= '0';
        LEDs            <= "00000";
        LED_error       <= '1';
        bebida_lista    <= '0';
        bebida_elegida  <= "000";
        salida          <= "00000000";
    end case;
  end process;  
end architecture;
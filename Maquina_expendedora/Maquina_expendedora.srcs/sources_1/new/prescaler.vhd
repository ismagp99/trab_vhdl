library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity prescaler is
  generic(
    fin  : positive := 100e6;
    fout : positive := 1e3
  );
  port (
    clk_in  : in  std_logic;
    clk_out : out std_logic
  );
end entity;

architecture Behavioral of prescaler is
  signal cuenta    : unsigned (15 downto 0) := (others => '0');
  signal clk_out_i : std_logic := '0';  
begin
  gen_clk : process (clk_in)
  begin
    if rising_edge(clk_in) then
      if cuenta = ((fin / fout) / 2) then
        cuenta   <= (others => '0');
        clk_out_i   <= not clk_out_i;
      else
        cuenta <= cuenta + 1;
      end if;
    end if;
  end process gen_clk;
  clk_out <= clk_out_i;
end architecture;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity fdivider_200Hz is
  generic (
    module: positive := 200 -- Default value is 200 Hz
  );
  port (
    reset : in  std_logic;
    CLK   : in  std_logic;
    --CE_IN : in  std_logic;
    CE_OUT: out std_logic
  );
end entity;

architecture Behavioral of fdivider_200Hz is
begin
  process (reset, CLK)
    subtype count_range is integer range 0 to module - 1;
    variable count: count_range;
  begin
    if reset = '1' then
      count := count_range'high;
      CE_OUT <= '0';
    elsif rising_edge(CLK) then
      CE_OUT <= '0';
      --if CE_IN = '1' then
        if count /= 0 then
          count := count - 1;
        else
          CE_OUT <= '1';
          count := count_range'high;
        end if;
      end if;        
    --end if;
  end process;
end Behavioral;

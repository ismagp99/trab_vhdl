library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.numeric_std.all;
use IEEE.std_logic_signed.all;

entity regis_dinero_tb is
end entity;

architecture test of regis_dinero_tb is

  -- Inputs
  signal clk, reset    : std_logic;
  signal monedas       : std_logic_vector(3 downto 0);
  
  -- Outputs
  signal dinero_actual : std_logic_vector(7 downto 0);
  signal estado_dinero : std_logic_vector(1 downto 0);

  component regis_dinero is
    port (
      clk           : in  std_logic;
      reset         : in  std_logic;
      monedas	    : in  std_logic_vector(3 downto 0);
      dinero_actual : out std_logic_vector(7 downto 0);
	  estado_dinero : out std_logic_vector(1 downto 0) 
	);
  end component;

  constant CLK_PERIOD : time := 1 ns;

begin
  uut: regis_dinero
    port map (
	  clk           => clk,
      reset         => reset,
      monedas 	    => monedas,
	  dinero_actual => dinero_actual,
      estado_dinero => estado_dinero
    );

  clkgen: process
  begin
    clk <= '1';
    wait for 0.5 * CLK_PERIOD;
    clk <= '0';
    wait for 0.5 * CLK_PERIOD;
  end process;
  
  tester: process
  begin
    reset   <= '0';
    monedas <= "0000";
    
    wait until clk = '1';
    wait for 0.5 * CLK_PERIOD;
    monedas(0) <= '1'; -- 10 cents
    wait for 1 * CLK_PERIOD;
    monedas(0) <= '0';
    monedas(1) <= '1'; -- 30 cents
    wait for 1 * CLK_PERIOD;
    monedas(1) <= '0';
    wait for 1 * CLK_PERIOD;
    monedas(1) <= '1'; -- 50 cents
    wait for 1 * CLK_PERIOD;
    monedas(1) <= '0';
    monedas(2) <= '1'; -- 1 �
    wait for 1 * CLK_PERIOD;
    monedas(2) <= '0';
    wait for 1 * CLK_PERIOD;
    monedas(3) <= '1'; -- 1 �
    wait for 1 * CLK_PERIOD;
    monedas(3) <= '0';
    wait for 1 * CLK_PERIOD;
    monedas(1) <= '1'; -- 20 cents
    wait for 1 * CLK_PERIOD;
    monedas(1) <= '0';
    monedas(3) <= '1'; -- 1,20 �
    wait for 1 * CLK_PERIOD;
    monedas(3) <= '0';
    wait for 6 * CLK_PERIOD;
    monedas(3) <= '1'; -- 1 �
    wait for 1 * CLK_PERIOD;
    monedas(3) <= '0';
    wait for 1 * CLK_PERIOD;
    reset <= '1';
    wait for 1 * CLK_PERIOD;
    reset <= '0';
    wait for 5* CLK_PERIOD;
    assert false
      report "[SUCCESS]: simulation finished."
      severity failure;
  end process;

end architecture;
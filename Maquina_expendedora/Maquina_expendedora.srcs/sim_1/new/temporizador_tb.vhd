library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity timer_tb is
end entity;

architecture behavioral of timer_tb is 
	
  --Inputs
  signal clk          : std_logic;
  signal reset        : std_logic;
  signal bebida_lista : std_logic;

  --Outputs
  signal fin_espera   : std_logic;
  
  component timer is
  	port(
      clk          : in std_logic;
      reset        : in std_logic;
      bebida_lista : in std_logic; -- Importe alcanzado y bebida lista para salir
      fin_espera   : out std_logic -- Salida indicadora de fin de la espera
  	);
  end component;

  constant clk_period : time := 1 ms;

begin
  uut: timer
    port map (
      clk          => clk,
      reset        => reset,
      bebida_lista => bebida_lista,
      fin_espera   => fin_espera
    );

  clk_process : process
  begin
    clk <= '0';
    wait for 0.5 * clk_period;
    clk <= '1';
    wait for 0.5 * clk_period;
  end process;
	
  reset <= '0', '1' after 0.25 * clk_period, '0' after 0.75 * clk_period;
  bebida_lista <= '0', '1' after 1 * clk_period, '0' after 2 * clk_period;
    
end;

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.numeric_std.all;
use IEEE.std_logic_signed.all;

entity top_tb is
end entity;

architecture test of top_tb is

  -- Inputs
  signal clk, reset_n : std_logic;
  signal monedas      : std_logic_vector(3 downto 0);
  signal selector     : std_logic_vector(2 downto 0); 
  
  -- Outputs
  signal bebida_saliente   : std_logic;
  signal LED_error   	   : std_logic;
  signal LEDs              : std_logic_vector(4 downto 0);
  signal bebida_elegida    : std_logic_vector(2 downto 0);
  signal display_number    : std_logic_vector(7 downto 0);
  signal display_selection : std_logic_vector(7 downto 0);

  component top is
    generic (
      CLKIN_FREQ  : positive   -- Frecuencia reloj placa 100MHz
    );
    port ( 
      RESET_N           : in std_logic;
      CLK               : in std_logic;
      monedas           : in std_logic_vector(3 downto 0);
      selector          : in std_logic_vector(2 downto 0);
      bebida_saliente   : out std_logic; 
      LED_error         : out std_logic; 
      LEDs              : out std_logic_vector(4 downto 0);
      display_number    : out std_logic_vector(7 downto 0);
      display_selection : out std_logic_vector(7 downto 0);  
      bebida_elegida    : out std_logic_vector(2 downto 0) 
    );
  end component;

  constant CLK_IN_PERIOD  : time := 100 us; -- Periodo reloj de la placa
  
begin
  -- Unit Under Test
  uut: top
    generic map(
        CLKIN_FREQ => 10_000
    )
    port map (
      clk               => clk,
      reset_n           => reset_n,
      monedas           => monedas,
      selector          => selector,
      bebida_saliente   => bebida_saliente,
      LED_error         => LED_error,
      LEDs              => LEDs,
      display_number    => display_number,
      display_selection => display_selection,
      bebida_elegida    => bebida_elegida
    );

  clkgen: process
  begin
    clk <= '1';
    wait for 0.5 * CLK_IN_PERIOD;
    clk <= '0';
    wait for 0.5 * CLK_IN_PERIOD;
  end process;
  
  tester: process
  begin
    reset_n <= '0';
    wait for 1 ms;
    reset_n <= '1';
    monedas <= "0000";
    selector <= "000";
    wait for 10 ms;
    monedas(0) <= '1'; -- 10 cents
    wait for 10 ms;
    monedas(0) <= '0';
    monedas(1) <= '1'; -- 30 cents
    wait for 10 ms;
    monedas(1) <= '0';
    wait for 10 ms;
    monedas(1) <= '1'; -- 50 cents
    wait for 10 ms;
    monedas(1) <= '0';
    monedas(2) <= '1'; -- 1 �
    wait for 10 ms;
    monedas(2) <= '0';
    selector(0) <= '1'; -- Cocacola
    wait for 10 ms;
    selector(0) <= '0';
    wait for 6000 ms;
    monedas(1) <= '1'; -- 20 cents
    wait for 10 ms;
    monedas(1) <= '0';
    monedas(3) <= '1'; -- 1,20 �
    wait for 10 ms;
    monedas(3) <= '0';
    wait for 6000 ms;
    --Pulsamos reset para volver del estado de error
    reset_n <= '1';
    wait for 10 ms;
    reset_n <= '0';
    wait for 2000 ms;
    assert false
      report "[SUCCESS]: simulation finished."
      severity failure;
  end process;

end architecture;
library IEEE;
use IEEE.Std_logic_1164.all;
use IEEE.Numeric_Std.all;

entity BCD_to_7seg_tb is
end;

architecture bench of BCD_to_7seg_tb is

  component BCD_to_7seg
      port (
        entrada : in  std_logic_vector(5 downto 0);
        salida  : out std_logic_vector(7 downto 0)
      );
  end component;

  signal entrada : std_logic_vector(5 downto 0);
  signal salida  : std_logic_vector(7 downto 0) ;

begin

  uut: BCD_to_7seg port map ( 
    entrada => entrada,
    salida  => salida );

  stimulus: process
  begin
    entrada <= "000000";
    wait for 100 ns;
    entrada <= "000100";
    wait for 100 ns;
    entrada <= "001010";
    wait for 100 ns;
    wait;
  end process;
end;

library IEEE;
use IEEE.std_logic_1164.all;

entity Maquina_refrescos_tb is
end entity;

architecture behavior of Maquina_refrescos_tb is 
  component Maquina_refrescos
    port (
      reset           : in  std_logic;
      CLK             : in  std_logic;
      fin_espera      : in  std_logic;
      estado_dinero   : in  std_logic_vector(1 downto 0); 
      dinero_actual   : in  std_logic_vector(7 downto 0);
      selector        : in  std_logic_vector(2 downto 0);
      bebida_lista    : out std_logic; 
      bebida_saliente : out std_logic; 
      LED_error       : out std_logic; 
      bebida_elegida  : out std_logic_vector(2 downto 0);
      LEDs	          : out std_logic_vector(4 downto 0);
      salida          : out std_logic_vector(7 downto 0) 
  );
  end component;

  --Inputs
  signal reset         : std_logic;
  signal clk           : std_logic;
  signal fin_espera    : std_logic;
  signal estado_dinero : std_logic_vector(1 downto 0); 
  signal selector      : std_logic_vector(2 downto 0);
  signal dinero_actual : std_logic_vector(7 downto 0);

  --Outputs
  signal bebida_lista    : std_logic;
  signal bebida_saliente : std_logic;
  signal LED_error       : std_logic;
  signal LEDs            : std_logic_vector(4 downto 0); 
  signal bebida_elegida  : std_logic_vector(2 downto 0);
  signal salida          : std_logic_vector(7 downto 0); 

  constant clk_period: time := 10 ns;

begin
  uut: Maquina_refrescos
    port map (
      reset           => reset,
      clk             => clk,
      fin_espera      => fin_espera,
      estado_dinero   => estado_dinero,
      dinero_actual   => dinero_actual,
      selector        => selector,
      bebida_lista    => bebida_lista,
      bebida_saliente => bebida_saliente,
      LED_error		  => LED_error,
      LEDs            => LEDs,
      bebida_elegida  => bebida_elegida,
      salida          => salida
    );

  clk_process :process
  begin
    clk <= '0';
    wait for 0.5 * clk_period;
    clk <= '1';
    wait for 0.5 * clk_period;
  end process;

  reset <= '0', '1' after 0.25 * clk_period, '0' after 0.75 * clk_period, '1' after 9.25 * clk_period, '0' after 11.25 * clk_period;

  stim_proc: process
  begin
    selector <= "000";
  	-- s0
  	estado_dinero <= "00";
    dinero_actual <= "00000000";
    fin_espera <= '0';
    wait for 2.25 * clk_period;
    estado_dinero <= "01";
    dinero_actual <= "01100100";
    --s1
    wait for 2 * clk_period;
    estado_dinero <= "00";
    dinero_actual <= "00000000";
    selector <= "001"; -- Cocacola
    --s3
    wait for 2 * clk_period;
    selector <= "000";
    fin_espera <= '1';
    --S0
    wait for 1 * clk_period;
    fin_espera <= '0';
    estado_dinero <= "10";
    dinero_actual <= "01111000";
    --S2 - S4
    wait for 2 * clk_period;
    estado_dinero <= "00";
    dinero_actual <= "00000000";
    wait for 2 * clk_period;
    estado_dinero <= "01";
    dinero_actual <= "01100100";
    --s1
    wait for 2 * clk_period;
    estado_dinero <= "00";
    dinero_actual <= "00000000";
    selector <= "010"; -- Fanta
    --s3
    wait for 2 * clk_period;
    selector <= "000";
    fin_espera <= '1';
    --S0
    wait for 1 * clk_period;
    fin_espera <= '0';    
    wait for 2 * clk_period;

    assert false
      report "[SUCCESS]: simulation finished."
      severity failure;
      
  end process;
  
end;